(function(){
	angular.module('controllers')
	.controller('addAgentCtrl', ['$q','$http','$scope', '$rootScope', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'baseURL','agent','excelFormat', function($q,$http,$scope, $rootScope, $state,user,customAlert,$fancyModal,modalAnimation,master,baseURL,agent,excelFormat){
		// $scope.bulkUploadFile
    // $q.all([aircraft.getAll()]).then(function(arr){
    //   $scope.aircrafts=arr[0];
    // })

    	$http.get('/admin/agent/country_list').then(function(r){
    		$scope.country = r.data
    		console.log('country',$scope.country);
    	})

    	$scope.newAgent = {}
    	$scope.agencyNo = {}
       $scope.months = [
          "Select",
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec"
        ];
      $scope.Companies = [
        {
          name:'IATA Agent',
          value:'143',
        },
        {
          name:'Non IATA',
          value:'91A',
        },
        {
          name:'Oerator',
          value:'91O',
        },
        {
          name:'GHA',
          value:'91G',
        },
        {
          name:'Catering',
          value:'91G',
        },
        {
          name:'Security',
          value:'91S',
        },
        {
          name:'Fuel',
          value:'91F',
        },
      ]
    	$scope.newAgent.owner_manegement_team = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];
    	$scope.newAgent.keyPerson = [{name:'',designation:'',date_of_birth:'',anniversery:'',mobile:'',}];
      $scope.newAgent.Revenu = [{month:'',designation:'',total_amount:''}];
    	$scope.newAgent.name_of_corparate_account = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];

    	$scope.add = {
    		owner_manegement_team : function(){
    			var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}
    			$scope.newAgent.owner_manegement_team.push(obj);
    		},
    		keyPerson : function(){
    			var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',mobile:'',}
    			$scope.newAgent.keyPerson.push(obj);
    		},
        Revenu : function(){
          var obj = {month:'',designation:'',total_amount:''}
          $scope.newAgent.Revenu.push(obj);
        },
    		name_of_corparate_account : function(){
    			var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}
    			$scope.newAgent.name_of_corparate_account.push(obj);
    		}
    	}

    	$scope.remove = {
    		owner_manegement_team : function(){
    			var lastItem = $scope.newAgent.owner_manegement_team.length-1;
    			$scope.newAgent.owner_manegement_team.splice(lastItem)
    		},
    		keyPerson : function(){
    			var lastItem = $scope.newAgent.keyPerson.length-1;
    			$scope.newAgent.keyPerson.splice(lastItem)
    		},
        Revenu : function(){
          var lastItem = $scope.newAgent.Revenu.length-1;
          $scope.newAgent.Revenu.splice(lastItem)
        },
    		name_of_corparate_account : function(){
    			var lastItem = $scope.newAgent.name_of_corparate_account.length-1;
    			$scope.newAgent.name_of_corparate_account.splice(lastItem)
    		},
    	}
      $scope.agencyCh = function(sel){
        console.log("sel",sel)
        $scope.agencyNo={};
        if(sel == 'iata'){
          $scope.agencyNo.one = '143'
        }
      }
		$scope.$watch('newAgent.country', function(newValue, oldValue) {
    		if(newValue == oldValue){return}
    		$scope.states = $scope.country[$scope.country.findIndex(i => i._id==$scope.newAgent.country)].states
    		console.log($scope.states)
        $scope.newAgent.state = ''
    		if($scope.type.selector == 'agent'){
    			$scope.agencyNo.one = '91A'
    			// $scope.agencyNo.one = $scope.country[$scope.country.findIndex(i => i.countryName==$scope.newAgent.country)].countryCode 
    		}
        if($scope.type.selector == 'corporate'){
          $scope.agencyNo.one = '91C'
          // $scope.agencyNo.one = $scope.country[$scope.country.findIndex(i => i.countryName==$scope.newAgent.country)].countryCode 
        }
        if($scope.type.selector == 'iata'){
          $scope.newAgent.type = 'agent';

          // $scope.agencyNo.one = $scope.country[$scope.country.findIndex(i => i.countryName==$scope.newAgent.country)].countryCode 
        }
    	});

    	$scope.$watch('newAgent.state', function(newValue, oldValue) {
    		if(newValue == oldValue){return}
    		console.log($scope.newAgent.state)
    		console.log($scope.states.findIndex(i => i.stateName==$scope.newAgent.state))
    		$scope.cities = $scope.states[$scope.states.findIndex(i => i._id==$scope.newAgent.state)].cities
        $scope.newAgent.city = ''
    		console.log($scope.cities);

        agent.generateAgencyCode({city_code : newValue}).then(function(objS){
          console.log(objS)
          var dd = JSON.stringify(objS.count)
          var two = dd.slice(0,2);
          var three = dd.slice(2,4);
          if($scope.type.selector != 'iata'){
            $scope.agencyNo.two = two;
            $scope.agencyNo.three = three;
          }
          
        },function(objE){
          console.log(objE)
        })
    	});

    	// $scope.$watch('newAgent.city', function(newValue, oldValue) {
    	// 	if(newValue == oldValue){return}
    	// 	agent.generateAgencyCode({city_code : newValue}).then(function(objS){
     //      console.log(objS)
     //      var dd = JSON.stringify(objS.count)
     //      var two = dd.slice(0,2);
     //      var three = dd.slice(2,4);

    	// 		$scope.agencyNo.two = two;
     //      $scope.agencyNo.three = three;
    	// 	},function(objE){
    	// 		console.log(objE)
    	// 	})
    	// });

		
		$scope.uploadFile = function(){
           // $scope.aircraft.files=$('#filesAirCraft').prop('files');
           var _file=$('#filesAgent').prop('files');
           // console.log(_file)
           var arrFile=_file[0].name.split('.');
           var _ext=arrFile[arrFile.length-1]
           if(excelFormat.indexOf(_ext)==-1){
            customAlert.show("Agent","File format is not supportable")
            return;
           }
           $scope.isFileUploading=true;
           agent.bulkUpload(_file)
           .then(function(objS){
            console.log(objS);
            return agent.startBulkUpload(objS.file)
           	// $scope.aircraft.files=$scope.aircraft.files.concat(objS)
           	
           },function(objE){
           	$scope.isFileUploading=false;
           	customAlert("Files Upload","Error in file uploading.")
           })
           // agent.startBulkUpload("temp/1505902151066.xlsx")
           .then(function(objS){
           	$scope.isFileUploading=false;
            console.log(objS);
           },function(objE){
           	$scope.isFileUploading=false;
            console.log(objE);
           })
        };
        // $scope.removeFile=function(file){
        // 	aircraft.deleteAircraftImg(file)
        // 	.then(function(objS){

        // 	},function(objE){
        // 		console.log("Error--->",objE)
        // 	})
        // };
		$scope.submitForm=function(isValid){
			// if(!isValid)
			// 	return;
      if($scope.type.selector == 'iata'){
          $scope.newAgent.type = 'agent';
          // $scope.agencyNo.one = $scope.country[$scope.country.findIndex(i => i.countryName==$scope.newAgent.country)].countryCode 
      }
      else{
        $scope.newAgent.type =  $scope.type.selector;
      }
      console.log('sdljfh',JSON.stringify($scope.newAgent));
			agent.createUpdateAgent($scope.newAgent)
			.then(function(objS){
				customAlert.show("Company","Company created successfully.");
				console.log(objS);
        $state.go('main.agent.list')
				$fancyModal.close();
			},function(objE){
				customAlert.show("Country",objE.msg)
			})
		}

    console.log($scope)


	}])
})()
/*

*/