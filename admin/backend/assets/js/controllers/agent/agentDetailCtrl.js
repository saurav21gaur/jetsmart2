(function(){
	angular.module('controllers')
	.controller('agentDetailCtrl', ['$q','$http','$scope', '$rootScope', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'baseURL','agent','$timeout', function($q,$http,$scope, $rootScope, $state,user,customAlert,$fancyModal,modalAnimation,master,baseURL,agent,$timeout){
  $scope.Title = localStorage.Title;
  $scope.months = [
          "Select",
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec"
        ];
      $scope.totalAmt = 0;  
  agent.agentDetail({'_id':$state.params.id}).then(function(objS){
      $scope.newAgent = objS;
      console.log("ddddd",$scope.newAgent.Revenu);
      for(var i=0;i<$scope.newAgent.Revenu.length;i++){
        console.log("fff",$scope.newAgent.Revenu[i].month)
        $scope.totalAmt = $scope.totalAmt + $scope.newAgent.Revenu[i].total_amount;
        if($scope.newAgent.Revenu[i].month == 'Jan')
          $scope.janAmt = $scope.newAgent.Revenu[i].total_amount;
        if($scope.newAgent.Revenu[i].month == 'Feb')
          $scope.febAmt = $scope.newAgent.Revenu[i].total_amount;
        if($scope.newAgent.Revenu[i].month == 'Mar')
          $scope.marAmt = $scope.newAgent.Revenu[i].total_amount;
        if($scope.newAgent.Revenu[i].month == 'Apr')
          $scope.aprAmt = $scope.newAgent.Revenu[i].total_amount;
        if($scope.newAgent.Revenu[i].month == 'May')
          $scope.mayAmt = $scope.newAgent.Revenu[i].total_amount;
        if($scope.newAgent.Revenu[i].month == 'Jun')
          $scope.junAmt = $scope.newAgent.Revenu[i].total_amount;
        if($scope.newAgent.Revenu[i].month == 'Jul')
          $scope.julAmt = $scope.newAgent.Revenu[i].total_amount;
        if($scope.newAgent.Revenu[i].month == 'Aug')
          $scope.augAmt = $scope.newAgent.Revenu[i].total_amount;
        if($scope.newAgent.Revenu[i].month == 'Sep')
          $scope.sepAmt = $scope.newAgent.Revenu[i].total_amount;
        if($scope.newAgent.Revenu[i].month == 'Oct')
          $scope.octAmt = $scope.newAgent.Revenu[i].total_amount;
        if($scope.newAgent.Revenu[i].month == 'Nov')
          $scope.novAmt = $scope.newAgent.Revenu[i].total_amount;
        if($scope.newAgent.Revenu[i].month == 'Dec')
          $scope.decAmt = $scope.newAgent.Revenu[i].total_amount;
      }
    },function(objE){console.log(objE)})

 

  $q.all([master.states(),master.countries(),master.regions()])
  .then(function(arr){
    $scope.states=arr[0];
    console.log("hdfjhfdg",$scope.states)
    $scope.Countries=arr[1];
    $scope.Regions=arr[2];
    for(i = 0; i < $scope.states.length; i++){
      let _tempState = $scope.states[i]
      for(j = 0; j < _tempState.states.cities.length; j++){
        if(_tempState.states.cities[j]._id == $scope.newAgent.city){
          $scope.cities = _tempState.states.cities
        }
      }
    }
    // console.log('State==>',$scope.states)
    // console.log('State==>',$scope.Countries)
  },function(objE){
    console.log(objE)
  })
  $scope.getCities = function(d){
    $scope.newAgent.city = '';
    for(i = 0; i < $scope.states.length; i++){
      let _tempState = $scope.states[i]
      if(_tempState.states._id == d){
        $scope.cities = _tempState.states.cities
      }
    }
  }

  /*var getStatesList = function(){
      master.states()
      .then(function(objS){
        $scope.states = objS
        console.log('states')
        console.dir($scope.States)
      },function(objE){
        console.log(objE)
      })
    }
    getStatesList()

    var getCountriesList = function(){
      master.countries()
      .then(function(objS){
        $scope.Countries = objS
        console.log('Countries')
        console.dir($scope.Countries)
      },function(objE){
        console.log(objE)
      })
    }
    getCountriesList()

    var getCitiesList = function(){
      master.cities()
      .then(function(objS){
        $scope.cities = objS
        console.log('Cities')
        console.dir($scope.Cities)
      },function(objE){
        console.log(objE)
      })
    }
    getCitiesList()*/


    $scope.newAgent = {}
    $scope.agencyNo = {}
     $scope.newAgent.Revenu = [{month:'',designation:'',total_amount:''}];
    $scope.newAgent.owner_manegement_team = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:''}];
    $scope.newAgent.key_personal_detail = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:''}];
    $scope.newAgent.name_of_corparate_account = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:''}];

      $scope.add = {
        owner_manegement_team : function(){
          var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}
          $scope.newAgent.owner_manegement_team.push(obj);
        },
        key_personal_detail : function(){
          var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}
          $scope.newAgent.keyPerson.push(obj);
        },
        Revenu : function(){
          var obj = {month:'',designation:'',total_amount:''}
          $scope.newAgent.Revenu.push(obj);
        },
        name_of_corparate_account : function(){
          var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}
          $scope.newAgent.name_of_corparate_account.push(obj);
        }
      }

      $scope.remove = {
        owner_manegement_team : function(){
          var lastItem = $scope.newAgent.owner_manegement_team.length-1;
          $scope.newAgent.owner_manegement_team.splice(lastItem)
        },
        key_personal_detail : function(){
          var lastItem = $scope.newAgent.keyPerson.length-1;
          $scope.newAgent.keyPerson.splice(lastItem)
        },
        Revenu : function(){
          var lastItem = $scope.newAgent.Revenu.length-1;
          $scope.newAgent.Revenu.splice(lastItem)
        },

        name_of_corparate_account : function(){
          var lastItem = $scope.newAgent.name_of_corparate_account.length-1;
          $scope.newAgent.name_of_corparate_account.splice(lastItem)
        },
      }

		    $scope.submitForm=function(isValid){
            if(!isValid)
              return;
            agent.createUpdateAgent($scope.newAgent)
            .then(function(objS){
              // customAlert.show("Aircraft","Aircraft "+$scope.Title+" successfully.");
              console.log(objS)
              $state.go('main.agent.list')
              getAgentList($scope.currentPage)
              $fancyModal.close();
            },function(objE){
              customAlert.show("Error",objE.msg)
            })
        }
	}])
})()
/*

*/