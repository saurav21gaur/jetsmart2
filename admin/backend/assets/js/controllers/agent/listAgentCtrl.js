(function(){
	angular.module('controllers')
	.controller('listAgentCtrl', ['$q','$http','$scope', '$rootScope', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'baseURL','agent','$timeout', function($q,$http,$scope, $rootScope, $state,user,customAlert,$fancyModal,modalAnimation,master,baseURL,agent,$timeout){
		// $scope.aircraft={files:[]}
  //   $q.all([aircraft.getAll()]).then(function(arr){
  //     $scope.aircrafts=arr[0];
  //   })
      $scope.currentPage = 1;
      $scope.limit = 10;
      $scope.totalData = Infinity;
      $scope.showPagination = false;

  //opretor list
  var getAgentList = function(page){
    agent.getAgentList(page).then(function(objS){
      // $scope.totalData = objS.pagination.total;
        // $scope.limit = objS.pagination.limit;
        console.log(objS)
      $scope.agents = objS;
      $scope.showPagination = true;
      // $scope.agents=$scope.agents.map(x=>{x.phoneNo =parseInt(x.phoneNo); x.pincode =parseInt(x.pincode); x.phoneNo1 =parseInt(x.phoneNo1); return x});
      console.log($scope.agents)
    },function(objE){console.log(objE)})
  }
  getAgentList($scope.currentPage)

    $scope.pageChange=function(page){
      $scope.currentPage = page;
      // getAgentList($scope.currentPage)
    }

    $scope.show = false;
      $scope.exportDataXLS = function () {
          var blob = new Blob([document.getElementById('exportTab').innerHTML], {
              type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
              });
          saveAs(blob, "Company List.xls");
      };
      $scope.exportDataPdf = function () {
          $scope.show = true;
          $timeout(function(){$scope.show = false; console.log('timeout')}, 100)
          html2canvas(document.getElementById('data-table'), {
              onrendered: function (canvas) {
                  var data = canvas.toDataURL();
                  var docDefinition = {
                      content: [{
                          image: data,
                          width: 500,
                      }]
                  };
                  pdfMake.createPdf(docDefinition).download("Company List.pdf");
              }
          });
      };



    $scope.newAgent = {}
    $scope.agencyNo = {}
    $scope.newAgent.owner_manegement_team = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];
    $scope.newAgent.key_personal_detail = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];
    $scope.newAgent.name_of_corparate_account = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];

      $scope.add = {
        owner_manegement_team : function(){
          var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}
          $scope.newAgent.owner_manegement_team.push(obj);
        },
        key_personal_detail : function(){
          var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}
          $scope.newAgent.key_personal_detail.push(obj);
        },
        name_of_corparate_account : function(){
          var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}
          $scope.newAgent.name_of_corparate_account.push(obj);
        }
      }

      $scope.remove = {
        owner_manegement_team : function(){
          var lastItem = $scope.newAgent.owner_manegement_team.length-1;
          $scope.newAgent.owner_manegement_team.splice(lastItem)
        },
        key_personal_detail : function(){
          var lastItem = $scope.newAgent.key_personal_detail.length-1;
          $scope.newAgent.key_personal_detail.splice(lastItem)
        },
        name_of_corparate_account : function(){
          var lastItem = $scope.newAgent.name_of_corparate_account.length-1;
          $scope.newAgent.name_of_corparate_account.splice(lastItem)
        },
      }

 $scope.openModal=function(type,obj){
      console.log(obj)
      // var IndxAnimation=getIndex(prev);
      // console.log(IndxAnimation);
      switch(type){
        case "new":
        $scope.Title="Add";
        $scope.newAgent={};
        $scope.newAgent.owner_manegement_team = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];
    	$scope.newAgent.key_personal_detail = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];
    	$scope.newAgent.name_of_corparate_account = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];
        $scope.showSubmit=true;
         $fancyModal.open({
          templateUrl: 'agent-Modal.html',
          scope: $scope,
          openingClass: 'animated '+modalAnimation[9].in,
          closingClass: 'animated '+modalAnimation[9].out,
          openingOverlayClass: 'animated fadeIn',
          closingOverlayClass: 'animated fadeOut',
        });
        // $scope.new.country=$scope.Countries[0];
        // $scope.new.region=$scope.Regions[0];
        break;

        case "update":
        localStorage.Title="Update";
        $scope.newAgent=obj;
        $state.go('main.agent.edit',{id:obj._id});
        // $scope.showSubmit=true;
        // $scope.new.country=$scope.Countries.find(x=>x._id==obj._id);
        // $scope.new.region=$scope.Regions.find(x=>x._id==obj.states.region._id);
        break;

        case "view":
        localStorage.Title="";
        $scope.newAgent=obj;
        $state.go('main.agent.view',{id:obj._id});

        // $scope.showSubmit=false;
        // $scope.new.country=$scope.Countries.find(x=>x._id==obj._id);
        // $scope.new.region=$scope.Regions.find(x=>x._id==obj.states.region._id);
        break;
        case "Delete":
        agent.agent_delete({_id:obj._id})
          .then(function(objS){
            console.log(objS);
            $state.reload();
          },function(objE){
            console.log("Error--->",objE)
          })
        break;
      }
    
     //prev=IndxAnimation;
    };


    $scope.uploadExcel = function(){
           // $scope.aircraft.files=$('#filesAirCraft').prop('files');
           $scope.isFileUploading=true;
           agent.uploadRevenueXls($('#revenueExcel').prop('files'))
           .then(function(objS){
            $scope.isFileUploading=false;
            console.log("filepathcitypage",objS.file);
             $scope.filesPath=objS.file;
             console.log("filePath", $scope.filesPath)
             agent.uploadExcelToServer($scope.filesPath).then(function(objS){
               console.log(objS)
              },function(objE){
               console.log(objE)
              })
            // $scope.aircraft.files=$scope.aircraft.files.concat(objS)
           },function(objE){
            $scope.isFileUploading=false;
            alert("Files Upload","Error in file uploading.")
           })
           //fileUpload.uploadFileToUrl(file, uploadUrl);
           
        };


		    $scope.submitForm=function(isValid){
      if(!isValid)
        return;
      agent.createUpdateAgent($scope.newAgent)
      .then(function(objS){
        // customAlert.show("Aircraft","Aircraft "+$scope.Title+" successfully.");
        console.log(objS)
        getAgentList($scope.currentPage)
        $fancyModal.close();
      },function(objE){
        customAlert.show("Error",objE.msg)
      })
    }
	}])
})()
/*

*/