(function(){
	angular.module('controllers')
	.controller('aircraftDetailCtrl', ['$q','$http','$scope', '$rootScope', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'baseURL','aircraft', '$timeout', function($q,$http,$scope, $rootScope, $state,user,customAlert,$fancyModal,modalAnimation,master,baseURL,aircraft, $timeout){

      var getAirportsList = function(){
          master.airports()
          .then(function(objS){
            var useableData = []
            // console.log("Data =>"+JSON.stringify(objS))
            for (var i = 0; i < objS.length; i++) {
              var allData = objS[i]
              /**/
              for (var j = 0; j < allData.airportList.length; j++) {
                var airport = allData.airportList[j]
                useableData.push({
                  airport : airport.airport,
                  stateName : allData.states.stateName,
                  state : allData.states._id,
                  countryName : allData.countryName,
                  country : allData._id,
                  cityName : allData.states.cities.cityName,
                  city : allData.states.cities._id,
                  iata : airport.iata,
                  icao : airport.icao,
                  category : airport.category,
                  role : airport.role,
                  runway : airport.runway,
                  groundhandling : airport.groundhandling,
                  _id : airport._id,
                })
              }
              /**/
            }

            $scope.Airports = useableData
            console.log('Airports')
            console.dir($scope.Airports)
          },function(objE){
            console.log(objE)
          })
        }
        getAirportsList()

        $scope.Title=localStorage.Title;
        $scope.showSubmit=localStorage.showSubmit;
        console.log("$state.params.id",$state.params.id)
        aircraft.aircraftDetail({'aircraftId':$state.params.id})
          .then(function(objS){
           $scope.new = objS.result;
           $timeout(function(){
              $scope.new.bases = $scope.new.bases.map(i=>{i.base = $scope.Airports[$scope.Airports.findIndex(x=>x._id == i.base_city)].cityName; return i})
           }, 100);
          
          },function(objE){
            console.log("Error--->",objE)
          })

	}])
})()
/*

*/