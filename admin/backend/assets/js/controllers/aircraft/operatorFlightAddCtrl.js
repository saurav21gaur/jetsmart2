
(function(){
	angular.module('controllers')
	.controller('operatorFlightAddCtrl', ['$q','$scope', '$rootScope', '$http', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'baseURL','aircraft', 'booking','$filter','$timeout', function($q,$scope, $rootScope, $http, $state,user,customAlert,$fancyModal,modalAnimation,master,baseURL,aircraft, booking,$filter,$timeout){

		$scope.new={
			tripType:'oneWay',
			roots:[{
				originName:'',
				destinationName: '',
				departureDate: '',
				departureTime: ''
			},{
				originName:'',
				destinationName: '',
				departureDate: '',
				departureTime: ''
			}]
		};
		$scope.userFair = {}
		$scope.isSearchResult = false
		$scope.isUseSystemFair = true

     $scope.userDetailll=JSON.parse(localStorage.userDetail);
     console.log($scope.userDetailll.user_name);

		$scope.autoFill = function(index){
			setTimeout(function(){
				if(angular.isDefined($scope.new.roots[index+1]))
				$scope.new.roots[index+1].originName = $scope.new.roots[index].destinationName;
			}, 500)
		}
		$scope.addRoot = function(index){
			var obj = {	
					originName : $scope.new.roots[$scope.new.roots.length - 1].destinationName,
					origin : $scope.new.roots[$scope.new.roots.length - 1].destination,
					destinationName : '',
					destination : '',
					departureDate : '',
					departureTime : '',
				};
			$scope.new.roots.push(obj);
		}
		$scope.removeRoot = function(index){
			$scope.new.roots.pop();
		}

      $scope.selectedValue = function(data){
        console.log("selectedValue", data)
        var newArray=[];
        $scope.valll= $scope.aircrafts.map(x=>{
            if(x.jet_name == data){
              x.jet_name = data;
              newArray.push(x)
              return x;
            }else{
              return x
            }
        });
        // $scope.valll =$scope.aircrafts.map(x=>{x.jet_name = data; return x});
        $scope.baseVal = newArray[0].bases;
        $scope.flightNo = newArray[0].flight_number;
        $scope.airType = newArray[0].aircraft_type;
        console.log(" $scope.baseVal",$scope.flightNo, $scope.airType, $scope.baseVal)
      }

    $scope.changeFlyingCost = function(){
      // changeFlyingCost
      console.log("d",$scope.userFair.netHour,"ff",$scope.userFair.netRate);
      $scope.userFair.netFlyingCost = $scope.userFair.netHour*$scope.userFair.netRate;
    }

		$q.all([aircraft.getAllOperator()]).then(function(objS){
		console.log("resresres",objS[0].data);
			if(objS[0].data.response_code ==  200){
		  		$scope.aircrafts=objS[0].data.result.map(x=>{x.baggage =parseInt(x.baggage); return x});;
		 		console.log("success",$scope.aircrafts);
		  	}else{
				alert("response error");
			}
	   	},function(objE){
			alert("Network Connection Problem!");
   		})



   		aircraft.getAllAirport().then(function(objS){
   			console.log('all details')
   			console.log(objS)
   			var useableData = []
   			for (var i = 0; i < objS.length; i++) {
   				var allData = objS[i]
   				/**/
   				for (var j = 0; j < allData.airportList.length; j++) {
   					var airport = allData.airportList[j]
   					useableData.push({
   						airport : airport.airport,
   						stateName : allData.states.stateName,
   						countryName : allData.countryName,
   						cityName : allData.states.cities.cityName,
   						iata : airport.iata,
   						icao : airport.icao,
   						_id : airport._id,
   					})
   				}
   			}

   			$scope.airportDropdown = useableData
   			console.log($scope.airportDropdown)
   		},function(objE){console.log(objE)})


      // SEARCHING // SEARCH & GIVE ME FAIR CALCUALTION
   		$scope.searchRoot = function(valid){
   			 console.log("Search")
   			// console.log("=>",$scope.new.roots[0].departureTime)
        if(!$scope.new.aircraft)
          customAlert.show("Please Select Aircraft")
        
   			if(!valid) return
   			d = angular.copy($scope.new);
        $scope.new={
      tripType:'oneWay',
      roots:[{
        originName:'',
        destinationName: '',
        departureDate: '',
        departureTime: ''
      },{
        originName:'',
        destinationName: '',
        departureDate: '',
        departureTime: ''
      }]
    };
   			d.searchType = 'normalSearch'
   			for (var i = 0; i < d.roots.length; i++) {
   				
   				var _root = d.roots[i]

   				var originArray = _root.originName.split(",");
   				var destinationArray = _root.destinationName.split(",");

   				for(var j = 0; j < $scope.airportDropdown.length; j++){
   					if(angular.isDefined(originArray[1])){
   						if(originArray[1].trim() == $scope.airportDropdown[j].airport){
   							_root.origin = $scope.airportDropdown[j]._id
   							break;
   						}
   					}
   				}
   				for(var j = 0; j < $scope.airportDropdown.length; j++){
   					if(angular.isDefined(destinationArray[1])){
   						if(destinationArray[1].trim() == $scope.airportDropdown[j].airport){
   							_root.destination = $scope.airportDropdown[j]._id
   							break;
   						}
   					}
   				}
   			}
   			switch (d.tripType){
   				case 'oneWay':
   					d.roots = [d.roots[0]];
   					break;
   				case 'roundTrip':
   					d.roots[1] = {
   						origin: d.roots[0].destination,
   						destination : d.roots[0].origin,
   						originName: d.roots[0].destinationName,
   						destinationName : d.roots[0].originName,
   						departureDate : d.roots[1].departureDate,
   						departureTime : d.roots[1].departureTime,
   					}
   					d.roots = [d.roots[0],d.roots[1]];
   					break;
   				case 'multicity':
   					console.log('multicity =>',d)
   					break;
   			}
        localStorage.setItem('localData', JSON.stringify(d))
   			booking.bookAircraft(d)
   			.then(objS=>{
   				console.log(objS)
          if(objS.message == 'No route found'){
              customAlert.show('JET SMART','Thank you for your query, \n please call on (011 28050066) for this particular routing. or send us a query')
              return
            }
            if(objS.length){


   				     objS[0].otherCharges = 0

          // FILTERING DATA

            
            
            for (var i = 0; i < objS.length; i++) {
                let fromBase = objS[0].documents.positioning.fromAirport;
                let from = objS[0].duration[0].data.fromAirport;
                let toBase = objS[0].documents.repositioning.toAirport;
                let to = objS[0].documents.repositioning.fromAirport;
                //positioning
                if(fromBase == from){
                  objS[0].duration[0].data.positioning = false;
                  objS[0].duration[0].data.base = true;
                }else{
                  objS[0].documents.positioning.positioning = true;
                  objS[0].documents.positioning.base = true;
                  objS[0].duration.unshift({
                    crewCharges :0,
                    data : objS[0].documents.positioning,
                    duration : objS[0].documents.positioning.duration,
                  });
                }
                //repositioning
                if(toBase == to){
                  objS[0].duration[objS[0].duration.length - 1].data.repositioning = false;
                }else{
                  objS[0].documents.repositioning.repositioning = true;
                  objS[0].duration.push({
                    crewCharges :0,
                    data : objS[0].documents.repositioning,
                    duration : objS[0].documents.repositioning.duration,
                  });
                }


              if(angular.isDefined(objS[0].duration)){
                for (var j = 0; j < objS[0].duration.length; j++){
                  /**/
                  var origin = objS[0].duration[j].data.fromAirport
                  var destination = objS[0].duration[j].data.toAirport

                  objS[0].duration[j].hours=Math.floor(objS[0].duration[j].duration/60);
                  objS[0].duration[j].min=Math.floor(objS[0].duration[j].duration%60)
                  /**/
                  let roots = d.roots
                  for(let k=0; k < roots.length; k++){
                    if(roots[k].origin == objS[0].duration[j].data.fromAirport && roots[k].destination == objS[0].duration[j].data.toAirport){
                      objS[0].duration[j].departureDate = roots[k].departureDate
                    }else{
                      // objS[0].duration[j].departureDate = ''
                    }
                  }

                  objS[0].duration[j].info = {};

                  for (var k = 0; k < $scope.airportDropdown.length; k++) {
                    var _data = $scope.airportDropdown[k]

                    //origin
                    if(_data._id == origin){
                      objS[0].duration[j].info.originIata = _data.iata
                      objS[0].duration[j].info.originIcao = _data.icao
                      objS[0].duration[j].info.originName = _data.cityName
                    }
                    //destination
                    if(_data._id == destination){
                      objS[0].duration[j].info.destinationIata = _data.iata
                      objS[0].duration[j].info.destinationIcao = _data.icao
                      objS[0].duration[j].info.destinationName = _data.cityName
                    }
                  }

                }
              }/*
              if(angular.isDefined(objS[0].documents.image_gallary)){
                if(objS[0].documents.image_gallary.length > 0){
                  for (var j = 0; j < objS[0].documents.image_gallary.length; j++) {
                    objS[0].documents.image_gallary[j] = baseUrl + '/' + objS[0].documents.image_gallary[j]
                  }
                }else{
                  objS[0].documents.image_gallary = ['assets/img/default-slide.jpg']
                }
              }else{
                objS[0].documents.image_gallary = ['assets/img/default-slide.jpg']
              }*/
            }

            $scope.systemFair = objS[0];
            console.log($scope.systemFair)

          // FILTERING DATA END


   				$scope.systemFair.netFlyingCost = Math.floor($scope.systemFair.netFlyingCost)
   				$scope.systemFair.groundHandlingCharges = Math.floor($scope.systemFair.groundHandlingCharges)
   				$scope.systemFair.totalCrewCharge = Math.floor($scope.systemFair.totalCrewCharge)
   				$scope.systemFair.otherCharges = Math.floor($scope.systemFair.otherCharges)
   				$scope.systemFair.netcost = Math.floor($scope.systemFair.netcost)
   				$scope.systemFair.gst = Math.floor($scope.systemFair.gst)
   				$scope.systemFair.total = Math.floor($scope.systemFair.total)
   				$scope.isSearchResult = true
        }
   			},objE=>{
   				console.log(objE)
   			})

   		}
   		$scope.setUserFair = function(){
   			$scope.userFair =  angular.copy($scope.systemFair);
   			// $scope.userFair.netcost = Math.floor($scope.userFair.netFlyingCost + $scope.userFair.groundHandlingCharges + $scope.userFair.totalCrewCharge + $scope.userFair.otherCharges);
   			// $scope.userFair.gst = Math.floor($scope.);
   			// $scope.userFair.total = Math.floor($scope.userFair.gst + $scope.userFair.netcost)
   		}


      // SAVE BOOKING AND SEND MAIL
      $scope.saveBooking = function(){
        let obj = {};
        obj = JSON.parse(localStorage.getItem('localData'))
        obj.costs = {
          flyingcost: $scope.systemFair.netFlyingCost,
          ground_handling: $scope.systemFair.groundHandlingCharges,
          other_charges: $scope.systemFair.otherCharges,
          crew_charges: $scope.systemFair.totalCrewCharge,
          sub_total: $scope.systemFair.netcost,
          gst: $scope.systemFair.gst,
          grand_total: $scope.systemFair.total
        }

        obj.client={
          client_name: $scope.cName,
          client_address : $scope.cAddress
        }
        let _temp_aircraft_name = $scope.systemFair.documents.jet_name
        // SEARCH AIRCRAFT ID
        console.log("$scope.aircrafts",$scope.aircrafts)
        for (var i = 0; i < $scope.aircrafts.length; i++) {
          let _aircraft_name_to_search = $scope.aircrafts[i].jet_name
          if(_aircraft_name_to_search.toLowerCase() == _temp_aircraft_name.toLowerCase()){
            obj.aircratft_id = $scope.aircrafts[i]._id;
            obj.aircraft_type = $scope.aircrafts[i].aircraft_type;
            console.log($scope.aircrafts[i])
            console.log(_temp_aircraft_name, obj.aircratft_id)
          }
        }
        obj.created_by = (JSON.parse(localStorage.getItem('userDetail')))._id
        
        obj.aircraft_name = obj.aircraft;
        obj.aircraft_registration =$scope.flightNo;
        obj.aircraft_type = $scope.airType; 
         obj.oprator_name = $scope.userDetailll.user_name 
        console.log("reqqqqq===>",obj);
        aircraft.saveBooking(obj)
        .then(objS=>{
          console.log(objS)
           $scope.isSearchResult = false
          $scope.msg = objS.data.bookingNumber
          $fancyModal.open({
            template: '<div style="padding:50px;text-align:center"><h1>Booking Number</h1><p style="font-size: 30px;background: #000;color: #fff;display: inline;padding: 8px 26px;">{{msg}}</P></div>',
            scope: $scope,
            openingClass: 'animated '+modalAnimation[9].in,
            closingClass: 'animated '+modalAnimation[9].out,
            openingOverlayClass: 'animated fadeIn',
            closingOverlayClass: 'animated fadeOut',
          });

            $scope.invoice = obj;
            console.log("data =>"+JSON.stringify($scope.invoice))
            $timeout(function(){
              aircraft.pdfOperator('pdf-dynamic','#pdfFormat','Quotation.pdf',function(path){
                aircraft.quotationEmail({"email_id":$scope.emailToSend,"path":path,"_id":objS.data._id}).then(objS=>{
                  console.log(objS);
                },objE=>console.log(objE))
              });
              $timeout(function(){
                $('#pdf-dynamic').css('display','none');
              },100)
            },10);
            // $state.reload();

        },objE=>console.log(objE))
      }
      // SAVE BOOKING AND SEND MAIL END

		$scope.openModal=function(type,obj){
			
			// var IndxAnimation=getIndex(prev);
			// console.log(IndxAnimation);
			switch(type){
				case "new":
				// $scope.Title="Add";
				// $scope.new={};
				// $scope.showSubmit=true;
				// $scope.new.country=$scope.Countries[0];
				// $scope.new.region=$scope.Regions[0];
				break;

				case "update":
				// $scope.Title="Update";
				// $scope.new=obj;
				// $scope.showSubmit=true;
				// $scope.new.country=$scope.Countries.find(x=>x._id==obj._id);
				// $scope.new.region=$scope.Regions.find(x=>x._id==obj.states.region._id);
				break;

				case "view":
				// $scope.Title="";
				// $scope.new=obj;
				// $scope.showSubmit=false;
				// $scope.new.country=$scope.Countries.find(x=>x._id==obj._id);
				// $scope.new.region=$scope.Regions.find(x=>x._id==obj.states.region._id);
				break;
			}
		 $fancyModal.open({
			templateUrl: 'journey-Modal.html',
			scope: $scope,
			openingClass: 'animated '+modalAnimation[9].in,
			closingClass: 'animated '+modalAnimation[9].out,
			openingOverlayClass: 'animated fadeIn',
			closingOverlayClass: 'animated fadeOut',
		  });
		 //prev=IndxAnimation;
		};

		
		
	}])
.directive('datePicker',function(){
	// Runs during compile
	
		return {
                restrict: "A",
                require: "ngModel",
                link: function (scope, element, attrs, ngModelCtrl) {
                    var parent = element.parent();
                    var dtp = parent.datetimepicker({
                        format: "DD MMMM YYYY"
                    });
                    dtp.on("dp.change", function (e) {
                        ngModelCtrl.$setViewValue(moment(e.date).format("DD MMMM YYYY"));
                        scope.$apply();
                    });
                }
            };
})
.directive('timePicker',function(){
	// Runs during compile

	return {
                restrict: "A",
                require: "ngModel",
                link: function (scope, element, attrs, ngModelCtrl) {
                    var parent = element.parent();
                    var dtp = parent.datetimepicker({
                        format: 'LT'
                    });
                    dtp.on("dp.change", function (e) {
                        ngModelCtrl.$setViewValue(moment(e.date).format("LT"));
                        scope.$apply();
                    });
                }
            };
	
})
.filter('roundoff', function() {
    return function(value) {
        return Math.floor(value)
    }
})
})()
/*

*/
