(function(){
	angular.module('controllers')
	.controller('operatorListAircraftCtrl', ['$q','$http','$scope', '$rootScope', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'baseURL','aircraft','$stateParams', function($q,$http,$scope, $rootScope, $state,user,customAlert,$fancyModal,modalAnimation,master,baseURL,aircraft,$stateParams){
       $q.all([aircraft.getAllOperator()]).then(function(objS){
        console.log("resresres",objS[0].data);
        if(objS[0].data.response_code ==  200){
          $scope.aircrafts=objS[0].data.result.map(x=>{x.baggage =parseInt(x.baggage); return x});;
         console.log("success",$scope.aircrafts);
          }else{
            alert("response error");
             }
       },function(objE){
        alert("Network Connection Problem!");
   })
       $scope.currentPage = 1;
       $scope.limit = 10;
       $scope.showPagination = false


   var getAirportsList = function(){
     master.airports()
     .then(function(objS){
       var useableData = []
       // console.log("Data =>"+JSON.stringify(objS))
       for (var i = 0; i < objS.length; i++) {
         var allData = objS[i]
         /**/
         for (var j = 0; j < allData.airportList.length; j++) {
           var airport = allData.airportList[j]
           useableData.push({
             airport : airport.airport,
             stateName : allData.states.stateName,
             state : allData.states._id,
             countryName : allData.countryName,
             country : allData._id,
             cityName : allData.states.cities.cityName,
             city : allData.states.cities._id,
             iata : airport.iata,
             icao : airport.icao,
             category : airport.category,
             role : airport.role,
             runway : airport.runway,
             groundhandling : airport.groundhandling,
             _id : airport._id,
           })
         }
         /**/
       }
       $scope.showPagination = true;
       $scope.Airports = useableData
       console.log('Airports')
       console.dir($scope.Airports)
     },function(objE){
       console.log(objE)
     })
   }
   getAirportsList()
   $scope.pageChange=function(page){
      $scope.currentPage = page;
      $scope.getList($scope.currentPage)
    }

       $scope.openModal=function(type,obj){
         
            switch(type){
              // case "new":
              // $scope.type = type;
              // $scope.Title="Add";
              // $scope.new={};
              // $scope.showSubmit=true;
              // break;

              // case "update":
              // $scope.type = type;
              // $scope.Title="Update";
              // $scope.new=obj;
              // $scope.showSubmit=true;
              // break;

              case "view":
              $scope.type = type;
              localStorage.Title="";
              $state.go('main.aircraft.operatorViewAircraft',{id:obj._id})
              localStorage.showSubmit=false;
              break;
            }
           // $fancyModal.open({
           //      templateUrl: 'aircraft-Modal.html',
           //      scope: $scope,
           //      openingClass: 'animated '+modalAnimation[9].in,
           //      closingClass: 'animated '+modalAnimation[9].out,
           //      openingOverlayClass: 'animated fadeIn',
           //      closingOverlayClass: 'animated fadeOut',
           //    });
           //prev=IndxAnimation;
      }; 


 }])
})();