(function(){
	angular.module('controllers')
	.controller('operatorViewAircraftCtrl', ['$q','$scope', '$rootScope', '$http', '$state','booking','customAlert','$fancyModal',"modalAnimation","master",'baseURL','$timeout','aircraft', function($q,$scope, $rootScope, $http, $state,booking,customAlert,$fancyModal,modalAnimation,master,baseURL,$timeout,aircraft){

		 // var getAirportsList = function(){
		     master.airports()
		     .then(function(objS){
		       var useableData = []
		       // console.log("Data =>"+JSON.stringify(objS))
		       for (var i = 0; i < objS.length; i++) {
		         var allData = objS[i]
		         /**/
		         for (var j = 0; j < allData.airportList.length; j++) {
		           var airport = allData.airportList[j]
		           useableData.push({
		             airport : airport.airport,
		             stateName : allData.states.stateName,
		             state : allData.states._id,
		             countryName : allData.countryName,
		             country : allData._id,
		             cityName : allData.states.cities.cityName,
		             city : allData.states.cities._id,
		             iata : airport.iata,
		             icao : airport.icao,
		             category : airport.category,
		             role : airport.role,
		             runway : airport.runway,
		             groundhandling : airport.groundhandling,
		             _id : airport._id,
		           })
		         }
		         /**/
		       }

		       $scope.Airports = useableData
		       
		       console.log($scope.Airports)
		     },function(objE){
		       console.log(objE)
		     })
		   // }
		   // getAirportsList()
		var id = $state.params.id;
		console.log("id ",id)
		$scope.Title = localStorage.Title;
		$scope.showSubmit = localStorage.showSubmit;
		aircraft.opertaor_aircrft_detail({'_id':id}).then(function(objS){
				$scope.new = objS.result;
				$timeout(function(){
					console.log('Airports',$scope.Airports)
					$scope.new.bases = $scope.new.bases.map(i=>{i.base = $scope.Airports[$scope.Airports.findIndex(x=>x._id == i.base_city)].cityName; return i})
				}, 100);
				console.log("Detail =>",$scope.new.bases)
			},function(objE){
				console.log(objE)
			})
	    
	}])
})()