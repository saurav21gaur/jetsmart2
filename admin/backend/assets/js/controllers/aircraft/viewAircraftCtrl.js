(function(){
	angular.module('controllers')
	.controller('listAircraftCtrl', ['$q','$http','$scope', '$rootScope', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'baseURL','aircraft', '$timeout', function($q,$http,$scope, $rootScope, $state,user,customAlert,$fancyModal,modalAnimation,master,baseURL,aircraft, $timeout){
		 $scope.aircraft={files:[]}
    $scope.new = {}
    $scope.new.tempImages = []
    $scope.currentPage = 1;
    $scope.limit = 10
    $scope.totalData = Infinity;
    $scope.aircraft={files:[]}
    $scope.showPagination = false
    $scope.show = false;
    
    aircraft.download_aircraft()
      .then(function(objS){
        $scope.aircraftsDownload = objS;
        // $scope.aircraftsDownload=$scope.aircraftsDownload.map(x=>{x.baggage =parseInt(x.baggage); x.passangers =parseInt(x.passangers); x.pilot =parseInt(x.pilot); x.speed =parseInt(x.speed); return x});
        console.log("Response =>"+objS);
      },function(objE){
        customAlert.show("Error",objE.msg)
      })
    $scope.exportDataXLS = function () {
        var blob = new Blob([document.getElementById('exportTab').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            });
        saveAs(blob, "Report.xls");
    };
    $scope.exportDataPdf = function () {
        $scope.show = true;
        $timeout(function(){$scope.show = false; console.log('timeout')}, 100)
        html2canvas(document.getElementById('exportTab'), {
            onrendered: function (canvas) {
                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("Report.pdf");
            }
        });
    };

    $scope.getList = function(page){
      aircraft.getAll(page)
      .then(function(objS){
        // $scope.totalData = objS.pagination.total;
        // $scope.limit = objS.pagination.limit;
        $scope.aircrafts = objS.result;

        $scope.aircrafts=$scope.aircrafts.map(x=>{x.baggage =parseInt(x.baggage); x.passangers =parseInt(x.passangers); x.pilot =parseInt(x.pilot); x.speed =parseInt(x.speed); return x});
        console.log("Response =>"+objS.result);
        $scope.showPagination = true;
      },function(objE){
        customAlert.show("Error",objE.msg)
      })
    }
    $scope.getList($scope.currentPage)
    // $q.all([aircraft.getAll(page)]).then(function(arr){
    //   $scope.aircrafts=arr[0].map(x=>{x.baggage =parseInt(x.baggage); x.passangers =parseInt(x.passangers); x.pilot =parseInt(x.pilot); x.speed =parseInt(x.speed); return x});
    //   console.log($scope.aircrafts)
    // })
    $scope.pageChange=function(page){
      $scope.currentPage = page;
      $scope.getList($scope.currentPage)
    }
      $scope.uploadFile = function(){
           // $scope.aircraft.files=$('#filesAirCraft').prop('files');
           $scope.isFileUploading=true;
           $scope.isPopupOpen = true;
           aircraft.uploadFiles($('#filesAirCraft').prop('files'))
           .then(function(objS){
            $scope.isFileUploading=false;
            console.log("file =>",objS)
            $scope.aircraft.files=$scope.aircraft.files.concat(objS.file)
            setTimeout(function(){
              $('.cls-gallary').width($('.cls-gallary .gallary-img').length* ($('.cls-gallary .gallary-img').width()+20))
            },2000)
           },function(objE){
            $scope.isFileUploading=false;
            customAlert("Files Upload","Error in file uploading.")
           })
           //fileUpload.uploadFileToUrl(file, uploadUrl);
           
        };



        // $scope.removeFile=function(file){
        //   aircraft.deleteAircraftImg(file)
        //   .then(function(objS){

        //   },function(objE){
        //     console.log("Error--->",objE)
        //   })
        // };

		// $scope.uploadFile = function(){
  //          // $scope.aircraft.files=$('#filesAirCraft').prop('files');

  //          $scope.isFileUploading=true;
  //          aircraft.uploadFiles($('#filesAirCraft').prop('files'))
  //          .then(function(objS){
  //           $scope.isFileUploading=false;
  //           // $scope.aircraft.files=$scope.aircraft.files.concat(objS)
  //           var tempGallary = []
  //           for (var i = 0; i < objS.length; i++) {
  //             tempGallary.push(objS[i])
  //           }
  //           $scope.new.tempImages=tempGallary
  //           console.log('img upload',$scope.new.tempImages)
  //          	setTimeout(function(){
  //          		$('.cls-gallary').width($('.cls-gallary .gallary-img').length* ($('.cls-gallary .gallary-img').width()+20))
  //          	},2000)
  //          },function(objE){
  //          	$scope.isFileUploading=false;
  //          	customAlert("Files Upload","Error in file uploading.")
  //          })
  //          //fileUpload.uploadFileToUrl(file, uploadUrl);
           
  //       };
        $scope.removeFile=function(file, index){
          var ind = $scope.aircraft.files.indexOf(file);
          console.log(ind)
        	aircraft.deleteAircraftImg(file)
        	.then(function(objS){
            $scope.aircraft.files.splice(ind,1);
           
        	},function(objE){
        		console.log("Error--->",objE)
        	})
        };

        var getAirportsList = function(){
          master.airports()
          .then(function(objS){
            var useableData = []
            // console.log("Data =>"+JSON.stringify(objS))
            for (var i = 0; i < objS.length; i++) {
              var allData = objS[i]
              /**/
              for (var j = 0; j < allData.airportList.length; j++) {
                var airport = allData.airportList[j]
                useableData.push({
                  airport : airport.airport,
                  stateName : allData.states.stateName,
                  state : allData.states._id,
                  countryName : allData.countryName,
                  country : allData._id,
                  cityName : allData.states.cities.cityName,
                  city : allData.states.cities._id,
                  iata : airport.iata,
                  icao : airport.icao,
                  category : airport.category,
                  role : airport.role,
                  runway : airport.runway,
                  groundhandling : airport.groundhandling,
                  _id : airport._id,
                })
              }
              /**/
            }

            $scope.Airports = useableData
            console.log('Airports')
            console.dir($scope.Airports)
          },function(objE){
            console.log(objE)
          })
        }
        getAirportsList()
/*
        $scope.addBase = function(){
          if($scope.new.bases.length >= 11)
            return;
            // alert('Cannot add more than 11 base')
          else
            $scope.new.bases.push({})
        }

        $scope.removeBase = function(){
          $scope.new.bases.pop({})
        }*/


  $scope.openModal=function(type,obj){
      console.log(obj)
      // alert("hello")
      // var IndxAnimation=getIndex(prev);
      // console.log(IndxAnimation);

      switch(type){
        case "new":
        $scope.aircraft.files = [];
        $scope.type = type;
        $scope.Title="Add";
         $scope.new={bases:
            [
              {'base_city':'5a3b98f630eac9846fbd9f24','city':'New Delhi','base_price':0},
              {'base_city':'5a3b99cb30eac9846fbda21a','city':'Mumbai','base_price':0},
              {'base_city':'5a3b99cb30eac9846fbda21c','city':'Pune','base_price':0},
              {'base_city':'5a3b98f630eac9846fbd9eeb','city':'Bangalore','base_price':0},
              {'base_city':'5a3b99cb30eac9846fbda25a','city':'Chennai','base_price':0},
              {'base_city':'5a3b99cb30eac9846fbda230','city':'Hydrabad','base_price':0},
              {'base_city':'5a3b98f630eac9846fbd9f32','city':'Ahemdabad','base_price':0},
              {'base_city':'5a3b99cb30eac9846fbda268','city':'Kolkata','base_price':0},
              {'base_city':'5a3b99cb30eac9846fbda242','city':'Bhopal','base_price':0},
              {'base_city':'5a3b98f630eac9846fbd9f10','city':'Raigarh','base_price':0},
              {'base_city':'5a3b99cb30eac9846fbda1e1','city':'Indore','base_price':0}
            ]
         };
        $scope.showSubmit=true;
         $scope.isPopupOpen = true;
        break;

        case "update":
        $scope.type = type;
        $scope.Title="Update";
        $scope.new=obj;
        $scope.new.bases = $scope.new.bases.map(i=>{i.city = $scope.Airports[$scope.Airports.findIndex(x=>x._id == i.base_city)].cityName; return i})
        if($scope.new.image_gallary){
          for(var i=0;i<$scope.new.image_gallary.length;i++){
            $scope.aircraft.files = 'http://35.154.217.225/'+$scope.new.image_gallary[i]; 
          }
        }
        
        $scope.aircraft.files = $scope.new.image_gallary;
        console.log("obj ==>"+JSON.stringify($scope.aircraft.files))
        $scope.showSubmit=true;
         $scope.isPopupOpen = true;
        break;

        case "view":
        // $scope.type = type;
        localStorage.Title="";
        // localStorage.airCraftId =obj._id;
        localStorage.showSubmit=false;
        $state.go('main.aircraft.aircraftDetail',{id:obj._id})
        // $scope.new.bases = $scope.new.bases.map(i=>{i.base = $scope.Airports[$scope.Airports.findIndex(x=>x._id == i.base_city)].cityName; return i})

        // $scope.isPopupOpen = false;
        // $fancyModal.open({
        //       templateUrl: 'aircraft-Modal.html',
        //       scope: $scope,
        //       openingClass: 'animated '+modalAnimation[9].in,
        //       closingClass: 'animated '+modalAnimation[9].out,
        //       openingOverlayClass: 'animated fadeIn',
        //       closingOverlayClass: 'animated fadeOut',
        //     });
        break;

        
        case "Delete":
        aircraft.delete({aircraftId:obj._id})
          .then(function(objS){
            console.log(objS);
            $state.reload();
          },function(objE){
            console.log("Error--->",objE)
          })
        break;
      }
     // $fancyModal.open({
     //      templateUrl: 'aircraft-Modal.html',
     //      scope: $scope,
     //      openingClass: 'animated '+modalAnimation[9].in,
     //      closingClass: 'animated '+modalAnimation[9].out,
     //      openingOverlayClass: 'animated fadeIn',
     //      closingOverlayClass: 'animated fadeOut',
     //    });
     //prev=IndxAnimation;
    };  
		$scope.submitForm=function(isValid){
			// if(!isValid)
			// 	return;

      // var tempGallary = [];
      // if($scope.new.tempImages.length){
      //   for (var i = 0; i < $scope.new.tempImages.length; i++) {
      //     tempGallary.push($scope.new.tempImages[i].file)
      //   }
      // }


      if($scope.new.bases.length > 0 && $scope.new.bases.length < 11)
         customAlert.show("Aircraft","Please Add all Bases");
        // alert('Please Add 11 bases')
      else{
       $scope.new.image_gallary=$scope.aircraft.files;
       console.log('Req =>',$scope.new)
        aircraft.createUpdateAircraft($scope.new)
        .then(function(objS){
          $state.reload();
          // customAlert.show("Aircraft","Aircraft "+$scope.Title+" successfully.");
          $scope.isPopupOpen = false;
          $fancyModal.close();
            console.log(objS)
        },function(objE){
          customAlert.show("Country",objE.msg)
        })
      }
      
		}
	}])
})()
/*

*/