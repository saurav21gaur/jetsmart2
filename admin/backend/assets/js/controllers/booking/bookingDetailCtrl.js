(function(){
	angular.module('controllers')
	.controller('bookingDetailCtrl', ['$q','$scope', '$rootScope', '$http', '$state','booking','customAlert','$fancyModal',"modalAnimation","master",'baseURL','$timeout', function($q,$scope, $rootScope, $http, $state,booking,customAlert,$fancyModal,modalAnimation,master,baseURL,$timeout){

		$scope.Title = localStorage.Title;
		$scope.showSubmit = localStorage.showSubmit;
		booking.booking_detail({'_id':$state.params.id}).then(function(objS){
					console.log("data =>"+JSON.stringify(objS))
						var _temp = objS
						let date = new Date(_temp.created_at)
						var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
						_temp.time = (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?' pm':' am')

						for (var j = 0; j < _temp.roots.length; j++) {
							_tempRoot = _temp.roots[j];

							let rootDate = new Date(_tempRoot.departureDate)
							console.log(rootDate)
							_tempRoot.deptime = (rootDate.getDate()<10?'0'+rootDate.getDate():rootDate.getDate())+' '+monthNames[rootDate.getMonth()]+' '+ rootDate.getFullYear()+' '+ (rootDate.getHours()>12?rootDate.getHours()-12:rootDate.getHours())+':'+ rootDate.getMinutes()+(rootDate.getHours()>12?' pm':' am')
						}
					
				$scope.new = objS;
				console.log("Detail =>",JSON.stringify(objS))
			},function(objE){
				console.log(objE)
			})

		$scope.ageType = function(age, idx){
			console.log("Age =>"+age)
			if(age <= 2 && age > 0)
				$scope.arrDetail[idx].ageUserType = 'Infant'
			else if(age >= 2 && age <= 12)
				$scope.arrDetail[idx].ageUserType = 'Child'
			else if(age >= 12)
				$scope.arrDetail[idx].ageUserType = 'Adult'
			else if(age == null)
				$scope.arrDetail[idx].ageUserType = '' 
		}


	   $scope.submitData  = function(){

	   	console.log("Action=>",$scope.actionTaken)
        	console.log("req==>",$scope.addPassData)
        	if($scope.actionTaken == 'Add'){
        		booking.addBookingDetail($scope.addPassData, $state.params.id).then(function(objS){
					console.log("data =>"+JSON.stringify(objS))
					$fancyModal.close();
					$state.reload();
				},function(objE){
					console.log(objE)
				})
        	}
        	else{
        		 // $scope.addPassData.pessenger_id = $scope.addPassData._id;
        		booking.updateBookingDetail($scope.addPassData, $state.params.id , $scope.addPassData._id).then(function(objS){
					console.log("data =>"+JSON.stringify(objS))
					$fancyModal.close();
					$state.reload();
				},function(objE){
					console.log(objE)
				})
        	}
        	
        }
	
        $scope.addPassanger = function(data,detail){
          console.log("Action",data);
          $scope.actionTaken = data;
          if($scope.actionTaken == 'Add'){
            $scope.addPassData = {};
          }
          else{
            $scope.addPassData = angular.copy(detail);
          }
          $fancyModal.open({
              templateUrl: 'passenger-Modal.html',
              scope: $scope,
              openingClass: 'animated '+modalAnimation[9].in,
              closingClass: 'animated '+modalAnimation[9].out,
              openingOverlayClass: 'animated fadeIn',
              closingOverlayClass: 'animated fadeOut',
            });
        }


	    
	}])
})()