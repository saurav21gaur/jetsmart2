(function(){
	angular.module('controllers')
	.controller('viewBookingCtrl', ['$q','$scope', '$rootScope', '$http', '$state','booking','customAlert','$fancyModal',"modalAnimation","master",'baseURL','$timeout', function($q,$scope, $rootScope, $http, $state,booking,customAlert,$fancyModal,modalAnimation,master,baseURL,$timeout){

		console.log('booking ctrl')
		$scope.currentPage = 1;
	    $scope.limit = 10
	    $scope.totalData = Infinity
	    $scope.showPagination = false
		var getBookingList = function(page){
			booking.list(page).then(function(objS){
				for (var i = 0; i < objS.length; i++) {
					var _temp = objS[i]
					let date = new Date(_temp.created_at)
					var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
					_temp.time = (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?' pm':' am')

					// for (var j = 0; j < _temp.roots.length; j++) {
					// 	_tempRoot = _temp.roots[j]
					// 	let rootDate = new Date(_tempRoot.departureDate)
					// 	_tempRoot.time = (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?' pm':' am')
					// }
					//  _temp.contactDet = objS[i].contact.title + objS[i].contact.name +' ('+objS[i].contact.phone_number+')';
				}
				// $scope.totalData = objS.pagination.total;
		        // $scope.limit = objS.pagination.limit;
				$scope.Bookings = objS;
				$scope.showPagination = true;
				console.log("$scope.Bookings",objS)
			},function(objE){
				console.log(objE)
			})
		}
		getBookingList($scope.currentPage)

		$scope.pageChange=function(page){
	      $scope.currentPage = page;
	      $scope.getBookingList($scope.currentPage)
	    }

	    $scope.show = false;
	    $scope.exportDataXLS = function () {
	        var blob = new Blob([document.getElementById('exportTab').innerHTML], {
	            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
	            });
	        saveAs(blob, "Booking List.xls");
	    };
	    $scope.exportDataPdf = function () {
	        $scope.show = true;
	        $timeout(function(){$scope.show = false; console.log('timeout')}, 100)
	        html2canvas(document.getElementById('data-table'), {
	            onrendered: function (canvas) {
	                var data = canvas.toDataURL();
	                var docDefinition = {
	                    content: [{
	                        image: data,
	                        width: 500,
	                    }]
	                };
	                pdfMake.createPdf(docDefinition).download("Booking List.pdf");
	            }
	        });
	    };

		$scope.openModal=function(type,obj){
			console.log(obj)

			switch(type){
				case "new":
					$scope.type = type;
					localStorage.Title="Add";
					$scope.new={};
					$scope.showSubmit=true;
					$state.go('main.booking.add');
					break;

				case "update":
					$scope.type = type;
					$scope.Title="Update";
					$scope.new=obj;
					localStorage.Title="Update";
					$state.go('main.booking.add',{id:obj._id});
					$scope.showSubmit=true;
					break;

				case "view":
					$scope.type = type;
					localStorage.Title="";
					localStorage.bookId = obj._id;
					$state.go('main.booking.view',{id:obj._id});
					localStorage.showSubmit=false;
					break;
			}

			// $fancyModal.open({
			// 	templateUrl: 'booking-Modal.html',
			// 	scope: $scope,
			// 	openingClass: 'animated '+modalAnimation[9].in,
			// 	closingClass: 'animated '+modalAnimation[9].out,
			// 	openingOverlayClass: 'animated fadeIn',
			// 	closingOverlayClass: 'animated fadeOut',
		 //    });
		};  

	}])
})()