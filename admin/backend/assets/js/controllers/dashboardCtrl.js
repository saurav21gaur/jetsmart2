(function(){
	angular.module('controllers')
	.controller('dashboardCtrl', ['$scope', '$rootScope', 'statistics', '$http', '$state','customAlert','$fancyModal',"modalAnimation", 'user', '$compile', "calendarConfig",'aircraft', '$q', function($scope, $rootScope, statistics, $http, $state,customAlert,$fancyModal,modalAnimation, user, $compile, calendarConfig, aircraft, $q){

		statistics.all()
		.then(function(objS){
			$scope.Stact = objS.result
         var number = parseInt($scope.Stact.totalrevenu[0].balance);
		    if(number > 999999999999) {
		      number = (number/1000000000000.0).toFixed(2)+ "T";
		    } else if(number > 999999999) {
		      number =(number/1000000000.0).toFixed(2) + "B";
		    } else if(number > 999999) {
		      number = (number/1000000.0).toFixed(2) + "M";
		    } else if(number > 999) {
		      number = (number/1000.0).toFixed(2) + "K";
		    }

      $scope.totalrev = number
       

			console.log(objS)
		},function(objE){
			console.log(objE)
		})
		$scope.events=[];
		$scope.access={}
		if(angular.isDefined(localStorage.adminType))
			{
				$scope.userType = localStorage.adminType;
				console.log("type =>"+$scope.userType)
				
				if(localStorage.access){
					$scope.accessEmp = JSON.parse(localStorage.access);
					console.log('access===>',$scope.accessEmp)
				}
			}
			else{
				$state.go('login')
			}

		if(angular.isDefined(localStorage.username))
			{
				$scope.username=localStorage.username
				console.log("name =>"+$scope.username)
			}


		if(angular.isDefined(localStorage.access_level && localStorage.access_level!='')){
			$scope.access = JSON.parse(localStorage.access_level)
		}

		   $q.all([aircraft.getAllOperator()]).then(function(objS){
		        console.log("resresres",objS[0].data);
		        if(objS[0].data.response_code ==  200){
		          $scope.aircrafts=objS[0].data.result.map(x=>{x.baggage =parseInt(x.baggage); return x});;
		         console.log("success",$scope.aircrafts);
		          }else{
		            alert("response error");
		             }
		       },function(objE){
		        alert("Network Connection Problem!");
		   })

		$scope.logout=function(){
			// localStorage.clear();
			delete localStorage.userloggedin;
			delete localStorage.token;
			delete localStorage.adminType;
			delete localStorage.userId;
			delete localStorage.userDetail;
			delete localStorage.access_level;
			delete localStorage.username;
			delete localStorage.access;
			$state.go("login");
		}
		$scope.changePassword=function(){
			$fancyModal.open({
		        templateUrl: 'change-Password.html',
		        scope: $scope,
		        openingClass: 'animated '+modalAnimation[9].in,
		        closingClass: 'animated '+modalAnimation[9].out,
		        openingOverlayClass: 'animated fadeIn',
		        closingOverlayClass: 'animated fadeOut',
		      });
		}
		$scope.new ={};
		$scope.submitPassword=function(isValid){
			if($scope.new.newPassword != $scope.new.confirmPassword){
				customAlert.show("Error","Please enter correct password to confirm");
			}
			else{
				var data = {
					"username" : $scope.username,
					"type" : $scope.userType,
					"password" :$scope.new.oldPassword,
					"newPassword" :$scope.new.newPassword
				}
				statistics.changePassword(data)
				.then(function(objS){
					if(objS.code == 200){
						$fancyModal.close();
						console.log(objS.msg);	
					}
					else{
						// $fancyModal.close();
						customAlert.show("Error",objS.msg)
					}
				},function(objE){
					customAlert.show("Error",objE.msg)
				})
			}
		}
		// console.log($scope.access)

		$scope.navigate = function(page){
			$state.go(page)
		}




		///////////////////// BEGIN EVENT CALENDAR ///////////////////////////

		  var date = new Date();
		  var d = date.getDate();
		  var m = date.getMonth();
		  var y = date.getFullYear();
		  $scope.isEvents = false;

		 

		  $scope.calenderData={
		    color: '#0088cc',
		    events: []
		  }

		 if(angular.isDefined(localStorage.adminType))
		 {
		   $scope.userType = localStorage.adminType;
		   console.log("type =>"+$scope.userType)
		 
		  user.allBookings()
		    .then(objS=>{
		      let events = objS;
		      console.log(objS)
		      $scope.bookings = events.length
		      console.log('bookings ====>',events)
		      $scope.calenderData.events = []
		      for (var i = 0; i < events.length; i++) {
		        let event = events[i]
		        // console.log('event==>',event)
		        $scope.events.push({
			      title: event.aircratft_id.flight_number,
			      startsAt: new Date(event.roots[0].departureDate),
			      color: calendarConfig.colorTypes.info,
			      allDay: true,
			      id:event._id
			    })
		        // $scope.calenderData.events.push({title: event.aircratft_id.flight_number, start: new Date(event.roots[0].departureDate),allDay: true, id:event._id})

		      }
		      // $scope.events = [
		      //   {title: 'All Day Event',start: new Date(y, m, 1)},
		      //   {title: 'Long Event',start: new Date(y, m, d - 5),end: new Date(y, m, d - 2)},
		      //   {id: 999,title: 'Repeating Event',start: new Date(y, m, d - 3, 16, 0),allDay: false},
		      //   {id: 999,title: 'Repeating Event',start: new Date(y, m, d + 4, 16, 0),allDay: false},
		      //   {title: 'Birthday Party',start: new Date(y, m, d + 1, 19, 0),end: new Date(y, m, d + 1, 22, 30),allDay: false},
		      //   {title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
		      // ];
		      $scope.eventSources = [$scope.calenderData];
		      $scope.isEvents = true
		    },objE=>console.log(objE))
		    }
		 else{
		   $state.go('login')
		 }
		  
		  $scope.changeTo = 'Hungarian';
		  /* event source that pulls from google.com */
		  /*$scope.eventSource = {
		          url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
		          className: 'gcal-event',           // an option!
		          currentTimezone: 'America/Chicago' // an option!
		  };*/
		  /* event source that contains custom events on the scope */
		  /*$scope.events = [
		    {title: 'All Day Event',start: new Date(y, m, 1)},
		    {title: 'Long Event',start: new Date(y, m, d - 5),end: new Date(y, m, d - 2)},
		    {id: 999,title: 'Repeating Event',start: new Date(y, m, d - 3, 16, 0),allDay: false},
		    {id: 999,title: 'Repeating Event',start: new Date(y, m, d + 4, 16, 0),allDay: false},
		    {title: 'Birthday Party',start: new Date(y, m, d + 1, 19, 0),end: new Date(y, m, d + 1, 22, 30),allDay: false},
		    {title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
		  ];*/
		  /* event source that calls a function on every view switch */
		  $scope.eventsF = function (start, end, timezone, callback) {
		    var s = new Date(start).getTime() / 1000;
		    var e = new Date(end).getTime() / 1000;
		    var m = new Date(start).getMonth();
		    var events = [{title: 'Feed Me ' + m,start: s + (50000),end: s + (100000),allDay: false, className: ['customFeed']}];
		    callback(events);
		  };

		  /*
		  $scope.calEventsExt = {
		     color: '#f00',
		     textColor: 'yellow',
		     events: [ 
		       {type:'party',title: 'Lunch',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
		       {type:'party',title: 'Lunch 2',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
		       {type:'party',title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
		     ]
		  };
		  */
		  /* alert on eventClick */
		  $scope.alertOnEventClick = function( date, jsEvent, view){
		      $scope.alertMessage = (date.id + ' was clicked ');
		      $state.go('main.flight.detail',{id:date.id})
		  };
		  /* alert on Drop */
		   $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
		     $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
		  };
		  /* alert on Resize */
		  $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
		     $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
		  };
		  /* add and removes an event source of choice */
		  $scope.addRemoveEventSource = function(sources,source) {
		    var canAdd = 0;
		    angular.forEach(sources,function(value, key){
		      if(sources[key] === source){
		        sources.splice(key,1);
		        canAdd = 1;
		      }
		    });
		    if(canAdd === 0){
		      sources.push(source);
		    }
		  };
		  /* add custom event*/
		  $scope.addEvent = function() {
		    $scope.events.push({
		      title: 'Open Sesame',
		      start: new Date(y, m, 28),
		      end: new Date(y, m, 29),
		      className: ['openSesame']
		    });
		  };
		  /* remove event */
		  $scope.remove = function(index) {
		    $scope.events.splice(index,1);
		  };
		  /* Change View */
		  $scope.changeView = function(view,calendar) {
		    uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
		  };
		  /* Change View */
		  $scope.renderCalender = function(calendar) {
		    if(uiCalendarConfig.calendars[calendar]){
		      uiCalendarConfig.calendars[calendar].fullCalendar('render');
		    }
		  };
		   /* Render Tooltip */
		  $scope.eventRender = function( event, element, view ) { 
		      element.attr({'tooltip': event.title,
		                   'tooltip-append-to-body': true});
		      $compile(element)($scope);
		  };
		  /* config object */
		  $scope.uiConfig = {
		    calendar:{
		      height: 450,
		      editable: false,
		      header:{
		        left: 'title',
		        center: '',
		        right: 'today prev,next'
		      },
		      eventClick: $scope.alertOnEventClick,
		      eventDrop: $scope.alertOnDrop,
		      eventResize: $scope.alertOnResize,
		      eventRender: $scope.eventRender
		    }
		  };

		  $scope.changeLang = function() {
		    if($scope.changeTo === 'Hungarian'){
		      $scope.uiConfig.calendar.dayNames = ["Vasárnap", "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat"];
		      $scope.uiConfig.calendar.dayNamesShort = ["Vas", "Hét", "Kedd", "Sze", "Csüt", "Pén", "Szo"];
		      $scope.changeTo= 'English';
		    } else {
		      $scope.uiConfig.calendar.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
		      $scope.uiConfig.calendar.dayNamesShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
		      $scope.changeTo = 'Hungarian';
		    }
		  };

		   // $scope.events = [{
			  //     title: 'No event end date',
			  //     startsAt: moment().hours(3).minutes(0).toDate(),
			  //     color: calendarConfig.colorTypes.info
			  //   }, {
			  //     title: 'No event end date',
			  //     startsAt: moment().hours(5).minutes(0).toDate(),
			  //     color: calendarConfig.colorTypes.warning
			  //   }];

			  
		  $scope.calendarView = 'month';
		    $scope.viewDate = new Date();

		    $scope.eventClicked = function(event){
		    	console.log("eventClicked", event.id)
		    	 $state.go('main.flight.detail',{id:event.id})
		    }

		    $scope.clickEvent = function(vall){
		    	$scope.calendarView = vall;
		    }
		///////////////////// END EVENT CALENDAR ///////////////////////////
	}])
})()
