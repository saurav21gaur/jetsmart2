(function(){
    angular.module('controllers')
    .controller('employeeListCtrl', ['customAlert','$timeout','agent','$scope','employee','$state','$window', function(customAlert,$timeout,agent,$scope,employee,$state,$window){
      $scope.showSubmit=false;
      $scope.isPopupOpen = false;
      $scope.currentPage = 1;
       $scope.showPagination = false;
    	employee.list(localStorage.userId).then(function(objS){
          $scope.employee = objS.data;
          console.log(JSON.stringify(objS));
           $scope.showPagination = true;
        },function(objE){
          console.log(objE)
        })

        $scope.view =function(data){
        	console.log("data =>"+JSON.stringify(data));
        	$state.go('main.employeeView',{id: data._id})
        }

        $scope.submitForm = function(){
          $scope.new.type = 'employee';
          // console.log("Req =>"+JSON.stringify($scope.new))
          if($scope.Title=="Add"){
            employee.add($scope.new).then(function(objS){
              console.log(JSON.stringify(objS));
            },function(objE){
              customAlert.show("Employee",objE.data.msg)
              console.log(objE)
            })
          }else if($scope.Title=="Update"){
            employee.update($scope.new)
            .then(objS=>console.log(objS),objE=>console.log(objE))
          }else{
            alert('somethng wents wrong');
          }
          $scope.isPopupOpen = false;
          $state.reload();
        }
    $scope.pageChange=function(page){
      $scope.currentPage = page
      // $state.go('.',{page_no:page})
    }

        $scope.show = false;
      $scope.exportDataXLS = function () {
          var blob = new Blob([document.getElementById('exportTab').innerHTML], {
              type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
              });
          saveAs(blob, "Employee List.xls");
      };
      $scope.exportDataPdf = function () {
          $scope.show = true;
          $timeout(function(){$scope.show = false; console.log('timeout')}, 5)
          html2canvas(document.getElementById('data-table'), {
              onrendered: function (canvas) {
                  var data = canvas.toDataURL();
                  var docDefinition = {
                      content: [{
                          image: data,
                          width: 500,
                      }]
                  };
                  pdfMake.createPdf(docDefinition).download("Employee List.pdf");
              }
          });
      };

        $scope.openModal=function(type,obj){

          switch(type){
            case "new":
            $scope.Title="Add";
            $scope.new={};
            $scope.showSubmit=true;
            $scope.isPopupOpen = true;
            break;

            case "update":
            $scope.Title="Update";
            $scope.new=obj;
            $window.scrollTo(0, 0);
            $scope.showSubmit=true;
            $scope.isPopupOpen = true;
            break;

            case "view":
            $scope.Title="";
            $scope.new=obj;
            $scope.showSubmit=false;
            $scope.isPopupOpen = false;
            $state.go('main.employeeView')
            break;

            case "Delete":
            agent.agent_delete({_id:obj._id})
              .then(function(objS){
                console.log(objS);
                $state.reload();
              },function(objE){
                console.log("Error--->",objE)
              })
            break;
          }
        
         
         //prev=IndxAnimation;
        };  
  }])
})()