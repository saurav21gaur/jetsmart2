(function(){
	angular.module('controllers')
	.controller('employeeViewCtrl', ['$scope','employee','$state', function($scope,employee,$state){
		employee.details($state.params.id)
		.then(objS=>{
			$scope.employee = objS
			console.log($scope.employee);
		},objE=>console.log(objE))
	}])
})()