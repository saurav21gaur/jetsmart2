(function(){
    angular.module('controllers')
    .controller('emptyLegCtrl', ['$scope','emptyLeg','$state','$fancyModal',"modalAnimation",'aircraft', function($scope,emptyLeg,$state, $fancyModal, modalAnimation,aircraft){
      $scope.currentPage = 1;
       $scope.limit = 10;
       $scope.showPagination = false

    	emptyLeg.list(localStorage.userId).then(function(objS){
          $scope.emptyLeg = objS.data;
          console.log(JSON.stringify(objS));
            $scope.showPagination = true;
        },function(objE){
          console.log(objE)
        })
        $scope.delete = function(id){
          emptyLeg.empty_jet_delete({_id:id}).then(function(objS){
            $state.reload();
          },function(objE){
            console.log(objE)
          })
        }

        // aircraft.getAllAirport().then(function(objS){
        //   console.log("getAllAirport", objS);
        // }, function(objE){
        //   console.log("getAllAirport Error", objE)
        // })
      aircraft.getAllAirport().then(function(objS){
        console.log('all details')
        console.log(objS)
        var useableData = []
        for (var i = 0; i < objS.length; i++) {
          var allData = objS[i]
          /**/
          for (var j = 0; j < allData.airportList.length; j++) {
            var airport = allData.airportList[j]
            useableData.push({
              airport : airport.airport,
              stateName : allData.states.stateName,
              countryName : allData.countryName,
              cityName : allData.states.cities.cityName,
              iata : airport.iata,
              icao : airport.icao,
              _id : airport._id,
            })
          }
        }

        $scope.airportDropdown = useableData
        console.log($scope.airportDropdown)
      },function(objE){console.log(objE)})


        $scope.edit = function(data,detail){
          console.log("Action",data);
          $scope.actionTaken = data;
          $scope.edit_flight = angular.copy(detail);
          
          $fancyModal.open({
              templateUrl: 'emptyLeg-Modal.html',
              scope: $scope,
              openingClass: 'animated '+modalAnimation[9].in,
              closingClass: 'animated '+modalAnimation[9].out,
              openingOverlayClass: 'animated fadeIn',
              closingOverlayClass: 'animated fadeOut',
            });
        }

        $scope.view =function(data){
        	console.log("data =>"+JSON.stringify(data));
        	$state.go('main.emptyLegView')
        }

        $scope.changePrice=function(){
          $scope.edit_flight.price_per_seat= Math.round($scope.edit_flight.costs / $scope.edit_flight.no_of_seats);
        }

         $scope.submit = function(){
          // $scope.addPassData.flight_id = $state.params.id;
          console.log("submitPassanger", $scope.edit_flight)
            emptyLeg.Editemptyleg($scope.edit_flight).then(objS=>{
              console.log("success",objS);
              $fancyModal.close();
              $state.reload();
            },objE=>{
              console.log("Error",objE)
            })
          }
        
  }])
})()