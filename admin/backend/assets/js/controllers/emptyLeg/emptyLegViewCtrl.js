(function(){
    angular.module('controllers')
    .controller('emptyLegViewCtrl', ['$fancyModal','modalAnimation','$scope','emptyLeg','$state','$timeout', function($fancyModal,modalAnimation,$scope,emptyLeg,$state,$timeout){

      $scope.oprator_name = (JSON.parse(localStorage.userDetail)).user_name;
      // console.log("localStorage.userDetail",localStorage.userDetail)                                                                                            
      $scope.isAdmin = ((JSON.parse(localStorage.userDetail).type == 'admin') || (JSON.parse(localStorage.userDetail).type == 'employee'));
      // $scope.isAdmin = (JSON.parse(localStorage.userDetail)).type == 'employee';
      console.log("typeeeeeee",$scope.isAdmin)

      emptyLeg.manifest({_id: $state.params.id})
      .then(objS=>{
        console.log('menifest==>',JSON.stringify(objS.result))
        $scope.menifest = objS.result;
        $scope.passenger = []
        for (let i = 0; i < objS.result.pessenger.length; i++) {
          let _temp = objS.result.pessenger[i];
          for(let j=0; j < _temp.pessanger_detail.length; j++){
            let _pasenger = _temp.pessanger_detail[j]
            $scope.passenger.push(_pasenger)
          }
        }
      },objE=>console.log(objE))
    	 
        $scope.exportManifest =function(){
        	// $scope.invoice = obj;
         //    console.log("data =>"+JSON.stringify($scope.invoice))
            $timeout(function(){
              emptyLeg.pdf('pdf-dynamic','#pdfFormat','Manifest.pdf');
              $timeout(function(){
                $('#pdf-dynamic').css('display','none');
              },100)
            },10);
        }

        $scope.action = function(data,detail){
          console.log("Action",data);
          $scope.actionTaken = data;
          if($scope.actionTaken == 'Add'){
            $scope.addData = {};
          }
          else{
            $scope.addData = angular.copy(detail);
          }
          $fancyModal.open({
              templateUrl: 'country-Modal.html',
              scope: $scope,
              openingClass: 'animated '+modalAnimation[9].in,
              closingClass: 'animated '+modalAnimation[9].out,
              openingOverlayClass: 'animated fadeIn',
              closingOverlayClass: 'animated fadeOut',
            });
        }

        $scope.submitForm = function(){
          $scope.addData.emptyJet_id = $state.params.id;

          if($scope.actionTaken == 'Add'){
            emptyLeg.addCrewMember($scope.addData).then(objS=>{
              console.log("success",objS);
              $fancyModal.close();
              $state.reload();
            },objE=>{
              console.log("Error",objE)
            })
          }else{
            emptyLeg.updateCrewMember($scope.addData).then(objS=>{
              console.log("success",objS);
              $fancyModal.close();
              $state.reload();
            },objE=>{
              console.log("Error",objE)
            })

          
          }
        }
  }])
})()