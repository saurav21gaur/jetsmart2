
(function(){
	angular.module('controllers')
	.controller('adminEmptyJetCtrl', ['emptyLeg','$q','$http','$scope', '$rootScope', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'baseURL','aircraft', '$timeout', function(emptyLeg,$q,$http,$scope, $rootScope, $state,user,customAlert,$fancyModal,modalAnimation,master,baseURL,aircraft, $timeout){
        $scope.currentPage = 1;
        $scope.limit = 10;
       $scope.showPagination = false;
		emptyLeg.empty_jet_list()
        	.then(function(objS){
            	$scope.emptyLeg = objS.data;
            	console.log(objS)
                $scope.showPagination = true;
        	},function(objE){
        		console.log("Error--->",objE)
        	})
        $scope.delete = function(id){
          emptyLeg.empty_jet_delete({_id:id}).then(function(objS){
            $state.reload();
          },function(objE){
            console.log(objE)
          })
        }

        $scope.pageChange=function(page){
        $scope.currentPage = page
        // $state.go('.',{page_no:page})
      }

      $scope.show = false;
      $scope.exportDataXLS = function () {
          var blob = new Blob([document.getElementById('exportTab').innerHTML], {
              type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
              });
          saveAs(blob, "Empty Jet List.xls");
      };
      $scope.exportDataPdf = function () {
          $scope.show = true;
          $timeout(function(){$scope.show = false; console.log('timeout')}, 5)
          html2canvas(document.getElementById('data-table'), {
              onrendered: function (canvas) {
                  var data = canvas.toDataURL();
                  var docDefinition = {
                      content: [{
                          image: data,
                          width: 500,
                      }]
                  };
                  pdfMake.createPdf(docDefinition).download("Empty Jet List.pdf");
              }
          });
      };


    	$scope.view =function(data){
        	console.log("data =>"+JSON.stringify(data));
        	$state.go('main.emptyLegView')
        }
        	
	}])
})()
/*

*/