
(function(){
	angular.module('controllers')
	.controller('adminViewFlightCtrl', ['opretor','$q','$http','$scope', '$rootScope', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'baseURL','aircraft', '$timeout', function(opretor,$q,$http,$scope, $rootScope, $state,user,customAlert,$fancyModal,modalAnimation,master,baseURL,aircraft, $timeout){
		$scope.currentPage = 1;
		$scope.limit = 10;
       $scope.showPagination = false;
		opretor.adminFlightList()
        	.then(function(objS){
            	$scope.flights = objS;
            	$scope.showPagination = true;
        	},function(objE){
        		console.log("Error--->",objE)
        	})

        	$scope.pageChange=function(page){
		        $scope.currentPage = page
		        // $state.go('.',{page_no:page})
		      }

		      $scope.show = false;
		      $scope.exportDataXLS = function () {
		          var blob = new Blob([document.getElementById('exportTab').innerHTML], {
		              type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
		              });
		          saveAs(blob, "Flight List.xls");
		      };
		      $scope.exportDataPdf = function () {
		          $scope.show = true;
		          $timeout(function(){$scope.show = false; console.log('timeout')}, 5)
		          html2canvas(document.getElementById('data-table'), {
		              onrendered: function (canvas) {
		                  var data = canvas.toDataURL();
		                  var docDefinition = {
		                      content: [{
		                          image: data,
		                          width: 500,
		                      }]
		                  };
		                  pdfMake.createPdf(docDefinition).download("Flight List.pdf");
		              }
		          });
		      };

        	
        	$scope.openModal=function(type,obj){
		      console.log(obj)
		      switch(type){
		        case "view":
		        $state.go('main.flight.detail',{id:obj._id})
		        break;
		        case "delete":
		        opretor.flight_delete({_id:obj._id}).then(function(objS){
		            $state.reload();
		          },function(objE){
		            console.log(objE)
		          })
		        break;
		      }
		    };  
	}])
})()
/*

*/