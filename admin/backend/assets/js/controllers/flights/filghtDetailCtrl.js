(function(){
	angular.module('controllers')
	.controller('flightDetailCtrl', ['emptyLeg','$q','$http','$scope', '$rootScope', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'baseURL','aircraft', '$timeout', function(emptyLeg,$q,$http,$scope, $rootScope, $state,user,customAlert,$fancyModal,modalAnimation,master,baseURL,aircraft, $timeout){
	  var id = $state.params.id;
    console.log("iddddd =>",id)
    $scope.isAdmin = ((JSON.parse(localStorage.userDetail).type == 'admin') || (JSON.parse(localStorage.userDetail).type == 'employee'));
    // user.filght_detail({'_id':id})
    //       .then(function(objS){
    //         console.log("objSsss===>",objS)
    //           $scope.new = objS;
    //       },function(objE){
    //         console.log("Error--->",objE)
    //       })
    user.flight_mainifist_list(id)
          .then(function(objS){
            console.log("objSsss===>",objS)
              $scope.new = objS.result;
          },function(objE){
            console.log("Error--->",objE)
          })

    $scope.action = function(data,detail){
          console.log("Action",data);
          $scope.actionTaken = data;
          if($scope.actionTaken == 'Add'){
            $scope.addData = {};
          }
          else{
            $scope.addData = angular.copy(detail);
          }
          $fancyModal.open({
              templateUrl: 'country-Modal.html',
              scope: $scope,
              openingClass: 'animated '+modalAnimation[9].in,
              closingClass: 'animated '+modalAnimation[9].out,
              openingOverlayClass: 'animated fadeIn',
              closingOverlayClass: 'animated fadeOut',
            });
        }

        $scope.addPassanger = function(data,detail){
          console.log("Action",data);
          $scope.actionTaken = data;
          if($scope.actionTaken == 'Add'){
            $scope.addPassData = {};
          }
          else{
            $scope.addPassData = angular.copy(detail);
          }
          $fancyModal.open({
              templateUrl: 'passenger-Modal.html',
              scope: $scope,
              openingClass: 'animated '+modalAnimation[9].in,
              closingClass: 'animated '+modalAnimation[9].out,
              openingOverlayClass: 'animated fadeIn',
              closingOverlayClass: 'animated fadeOut',
            });
        }
        $scope.exportManifest =function(){
          // $scope.invoice = obj;
         //    console.log("data =>"+JSON.stringify($scope.invoice))
            $timeout(function(){
              emptyLeg.pdf('pdf-dynamic','#pdfFormat','Manifest.pdf');
              $timeout(function(){
                $('#pdf-dynamic').css('display','none');
              },100)
            },10);
        }

        



    $scope.submitForm = function(){
          $scope.addData.flight_id = $state.params.id;
          if($scope.actionTaken == 'Add'){
            emptyLeg.addCrewMember($scope.addData).then(objS=>{
              console.log("success",objS);
              $fancyModal.close();
              $state.reload();
            },objE=>{
              console.log("Error",objE)
            })
          }else{
            emptyLeg.updateCrewMember($scope.addData).then(objS=>{
              console.log("success",objS);
              $fancyModal.close();
              $state.reload();
            },objE=>{
              console.log("Error",objE)
            })
          }
        }

        $scope.submitPassanger = function(){
          $scope.addPassData.flight_id = $state.params.id;
          console.log("submitPassanger", $scope.addPassData)
          if($scope.actionTaken == 'Add'){
            emptyLeg.addPassanger($scope.addPassData).then(objS=>{
              console.log("success",objS);
              if(objS.data.responseCode == 204)
                customAlert.show("Flight","Please Add payment for this flight to add passengers.")
              $fancyModal.close();
              $state.reload();
            },objE=>{
              console.log("Error",objE)
            })
          }else{
            console.log($scope.addPassData);
            // obj.passenger_id= obj._id;
            // var obj={
            //   name: $scope.addPassData.name,
            //   passenger_id: $scope.addPassData._id,
            //   age: $scope.addPassData.age,
            //   flight_id: $scope.addPassData.flight_id,
            //   gender: $scope.addPassData.gender,
            //   passportNumber: $scope.addPassData.passportNumber,
            //   nationlity: $scope.addPassData.nationlity
            // }
             // console.log("obj", obj);
            emptyLeg.updatePassanger($scope.addPassData).then(objS=>{
              console.log("success",objS);
              $fancyModal.close();
              $state.reload();
            },objE=>{
              console.log("Error",objE)
            })
          }
        }
	}])
})()
/*

*/