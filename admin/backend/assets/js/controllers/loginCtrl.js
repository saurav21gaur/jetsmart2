(function(){
	angular.module('controllers')
	.controller('loginCtrl', ['$scope', '$rootScope', '$http', '$state','user','customAlert', function($scope, $rootScope, $http, $state,user,customAlert){
		console.log(localStorage.userloggedin)
		
		if(localStorage.userloggedin){
			$state.go('main.dashboard');
		}
			
		$scope.admin= {
			'username': '',
			'password': '',
		}
		$scope.loginAttempt = function(){
			user.login($scope.admin)
			.then(function(data){
				console.log("data =>"+JSON.stringify(data))
				if(data.status == 200 ){
					var result = data.data
					console.log(result)
					localStorage.token = result.token;
					if(result.token){
						localStorage.userloggedin = true;
						console.log("login =>"+localStorage.userloggedin)
					}
						  

					if(angular.isDefined(result.result)){
							localStorage.adminType = result.result.type;
							if(angular.isDefined(result.result.access_level)){
								localStorage.access = JSON.stringify(result.result.access_level);
							}
							localStorage.username = result.result.user_name;
				            localStorage.userId=result.result._id;
				            localStorage.userDetail=JSON.stringify(result.result);
							/*if(angular.isDefined(result.result.access_level)){
								localStorage.access_level = JSON.stringify(result.result.access_level)
							}*/
						}
						$state.go('main.dashboard');
				}
				else{
					if(data.data.code == 400){
						customAlert.show(data.data.msg, '')
					}
				}

				// if(data.status == 200){
				// 	// console.log('status 200');
				// 	localStorage.token = result.token;
				// 	// localStorage.username = result.username;
				// 	if(angular.isDefined(result.result)){
				// 		console.log('type',result.result.type)
				// 		localStorage.userType = result.result.type
				//
				// 		if(angular.isDefined(result.result.access_level)){
				// 			localStorage.access_level = JSON.stringify(result.result.access_level)
				// 		}
				// 	}
				// 	$state.go('main.dashboard');
				// }
			},function(err){
				customAlert.show("Login",err.data.msg)
			});

		}
	}])
})()
