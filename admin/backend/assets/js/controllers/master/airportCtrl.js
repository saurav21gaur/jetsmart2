(function(){
	angular.module('controllers')
	.controller('airportCtrl', ['$window','$q','$scope', '$rootScope', '$http', '$state','user','customAlert','$fancyModal',"modalAnimation","master","$timeout",'aircraft', function($window,$q,$scope, $rootScope, $http, $state,user,customAlert,$fancyModal,modalAnimation,master,$timeout,aircraft){
		$scope.new={};
		$scope.aircraft={files:[]}
		$scope.Airports = []
		$scope.States = []
		$scope.Cities = []
		$scope.Countries = []

		$scope.currentPage = 1;
		$scope.limit = 10
		$scope.totalData = Infinity;
		$scope.aircraft={files:[]}
		$scope.showPagination = false
		$scope.show = false;

		var getAirportsList = function(){
			master.airports()
			.then(function(objS){
				var useableData = []
				// console.log("Data =>"+JSON.stringify(objS))
				for (var i = 0; i < objS.length; i++) {
					var allData = objS[i]
					/**/
					for (var j = 0; j < allData.airportList.length; j++) {
						var airport = allData.airportList[j]
						useableData.push({
							airport : airport.airport,
							stateName : allData.states.stateName,
							state : allData.states._id,
							countryName : allData.countryName,
							country : allData._id,
							cityName : allData.states.cities.cityName,
							city : allData.states.cities._id,
							iata : airport.iata,
							icao : airport.icao,
							category : airport.category,
							role : airport.role,
							runway : airport.runway,
							groundhandling : airport.groundhandling,
							_id : airport._id,
						})
					}
					/**/
				}

				$scope.Airports = useableData
				$scope.showPagination = true;
				console.log('Airports')
				console.dir($scope.Airports)
			},function(objE){
				console.log(objE)
			})
		}
		getAirportsList()

		$scope.show = false;
	    $scope.exportDataXLS = function () {
	        var blob = new Blob([document.getElementById('exportTab').innerHTML], {
	            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
	            });
	        saveAs(blob, "Airport List.xls");
	    };
	    $scope.exportDataPdf = function () {
	        $scope.show = true;
	         $timeout(function(){$scope.show = false; console.log('timeout')}, 50)
	        html2canvas(document.getElementById('data-table'), {
	            onrendered: function (canvas) {
	                var data = canvas.toDataURL();
	                var docDefinition = {
	                    content: [{
	                        image: data,
	                        width: 500,
	                    }]
	                };
	                pdfMake.createPdf(docDefinition).download("Airport List.pdf");
	            }
	        });
	    };


		var getStatesList = function(){
			master.states()
			.then(function(objS){
				$scope.States = objS
				console.log('states')
				console.dir($scope.States)
			},function(objE){
				console.log(objE)
			})
		}
		getStatesList()

		var getCountriesList = function(){
			master.countries()
			.then(function(objS){
				$scope.Countries = objS
				console.log('Countries')
				console.dir($scope.Countries)
			},function(objE){
				console.log(objE)
			})
		}
		getCountriesList()

		var getCitiesList = function(){
			master.cities()
			.then(function(objS){
				$scope.Cities = objS
				console.log('Cities')
				console.dir($scope.Cities)
			},function(objE){
				console.log(objE)
			})
		}
		getCitiesList()
		
		$scope.openModal=function(type,obj){

			switch(type){
				case "new":
				$scope.Title="Add";
				$scope.new={};
				$scope.showSubmit=true;
				$scope.isPopupOpen = true;
				break;

				case "update":
				$scope.Title="Update";
				$scope.new=obj;
				$window.scrollTo(0, 0);
				$scope.showSubmit=true;
				$scope.isPopupOpen = true;
				break;

				case "view":
				$scope.Title="";
				$scope.new=obj;
				$scope.showSubmit=false;
				$scope.isPopupOpen = false;
				$state.go('main.master.airportDetail',{id:obj._id})
				break;
				case "Delete":
			        master.airport_delete({_id:obj._id})
			          .then(function(objS){
			            console.log(objS);
			            $state.reload();
			          },function(objE){
			            console.log("Error--->",objE)
			          })
			        break;
			}
		
		 
		 //prev=IndxAnimation;
		};	

		$scope.submitForm=function(isValid){
			if(!isValid)
				return;
			$scope.new.image_gallary=$scope.aircraft.files
			master.saveUpdateAirport($scope.new)
			.then(function(objS){
				customAlert.show("Airport","Airport save");
				// console.log(objS)
				$scope.isPopupOpen = false;
				$fancyModal.close();
				getAirportsList()
			},function(objE){
				customAlert.show("Error",objE.msg)
			})
		}

        $scope.uploadFile = function(){
           // $scope.aircraft.files=$('#filesAirCraft').prop('files');
           $scope.isFileUploading=true;
           $scope.isPopupOpen = true;
           aircraft.uploadFiles($('#filesAirCraft').prop('files'))
           .then(function(objS){
            $scope.isFileUploading=false;
            $scope.aircraft.files=$scope.aircraft.files.concat(objS)
            setTimeout(function(){
              $('.cls-gallary').width($('.cls-gallary .gallary-img').length* ($('.cls-gallary .gallary-img').width()+20))
            },50)
           },function(objE){
            $scope.isFileUploading=false;
            customAlert("Files Upload","Error in file uploading.")
           })
           //fileUpload.uploadFileToUrl(file, uploadUrl);
           
        };

        $scope.removeFile=function(file, index){
          var ind = $scope.aircraft.files.indexOf(file);
          console.log(ind)
        	aircraft.deleteAircraftImg(file)
        	.then(function(objS){
            $scope.aircraft.files.splice(ind,1);
           
        	},function(objE){
        		console.log("Error--->",objE)
        	})
        };


	}])
})()
/*

*/