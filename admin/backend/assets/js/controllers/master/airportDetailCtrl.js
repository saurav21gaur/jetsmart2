(function(){
	angular.module('controllers')
	.controller('airportDetailCtrl', ['$q','$scope', '$rootScope', '$http', '$state','user','customAlert','$fancyModal',"modalAnimation","master","$timeout", function($q,$scope, $rootScope, $http, $state,user,customAlert,$fancyModal,modalAnimation,master,$timeout){

		var getStatesList = function(){
			master.states()
			.then(function(objS){
				$scope.States = objS
				console.log('states',$scope.States)
				console.dir($scope.States)
			},function(objE){
				console.log(objE)
			})
		}
		getStatesList()

		var getCountriesList = function(){
			master.countries()
			.then(function(objS){
				$scope.Countries = objS;
				console.log('Countries')
				console.dir($scope.Countries)
			},function(objE){
				console.log(objE)
			})
		}
		getCountriesList()

		var getCitiesList = function(){
			master.cities()
			.then(function(objS){
				$scope.Cities = objS
				console.log('Cities')
				console.dir($scope.Cities)
			},function(objE){
				console.log(objE)
			})
		}
		getCitiesList()

		var id = $state.params.id;
		console.log("id =>",id);
		master.airport_detail(id).then(function(objS){
				$scope.new = objS.result;
				$timeout(function(){
					for(var i=0;i<$scope.Countries.length;i++){
						if($scope.Countries[i]._id == $scope.new.country){
							$scope.countryName = $scope.Countries[i].countryName;
						}
					}
					for(var i=0;i<$scope.States.length;i++){
						if($scope.States[i].states._id == $scope.new.state){
							$scope.stateName = $scope.States[i].states.stateName;
						}
					}
					for(var i=0;i<$scope.Cities.length;i++){
						if($scope.Cities[i].states.cities._id == $scope.new.city){
							$scope.cityName = $scope.Cities[i].states.cities.cityName;
						}
					}
					console.log("s",$scope.stateName)
				}, 100);
				
			},function(objE){
			console.log(objE)
		})
	}])
})()
/*

*/