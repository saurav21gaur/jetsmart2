(function(){
	angular.module('controllers')
	.controller('countryCtrl', ['$scope', '$rootScope', '$http', '$state','user','customAlert','$fancyModal',"modalAnimation","master", function($scope, $rootScope, $http, $state,user,customAlert,$fancyModal,modalAnimation,master){
		$scope.new={};
		$scope.isPopupOpen = false;
		$scope.Countries=[];
		var getCountries=function(){
			master.countries(1).then(function(countries){
				$scope.Countries=countries;
			},function(){})
		}
		getCountries()
		$scope.openModal=function(type,obj){
			
			// var IndxAnimation=getIndex(prev);
			// console.log(IndxAnimation);
			switch(type){
				case "new":
				$scope.Title="Add";
				$scope.new={};
				$scope.showSubmit=true;
        		$scope.isPopupOpen = true;
				break;

				case "update":
				$scope.Title="Update";
				$scope.new=obj;
				$scope.showSubmit=true;
         		$scope.isPopupOpen = true;
				break;

				case "view":
				$scope.Title="";
				$scope.new=obj;
				$scope.showSubmit=false;
				$scope.isPopupOpen = false;
				$fancyModal.open({
			        templateUrl: 'country-Modal.html',
			        scope: $scope,
			        openingClass: 'animated '+modalAnimation[9].in,
			        closingClass: 'animated '+modalAnimation[9].out,
			        openingOverlayClass: 'animated fadeIn',
			        closingOverlayClass: 'animated fadeOut',
			      });
				break;

				case "Delete":
		        master.country_Delete({_id:obj._id})
		          .then(function(objS){
		            console.log(objS);
		            $state.reload();
		          },function(objE){
		            console.log("Error--->",objE)
		          })
		        break;
			}
		 
		 //prev=IndxAnimation;
		};	

		$scope.submitForm=function(isValid){
			if(!isValid)
				return;
			master.saveUpdateCountry($scope.new)
			.then(function(objS){
				customAlert.show("Country","Country "+$scope.Title+" successfully.");
				$scope.isPopupOpen = false;
				$fancyModal.close();
				getCountries();
			},function(objE){
				customAlert.show("Country",objE.msg)
			})
		}
	}])
})()
/*

*/