(function(){
	angular.module('controllers')
	.controller('distanceCtrl', ['$q','$scope', '$rootScope', '$http', '$state','user','customAlert','$fancyModal',"modalAnimation","master","$timeout",'$window', function($q,$scope, $rootScope, $http, $state,user,customAlert,$fancyModal,modalAnimation,master,$timeout,$window){
		$scope.new={};

		$scope.airportsDistance = []
		$scope.Airports = []
		$scope.currentPage = $state.params.page_no
		$scope.limit = 10
		$scope.totalData = Infinity
		$scope.showPagination = false
		$scope.isPopupOpen = false;


		var getAirportsList = function(){
			master.airports()
			.then(function(objS){
				var useableData = []
				for (var i = 0; i < objS.length; i++) {
					var allData = objS[i]
					/**/
					for (var j = 0; j < allData.airportList.length; j++) {
						var airport = allData.airportList[j]
						useableData.push({
							airport : airport.airport,
							stateName : allData.states.stateName,
							stateId : allData.states._id,
							countryName : allData.countryName,
							countryId : allData._id,
							cityName : allData.states.cities.cityName,
							cityId : allData.states.cities._id,
							iata : airport.iata,
							icao : airport.icao,
							_id : airport._id,
						})
					}
					/**/
				}
				$scope.Airports = useableData
				console.log('Airports')
				console.dir($scope.Airports)
				getAirportsDistance($scope.currentPage)
			},function(objE){
				console.log(objE)
			})
		}
		getAirportsList()

		$scope.changeAirVal =function(data,con){
			var res = data.split("[",1);
			for(var i=0;i<$scope.Airports.length;i++){
				if($scope.Airports[i].airport == res[0]){
					console.log("true",$scope.Airports[i]._id);
					(con == 'from')?$scope.fromAirportId = $scope.Airports[i]._id:$scope.toAirportId = $scope.Airports[i]._id;
				}
			}
			console.log("from",$scope.fromAirportId,"to",$scope.toAirportId)
		}

		$scope.show = false;
	    $scope.exportDataXLS = function () {
	    	 $timeout(function(){ var blob = new Blob([document.getElementById('exportTab').innerHTML], {
	            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
	            });
	        saveAs(blob, "Airport Distance.xls");}, 1000)
	       
	    };
	    $scope.exportDataPdf = function () {
	        $scope.show = true;
	        $timeout(function(){ console.log('timeout');
	        	html2canvas(document.getElementById('data-table'), {
	            onrendered: function (canvas) {
	                var data = canvas.toDataURL();
	                var docDefinition = {
	                    content: [{
	                        image: data,
	                        width: 500,
	                    }]
	                };
	                pdfMake.createPdf(docDefinition).download("Airport Distance.pdf");
	            }
	        });
	        	$scope.show = false;
	    	}, 100)
	       
	        
	    };


		var getAirportsDistance = function(page_no){
			master.distance(page_no)
			.then(function(objS){
				// $scope.totalData = objS.pagination.total
				$scope.airportsDistance = objS.result
				for(var i=0; i < $scope.airportsDistance.length; i++){
					
					$scope.airportsDistance[i].airportNameArr = []
					$scope.airportsDistance[i].iataArr = []
					$scope.airportsDistance[i].icaoArr = []
					$scope.airportsDistance[i].cityArr = []
					$scope.airportsDistance[i].stateArr = []

					var tempObj1 = $scope.airportsDistance[i]
					for(var j=0; j<$scope.Airports.length; j++){
						var tempObj2 = $scope.Airports[j]
						if(tempObj1.fromAirport == tempObj2._id){
							$scope.airportsDistance[i].fromAirportName = tempObj2.airport
							$scope.airportsDistance[i].fromAirportIcao = tempObj2.icao
							$scope.airportsDistance[i].fromAirportIata = tempObj2.iata
							$scope.airportsDistance[i].fromAirportCity = tempObj2.cityName
							$scope.airportsDistance[i].fromAirportState = tempObj2.stateName
							/*searching arr*/
							$scope.airportsDistance[i].airportNameArr[0] = $scope.airportsDistance[i].fromAirportName
							$scope.airportsDistance[i].icaoArr[0] = $scope.airportsDistance[i].fromAirportIcao
							$scope.airportsDistance[i].iataArr[0] = $scope.airportsDistance[i].fromAirportIata
							$scope.airportsDistance[i].cityArr[0] = $scope.airportsDistance[i].fromAirportCity
							$scope.airportsDistance[i].stateArr[0] = $scope.airportsDistance[i].fromAirportState
							/**/
						}
						if(tempObj1.toAirport == tempObj2._id){
							$scope.airportsDistance[i].toAirportName = tempObj2.airport
							$scope.airportsDistance[i].toAirportIcao = tempObj2.icao
							$scope.airportsDistance[i].toAirportIata = tempObj2.iata
							$scope.airportsDistance[i].toAirportCity = tempObj2.cityName
							$scope.airportsDistance[i].toAirportState = tempObj2.stateName
							/*searching arr*/
							$scope.airportsDistance[i].airportNameArr[1] = $scope.airportsDistance[i].toAirportName
							$scope.airportsDistance[i].icaoArr[1] = $scope.airportsDistance[i].toAirportIcao
							$scope.airportsDistance[i].iataArr[1] = $scope.airportsDistance[i].toAirportIata
							$scope.airportsDistance[i].cityArr[1] = $scope.airportsDistance[i].toAirportCity
							$scope.airportsDistance[i].stateArr[1] = $scope.airportsDistance[i].toAirportState
							/**/


						}
						if(angular.isDefined($scope.airportsDistance[i].fromAirportName) && angular.isDefined($scope.airportsDistance[i].toAirportName)){
							break;
						}
					}
				}
				$scope.showPagination = true;
				console.log($scope.airportsDistance)
			},function(objE){
				console.log(objE)
			})
		}
		// getAirportsDistance($scope.currentPage)
		
		$scope.openModal=function(type,obj){
			switch(type){
				case "new":
				$scope.Title="Add";
				$scope.new={};
				$scope.new.keyType = "";
				$scope.showSubmit=true;
				$scope.isPopupOpen = true;
				break;

				case "update":
				$scope.Title="Update";
				console.log("obj",obj)
				for(var i=0;i<$scope.Airports.length;i++){
					if($scope.Airports[i]._id == obj.fromAirport){
						console.log("obj.fromAirport",obj.fromAirport)
						$scope.fromAirportId = obj.fromAirport;
						$scope.new.fromAirport = $scope.Airports[i].airport+'['+$scope.Airports[i].iata+'/'+$scope.Airports[i].icao+']';
					}
					if($scope.Airports[i]._id == obj.toAirport){
						$scope.toAirportId = obj.toAirport;
						$scope.new.toAirport = $scope.Airports[i].airport+'['+$scope.Airports[i].iata+'/'+$scope.Airports[i].icao+']';
					}
				}
				$scope.new.distance = obj.distance;
				$scope.new.keyType = obj._id;
				console.log("$scope.new",$scope.new)
				$window.scrollTo(0, 0);
				$scope.showSubmit=true;
				$scope.isPopupOpen = true;
				break;

				case "view":
				$scope.Title="";
				$scope.new=obj;
				$scope.isPopupOpen = false;
				 $fancyModal.open({
			        templateUrl: 'distance-Modal.html',
			        scope: $scope,
			        openingClass: 'animated '+modalAnimation[9].in,
			        closingClass: 'animated '+modalAnimation[9].out,
			        openingOverlayClass: 'animated fadeIn',
			        closingOverlayClass: 'animated fadeOut',
			      });
				break;
				case "Delete":
		        master.distance_delete({_id:obj._id})
		          .then(function(objS){
		            console.log(objS);
		            $state.reload();
		          },function(objE){
		            console.log("Error--->",objE)
		          })
		        break;
			}
		
		 //prev=IndxAnimation;
		};	

		$scope.submitForm=function(isValid){
			console.log("type=====>",$scope.new.keyType)
			if(!isValid)
				return;
			if($scope.new.keyType == '')
				var req = {
					distance:$scope.new.distance,
					fromAirport:$scope.fromAirportId,
					toAirport:$scope.toAirportId
				}
			else
				var req = {
					_id:$scope.new.keyType,
					distance:$scope.new.distance,
					fromAirport:$scope.fromAirportId,
					toAirport:$scope.toAirportId
				}
			console.log("requesrt",req)
			master.saveUpdateDistance(req)
			.then(function(objS){
				customAlert.show("Success","Distance Save");
				$scope.isPopupOpen = false;
				$fancyModal.close();
				getAirportsDistance()
			},function(objE){
				customAlert.show("Error",objE.msg)
			})
		}

		$scope.pageChange=function(page){
			$scope.currentPage = page
			// $state.go('.',{page_no:page})
		}

		$scope.uploadFile = function(){
           // $scope.aircraft.files=$('#filesAirCraft').prop('files');
           $scope.isFileUploading=true;
           aircraft.uploadFiles($('#filesAirCraft').prop('files'))
           .then(function(objS){
           	$scope.isFileUploading=false;
           	console.log("uploadFilepathAirportDistance",objS.file)
   //         	$scope.filesPath=objS.file;
   //         	master.bulkUploadOprator($scope.filesPath).then(function(objS){
			// 	console.log(objS)
			// },function(objE){
			// 	console.log(objE)
			// })
           },function(objE){
           	$scope.isFileUploading=false;
           	customAlert("Files Upload","Error in file uploading.")
           })
           //fileUpload.uploadFileToUrl(file, uploadUrl);
           
        };

	}])
})()
/*

*/