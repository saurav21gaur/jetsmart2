(function(){
	angular.module('controllers')
	.controller('regionCtrl', ['$q','$scope', '$rootScope', '$http', '$state','user','customAlert','$fancyModal',"modalAnimation","master", function($q,$scope, $rootScope, $http, $state,user,customAlert,$fancyModal,modalAnimation,master){
		$scope.new={};
		$scope.Regions=[];
		
		$q.all([master.regions(),master.countries()])
		.then(function(arr){
			$scope.Regions=arr[0];
			$scope.Countries=arr[1];
		},function(objE){
			console.log(objE)
		})
		
		$scope.openModal=function(type,obj){
			
			// var IndxAnimation=getIndex(prev);
			// console.log(IndxAnimation);
			switch(type){
				case "new":
				$scope.Title="Add";
				$scope.new={};
				$scope.showSubmit=true;
				$scope.new.country=$scope.Countries[0];
				break;

				case "update":
				$scope.Title="Update";
				$scope.new=obj;
				$scope.showSubmit=true;
				break;

				case "view":
				$scope.Title="";
				$scope.new=obj;
				$scope.showSubmit=false;
				break;
			}
		 $fancyModal.open({
	        templateUrl: 'region-Modal.html',
	        scope: $scope,
	        openingClass: 'animated '+modalAnimation[9].in,
	        closingClass: 'animated '+modalAnimation[9].out,
	        openingOverlayClass: 'animated fadeIn',
	        closingOverlayClass: 'animated fadeOut',
	      });
		 //prev=IndxAnimation;
		};	

		$scope.submitForm=function(isValid){
			if(!isValid)
				return;
			master.saveUpdateRegion($scope.new)
			.then(function(objS){
				customAlert.show("Region","Region "+$scope.Title+" successfully.");
				$fancyModal.close();
				master.regions().then(function(arr){$scope.Regions=arr});
			},function(objE){
				customAlert.show("Region",objE.msg)
			})
		}
	}])
})()
/*

*/