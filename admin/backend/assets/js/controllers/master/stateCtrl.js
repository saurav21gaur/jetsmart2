(function(){
	angular.module('controllers')
	.controller('stateCtrl', ['$q','$scope', '$rootScope', '$http', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'$window','$timeout', function($q,$scope, $rootScope, $http, $state,user,customAlert,$fancyModal,modalAnimation,master,$window,$timeout){
		$scope.new={};
		$scope.currentPage = 1;
      	$scope.limit = 10;
     
      $scope.showPagination = false;
		$scope.isPopupOpen = false;
		$q.all([master.states(),master.countries(),master.regions()])
		.then(function(arr){
			$scope.States=arr[0];
			$scope.Countries=arr[1];
			// console.log("cccc",$scope.Countries)
			
			$scope.showPagination = true;
		},function(objE){
			console.log(objE)
		})

		$scope.getRegion = function(countryy){
			console.log("country idddd",countryy._id)
			master.regionCountry(countryy._id).then(objS=>{
				console.log("Success",objS)
				$scope.Regions=objS.data;
			},objE=>{
				console.log("Error",objE)
			})
		}
		
		$scope.pageChange=function(page){
      		$scope.currentPage = page;
   		 }
		
		$scope.openModal=function(type,obj){
			
			// var IndxAnimation=getIndex(prev);
			 console.log("IndxAnimation",obj);
			switch(type){
				case "new":
				$scope.Title="Add";
				$scope.new={};
				$scope.showSubmit=true;
				$scope.isPopupOpen = true;
				// $scope.new.country=$scope.Countries[0];
				break;

				case "update":
				console.log("update Data===>",obj)
				$scope.Title="Update";
				$scope.new=obj;
				$scope.isPopupOpen = true;
				$window.scrollTo(0, 0);
				$scope.showSubmit=true;
				$scope.new.country=$scope.Countries.find(x=>x._id==obj._id);
				$scope.getRegion($scope.new.country);
				$timeout(function() {
					$scope.new.region=$scope.Regions.find(x=>x._id==obj.states.region._id);
					console.log("$scope.new.region",$scope.new.region)
				}, 100);
				 
				break;

				case "view":
				$scope.Title="";
				$scope.new=obj;
				$scope.isPopupOpen = false;
				$scope.showSubmit=false;
				$scope.new.country=$scope.Countries.find(x=>x._id==obj._id);
				// $scope.new.region=$scope.Regions.find(x=>x._id==obj.states.region._id);
				$fancyModal.open({
			        templateUrl: 'state-Modal.html',
			        scope: $scope,
			        openingClass: 'animated '+modalAnimation[9].in,
			        closingClass: 'animated '+modalAnimation[9].out,
			        openingOverlayClass: 'animated fadeIn',
			        closingOverlayClass: 'animated fadeOut',
			      });
				break;

				case "Delete":
		        master.state_delete({_id:obj._id})
		          .then(function(objS){
		            console.log(objS);
		            $state.reload();
		          },function(objE){
		            console.log("Error--->",objE)
		          })
		        break;
			}
		 
		 //prev=IndxAnimation;
		};	

		$scope.submitForm=function(isValid){
			console.log("$scope.new",$scope.new)
			if(!isValid)
				return;
			master.saveUpdateState($scope.new)
			.then(function(objS){
				customAlert.show("States","State "+$scope.Title+" successfully.");
				$scope.isPopupOpen = false;
				$fancyModal.close();
				 $state.reload();
				 master.states().then(function(arr){$scope.States=arr});
			},function(objE){
				customAlert.show("Region",objE.msg)
			})
		}
	}])
})()
/*

*/