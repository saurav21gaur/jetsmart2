
(function(){
	angular.module('controllers')
	.controller('addOpretorCtrl', ['$timeout','$q','$scope', '$rootScope', '$http', '$state','opretor','customAlert','$fancyModal',"modalAnimation","master",'baseURL', function($timeout,$q,$scope, $rootScope, $http, $state,opretor,customAlert,$fancyModal,modalAnimation,master,baseURL){
		console.log('addUserCtrl')
        $scope.filesPath={filesPath:[]}
		$scope.newUser = {}

		$scope.newUser.code = '';

		$scope.codeGenerator = function(){
			opretor.generatorNumber().
			then(objS=>{
				$scope.newUser.code = '91O'+objS.count
			},objE=>console.log(objE))
		}
		$scope.months = [
          "Select",
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec"
        ];

		$scope.newUser.owner_manegement_team = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];
    	$scope.newUser.keyPerson = [{name:'',designation:'',date_of_birth:'',anniversery:'',mobile:'',}];
      	$scope.newUser.Revenu = [{month:'',designation:'',total_amount:''}];

		$scope.add = {
    		owner_manegement_team : function(){
    			var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}
    			$scope.newUser.owner_manegement_team.push(obj);
    		},
    		keyPerson : function(){
    			var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',mobile:'',}
    			$scope.newUser.keyPerson.push(obj);
    		},
	        Revenu : function(){
	          var obj = {month:'',designation:'',total_amount:''}
	          $scope.newUser.Revenu.push(obj);
	        }
    	}

    	$scope.remove = {
    		owner_manegement_team : function(){
    			var lastItem = $scope.newUser.owner_manegement_team.length-1;
    			$scope.newUser.owner_manegement_team.splice(lastItem)
    		},
    		keyPerson : function(){
    			var lastItem = $scope.newUser.keyPerson.length-1;
    			$scope.newUser.keyPerson.splice(lastItem)
    		},
	        Revenu : function(){
	          var lastItem = $scope.newUser.Revenu.length-1;
	          $scope.newUser.Revenu.splice(lastItem)
	        },
    	}


		$scope.submitForm = function(valid){
			
			$scope.newUser.type = "oprator";
			opretor.add($scope.newUser)
			.then(function(objS){
				console.log(objS)
				if(objS.status == 200){
					$scope.newUser = {}
					customAlert.show('Success',objS.data.response_message)
					$state.go("main.opretor.list");
				}
				if(objS.status == 404)
					customAlert.show('Error',objS.data.msg)
			},function(objE){
				console.log(objE)
			})
		}

		var getCountriesList = function(){
			master.countries()
			.then(function(objS){
				$scope.Countries = objS
				console.log('Countries', $scope.Countries)
			},function(objE){
				console.log(objE)
			})
		}
		getCountriesList()

		function findObjectByKey(array, key, value) {
		    for (var i = 0; i < array.length; i++) {
		        if (array[i][key] === value) {
		            return array[i];
		        }
		    }
		    return null;
		}

		$scope.createCityArray = function(state){
			getCountriesList();
			$timeout(function(){
				$scope.State = findObjectByKey($scope.Countries[0].states, '_id', state); 
			}, 200);
			
		}

		$scope.uploadFile = function(type){
            $scope.nn =false;
           $scope.isFileUploading=true;
           if(type == 'pan'){
           		opretor.imgUpload($('#filesAirCraft').prop('files'))
	           .then(function(objS){
	           	$scope.isFileUploading=false;
	           	console.log("uploadFilepath",objS.file)
	           	$scope.newUser.panCardImage = objS.file;
	           },function(objE){
	           	$scope.isFileUploading=false;
	           	customAlert("Files Upload","Error in file uploading.")
	           })
           }
           else{
           		opretor.imgUpload($('#logoAirCraft').prop('files'))
	           .then(function(objS){
	           	$scope.isFileUploading=false;
	           	console.log("uploadFilepath",objS.file)
	           	$scope.newUser.logo = objS.file;
	           },function(objE){
	           	$scope.isFileUploading=false;
	           	customAlert("Files Upload","Error in file uploading.")
	           })
           }
           
        };

	}])
})()
/*

*/