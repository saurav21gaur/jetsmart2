
(function(){
	angular.module('controllers')
	.controller('operatorDetailCtrl', ['$q','$scope', '$rootScope', '$http', '$state','opretor','customAlert','$fancyModal',"modalAnimation","master",'baseURL','$timeout', function($q,$scope, $rootScope, $http, $state,opretor,customAlert,$fancyModal,modalAnimation,master,baseURL,$timeout){
	    $scope.showPagination = false
	    var id = $state.params.id;
	  	// $scope.newUser = JSON.parse(localStorage.detailsOperator);
	  	$scope.showSubmit = localStorage.showSubmit;
	  	$scope.Title = localStorage.Title;
	  	function dateFormat(date){
	  		console.log("date",date)
	  		var yy = date.getFullYear();
			var mm = (date.getMonth())+1;
			var dd = date.getDate();
			if(mm < 10) mm= '0'+ mm;
			if(dd < 10) dd = '0'+dd;
			$scope.minDate = dd+'/'+mm+'/'+yy;
			return $scope.minDate;
	  	}
	  	console.log('Res =>'+JSON.stringify(id))
	  	$scope.totalAmt = 0;
	  	opretor.view_oprator({"_id": id})
           .then(function(objS){
           	$scope.newUser = objS.result;
           	$scope.newUser.owner_manegement_team = $scope.newUser.owner_manegement_team.map(x=>{
           		if(x.date_of_birth)
           			x.date_of_birth = dateFormat(new Date(x.date_of_birth));
           		if(x.anniversary)
           			x.anniversary = dateFormat(new Date(x.anniversary));
           		return x;
           	})
           	$scope.newUser.keyPerson = $scope.newUser.keyPerson.map(x=>{
           		if(x.date_of_birth)
           			x.date_of_birth = dateFormat(new Date(x.date_of_birth));
           		if(x.anniversary)
           			x.anniversary = dateFormat(new Date(x.anniversary));
           		return x;
           	})
           	console.log("$scope.newUser.keyPerson",$scope.newUser.keyPerson)
           	// $scope.State = findObjectByKey($scope.Countries[0].states, '_id', $scope.newUser.state)
		   opretor.jetList($scope.newUser._id).then(function(objS){$scope.userAircrafts=objS},function(objE){console.log(objE)})
           	$scope.createCityArray(objS.result.state);
           	console.log("Response =>"+JSON.stringify($scope.newUser))
           	for(var i=0;i<$scope.newUser.Revenu.length;i++){
	        console.log("fff",$scope.newUser.Revenu[i].total_amount)
	        $scope.totalAmt = $scope.totalAmt + $scope.newUser.Revenu[i].total_amount;
	        if($scope.newUser.Revenu[i].month == 'Jan')
	          $scope.janAmt = $scope.newUser.Revenu[i].total_amount;
	        if($scope.newUser.Revenu[i].month == 'Feb')
	          $scope.febAmt = $scope.newUser.Revenu[i].total_amount;
	        if($scope.newUser.Revenu[i].month == 'Mar')
	          $scope.marAmt = $scope.newUser.Revenu[i].total_amount;
	        if($scope.newUser.Revenu[i].month == 'Apr')
	          $scope.aprAmt = $scope.newUser.Revenu[i].total_amount;
	        if($scope.newUser.Revenu[i].month == 'May')
	          $scope.mayAmt = $scope.newUser.Revenu[i].total_amount;
	        if($scope.newUser.Revenu[i].month == 'Jun')
	          $scope.junAmt = $scope.newUser.Revenu[i].total_amount;
	        if($scope.newUser.Revenu[i].month == 'Jul')
	          $scope.julAmt = $scope.newUser.Revenu[i].total_amount;
	        if($scope.newUser.Revenu[i].month == 'Aug')
	          $scope.augAmt = $scope.newUser.Revenu[i].total_amount;
	        if($scope.newUser.Revenu[i].month == 'Sep')
	          $scope.sepAmt = $scope.newUser.Revenu[i].total_amount;
	        if($scope.newUser.Revenu[i].month == 'Oct')
	          $scope.octAmt = $scope.newUser.Revenu[i].total_amount;
	        if($scope.newUser.Revenu[i].month == 'Nov')
	          $scope.novAmt = $scope.newUser.Revenu[i].total_amount;
	        if($scope.newUser.Revenu[i].month == 'Dec')
	          $scope.decAmt = $scope.newUser.Revenu[i].total_amount;
	      }
           },function(objE){
           	$scope.isFileUploading=false;
           	customAlert("Files Upload","Error in file uploading.")
           })

        opretor.orerator_view_air({_id:$state.params.id}).then(objS=>{
        	console.log("success",objS.result);
        	$scope.flightData = objS.result;
        },objE=>{
        	console.log("error",objE)
        })

        $scope.removeFlight = function(id){
        	console.log("data",id)
        	opretor.removeOperatorFlight(id).then(objS=>{
        		console.log("Success",objS);
        		$state.reload();
        	},objE=>{
        		console.log("error",objE)
        	})
        }
        $scope.editFlight = function(data){
        	console.log("data",data)
        	opretor.editOperatorFlight(data,data._id).then(objS=>{
        		console.log("Success",objS);
        		$state.reload();
        	},objE=>{
        		console.log("error",objE)
        	})
        }
        $scope.months = [
          "Select",
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec"
        ];

		

		$scope.add = {
    		owner_manegement_team : function(){
    			var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}
    			$scope.newUser.owner_manegement_team.push(obj);
    		},
    		keyPerson : function(){
    			var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',mobile:'',}
    			$scope.newUser.keyPerson.push(obj);
    		},
	        Revenu : function(){
	          var obj = {month:'',designation:'',total_amount:''}
	          $scope.newUser.Revenu.push(obj);
	        }
    	}

    	$scope.remove = {
    		owner_manegement_team : function(){
    			var lastItem = $scope.newUser.owner_manegement_team.length-1;
    			$scope.newUser.owner_manegement_team.splice(lastItem)
    		},
    		keyPerson : function(){
    			var lastItem = $scope.newUser.keyPerson.length-1;
    			$scope.newUser.keyPerson.splice(lastItem)
    		},
	        Revenu : function(){
	          var lastItem = $scope.newUser.Revenu.length-1;
	          $scope.newUser.Revenu.splice(lastItem)
	        },
    	}

        // $scope.State = findObjectByKey($scope.Countries[0].states, '_id', $scope.newUser.state)
        opretor.jetList(localStorage.detailsOperatorId).then(function(objS){$scope.userAircrafts=objS},function(objE){console.log(objE)})

	    $scope.uploadFile = function(type){
            $scope.nn =false;
           $scope.isFileUploading=true;
           if(type == 'pan'){
           		opretor.imgUpload($('#filesAirCraft').prop('files'))
	           .then(function(objS){
	           	$scope.isFileUploading=false;
	           	console.log("uploadFilepath",objS.file)
	           	$scope.newUser.panCardImage = objS.file;
	           },function(objE){
	           	$scope.isFileUploading=false;
	           	customAlert("Files Upload","Error in file uploading.")
	           })
           }
           else{
           		opretor.imgUpload($('#logoAirCraft').prop('files'))
	           .then(function(objS){
	           	$scope.isFileUploading=false;
	           	console.log("uploadFilepath",objS.file)
	           	$scope.newUser.logo = objS.file;
	           },function(objE){
	           	$scope.isFileUploading=false;
	           	customAlert("Files Upload","Error in file uploading.")
	           })
           }
           
        };


		var getCountriesList = function(){
			master.countries()
			.then(function(objS){
				$scope.Countries = objS
				console.log('Countries', $scope.Countries)
			},function(objE){
				console.log(objE)
			})
		}
		getCountriesList()

		function findObjectByKey(array, key, value) {
		    for (var i = 0; i < array.length; i++) {
		        if (array[i][key] === value) {
		            return array[i];
		        }
		    }
		    return null;
		}

		$scope.createCityArray = function(state){
			getCountriesList();
			$timeout(function(){
				$scope.State = findObjectByKey($scope.Countries[0].states, '_id', state); 
			}, 200);
			
		}
		// $scope.State = findObjectByKey($scope.Countries[0].states, '_id', $scope.newUser.state)
		
    	$scope.submitForm = function(valid){
			// if(!valid)
			// 	return
			console.log("Edit Data =>"+JSON.stringify($scope.newUser))
			opretor.update_operator($scope.newUser)
			.then(function(objS){
				console.log(objS)
				customAlert.show('success',objS.response_message)
				// $fancyModal.close();
				$state.go('main.opretor.list');
			},function(objE){
				console.log(objE)
			})
		}

	}])
})()