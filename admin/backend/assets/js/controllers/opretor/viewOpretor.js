
(function(){
	angular.module('controllers')
	.controller('viewOpretorCtrl', ['agent','$q','$scope', '$rootScope', '$http', '$state','opretor','customAlert','$fancyModal',"modalAnimation","master",'baseURL','$timeout','aircraft', function(agent,$q,$scope, $rootScope, $http, $state,opretor,customAlert,$fancyModal,modalAnimation,master,baseURL,$timeout,aircraft){

		$scope.currentPage = 1;
	    $scope.limit = 10
	    $scope.totalData = Infinity
	    $scope.showPagination = false;
	    $scope.airD = [];

	    $scope.aircraftPop = function(){
	    	console.log("selet Dtaa",$scope.airD)
	    	 $scope.aircraftsDownload.map(x =>{
	    	 	console.log("ddd",x.jet_name)
	    	 	
	    	 	$scope.airD.map(y=>{
	    	 		if(x.jet_name == y.jet_name){
	    	 			x.aircraftDataaa = true;
	    	 		}
	    	 	})
	    	 	return x;
	    	 });
	    	console.log("innnn",$scope.aircraftsDownload)
	    	$fancyModal.open({
			        templateUrl: 'country-Modal.html',
			        scope: $scope,
			        openingClass: 'animated '+modalAnimation[9].in,
			        closingClass: 'animated '+modalAnimation[9].out,
			        openingOverlayClass: 'animated fadeIn',
			        closingOverlayClass: 'animated fadeOut',
			      });
	    }
	    $scope.selectedAir = function(dd,data,ind){
	    	console.log("dd",dd,"data",data);
	    	data.opratorId = $scope.opratorId;
	    	data.newKey = true;
	    	if(dd == true){
	    		$scope.airD.push(data)
	    	}
	    	else{
	    		var index = $scope.airD.findIndex(x => x._id == data._id);
	    		 $scope.airD.splice(index,1)
	    	}
	    	console.log("$scope.airD",$scope.airD)
	    }
	    

		var getUserList = function(page){
			opretor.list(page).then(function(objS){
				// $scope.totalData = objS.pagination.total;
		        // $scope.limit = objS.pagination.limit;
				console.log("operatorList",objS.result)
				$scope.userList=objS.result.map(x=>{x.pin_code =parseInt(x.pin_code); return x});
				 $scope.showPagination = true;
			},function(objE){
				console.log(objE)
			})
		}
		getUserList($scope.currentPage)

		$scope.pageChange=function(page){
	      $scope.currentPage = page;
	      // getUserList($scope.currentPage)
	    }

	    $scope.show = false;
	    $scope.exportDataXLS = function () {
	        var blob = new Blob([document.getElementById('exportTab').innerHTML], {
	            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
	            });
	        saveAs(blob, "Operator List.xls");
	    };
	    $scope.exportDataPdf = function () {
	        $scope.show = true;
	        $timeout(function(){$scope.show = false; console.log('timeout')}, 50)
	        html2canvas(document.getElementById('data-table'), {
	            onrendered: function (canvas) {
	                var data = canvas.toDataURL();
	                var docDefinition = {
	                    content: [{
	                        image: data,
	                        width: 500,
	                    }]
	                };
	                pdfMake.createPdf(docDefinition).download("Operator List.pdf");
	            }
	        });
	    };

	    $scope.uploadFile = function(){
            $scope.nn =false;
           $scope.isFileUploading=true;
           opretor.imgUpload($('#filesAirCraft').prop('files'))
           .then(function(objS){
           	$scope.isFileUploading=false;
           	console.log("uploadFilepath",objS.file)
           	$scope.newUser.panCardImage = objS.file;
           },function(objE){
           	$scope.isFileUploading=false;
           	customAlert("Files Upload","Error in file uploading.")
           })
        };


		var getCountriesList = function(){
			master.countries()
			.then(function(objS){
				$scope.Countries = objS
				console.log('Countries', $scope.Countries)
			},function(objE){
				console.log(objE)
			})
		}
		getCountriesList()

		function findObjectByKey(array, key, value) {
		    for (var i = 0; i < array.length; i++) {
		        if (array[i][key] === value) {
		            return array[i];
		        }
		    }
		    return null;
		}

		$scope.createCityArray = function(id){
			$scope.State = findObjectByKey($scope.Countries[0].states, '_id', id)
		}

		$scope.deleteUser = function(id){
			$scope.currentPage = 1;
			opretor.delete(id)
			.then(function(objS){
				customAlert.show('success',objS.response_message)
				getUserList($scope.currentPage)
			},function(objE){
				console.log(objE)
			})
		}

		$scope.openModal=function(type,obj){
			
		  switch(type){
		    case "new":
		    $scope.type = type;
		    $scope.Title="Add";
		    $scope.newUser={};
		    $scope.showSubmit=true;
		    $scope.isPopupOpen = false;
		    break;

		    case "update":
		    // $scope.type = type;
		    localStorage.Title="Update";
		    $state.go('main.opretor.edit',{id:obj._id});
		    localStorage.showSubmit=true;
		    $scope.isPopupOpen = false;
		    break;

		    case "view":
		    // $scope.type = type;
		    localStorage.Title="";
		    $state.go('main.opretor.view',{id:obj._id});
		    localStorage.showSubmit=false;
		    $scope.isPopupOpen = false
		    break;

		    case "aircraftAdd":
		    opretor.orerator_view_air({_id:obj._id}).then(objS=>{
	        	console.log("success",objS.result);
	        	(objS.result)?$scope.airD = objS.result:$scope.airD = [];
	        },objE=>{
	        	console.log("error",objE)
	        })

		    console.log("fdjghjdfh",obj)
		    aircraft.download_aircraft()
		      .then(function(objS){
		        $scope.aircraftsDownload = objS;

		        // $scope.aircraftsDownload=$scope.aircraftsDownload.map(x=>{x.baggage =parseInt(x.baggage); x.passangers =parseInt(x.passangers); x.pilot =parseInt(x.pilot); x.speed =parseInt(x.speed); return x});
		        // console.log("Response =>"+JSON.stringify(objS));
		      },function(objE){
		        customAlert.show("Error",objE.msg)
		      })
		    $scope.opr = obj.user_name;
		    // $scope.new.user_name = obj.user_name;
		    $scope.opratorId = obj._id
		    $scope.isPopupOpen = true;
		    break;

		    case "Delete":
	        agent.agent_delete({_id:obj._id})
	          .then(function(objS){
	            console.log(objS);
	            $state.reload();
	          },function(objE){
	            console.log("Error--->",objE)
	          })
	        break;
		  }

		  // $scope.State = findObjectByKey($scope.Countries[0].states, '_id', $scope.newUser.state)
		  // opretor.jetList($scope.newUser._id).then(function(objS){$scope.userAircrafts=objS},function(objE){console.log(objE)})
		 // $fancyModal.open({
		 //      templateUrl: 'user-Modal.html',
		 //      scope: $scope,
		 //      openingClass: 'animated '+modalAnimation[9].in,
		 //      closingClass: 'animated '+modalAnimation[9].out,
		 //      openingOverlayClass: 'animated fadeIn',
		 //      closingOverlayClass: 'animated fadeOut',
		 //    });
    	};

    	$scope.addAircraft = function(valid){
    		if(!valid)
				return
			opretor.add_oprator_aircraft({'aircrafts' :$scope.airD})
			.then(function(objS){
				console.log(objS);
				$scope.isPopupOpen = false;
				customAlert.show('success',objS.response_message)
				$fancyModal.close();
			},function(objE){
				console.log(objE)
			})

    	}

    	$scope.submitForm = function(valid){
			if(!valid)
				return
			console.log("Edit Data =>"+JSON.stringify($scope.newUser))
			opretor.update_operator($scope.newUser)
			.then(function(objS){
				console.log(objS)
				customAlert.show('success',objS.response_message)
				$fancyModal.close();
			},function(objE){
				console.log(objE)
			})
		}

	}])
})()