(function(){
	angular.module('controllers')
	.controller('paymentDetailCtrl', ['$q','$scope', '$rootScope', '$http', '$state','booking','customAlert','$fancyModal',"modalAnimation","master",'baseURL','payment','$timeout','pdfInvoice', function($q,$scope, $rootScope, $http, $state,booking,customAlert,$fancyModal,modalAnimation,master,baseURL,payment,$timeout,pdfInvoice){

		$scope.Title = localStorage.Title;
		$scope.showSubmit = localStorage.showSubmit;
		console.log("id =>",$state.params.id)
	 payment.payment_detail({"_id":$state.params.id}).then(function(objS){
			
			var _temp = objS;
			let date = new Date(_temp.created_at)
			var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
			_temp.time = (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?' pm':' am')

			for (var j = 0; j < _temp.booking_id.roots.length; j++) {
				_tempRoot = _temp.booking_id.roots[j]
				let rootDate = new Date(_temp.booking_id.roots[j].departureDate);
				console.log(rootDate)
				_tempRoot.destime = (rootDate.getDate()<10?'0'+rootDate.getDate():rootDate.getDate())+' '+monthNames[rootDate.getMonth()]+' '+ rootDate.getFullYear()+' '+ (rootDate.getHours()>12?rootDate.getHours()-12:rootDate.getHours())+':'+ rootDate.getMinutes()+(rootDate.getHours()>12?' pm':' am')
			}
			$scope.new = objS;
			console.log("res",JSON.stringify($scope.new))
		},function(objE){
			console.log(objE)
		})


	}])
})()