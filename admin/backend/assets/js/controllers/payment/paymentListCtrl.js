(function(){
	angular.module('controllers')
	.controller('paymentListCtrl', ['$q','$scope', '$rootScope', '$http', '$state','booking','customAlert','$fancyModal',"modalAnimation","master",'baseURL','payment','$timeout','pdfInvoice', function($q,$scope, $rootScope, $http, $state,booking,customAlert,$fancyModal,modalAnimation,master,baseURL,payment,$timeout,pdfInvoice){

		console.log('payment ctrl')
		$scope.currentPage = 1;$scope.paymentList = [];
	    $scope.limit = 10
	    $scope.totalData = Infinity
	    $scope.showPagination = false

		// http://35.154.217.225/admin/master/payment_list
		var getpaymentList = function(page){

			payment.list(page).then(function(objS){
				console.log("Response =>"+page)


				for (var i = 0; i <objS.length; i++) {
					var _temp = objS[i]
					let date = new Date(_temp.created_at)
					var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
					_temp.time = (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?' pm':' am')

					// for (var j = 0; j < _temp.booking_id.roots.length; j++) {
					// 	_tempRoot = _temp.roots[j]
					// 	let rootDate = new Date(_tempRoot.departureDate)
					// 	_tempRoot.time = (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?' pm':' am')
					// }
				}

				console.log("ob ==>",objS)
				for(var i=0;i<objS.length;i++){
					if(objS[i].payment_id)
						$scope.paymentList.push(objS[i]);
				}
				console.log("ob ==>",$scope.paymentList)
				// $scope.totalData = objS.pagination.total;
		        // $scope.limit = objS.pagination.limit;
		        $scope.showPagination = true;
			},function(objE){
				console.log(objE)
			})
		}
		getpaymentList($scope.currentPage)

		$scope.pageChange=function(page){
	      $scope.currentPage = page;
	      getpaymentList($scope.currentPage)
	    }

	    $scope.downldPdf=function(data){
	    	$scope.invoice = data
	    	console.log($scope.invoice)

	    		$timeout(function(){
	    			payment.pdf('pdf-dynamic','#pdfFormat','Quotation.pdf');
	    			$timeout(function(){
	    				$('#pdf-dynamic').css('display','none');
	    			},100)
	    		},10);
	    	
	    }
	    $scope.split = function(id){
	    	$scope.payment_id_new = id;
	    	$fancyModal.open({
			        templateUrl: 'country-Modal.html',
			        scope: $scope,
			        openingClass: 'animated '+modalAnimation[9].in,
			        closingClass: 'animated '+modalAnimation[9].out,
			        openingOverlayClass: 'animated fadeIn',
			        closingOverlayClass: 'animated fadeOut',
			      });
	    }
	    $scope.splitPayment = function(){
	    	var idd = $scope.payment_id_new;
				var req = {
							 "transfers": [
							   {
							     "account": $scope.new.account,
							     "amount" : $scope.new.amount,
							     "currency": $scope.new.currency
							   }
							 ],
							"payment_id": $scope.payment_id_new
							}
	    	console.log("data =>"+JSON.stringify(req));

	    	payment.split(req).then(function(objS){
	    		$fancyModal.close();
	    		// $state.go('main.payment.detail',{id:idd});
				console.log("split pay =>"+JSON.stringify(objS))
			},function(objE){
				console.log(objE)
			})
	    }


	    $scope.show = false;
	    $scope.exportDataXLS = function () {
	        var blob = new Blob([document.getElementById('exportTab').innerHTML], {
	            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
	            });
	        saveAs(blob, "Payment List.xls");
	    };
	    $scope.exportDataPdf = function () {
	        $scope.show = true;
	        $timeout(function(){$scope.show = false; console.log('timeout')}, 100)
	        html2canvas(document.getElementById('data-table'), {
	            onrendered: function (canvas) {
	                var data = canvas.toDataURL();
	                var docDefinition = {
	                    content: [{
	                        image: data,
	                        width: 500,
	                    }]
	                };
	                pdfMake.createPdf(docDefinition).download("Payment List.pdf");
	            }
	        });
	    };

		$scope.openModal=function(type,obj){
			console.log(obj)

			switch(type){
				case "new":
					$scope.type = type;
					$scope.Title="Add";
					$scope.new={};
					$scope.showSubmit=true;
					break;

				case "update":
					$scope.type = type;
					$scope.Title="Update";
					// localStorage.payId = obj._id;
					$scope.new=obj;
					// $state.go('main.payment.detail')
					$scope.showSubmit=true;
					$fancyModal.open({
				templateUrl: 'booking-Modal.html',
				scope: $scope,
				openingClass: 'animated '+modalAnimation[9].in,
				closingClass: 'animated '+modalAnimation[9].out,
				openingOverlayClass: 'animated fadeIn',
				closingOverlayClass: 'animated fadeOut',
		    });

					break;

				case "view":
					$scope.type = type;
					localStorage.Title="";
					var idd = obj._id;
					$scope.new=obj;
					$state.go('main.payment.detail',{id:idd})
					localStorage.showSubmit=false;
					break;
			}

			// $fancyModal.open({
			// 	templateUrl: 'booking-Modal.html',
			// 	scope: $scope,
			// 	openingClass: 'animated '+modalAnimation[9].in,
			// 	closingClass: 'animated '+modalAnimation[9].out,
			// 	openingOverlayClass: 'animated fadeIn',
			// 	closingOverlayClass: 'animated fadeOut',
		 //    });
		};  

	}])
})()