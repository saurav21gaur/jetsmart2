(function(){
	angular.module('controllers')
	.controller('accountCtrl', ['$q','$http','$scope', '$rootScope', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'baseURL','aircraft','$stateParams', function($q,$http,$scope, $rootScope, $state,user,customAlert,$fancyModal,modalAnimation,master,baseURL,aircraft,$stateParams){
       
    $scope.userDetail=JSON.parse(localStorage.userDetail);
    // console.log($scope.userDetail);

	var getCountries = function(){
      aircraft.getCountries()
      .then(function(objS){
        var citiesList = []
        var stateList = []
        for(var i = 0; i < objS.length; i++ ){
          var tempObj = objS[i]
          stateList.push(tempObj.states)
          citiesList.push(tempObj.states.cities)
        }
        
        $scope.userDetail.cityName = (citiesList.find(o => o._id === $scope.userDetail.city)).cityName
        $scope.userDetail.stateName = (stateList.find(o => o._id === $scope.userDetail.state)).stateName
        console.log($scope.userDetail.cityName)
        console.log($scope.userDetail.stateName)

      },function(objE){
        console.log(objE)
      })
    }
    getCountries()
    $scope.openImage = function () {
      let idx = Math.floor(Math.random() * 10)
      $fancyModal.open({
        template: '<img ng-src="{{userDetail.panCardImage}}">',
        scope: $scope,
        openingClass: 'animated '+modalAnimation[idx].in,
        closingClass: 'animated '+modalAnimation[idx].out,
        openingOverlayClass: 'animated fadeIn',
        closingOverlayClass: 'animated fadeOut',
      });
    }

 }])
})();