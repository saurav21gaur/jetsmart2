(function(){
	angular.module('controllers')
	.controller('viewUserCtrl', ['$q','$scope', '$rootScope', '$http', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'baseURL','$timeout','agent', function($q,$scope, $rootScope, $http, $state,user,customAlert,$fancyModal,modalAnimation,master,baseURL,$timeout,agent){

		$scope.currentPage = $state.params.page_no
	    $scope.limit = 10
	    $scope.totalData = Infinity
	    $scope.showPagination = false

		var getUserList = function(page){
			user.list(page).then(function(objS){
				for (var i = 0; i < objS.length; i++) {
					var _temp = objS[i]
					let date = new Date(_temp.created_at)
					var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
					_temp.time = (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?' pm':' am')
				}
				// $scope.totalData = objS.pagination.total;
		        // $scope.limit = objS.pagination.limit;
				$scope.Users = objS;
				$scope.showPagination = true;
				console.log($scope.Users)
			},function(objE){
				console.log(objE)
			})
		}
		getUserList($scope.currentPage)
		 $scope.pageChange=function(page){
	      $scope.currentPage = page;
	      $scope.getUserList($scope.currentPage)
	    }


	    $scope.openModal=function(type,obj){
			
		  switch(type){
		    case "Delete":
	        agent.agent_delete({_id:obj._id})
	          .then(function(objS){
	            console.log(objS);
	            $state.reload();
	          },function(objE){
	            console.log("Error--->",objE)
	          })
	        break;
		  }
    	};

	     $scope.show = false;
	    $scope.exportDataXLS = function () {
	        var blob = new Blob([document.getElementById('exportTab').innerHTML], {
	            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
	            });
	        saveAs(blob, "User List.xls");
	    };
	    $scope.exportDataPdf = function () {
	        $scope.show = true;
	        $timeout(function(){$scope.show = false; console.log('timeout')}, 100)
	        html2canvas(document.getElementById('data-table'), {
	            onrendered: function (canvas) {
	                var data = canvas.toDataURL();
	                var docDefinition = {
	                    content: [{
	                        image: data,
	                        width: 500,
	                    }]
	                };
	                pdfMake.createPdf(docDefinition).download("User List.pdf");
	            }
	        });
	    };
	}])
})()