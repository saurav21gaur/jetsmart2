(function(){
	angular.module('controllers')
	.service('agent', ['$q', '$http','baseURL', function($q, $http,baseURL){
		var self=this;
		this.bulkUpload =function(files){
			var deff=$q.defer();
			var fd = new FormData();
           var arr=[];
           for(var key in files)
           	fd.append('imgs', files[key]);
           	//arr.push($scope.aircraft.files[key])
           //fd.append('imgs', arr);
            
           $http.post("/media/upload", fd, {
              transformRequest: angular.identity,
              headers: {'Content-Type': undefined}
           })
        
           .success(function(objS){
           	console.log(objS);
           	deff.resolve(objS)
           })
        
           .error(function(objE){
           	console.log(objE)
           	deff.reject(objE);
           });
			return deff.promise;
		}
		this.startBulkUpload=function(filesPath){
			var deff=$q.defer();
           // $http.post("admin/agent/startBulkUpload",{filesPath:filesPath})
            $http.post("admin/airport/bulkuploadAirport",{filesPath:filesPath})
        
           .success(function(objS){
           	console.log(objS);
           	deff.resolve(objS)
           })
        
           .error(function(objE){
           	console.log(objE)
           	deff.reject(objE);
           });
			return deff.promise;
		}
		this.createUpdateAgent=function(agent){
			var deff=$q.defer();
			var url="/admin/agent/createUpdateAgent";
			$http.post(url,agent)
			.then(function(objS){
				deff.resolve(objS)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		};
		this.getAll=function(){
			var deff=$q.defer();
			//http://13.126.28.231:1105/admin/aircraft/getAllaircraft 
			var url="/admin/agent/viewAllagent";
			$http.get(baseURL+url)
			.then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		};
		this.agent_delete=function(data){
			var deff=$q.defer();
			//http://13.126.28.231:1105/admin/aircraft/getAllaircraft 
			var url="/admin/agent/agent_delete";
			$http.post(url,data)
			.then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		};
		this.agentDetail=function(id){
			var deff=$q.defer();
			//http://13.126.28.231:1105/admin/aircraft/getAllaircraft 
			var url="/admin/agent/agentDetail";
			$http.post(url,id)
			.then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		};
		this.generateAgencyCode=function(city_code){
			var deff=$q.defer();
			var url="/admin/agent/genrateNumber";
			$http.get(url)
			.then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		};
		this.getAgentList=function(page){
			var deff=$q.defer();
			//http://13.126.28.231:1105/admin/aircraft/getAllaircraft 
			var url="/admin/agent/viewAllagent/"+page;
			$http.get(url)
			.then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		};
		this.uploadRevenueXls=function(files){
            var deff=$q.defer();
            var fd = new FormData();
           var arr=[];
           for(var key in files)
            fd.append('imgs', files[key]);
            //arr.push($scope.aircraft.files[key])
           //fd.append('imgs', arr);
            
           $http.post("/media/upload", fd, {
              transformRequest: angular.identity,
              headers: {'Content-Type': undefined}
           })
        
           .success(function(objS){
            console.log(objS);
            deff.resolve(objS)
           })
        
           .error(function(objE){
            console.log(objE)
            deff.reject(objE);
           });
            return deff.promise;
        },

        this.uploadExcelToServer= function(filesPath){
        	console.log("agent", typeof filesPath)

			var deff=$q.defer();
				
				var url ="https://gojetsmart.com/admin/oprator/revenuBulkUpload";
				$http.post(url, {filesPath: filesPath}).then(function(result){
					deff.resolve(result.data);
				},function(err){
					deff.reject(err);
				})

				return deff.promise;
		}
	}])
})()