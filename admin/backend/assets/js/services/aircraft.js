(function(){
	angular.module('controllers')
	.service('aircraft', ['$q', '$http','baseURL', function($q, $http,baseURL){
		var self=this;
		this.userId=0;
		this.uploadFiles=function(files){
			var deff=$q.defer();
			var fd = new FormData();
           var arr=[];
           for(var key in files)
           	fd.append('imgs', files[key]);
           	//arr.push($scope.aircraft.files[key])
           //fd.append('imgs', arr);
            
           $http.post("/media/upload", fd, {
              transformRequest: angular.identity,
              headers: {'Content-Type': undefined}
           })
        
           .success(function(objS){
           	console.log(objS);
           	deff.resolve(objS)
           })
        
           .error(function(objE){
           	console.log(objE)
           	deff.reject(objE);
           });
			return deff.promise;
		}

		this.getAllAirport =function(){

			var deff=$q.defer();
			
			$http.get('/admin/aircraft/from_to_airport').then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		}
		
		this.delete =function(data){

			var deff=$q.defer();
			
			$http.post('/admin/aircraft/aircraft_Delete',data).then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		}
		this.aircraftDetail =function(data){

			var deff=$q.defer();
			
			$http.post('/admin/aircraft/aircraftDetail',data).then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		}

		this.deleteAircraftImg=function(file){
			var deff=$q.defer();
			var url="/media/remove";
			if(!file.isTemp)
				url="admin/aircraft/removeimage/";
			$http.post(url,file)
			.then(function(objS){
				deff.resolve(objS)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		}
		this.createUpdateAircraft=function(aircraft){
			var deff=$q.defer();
			var url="/admin/aircraft/createUpdateAircraft";
			$http.post(url,aircraft)
			.then(function(objS){
				deff.resolve(objS)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		};
		this.pdf = function(bodyAppendId,pdfDom,fileName,cb){
			if($("#"+bodyAppendId).length==0)
				$('body').append("<div id='"+bodyAppendId+"'></div>")
			$('#'+bodyAppendId).html($(pdfDom).html())
			$('#'+bodyAppendId).css('display','block')
			$http.post('/fareportal/generateQuotation',{htmlContent:$('#'+bodyAppendId).html()})
			.then(objS=>{
				console.log('http://35.154.217.225'+objS.data.filePath);
				setTimeout(function(){
					var link = document.createElement('a');
					link.href = 'http://35.154.217.225'+objS.data.filePath;
					link.download = 'Invoice.pdf';
					link.dispatchEvent(new MouseEvent('click'));
				},5000)
				cb(objS.data.filePath)
			},objE=>console.log(objE))
		}

		this.pdfOperator = function(bodyAppendId,pdfDom,fileName,cb){
			if($("#"+bodyAppendId).length==0)
				$('body').append("<div id='"+bodyAppendId+"'></div>")
			$('#'+bodyAppendId).html($(pdfDom).html())
			$('#'+bodyAppendId).css('display','block')
			$http.post('/fareportal/generateQuotOpeartor',{htmlContent:$('#'+bodyAppendId).html()})
			.then(objS=>{
				console.log('https://gojetsmart.com'+objS.data.filePath);
				setTimeout(function(){
					var link = document.createElement('a');
					link.href = 'https://gojetsmart.com'+objS.data.filePath;
					link.download = 'Invoice.pdf';
					link.dispatchEvent(new MouseEvent('click'));
				},5000)
				cb(objS.data.filePath)
			},objE=>console.log(objE))
		}

		this.getAll=function(page){
			var deff=$q.defer();
			//http://13.126.28.231:1105/admin/aircraft/getAllaircraft 
			var url="aircraft/getAllaircraft/"+page;
			$http.get(baseURL+url)
			.then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		}
		this.download_aircraft=function(){
			var deff=$q.defer();
			var url="aircraft/aircraft_download";
			$http.get(baseURL+url)
			.then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		}
		this.quotationEmail = function(pdf){
			 
			var deff=$q.defer();
			//http://13.126.28.231:1105/admin/aircraft/getAllaircraft 
			var url="oprator/emailSend";
			$http.post(baseURL+url,pdf)
			.then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		} 
       this.getAllOperator=function(){
			var deferred = $q.defer();
	        $http({
	            method: 'GET',
	            url: baseURL+"oprator/opertaor_aircrft/"+localStorage.userId,
	            data: {},
	            headers: {
	                'Content-Type': 'application/json'
	            }
	        }).then(function(objS){
	            //alert("success",JSON.stringify(objS));
	            if(objS.status==200)
	                deferred.resolve(objS);
	            else
	                deferred.reject(objS);
	        })
	        .catch(function(objE){
	            //alert("Network Connection Problem!");
	            console.log(objE);
	            deferred.reject(objE)
	        })
	        return deferred.promise;
		}

		this.airports=function(){
			var deff=$q.defer();
			//http://13.126.28.231:1105/admin/aircraft/getAllaircraft 
			var url="/libs/airports.json";
			$http.get(url)
			.then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		}
		this.opertaor_aircrft_detail = function(data){
			var deff=$q.defer();
			
			var url ='/admin/oprator/opertaor_aircrft_detail'

			$http.post(url,data).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		};
		this.getCountries = function(){
			var deff=$q.defer();
			var url="/admin/master/city";
			$http.get(url)
			.then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		}
		this.saveBooking=function(data){
			var deff=$q.defer();
			var url="/admin/oprator/opratorsaveflight";
			$http.post(url,data)
			.then(function(objS){
				deff.resolve(objS)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		};
	}])
})()