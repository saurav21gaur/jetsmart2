(function(){
	angular.module('controllers')
	.service('booking', ['$q', '$http','baseURL', function($q, $http,baseURL){

		this.list = function(page){
			var deff=$q.defer();
			var url = baseURL+'master/all_booking_list/'+page

			$http.get(url).then(function(result){
				// console.log(result)
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		}

		this.bookAircraft = function(data){
			var deff=$q.defer();
			var url = baseURL+'master/opretor_flight_add'

			$http.post(url, data).then(function(result){
				// console.log(result)
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		}
		this.booking_detail= function(data){
			var deff=$q.defer();
			var url = baseURL+'master/booking_list_detail'

			$http.post(url, data).then(function(result){
				// console.log(result)
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		}

		this.addBookingDetail =function(data,id){
			var deff = $q.defer();
			var url = baseURL+'oprator/addBookingDetail/'+id
			$http.put(url,data).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}

		this.updateBookingDetail =function(data,id,id2){
			var deff = $q.defer();
			var url = baseURL+'oprator/editBookingDetail/'+id+'/'+id2
			$http.put(url,data).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}

	}])
})()