(function(){
	angular.module('controllers')
	.service('employee', ['$q', '$http','baseURL', function($q, $http,baseURL){
		var self=this;

       this.list = function(id) {
	        var deferred = $q.defer();
	        $http({
	            method: 'GET',
	            url: baseURL+"master/employee_list",
	            headers: {
	                'Content-Type': 'application/json'
	            }
	        }).then(function(objS){
	            if(objS.status==200)
	                deferred.resolve(objS);
	            else
	                deferred.reject(objS);
	        })
	        .catch(function(objE){
	            console.log(objE);
	            deferred.reject(objE)
	        })
	        return deferred.promise;
       };
       this.add = function(data) {
	        var deferred = $q.defer();
	        $http({
	            method: 'POST',
	            url: baseURL+"oprator/oprator_signup",
	            data:data,
	            headers: {
	                'Content-Type': 'application/json'
	            }
	        }).then(function(objS){
	            if(objS.status==200)
	                deferred.resolve(objS);
	            else
	                deferred.reject(objS);
	        })
	        .catch(function(objE){
	            console.log(objE);
	            deferred.reject(objE)
	        })
	        return deferred.promise;
       };

       this.details = function(id) {
	        var deferred = $q.defer();
	        $http({
	            method: 'GET',
	            url: baseURL+"oprator/particular_employee/"+id,
	            headers: {
	                'Content-Type': 'application/json'
	            }
	        }).then(function(objS){
	            if(objS.status==200)
	                deferred.resolve(objS.data);
	            else
	                deferred.reject(objS);
	        })
	        .catch(function(objE){
	            console.log(objE);
	            deferred.reject(objE)
	        })
	        return deferred.promise;
       };

       this.update = function(data) {
	        var deferred = $q.defer();
	        $http({
	            method: 'POST',
	            url: baseURL+"oprator/edit_employee",
	            data:data,
	            headers: {
	                'Content-Type': 'application/json'
	            }
	        }).then(function(objS){
	            if(objS.status==200)
	                deferred.resolve(objS.data);
	            else
	                deferred.reject(objS);
	        })
	        .catch(function(objE){
	            console.log(objE);
	            deferred.reject(objE)
	        })
	        return deferred.promise;
       };
	}])
})()