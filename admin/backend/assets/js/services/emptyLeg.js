(function(){
	angular.module('controllers')
	.service('emptyLeg', ['$q', '$http','baseURL', function($q, $http,baseURL){
		var self=this;

		this.add = function(data) {
	        var deferred = $q.defer();
	        $http({
	            method: 'POST',
	            url: baseURL+"oprator/add_empty_jet",
	            data: data,
	            headers: {
	                'Content-Type': 'application/json'
	            }
	        }).then(function(objS){
	            if(objS.status==200)
	                deferred.resolve(objS);
	            else
	                deferred.reject(objS);
	        })
	        .catch(function(objE){
	            console.log(objE);
	            deferred.reject(objE)
	        })
	        return deferred.promise;
       };
       this.manifest = function(data) {
	        var deferred = $q.defer();
	        $http({
	            method: 'POST',
	            url: baseURL+"oprator/mainifist_list",
	            data: data,
	            headers: {
	                'Content-Type': 'application/json'
	            }
	        }).then(function(objS){
	            if(objS.status==200)
	                deferred.resolve(objS.data);
	            else
	                deferred.reject(objS);
	        })
	        .catch(function(objE){
	            console.log(objE);
	            deferred.reject(objE)
	        })
	        return deferred.promise;
       };

       this.addPassanger = function(data) {
       	console.log("data in emptyLeg", data)
       	
	        var deferred = $q.defer();
	        $http({
	            method: 'POST',
	            url: baseURL+"oprator/add_edit_pessenger",
	           	data: data,
	            headers: {
	                'Content-Type': 'application/json'
	            }
	        }).then(function(objS){
	            if(objS.status==200)
	                deferred.resolve(objS);
	            else
	                deferred.reject(objS);
	        })
	        .catch(function(objE){
	            console.log(objE);
	            deferred.reject(objE)
	        })
	        return deferred.promise;
       };

       this.Editemptyleg = function(data) {
       	console.log("data in emptyLeg", data)
       	
	        var deferred = $q.defer();
	        $http({
	            method: 'POST',
	            url: baseURL+"oprator/Editemptyleg",
	           	data: data,
	            headers: {
	                'Content-Type': 'application/json'
	            }
	        }).then(function(objS){
	            if(objS.status==200)
	                deferred.resolve(objS);
	            else
	                deferred.reject(objS);
	        })
	        .catch(function(objE){
	            console.log(objE);
	            deferred.reject(objE)
	        })
	        return deferred.promise;
       };

       this.updatePassanger = function(data) {
       	console.log("data in emptyLeg", data)
       	var passenger_id= data._id
       		console.log("data in passenger_id", passenger_id)
       	 var obj={
              name: data.name,
              age: data.age,
              gender: data.gender,
              passportNumber: data.passportNumber,
              nationlity: data.nationlity
            }
	        var deferred = $q.defer();
	        $http({
	            method: 'PUT',
	            url: baseURL+"oprator/edit_pessenger/"+data.flight_id+"/"+passenger_id,
	           	data: obj,
	            headers: {
	                'Content-Type': 'application/json'
	            }
	        }).then(function(objS){
	            if(objS.status==200)
	                deferred.resolve(objS);
	            else
	                deferred.reject(objS);
	        })
	        .catch(function(objE){
	            console.log(objE);
	            deferred.reject(objE)
	        })
	        return deferred.promise;
       };



       this.addCrewMember = function(data) {
	        var deferred = $q.defer();
	        $http({
	            method: 'POST',
	            url: baseURL+"oprator/add_emptyleg",
	            data:data,
	            headers: {
	                'Content-Type': 'application/json'
	            }
	        }).then(function(objS){
	            if(objS.status==200)
	                deferred.resolve(objS);
	            else
	                deferred.reject(objS);
	        })
	        .catch(function(objE){
	            console.log(objE);
	            deferred.reject(objE)
	        })
	        return deferred.promise;
       };
        this.updateCrewMember = function(data) {
	        var deferred = $q.defer();
	        $http({
	            method: 'POST',
	            url: baseURL+"oprator/edit_emptyleg",
	            data:data,
	            headers: {
	                'Content-Type': 'application/json'
	            }
	        }).then(function(objS){
	            if(objS.status==200)
	                deferred.resolve(objS);
	            else
	                deferred.reject(objS);
	        })
	        .catch(function(objE){
	            console.log(objE);
	            deferred.reject(objE)
	        })
	        return deferred.promise;
       };
       

       this.list = function(id) {
	        var deferred = $q.defer();
	        $http({
	            method: 'GET',
	            url: baseURL+"oprator/operator_empty_jet/"+id,
	            headers: {
	                'Content-Type': 'application/json'
	            }
	        }).then(function(objS){
	            if(objS.status==200)
	                deferred.resolve(objS);
	            else
	                deferred.reject(objS);
	        })
	        .catch(function(objE){
	            console.log(objE);
	            deferred.reject(objE)
	        })
	        return deferred.promise;
       };

       this.empty_jet_list = function(id) {
	        var deferred = $q.defer();
	        $http({
	            method: 'GET',
	            url: baseURL+"oprator/empty_jet_list",
	            headers: {
	                'Content-Type': 'application/json'
	            }
	        }).then(function(objS){
	            if(objS.status==200)
	                deferred.resolve(objS);
	            else
	                deferred.reject(objS);
	        })
	        .catch(function(objE){
	            console.log(objE);
	            deferred.reject(objE)
	        })
	        return deferred.promise;
       };
       this.empty_jet_delete = function(id) {
	        var deferred = $q.defer();
	        $http({
	            method: 'POST',
	            url: baseURL+"oprator/empty_jet_delete",
	            data:id,
	            headers: {
	                'Content-Type': 'application/json'
	            }
	        }).then(function(objS){
	            if(objS.status==200)
	                deferred.resolve(objS);
	            else
	                deferred.reject(objS);
	        })
	        .catch(function(objE){
	            console.log(objE);
	            deferred.reject(objE)
	        })
	        return deferred.promise;
       };
       this.pdf = function(bodyAppendId,pdfDom,fileName){
			if($("#"+bodyAppendId).length==0)
				$('body').append("<div id='"+bodyAppendId+"'></div>")
			$('#'+bodyAppendId).html($(pdfDom).html())
			$('#'+bodyAppendId).css('display','block')
			$http.post('/fareportal/generateMainfist',{htmlContent:$('#'+bodyAppendId).html()})
			.then(objS=>{
				console.log('https://gojetsmart.com'+objS.data.filePath);
				setTimeout(function(){
					var link = document.createElement('a');
					link.href = 'https://gojetsmart.com'+objS.data.filePath;
					link.download = 'manifest.pdf';
					link.dispatchEvent(new MouseEvent('click'));
				},5000)
			},objE=>console.log(objE))
		}

	}])
})()