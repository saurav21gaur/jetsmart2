(function(){
	angular.module('controllers')
	.service('master', ['$q', '$http','baseURL', function($q, $http,baseURL){
		var self=this;
		this.saveUpdateCountry=function(country){
			var deff=$q.defer();
			country.createdBy=localStorage.username;
			$http.post(baseURL+'master/country',country).then(function(result){
				console.log(result);
				if(result.status == 200){
					deff.resolve(result);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}
		this.regionCountry=function(country){
			var deff=$q.defer();
			$http.get(baseURL+'master/regionaccordingCountry/'+country).then(function(result){
				console.log(result);
				if(result.status == 200){
					deff.resolve(result);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}

		
		this.countries=function(page){
			var deff=$q.defer();
			$http.get(baseURL+'master/country/'+page).then(function(result){
				// console.log(result);
				if(result.status == 200){
					deff.resolve(result.data.result);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}
		this.country_Delete=function(data){
			var deff=$q.defer();
			$http.post(baseURL+'master/country_Delete',data).then(function(result){
				// console.log(result);
				if(result.status == 200){
					deff.resolve(result.data.result);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}

		this.saveUpdateRegion=function(region){
			var deff=$q.defer();
			region.createdBy=localStorage.username;
			// var _id= region.country._id
			// delete region.country;
			// region.country={_id:_id};
			$http.post(baseURL+'master/region',region).then(function(result){
				console.log(result);
				if(result.status == 200){
					deff.resolve(result);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}

		this.regions=function(){
			var deff=$q.defer();
			$http.get(baseURL+'master/region').then(function(result){
				console.log(result);
				if(result.status == 200){
					deff.resolve(result.data);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}
		this.saveUpdateState=function(state){
			var deff=$q.defer();
			state.createdBy=localStorage.username;
			// var _id= region.country._id
			// delete region.country;
			// region.country={_id:_id};
			$http.post(baseURL+'master/states',state).then(function(result){
				console.log(result);
				if(result.status == 200){
					deff.resolve(result);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}

		this.states=function(){
			var deff=$q.defer();
			$http.get(baseURL+'master/states').then(function(result){
				// console.log(result);
				if(result.status == 200){
					deff.resolve(result.data);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}
		this.state_delete=function(data){
			var deff=$q.defer();
			$http.post(baseURL+'master/state_delete',data).then(function(result){
				// console.log(result);
				if(result.status == 200){
					deff.resolve(result.data.result);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}
		
		this.saveUpdateCity=function(city){
			var deff=$q.defer();
			city.createdBy=localStorage.username;
			// var _id= region.country._id
			// delete region.country;
			// region.country={_id:_id};
			$http.post(baseURL+'master/city',city).then(function(result){
				console.log(result);
				if(result.status == 200){
					deff.resolve(result);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}

		this.cities=function(){
			var deff=$q.defer();
			$http.get(baseURL+'master/city').then(function(result){
				// console.log(result);
				if(result.status == 200){
					deff.resolve(result.data);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}

		this.airports=function(){
			var deff=$q.defer();
			$http.get(baseURL+'aircraft/from_to_airport').then(function(result){
				// console.log(result);
				if(result.status == 200){
					deff.resolve(result.data);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}
		this.airport_delete=function(data){
			var deff=$q.defer();
			$http.post(baseURL+'airport/airport_delete',data).then(function(result){
				// console.log(result);
				if(result.status == 200){
					deff.resolve(result.data);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}
		this.airport_detail=function(id){
			var deff=$q.defer();
			$http.get(baseURL+'airport/airportDetail/'+id).then(function(result){
				// console.log(result);
				if(result.status == 200){
					deff.resolve(result.data);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}
		//airport/addUpdateAirport
		this.saveUpdateAirport=function(airport){
			var deff=$q.defer();
			$http.post(baseURL+'airport/addUpdateAirport',airport).then(function(result){
				console.log(result);
				if(result.status == 200){
					deff.resolve(result.data);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}

		this.distance=function(page_no){
			var deff=$q.defer();
			$http.get(baseURL+'airport/allairportDistance/'+page_no).then(function(result){
				// console.log(result);
				if(result.status == 200){
					deff.resolve(result.data);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}
		this.distance_delete=function(data){
			var deff=$q.defer();
			$http.post(baseURL+'airport/airportDistance_delete',data).then(function(result){
				// console.log(result);
				if(result.status == 200){
					deff.resolve(result.data);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}

		this.saveUpdateDistance=function(data){
			var deff=$q.defer();
			$http.post(baseURL+'airport/addUpdateAirportDistance',data).then(function(result){
				console.log(result);
				if(result.status == 200){
					deff.resolve(result.data);
				}
				else{
					deff.reject(result)
				}
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		}

		this.uploadFiles=function(files){
			var deff=$q.defer();
			var fd = new FormData();
           var arr=[];
           for(var key in files)
           	fd.append('imgs', files[key]);
           	//arr.push($scope.aircraft.files[key])
           //fd.append('imgs', arr);
            
           $http.post("/media/upload", fd, {
              transformRequest: angular.identity,
              headers: {'Content-Type': undefined}
           })
        
           .success(function(objS){
           	console.log(objS);
           	deff.resolve(objS)
           })
        
           .error(function(objE){
           	console.log(objE)
           	deff.reject(objE);
           });
			return deff.promise;
		}


	this.bulkUploadOprator=function(filesPath){
		var deff=$q.defer();
		$http({
            method: 'POST',
            url: baseURL+"oprator/bulkUploadOprator",
            data: {"filesPath" : filesPath},
            headers: {
                'Content-Type': 'application/json'
            }
		}).then(function(objS){
			//alert("success",JSON.stringify(objS));
			if(objS.status==200)
				deff.resolve(objS);
			else
				deff.reject(objS);
		})
		.catch(function(objE){
			//alert("Network Connection Problem!");
			console.log(objE);
			deff.reject(objE)
		})
		return deff.promise;
	}



	}])
})()