(function(){
	angular.module('controllers')
	.service('opretor', ['$q', '$http','baseURL', function($q, $http,baseURL){
		var self=this;

		this.add = function(data) {
	        var deferred = $q.defer();
	        $http({
	            method: 'POST',
	            url: baseURL+"oprator/oprator_signup",
	            data: data,
	            headers: {
	                'Content-Type': 'application/json'
	            }
	        }).then(function(objS){
	            //alert("success",JSON.stringify(objS));
	            if(objS.status==200)
	                deferred.resolve(objS);
	            else
	                deferred.reject(objS);
	        })
	        .catch(function(objE){
	            //alert("Network Connection Problem!");
	            console.log(objE);
	            deferred.reject(objE)
	        })
	        return deferred.promise;
       };


       this.imgUpload=function(files){
			var deff=$q.defer();
			var fd = new FormData();
           var arr=[];
           for(var key in files)
           	fd.append('imgs', files[key]);
           	//arr.push($scope.aircraft.files[key])
           //fd.append('imgs', arr);
            
           $http.post("/media/upload", fd, {
              transformRequest: angular.identity,
              headers: {'Content-Type': undefined}
           })
        
           .success(function(objS){
           	console.log(objS);
           	deff.resolve(objS)
           })
        
           .error(function(objE){
           	console.log(objE)
           	deff.reject(objE);
           });
			return deff.promise;
		}

		// this.add = function(data){
		// 	var deff=$q.defer();
			
		// 	var url ='/admin/user/user_signup'

		// 	$http.post(url,data).then(function(result){
		// 		deff.resolve(result);
		// 	},function(err){
		// 		deff.reject(err);
		// 	})

		// 	return deff.promise;
		// };

		this.login = function(data){
			var deff=$q.defer();

			$http.post(baseURL+"oprator/login",data).then(function(result){
				deff.resolve(result);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		};

		this.jetList = function(id){
			var deff=$q.defer();

			$http.get(baseURL+"oprator/opertaor_aircrft/"+id).then(function(res){
				deff.resolve(res.data.result);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		};

		this.list = function(page){
			var deff=$q.defer();
			
			var url = 'admin/oprator/oprator_list/'+page

			$http.get(url).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		}

		this.delete = function(id){
			var deff=$q.defer();
			
			var url = '/admin/user/delete_user'

			$http.post(url, {user_id : id}).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		}

		this.edit = function(data){
			var deff=$q.defer();
			
			var url ='/admin/user/edit_user'

			$http.post(url,data).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		};

		this.update_operator = function(data){
			var deff=$q.defer();
			
			var url ='/admin/oprator/edit_oprator'

			$http.post(url,data).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		};

		this.add_oprator_aircraft = function(data){
			var deff=$q.defer();
			
			var url ='/admin/oprator/add_oprator_aircraft'

			$http.post(url,data).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		};

		this.view_oprator = function(data){
			var deff=$q.defer();
			
			var url ='/admin/oprator/view_oprator'

			$http.post(url,data).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		};
		this.adminFlightList= function(data){
			var deff=$q.defer();
			var url ='/admin/oprator/all_flight_list'
			$http.get(url).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		};

		this.orerator_view_air= function(data){
			var deff=$q.defer();
			var url ='/admin/oprator/oprator_aircraft_view_edit'
			$http.post(url,data).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		};
		this.removeOperatorFlight= function(id){
			var deff=$q.defer();
			var url ='/admin/oprator/oprator_aircraft_delete/'+id
			$http.get(url).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		};
		
		this.editOperatorFlight= function(data,id){
			var deff=$q.defer();
			var url ='/admin/oprator/oprator_aircraft_edit/'+id
			$http.put(url,data).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})
			return deff.promise;
		};
		this.flight_delete= function(data){
			var deff=$q.defer();
			
			var url ='/admin/oprator/flight_delete'

			$http.post(url,data).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		};

		this.generatorNumber = function(){
			var deff=$q.defer();
			
			var url ='/admin/agent/genrateNumber'

			$http.get(url).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		};

        //  this.uploadRevenueXls=function(files){
        //     var deff=$q.defer();
        //     var fd = new FormData();
        //    var arr=[];
        //    for(var key in files)
        //     fd.append('imgs', files[key]);
        //     //arr.push($scope.aircraft.files[key])
        //    //fd.append('imgs', arr);
            
        //    $http.post("/media/upload", fd, {
        //       transformRequest: angular.identity,
        //       headers: {'Content-Type': undefined}
        //    })
        
        //    .success(function(objS){
        //     console.log(objS);
        //     deff.resolve(objS)
        //    })
        
        //    .error(function(objE){
        //     console.log(objE)
        //     deff.reject(objE);
        //    });
        //     return deff.promise;
        // },

		

		/*this.details = function(data){
			var deff=$q.defer();
			
			var url ='/admin/user/user_detail/'

			$http.post(url,data).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		};*/

	}])
})()