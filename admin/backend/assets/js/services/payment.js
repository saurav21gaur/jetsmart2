(function(){
	angular.module('controllers')
	.service('payment', ['$q', '$http','baseURL', function($q, $http,baseURL){

		this.list = function(page){
			var deff=$q.defer();
			var url = baseURL+'master/payment_list/'+page

			$http.get(url).then(function(result){
				// console.log(result)
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		}
		this.payment_detail = function(id){
			var deff=$q.defer();
			var url = baseURL+'master/payment_detail'

			$http.post(url,id).then(function(result){
				// console.log(result)
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		}

		this.split = function(data){
			var deff=$q.defer();
			var url = '/fareportal/splitpayment'

			$http.post(url,data).then(function(result){
				// console.log(result)
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		}

		this.pdf = function(bodyAppendId,pdfDom,fileName){
			if($("#"+bodyAppendId).length==0)
				$('body').append("<div id='"+bodyAppendId+"'></div>")
			$('#'+bodyAppendId).html($(pdfDom).html())
			$('#'+bodyAppendId).css('display','block')
			$http.post('/fareportal/generateQuotation',{htmlContent:$('#'+bodyAppendId).html()})
			.then(objS=>{
				console.log('http://35.154.217.225'+objS.data.filePath);
				setTimeout(function(){
					var link = document.createElement('a');
					link.href = 'http://35.154.217.225'+objS.data.filePath;
					link.download = 'Invoice.pdf';
					link.dispatchEvent(new MouseEvent('click'));
				},3000)
			},objE=>console.log(objE))
		}

	}])
})()