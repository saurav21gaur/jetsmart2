


angular.module('controllers')
.factory('pdfInvoice', ['$http', '$q', function($http, $q){

    return {
        create:function(bodyAppendId,pdfDom,fileName){
            if($("#"+bodyAppendId).length==0)
                $('body').append("<div id='"+bodyAppendId+"'></div>")
            $('#'+bodyAppendId).html($(pdfDom).html())
            $('#'+bodyAppendId).css('display','block')
            $http.post('/fareportal/downloadIncoice',{htmlContent:$('#'+bodyAppendId).html()})
            .then(objS=>{
                console.log('http://35.154.217.225'+objS.data.filePath);
                setTimeout(function(){
                    var link = document.createElement('a');
                    link.href = 'http://35.154.217.225'+objS.data.filePath;
                    link.download = 'Invoice.pdf';
                    link.dispatchEvent(new MouseEvent('click'));
                },2500)
            },objE=>console.log(objE))
        }
    }
}])