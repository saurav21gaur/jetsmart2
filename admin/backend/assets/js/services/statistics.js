(function(){
	angular.module('controllers')
	.service('statistics', ['$q', '$http','baseURL', function($q, $http,baseURL){

		this.all = function(){
			var deff=$q.defer();
			var url = baseURL+'master/Number_of_counts'

			$http.get(url).then(function(result){
				if(result.status == 200){
					deff.resolve(result.data);
				}
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		},
		this.changePassword = function(data){
			console.log("req =>"+JSON.stringify(data))
			var deff=$q.defer();
			var url = baseURL+'oprator/change_password_operator'
			$http.post(url,data).then(function(result){
				if(result.status == 200){
					deff.resolve(result.data);
				}
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		}

	}])
})()