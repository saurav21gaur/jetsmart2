(function(){
	angular.module('controllers')
	.service('user', ['$q', '$http','baseURL', function($q, $http,baseURL){

		this.login = function(data){
			var deff=$q.defer();

			$http.post(baseURL+"oprator/login",data).then(function(result){
				deff.resolve(result);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		};
		
		this.list = function(page){
			var deff=$q.defer();
			var url = baseURL+'master/all_user_list/'+page

			$http.get(url).then(function(result){
				console.log(result)
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		};
		
		this.bookings = function(id){
			var deff=$q.defer();
			var url = baseURL+'oprator/oprator_flight_list/'+id

			$http.get(url).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		}
		
		this.allBookings = function(){
			var deff=$q.defer();
			var url = baseURL+'master/operator_aircraft_list'

			$http.get(url).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		}
		this.filght_detail = function(id){
			var deff=$q.defer();
			var url = baseURL+'oprator/oprator_flight_detail'

			$http.post(url,id).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		}
		this.flight_mainifist_list = function(id){
			var deff=$q.defer();
			var url = baseURL+'oprator/flight_mainifist_list/'+id

			$http.get(url).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		}

	}])
})()