
// Gateway Controller //
var config=require.main.require('./global/config');
var jwt = require('jsonwebtoken');
var countrySchema=require.main.require('./db/countrySchema');
var airport=require.main.require('./db/airportSchema.js');
var regionSchema=require.main.require('./db/regionSchema');
var aircraftType = require.main.require('./db/aircraftTypeSchema');
var aircraft = require.main.require('./db/aircraftSchema');
var airportdistance = require.main.require('./db/airportDistanceSchema.js')
var user = require.main.require('./db/userSchema.js')

var agent = require.main.require('./db/opratorSchema.js')
var saveflight = require.main.require('./db/addFlightSchema.js')
var book_list = require.main.require('./db/passengerBookingSchema.js')
var transaction = require.main.require('./db/transaction.js')
var airportCtrl=require.main.require('./admin/airport/controllers.js');
var waterfall = require('async-waterfall');
var mongoose = require('mongoose');
var async=require("async");
var ObjectId = require('mongoose').Types.ObjectId; 
var mongoXlsx = require('mongo-xlsx');

var controller={
    addUpdateCounty:function(req,res){
    	console.log(req.body);
    	if(!req.body._id){
    		console.log("if--------->")
    		var objSchema=new countrySchema(req.body)
    		objSchema.save(function(error,result){
    			error?res.status(500).send(error):res.send(result)
			})
    	}
    	else{
			countrySchema.findByIdAndUpdate(req.body._id, {$set:req.body}, function(error,result) {
    		if(error) { throw error; }
    		error?res.status(500).send(error):res.send(result)
			    //...
			})
    	}
    },

    operator_aircraft_list : function(req,res){
     saveflight.find({}).populate('aircratft_id').exec(function(err,result){
            err?res.status(500).send(err):res.send(result)
        })

    },
     employee_list : function(req,res){
     agent.find({type:'employee',is_active:'active'}).exec(function(err,result){
            err?res.status(500).send(err):res.send(result)
        })

    },
    getAllCountries:function(req,res){
        var query  = {status:'active'};
        var options = {
                  page:   req.params.page_number, 
                  limit:   10
        };
        countrySchema.paginate(query,options,function(err,result){
            console.log("result",result)
            if(err){
                res.send({response_code:500,response_message:"server err"})
            }
            else{
                var pagination ={};
                        pagination.total = result.total;
                        pagination.limit= result.limit;
                        pagination.page= result.page;
                        pagination.pages= result.pages;
                       res.send({result:result.docs ,pagination:pagination})
            }
        })
    	// var obj={}
    	// countrySchema.find(obj,function(err,result){
    	// 	err?res.status(500).send(err):res.send(result);
    	// })
    },

    'country_Delete': function(req,res){
        console.log("req---",req.body);
        countrySchema.update({_id:req.body._id},{$set:{status:'Block'}},function(err,result){
             if(err) {res.send({status:500,message:"something wents wrong"})}
                else if(!result){res.send({status:400,message:"No data found"})}
                    else{return res.send({status:200,message:"Country deleted successfully.",result})}
            // error?res.status(500).send(error):res.send(result)     

        })
    },
    addUpdateRegion:function(req,res){
        console.log(req.body);
        if(!req.body._id){
            console.log("if--------->",req.body)
            console.log("req.body.country--------->",req.body.country)
            console.log("typeof--------->",typeof(req.body.country))
            var objSchema=new regionSchema(req.body)
            objSchema.save(function(error,result){
                error?res.status(500).send(error):res.send(result)
            })
        }
        else{
            regionSchema.findByIdAndUpdate(req.body._id, {$set:req.body}, function(error,result) {
            if(error) { throw error; }
            error?res.status(500).send(error):res.send(result)
                //...
            })
        }
    },
    getAllRegions:function(req,res){
        var obj={}
        regionSchema.find(obj).populate('country').exec(function(err,result){
            err?res.status(500).send(err):res.send(result);
        })
    },
    addUpdateStates:function(req,res){
        console.log("req.body-------->",req.body)
       if(!req.body.states._id){
              countrySchema.findOneAndUpdate({
            countryName: req.body.country.countryName
        }, {
            "$push": {
                "states": {
                    "stateName": req.body.states.stateName,
                    "region":req.body.region._id,
                    "createdBy":req.body.createdBy

                }
            }
        },{new:true}, function (err, result) {
           // console.log("result" + JSON.stringify(result));
              err?res.status(500).send(err):res.send(result);
        })
       }
       else{
         countrySchema.update({'states._id':req.body.states._id},{$set:{'states.$.stateName':req.body.states.stateName,'states.$.region':req.body.region._id,'states.$.createdBy':req.body.createdBy}},function(err,result){
            // countrySchema.update({'states._id':req.body.states_id},{$set:{'states':{req.body}}},function(err,result){
            err?res.status(500).send(err):res.send(result);

        })

       }

    },
    getAllStates:function(req,res){
        console.log("re");

        var obj={}
        // countrySchema.aggregate([{ 
        //     $unwind : "$states" 
        //     },{
        //         "$lookup":{
        //             "from":"region",
        //             "localField":"states",
        //             "foreignField":"states.region",
        //             "as":"region_name"
        //         }
        //     }
        //     // { "$unwind": "$region_name" },
        //     // { "$group": {
        //     //     "_id": "$_id",
        //     //     "states": { "$push": "$states" },
        //     //     "region_name": { "$push": "$region_name" }
        //     // }}
        // ] ).exec(function(err,result){
        //     //region
        //     err?res.status(500).send(err):res.send(result);
        // })

          countrySchema.aggregate([{
                            $unwind: '$states'
                        }])
            .exec(function(err, transactions) {
                regionSchema.populate(transactions, {path: 'states.region'}, function(err, result) {
                    console.log("result",result)
                   err?res.status(500).send(err):res.send(result);
                });
            });

    },

    // 'state_delete': function(req,res){
    //     console.log("req---",req.body);
    //     countrySchema.update({_id:req.body._id},{$set:{status:'Block'}},function(err,result){
    //          if(err) {res.send({status:500,message:"something wents wrong"})}
    //             else if(!result){res.send({status:400,message:"No data found"})}
    //                 else{return res.send({status:200,message:"Country deleted successfully.",result})}
    //         // error?res.status(500).send(error):res.send(result)     

    //     })
    // },

    addCity: function (req, res) {
        console.log("request" + JSON.stringify(req.body));
     if(!req.body.city_id){
        countrySchema.update({
            countryName: req.body.countryName,
            'states._id': req.body.states_id
        }, {
            "$push": {
                "states.$.cities": {
                    "cityName": req.body.cityName,
                    "cityCode":req.body.cityCode,
                    "createdBy":req.body.createdBy
                }
            }
        }, function (err, result) {
            console.log("result" + JSON.stringify(result));
              err?res.status(500).send(err):res.send(result);
        })
    }
    else{
        console.log("request"+JSON.stringify(req.body));
   var cityname = req.body.cityName;
   var cityCode = req.body.cityCode
        countrySchema.findOne({            
            countryName: req.body.countryName
        }, function (err, result) {
            console.log("result",result.states[0]);
            if (err) { 
              return res.send({status:500,message:"something went wrong"})
            } else {             
                result.states.id(req.body.states_id).cities.id(req.body.city_id).cityName = cityname
                 result.states.id(req.body.states_id).cities.id(req.body.city_id).cityCode = cityCode
                result.save(function (err, result2) {
                    err?res.status(500).send(err):res.send(result2);

                })
            }
        })
    }

    },


 regionaccordingCountry : function(req,res){
  console.log("req",req.params)
  regionSchema.find({country : req.params.country_id},function(err,result){
     err?res.status(500).send(err):res.send(result);
  })


 },
    
    viewCity : function(req,res){
        console.log("reqqq");

        countrySchema.aggregate([{
            "$unwind": "$states"
    }, {
            "$unwind": "$states.cities"
    // },{ "$project" : { states: 1} }], function (err, result) {
    }], function (err, result) {
            console.log("result" + JSON.stringify(result));
               err?res.status(500).send(err):res.send(result);

        })
    },

    'updateCity' : function(req,res){
   console.log("request"+JSON.stringify(req.body));
   var cityname = req.body.cityName;
   var cityCode = req.body.cityCode
        countrySchema.findOne({            
            countryName: req.body.countryName
        }, function (err, result) {
            console.log("result",result.states[0]);
            if (err) { 
              return res.send({status:500,message:"something went wrong"})
            } else {             
                result.states.id(req.body.states_id).cities.id(req.body.city_id).cityName = cityname
                 result.states.id(req.body.states_id).cities.id(req.body.city_id).cityCode = cityCode
                result.save(function (err, result2) {
                    err?res.status(500).send(err):res.send(result2);

                })
            }
        })

    },

    'addUpdateAircraftType': function(req,res){
        console.log("req",req.body);
        if(!req.body._id){
         var aircraftType_save = new aircraftType(req.body);
         aircraftType_save.save(function(error,result){
              error?res.status(500).send(error):res.send(result)
         })
        }
        else{
         aircraftType.findByIdAndUpdate(req.body._id, {$set:req.body}, function(error,result) {
            if(error) { throw error; }
            error?res.status(500).send(error):res.send(result)
                //...
            })

        
        }

    },

    all_user_list : function(req,res){
     agent.find({type:"user",is_active:'active'},function(err,result){
        err?res.status(500).send(err):res.send(result)

     })
        // var query  = {type:"user",is_active:'active'};
        // var options = {
        //           page:   req.params.page_number, 
        //           limit:   10
        // };
        // agent.paginate(query,options,function(err,result){
        //     console.log("result",result)
        //     if(err){
        //         res.send({response_code:500,response_message:"server err"})
        //     }
        //     else{
        //         var pagination ={};
        //                 pagination.total = result.total;
        //                 pagination.limit= result.limit;
        //                 pagination.page= result.page;
        //                 pagination.pages= result.pages;
        //                res.send({result:result.docs ,pagination:pagination})
        //     }
        // })

    },


    view_user_detail : function(req,res){
    console.log("req",req.body)
    agent.findOne({_id:req.body._id},function(err,result){
 err?res.status(500).send(err):res.send(result)

    })


    },

    all_booking_list : function(req,res){
    book_list.find({},function(err,result){
        err?res.status(500).send(err):res.send(result)
    })
    // var query  = {};
    // var options = {
    //           page:   req.params.page_number, 
    //           limit:   10
    // };
    // book_list.paginate(query,options,function(err,result){
    //     console.log("result",result)
    //     if(err){
    //         res.send({response_code:500,response_message:"server err"})
    //     }
    //     else{
    //         var pagination ={};
    //                 pagination.total = result.total;
    //                 pagination.limit= result.limit;
    //                 pagination.page= result.page;
    //                 pagination.pages= result.pages;
    //                res.send({result:result.docs ,pagination:pagination})
    //     }
    // })

    },

    booking_list_detail:function(req,res){
        console.log("req",req.body)
        book_list.findOne({_id:req.body._id}).populate({path:'aircraftId',select:'jet_name aircraft_type'}).exec(function(err,result){

            err?res.status(500).send(err):res.send(result)

        })
    },

   payment_list: function(req,res){
    //     var query  = {};
    // var options = {
    //           page:   req.params.page_number, 
    //           limit:   10,
    //           populate : 'booking_id'
    // };
    // transaction.paginate(query,options,function(err,result){
    //     console.log("result",result)
    //     if(err){
    //         res.send({response_code:500,response_message:"server err"})
    //     }
    //     else{
    //         var pagination ={};
    //                 pagination.total = result.total;
    //                 pagination.limit= result.limit;
    //                 pagination.page= result.page;
    //                 pagination.pages= result.pages;
    //                res.send({result:result.docs ,pagination:pagination})
    //     }
    // })
    
    transaction.find({}).populate('booking_id').exec(function(err,result){
   err?res.status(500).send(err):res.send(result)
    })

   },

   payment_detail:function(req,res){
        console.log("req",req.body)
        transaction.findOne({_id:req.body._id}).populate('booking_id').exec(function(err,result){
           err?res.status(500).send(err):res.send(result)
            })
    },
   



    'getAircraftType' : function(req,res){
        aircraftType.find({},function(err,result){
                err?res.status(500).send(err):res.send(result)
        })

    },
    detailAircrafttype:function(req,res){
        aircraftType.findOne({_id:req.body._id},function(err,result){
               err?res.status(500).send(err):res.send(result)
        })
    },

    test:function(req,res){

        countrySchema.findOne({countryName:"India"},function(err,result){
          for(var i=0;i<result.states.length;i++){
         countrySchema.update({countryName:"India","states._id":result.states[i]._id},{$set:{"states.$.cities":[]}},function(err,result2){
      console.log("result",result2)

         })

          }


        })
    },

    Number_of_counts : function(req,res){
        async.parallel({
    user: function(callback) {
        agent.count({"type":"user"},function(err,result){
             callback(null, result);
        })
       
    },
    aircraft: function(callback) {
        aircraft.count({},function(err,result){
             callback(null, result);
        })  
    },
    agent: function(callback) {
        agent.count({"type":"agent"},function(err,result1){
             callback(null, result1);
        })
       
    },
    oprator: function(callback) {
        agent.count({"type":"oprator"},function(err,result1){
             callback(null, result1);
        })  
    },
  activeUser:function(callback){
   agent.count({is_login:true},function(err,result1){
  console.log("result1-----",result1)
    callback(null,result1)
   })

  },
  totalrevenu:function(callback){
   
 var query = [
{
$unwind:"$Revenu"
},
{
    $group:{
      "_id": null,
            "balance": { "$sum": "$Revenu.total_amount" }
    }

}
 ]
 agent.aggregate(query).exec(function(err,result1){

callback(null,result1)
 })
  },
    quotation : function(callback){
        book_list.count({},function(err,result2){
                 callback(null, result2);
        })
    }
}, function(err, results) {
    console.log("results",results)
      err?res.status(500).send(err):res.send({result:results})
    // results now equals to: [one: 'abc\n', two: 'xyz\n']
});
    },



   'opretor_flight_add':function(req,res){
    console.log("request",req.body);
    _planeStandByTime=20;
var obj = req.body.routs
var _roots=[];
 var dateDef  = 0,minimumDuration=0;
          var minRunway = 0;
 var extraHalt=0;
        if(req.body.tripType=="oneWay"){
          req.body.roots[0].flightDuration=0;
          _roots.push(req.body.roots[0]);
          minimumDuration=120;
          // var objRoots={
          //   departureDate:"",
          //   departureTime:"",
          //   flightDuration:0,
          //   destination:req.body.roots[0].origin,
          //   destinationName:req.body.roots[0].originName,
          //   origin:req.body.roots[0].destination,
          //   originName:req.body.roots[0].destinationName
          // };
          // req.body.roots.push(objRoots);
          // _roots.push(objRoots);
        }
        else{
          console.log("req.body.roots[req.body.roots.length-1].departureDate-------",req.body.roots[req.body.roots.length-1].departureDate);
          console.log("req.body.roots[0].departureDate--------",req.body.roots[0].departureDate)

           dateDef = new Date(req.body.roots[req.body.roots.length-1].departureDate).getDate() - new Date(req.body.roots[0].departureDate).getDate()
          // dateDef = Math.ceil(dateDef/(1000*60*60*24))
           console.log("dateDef--->",dateDef)
          // new Date(req.body.roots[i+1].departureDate).getDate()- new Date(req.body.roots[i].departureDate).getDate();
           minimumDuration = (dateDef+1)*120
           console.log("minimumDuration--->",minimumDuration);
          for(var i=0;i<req.body.roots.length;i++){
            req.body.roots[i].flightDuration=0;
            if(i<(req.body.roots.length-1)){
              var dayDeff=new Date(req.body.roots[i+1].departureDate).getDate()- new Date(req.body.roots[i].departureDate).getDate();
              if(dayDeff)
              extraHalt+=dayDeff-1;
              req.body.roots[i].flightDuration=new Date(req.body.roots[i+1].departureDate+" "+req.body.roots[i+1].departureTime)-new Date(req.body.roots[i].departureDate+" "+req.body.roots[i].departureTime);
              req.body.roots[i].flightDuration=req.body.roots[i].flightDuration/1000/60
            }
            else{
              req.body.roots[i].flightDuration=0;
            }
            
        
          }
          _roots=req.body.roots;
          console.log("_roots--->",_roots)
        }
    
    //new Date(tt.roots[0].departureDate+" "+tt.roots[0].departureTime)
    
      if(req.body.searchType == "normalSearch"){
         waterfall([
           function(cb){
    
            async.parallel({
              roots: function(callback) {
              
                  // async.map(req.body.roots.filter(x=>x.fromAirport.length>0&& x.toAirport.length>0),airportCtrl.airportdistance.bind(airportCtrl),function(err,result){

                     async.map(_roots,airportCtrl.airportdistance.bind(airportCtrl),function(err,result){
                    
                    console.log("result",result)
                    for(var i=0 ;i<result.length;i++){
                      if(result[i].distance == 0){
                        console.log("enter in if")
                        return res.send({message:"No route found"})
                      }
                      // err?callback(err,null):callback(null,result);
                    }
                    err?callback(err,null):callback(null,result);
                  })
              },
              groundhandling: function(callback) {
                var airportsInJourney=[];
                  // console.log("req.body.roots---->",_roots);

                  for(var i=0;i<_roots.length;i++){
                    if(airportsInJourney.indexOf(_roots[i].origin)==-1)
                      airportsInJourney.push(_roots[i].origin)
                    if(airportsInJourney.indexOf(_roots[i].destination)==-1)
                      airportsInJourney.push(_roots[i].destination)
                  }
                   async.map(airportsInJourney,airportCtrl.groundhandlingCharge.bind(airportCtrl),function(err,result){
                      err?callback(err,null):callback(null,result);
                   })

                },
            baseDistance:function(callback){
                    var _bases=config.airportBases.map(x=>{
                        return {
                            _fromAirport:x.airportId,
                            _toAirport:_roots[0].origin,
                            fromAirport:mongoose.Types.ObjectId(x.airportId),
                            toAirport:mongoose.Types.ObjectId(_roots[0].origin)
                        };
                    })
                    var query={
                        $or:_bases
                    }
                    async.map(_bases,function(x,cbMap){
                      // console.log("x.fromAirport----->",x.fromAirport);
                      // console.log("x.toAirport----->",x.toAirport);
                         if (x._fromAirport == x._toAirport){
                          console.log("if-------------------->");
                          cbMap(null,{
                               fromAirport:ObjectId(x.fromAirport),
                                toAirport:ObjectId(x.toAirport),
                                distance:0,
                                groundhandling:{}
                            })
                         }
                         else{
                          console.log("else----------------->");
                          airportdistance.find({
                            fromAirport:x.fromAirport,
                            toAirport:x.toAirport
                          }).exec(function(err,result){
                           if(err){
                               cbMap(err)
                           }
                          else{
                              if(result.length == 0){
                                // console.log("fromAirport---->",x.fromAirport);
                                // console.log("toAirport---->",x.toAirport);
                                
                                //   console.log("else-----------------------")
                                  cbMap(null,{
                                     fromAirport:ObjectId(x.fromAirport),
                                      toAirport:ObjectId(x.toAirport),
                                      distance:2000000,
                                      groundhandling:{}
                                  })
                                  
                              }
                              else{
                               airport.findOne({_id:ObjectId(x.fromAirport)})
                               .exec(function(err,resultGH){
                                if(err)
                                  cbMap(err);
                                else{
                                  cbMap(null,{
                                     _id:result[0]._id,
                                     fromAirport:ObjectId(result[0].fromAirport),
                                      toAirport:ObjectId(result[0].toAirport),
                                      distance:result[0].distance,
                                      groundhandling:resultGH.groundhandling
                                  })
                                }
                               })
                                  //console.log("result[0]---",result)
                                  
                              }
                          }
                        })
                     }
                      
                        
                    },function(err,result){
                          err?callback(err,null):callback(null,result);
                    })
          
                },
                reverseBaseDistance:function(callback){
                    var _bases=config.airportBases.map(x=>{
                        return {
                            fromAirport:mongoose.Types.ObjectId(_roots[_roots.length-1].destination),
                            toAirport:mongoose.Types.ObjectId(x.airportId)
                        };
                    })
          
                    // var query={
                    //     $or:_bases
                    // }
                    async.map(_bases,function(x,cbMap){
                      airportdistance.find(x).exec(function(err,result){
                       if(err){
                           cbMap(err)
                       }
                      else{
                          if(result.length == 0){
                              cbMap(null,{
                                 fromAirport:mongoose.Types.ObjectId(x.fromAirport),
                                  toAirport:mongoose.Types.ObjectId(x.toAirport),
                                  distance:2000000
                              })
                          }
                          else{
                              cbMap(null,{
                                    _id:result[0]._id,
                                 fromAirport:ObjectId(result[0].fromAirport),
                                  toAirport:ObjectId(result[0].toAirport),
                                  distance:result[0].distance
                              })
                          }
                      }
                    })
                        
                    },function(err,result){
                        //console.log("repo result------------",result)
                          err?callback(err,null):callback(null,result);
                    })
          
                }
              }, 
                           function(err, results) {
                err?(err,null,null):cb(null,results.roots,results.groundhandling,results.baseDistance,results.reverseBaseDistance);

              });
           
            },
           function(roots,airports,baseDistance,reverseBaseDistance,cb){
               for(var i=0;i<baseDistance.length;i++){ 
                   //console.log("check",i,baseDistance[i])
                   baseDistance[i]['indx'] = i;
        
               }
                  for(var i=0;i<reverseBaseDistance.length;i++){ 
                   //console.log("check",i,baseDistance[i])
                   reverseBaseDistance[i]['indx'] = i;
        
               }
               console.log("baseDistance---->",baseDistance)
               console.log("reverseBaseDistance---->",reverseBaseDistance)
               console.log("airports---->",airports)
               console.log("roots---->",roots)
              
          //     baseDistance=baseDistance.map((x,indx)=>{ x['indx'] = i; return x});
//               reverseBaseDistance=reverseBaseDistance.map((x,indx)=>{{x.indx=indx; return x}});


              var minRunway = Math.min.apply(Math,airports.map(function(o){return o.runway;}))

            var query=[{
                    $match:{
            minimum_distance:{$lte:minRunway},
             jet_name:req.body.aircraft
        
    }
//    $match:{
//        $and:[{
//            $or:[{minimum_distance:{$gt:5000}},{minimum_distance:{$lte:3000}}]
//        },{
//            aircraft_max_range:{$gte:1500}
//        }]
//    }
},{
    $addFields:{
        "hasBase":{
            $gt:[{
                $size:{
                $filter:{
                    input:"$bases",
                    as:"x",
                    cond:{
                        $and:[{
                           $eq:["$$x.base_city",mongoose.Types.ObjectId(_roots[0].origin)]
                          //    $eq:["$$x.base_city",0]
                  
                        },{
                            $gt:["$$x.base_price",0]
                        }]
                    }
                }
            }
            },0]
            
        },
        activeBases:{
           $filter:{
                input:"$bases",
                as:"x",
                cond:{
                   $gt:["$$x.base_price",0]
                }
            }
        },
        baseDistance:baseDistance,
        reverseBaseDistance:reverseBaseDistance,
        airports:airports
    }
},{
$addFields:{
        positioning:{
            $reduce:{
                input:{
                    $map:{
                        input:"$activeBases",
                        as:"x",
                        in:{
                            price:"$$x.base_price",
                            obj:{
                              $arrayElemAt:[{$filter:{
                                input:"$baseDistance",
                                as:"bd",
                                cond:{
                                  $eq:["$$x.base_city","$$bd.fromAirport"]
                                }
                              }},0]
                            //     $arrayElemAt:["$baseDistance",{
                            //     $reduce:{
                            //         input:"$baseDistance",
                            //         initialValue:-1,
                            //         in:{
                            //             $cond:{
                            //                 if:{$eq:["$$x.base_city","$$this.fromAirport"]},
                            //                 then:"$$this.indx",
                            //                 else:"$$value"
                            //             }
                            //         }
                            //     }
                            // }]
                            }
                            
                        }
                    }
                },
                initialValue:{fromAirport:"",toAirport:"",distance:10000000000,price:0,groundhandling:{}},
                in:{
                    $cond:{
                        if:{$eq:["$$value.distance",10000000000]},
                        then:{fromAirport:"$$this.obj.fromAirport",toAirport:"$$this.obj.toAirport",distance:"$$this.obj.distance",price:"$$this.price",groundhandling:"$$this.obj.groundhandling"},
                        else:{
                            $cond:{
                                if:{
                                    $lte:["$$value.distance","$$this.obj.distance"]
                                },
                                then:{fromAirport:"$$value.fromAirport",toAirport:"$$value.toAirport",distance:"$$value.distance",price:"$$value.price",groundhandling:"$$value.groundhandling"},
                                else:{fromAirport:"$$this.obj.fromAirport",toAirport:"$$this.obj.toAirport",distance:"$$this.obj.distance",price:"$$this.price",groundhandling:"$$this.obj.groundhandling"}
                            }
                        }
                    }
                }
            }
        }
    }
},{
 $addFields:{
         repositioning:{
            $arrayElemAt:[
                {
                    $filter:{
                        input:"$reverseBaseDistance",
                        as:"x",
                        cond:{
                          $eq:["$$x.toAirport","$positioning.fromAirport"]
                            // $and:[{$eq:["$$x.toAirport","$positioning.fromAirport"]},{$eq:["$$x.fromAirport","$positioning.toAirport"]}]
                        }
                    }
                },0]
        }
    }
},{
  $addFields:{
      positioning:{
            "fromAirport" : "$positioning.fromAirport",
            "toAirport" : "$positioning.toAirport",
            "distance" : "$positioning.distance",
            "price" : "$positioning.price",
            "groundhandling":"$positioning.groundhandling",
            "isAvail":{
           $cond:{
                    if: { $eq: [ "$positioning.distance", 2000000 ] },
                            then:false,
                        else:true
                    }
                 },
           duration:{
                $add:[_planeStandByTime,{$multiply:[{$divide:["$positioning.distance","$$ROOT.speed"]},60]}]
            },
           cost:{
                $divide:[{
                    $multiply:["$positioning.price",{$add:[_planeStandByTime,{$multiply:[{$divide:["$positioning.distance","$$ROOT.speed"]},60]}]}]
                },60]
            }
        },
     repositioning:{
            fromAirport:"$repositioning.fromAirport",
            toAirport:"$repositioning.toAirport",
            distance:"$repositioning.distance",
            price:"$positioning.price",
            "isAvail":{
           $cond:{
                    if: { $eq: [ "$repositioning.distance", 2000000 ] },
                            then:false,
                        else:true
                    }
                 },
          duration:{
                $add:[_planeStandByTime,{$multiply:[{$divide:["$repositioning.distance","$$ROOT.speed"]},60]}]
            },
           cost:{
             $divide:[{
                    $multiply:["$positioning.price",{$add:[_planeStandByTime,{$multiply:[{$divide:["$repositioning.distance","$$ROOT.speed"]},60]}]}]
                },60]
            }
        },
        testIndex:{
          $indexOfArray:["$airports._id","$positioning.fromAirport"]
        }
    }
},{
 $project: {
        documents:"$$ROOT",
        //airports:airports,
        airports:{
          $cond:{
            if:{
              $and:["$positioning.isAvail",{
                $eq:[{ $indexOfArray:["$airports._id","$positioning.fromAirport"]},-1]
              }]
            },
              then:{
               $concatArrays:["$airports",[{
                  _id:"$positioning.fromAirport",
                  groundhandling:"$positioning.groundhandling"
                }]]
              },
              else:"$airports"
          }
        },
        // airports:{
        //   $concatArrays:[airports,[{
        //     _id:"$positioning.fromAirport",
        //     groundhandling:"$positioning.groundhandling"
        //   }]]
        // },
      price:"$positioning.price",
      duration: {
         $map:{
              input: roots,
              as: "x",
              in: { 
                "data":"$$x",
                "duration":{
                  $add:[_planeStandByTime,{$multiply:[{$divide:["$$x.distance","$$ROOT.speed"]},60]}]
                },
                "crewCharges":{
                    $cond:{
                        if:{$gt:[{$subtract:["$$x.flightDuration",{$add:[_planeStandByTime,{$multiply:[{$divide:["$$x.distance","$$ROOT.speed"]},60]}]}]},4*60]},
                            then:{$multiply:["$$ROOT.crew_charges",{$sum:["$$ROOT.pilot","$$ROOT.crew_members"]}]},
                        else:0
                    }
                }
              }
            }
          }
        }
},{
        $project:{
                documents:"$documents",
                price:"$price",
                duration:"$duration",
                calculatedDuration:{
                  $cond:{
                    if:{$gt:[minimumDuration,{
                      $add:[{
                      $reduce:{
                        input:"$duration",
                        initialValue:0,
                        in:{
                          $add:["$$value","$$this.duration"]
                        }
                      }
                    },{
                       $cond:{
                            if:"$documents.positioning.isAvail",
                           then:"$documents.positioning.duration",
                           else:0
                       }
                   },{
                       $cond:{
                            if:"$documents.repositioning.isAvail",
                           then:"$documents.repositioning.duration",
                           else:0
                       }
                   }]
                    }
                      ]},
                    then:minimumDuration,
                    else:{
                      $add:[{
                      $reduce:{
                        input:"$duration",
                        initialValue:0,
                        in:{
                          $add:["$$value","$$this.duration"]
                        }
                      }
                    },{
                       $cond:{
                            if:"$documents.positioning.isAvail",
                           then:"$documents.positioning.duration",
                           else:0
                       }
                   },{
                       $cond:{
                            if:"$documents.repositioning.isAvail",
                           then:"$documents.repositioning.duration",
                           else:0
                       }
                   }]
                    }
                  }
                }, totalCrewCharge:{
                  $sum:[{
                    $reduce:{
                      input:"$duration",
                      initialValue:0,
                      in:{
                        $add:["$$value","$$this.crewCharges"]
                      }
                    }
                  },{$multiply:[15000,extraHalt,{$sum:["$documents.pilot","$documents.crew_members"]}]}]
                },
                groundHandlingCharges:{
                  $reduce:{
                    input:"$airports",
                    initialValue:0,
                    in:{$add:["$$value", {
                      $cond:{
                        

                          if:{
                            $and:[{$gte:["$documents.weight",0]},{$lte:["$documents.weight",5000]}]
                          },
                            then:'$$this.groundhandling.W05',
                          else:{
                            $cond:{
                              if:{
                                $and:[{$gt:["$documents.weight",5000]},{$lte:["$documents.weight",10000]}]
                              },
                                then:'$$this.groundhandling.W510',
                              else:{
                                $cond:{

                                  if:{
                                    $and:[{$gt:["$documents.weight",10000]},{$lte:["$documents.weight",15000]}]
                                  },
                                    then:'$$this.groundhandling.W1015',
                                    else:{
                                      $cond:{
                                        if:{
                                          $and:[{$gt:["$documents.weight",15000]},{$lte:["$documents.weight",20000]}]
                                        },
                                        then:'$$this.groundhandling.W1520',
                                        else:{
                                          $cond:{
                                            if:{
                                              $and:[{$gt:["$documents.weight",20000]},{$lte:["$documents.weight",30000]}]
                                            },
                                              then:'$$this.groundhandling.W2030',
                                            else:{
                                              $cond:{
                                                if:{
                                                  $and:[{$gt:["$documents.weight",30000]},{$lte:["$documents.weight",40000]}]
                                                },
                                                  then:'$$this.groundhandling.W3040',
                                                else:{
                                                  $cond:{
                                                    if:{
                                                      $and:[{$gt:["$documents.weight",40000]},{$lte:["$documents.weight",50000]}]
                                                    },
                                                      then:'$$this.groundhandling.W4050',
                                                    else:{
                                                      $cond:{
                                                        if:{
                                                          $and:[{$gt:["$documents.weight",50000]},{$lte:["$documents.weight",70000]}]
                                                        },
                                                          then:'$$this.groundhandling.W5070',
                                                        else:'$$this.groundhandling.W70M'
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                }
                              }
                            }
                          }
                      }
                    }]}
                  }
                }
              }
            },{
            $project:{
            documents:"$documents",
            calculatedDuration:"$calculatedDuration",
            duration:"$duration",
            price:"$price",
            totalCrewCharge:"$totalCrewCharge",
            groundHandlingCharges:"$groundHandlingCharges",
            netFlyingCost:{
              $add:[{$multiply:[{$divide:["$price",60]},"$calculatedDuration"]}]
            },
            netcost:{
              $let:{
               vars:{
               // totalBaseprice:{$multiply:["$price.price","$duration"]}
               totalBaseprice:{$multiply:[{$divide:["$price",60]},"$calculatedDuration"]}
               },
             //  in:{$add:["$$totalBaseprice","$groundHandlingCharges","$totalCrewCharge"]}
       //           in:{$add:["$$totalBaseprice","$groundHandlingCharges","$totalCrewCharge","$documents.positioning.cost","$documents.repositioning.cost"]}
                   in:{$add:["$$totalBaseprice","$groundHandlingCharges","$totalCrewCharge"]}
              }
              }
            }
            },{
            $project:{
              netFlyingCost:"$netFlyingCost",
              calculatedDuration:"$calculatedDuration",
              price:"$price",
            gst:{$multiply:["$netcost",0.18]},
             total:{$add:["$netcost",{$multiply:["$netcost",0.18]}]},
             documents:"$documents",
             airports:"$airports",
             groundHandlingCharges:"$groundHandlingCharges",
             netcost:"$netcost",
             duration:"$duration",
             totalCrewCharge:"$totalCrewCharge"
            }
            }]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
               
           aircraft.aggregate(query,function(err,result1){
              err?cb(err,null):cb(null,result1);
            })
         }

          ],function(err,success){

             err?res.status(500).send(err):res.send(success)

          })
        } 

     
 }





}

module.exports = controller;


