// admin routes//

var express = require('express'); 
var path=require('path');   
var router = express.Router();  
var authorisation = require('../authorisation');
var controller = require('./controller');

router.post('/country',controller.addUpdateCounty)
router.get('/country/:page_number',controller.getAllCountries)

router.post('/region',controller.addUpdateRegion)
router.get('/region',controller.getAllRegions)

router.post('/states',controller.addUpdateStates)
router.get('/states',controller.getAllStates)

router.post('/city',controller.addCity);
router.get('/city',controller.viewCity);
router.post('/updateCity',controller.updateCity);

router.post('/type',controller.addUpdateAircraftType);
router.get('/type',controller.getAircraftType);
router.post('/type',controller.detailAircrafttype);

router.get('/all_user_list/:page_number',controller.all_user_list);
router.post('/view_user_detail',controller.view_user_detail);
router.get('/all_booking_list/:page_number',controller.all_booking_list);
router.post('/booking_list_detail',controller.booking_list_detail);
router.post('/opretor_flight_add',controller.opretor_flight_add);
router.get('/Number_of_counts',controller.Number_of_counts)
// router.get('/payment_list/:page_number',controller.payment_list);
router.get('/payment_list/:page_number',controller.payment_list);
router.post('/payment_detail',controller.payment_detail);
router.get('/test',controller.test);

router.get('/operator_aircraft_list',controller.operator_aircraft_list);

router.get('/employee_list',controller.employee_list);
router.post('/country_Delete',controller.country_Delete);
router.get('/regionaccordingCountry/:country_id',controller.regionaccordingCountry)
// router.post('/state_delete',controller.state_delete);






// router.get('/getAddresses',authorisation.isAuthenticate,controller.getAddresses)
// router.post('/distributeGas',authorisation.isAuthenticate,controller.distributeGas)
// router.post('/moveToMaster',authorisation.isAuthenticate,controller.moveToMaster)


// router.get('/',authorisation.);  

module.exports = router;  