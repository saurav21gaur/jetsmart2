// Gateway Controller //

var config=require.main.require('./global/config');
var jwt = require('jsonwebtoken');
var mongoose = require('mongoose');
var opertaor=require.main.require('./db/opratorSchema.js');
var aircraft = require.main.require('./db/aircraftSchema.js');
 var manifestTemp = require('./manifestTemp.js');
var country=require.main.require('./db/countrySchema.js');
var opertaorJet = require.main.require('./db/opretorAircraft.js')
var aircraftPrice = require.main.require('./db/priceSchema.js')
var saveflight = require.main.require('./db/addFlightSchema.js')
var booking = require.main.require('./db/bookingCountSchema.js')
var pessangeBook = require.main.require('./db/passengerBookingSchema.js')
var sendMessage = require('./sendMessage.js')
var emptyjet = require.main.require('./db/Emptyjet.js')
var mailClient = require('./function.js')
var functions = require('./email.js')
var waterfall = require('async-waterfall');
var async=require("async");
var xmlCsvParser=require.main.require('./global/xmlCsvParser');
var pdf = require('pdfcrowd');
  var flash = require('express-flash');
var urllib = require('urllib');
var http = require('http')
var httpclient = require('urllib').create();


var controller={

//
           bulkUploadOprator: function(req,res,next){
               console.log("enter")
        var missingCities=[]
        async.waterfall([function(cb){
        //	xmlCsvParser.parse("temp/1517546784295.xlsx")
           xmlCsvParser.parse("temp/1510141436346.xlsx")
               //   xmlCsvParser.parse(req.body.filesPath)   
         
          .then(function(objS){
              cb(null,objS["Operator "])
          },function(objE){
            cb(objE,null)
          })
      },
      function(xclData,cb){
          var duplicate = []
         //  console.log("xclData",xclData)
           async.map(xclData,function(x,cbMap){
        var query=[{
          $unwind:"$states"
        },{
          $unwind:"$states.cities"
        },{
                    $match:{
                        "states.cities.cityName":x["CITY"].toUpperCase()
                    }
                }];
        country.aggregate(query)
        .exec(function(err,result){
          if(err)
            cb(err,null)
 
              if(result.length>0){
    function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}
    var rString = randomString(8, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');


                x.cityId=result[0].states.cities._id;
                x.stateId=result[0].states._id;
                var query={
                    user_name:x["OPERATOR'S NAME"]
                };
                var value={
                    city:x.cityId,
                    state:x.stateId,
                    user_name:x["OPERATOR'S NAME"],
                    code:x["Code"],
                    address1:x["COMMUNICATION ADDRESS"],
                   fax:x["FAX"],
                   // model:x["Model"],
                  //  aircraftId:[],
                    email:x["EMAIL"],
                      phone_number:x["TELEPHONE"],
                         valid_upto:x["Valid Upto"],
                         AOP_NO:x["AOP NO"],
                          keyPerson:[{
                          name:x["Keyperson"],
                          designation:x["Key Person Designation"]
                        }],
                     flight_number:x["Aircraft Regn"],
                    password:rString,
                   type:"oprator",
                   is_active:"active"
                };
                opertaor.findOneAndUpdate(query,value,{upsert:true,new:true})
                .exec(function(err,savedAirport){
                  opertaor.findOneAndUpdate({user_name:x["OPERATOR'S NAME"]},{$addToSet:{ model:x["Model"]}},function(err,result2){
                     err?cbMap(err,null):cbMap(null,result2);
                  })
             
                })
              }
              else{
                   cbMap(null,{})
              }
            })
            //  }
              
            },function(err,result){
                 err?cb(err):cb(null,result)

            })
    
      },function(data,cb){
        console.log("data",data)
           async.map(data,function(x,cbMap){
            aircraft.findOne({"jet_name":{$in:x.model}},function(err,result2){
                   console.log("result2",result2)
                   if(result2!=null){
  
                           var query1 = {
                               jet_name:result2.jet_name,
                                 opratorId:x._id
                           };
                           
                         var value1={
                               jet_name:result2.jet_name,
                               passangers:result2.passangers,
                               opratorId:x._id,
                               AOP_NO:x.AOP_NO,
                               valid_upto:x.valid_upto,
                               aircraft_type:result2.aircraft_type,
                               image_gallary:result2.image_gallary,
                               bases:result2.bases,
                               baggage:result2.baggage,
                               weight:result2.weight,
                               speed:result2.speed,
                               minimum_distance:result2.minimum_distance,
                               pilot:result2.pilot,
                               flight_number:x.flight_number
                               // price:result3.price

                               
                           }
                           // var saveopertaorJet = new opertaorJet(value1)
                           // saveopertaorJet.save(function(err,result33){
                           //  cbMap(null,result33);
                           // })
                         opertaorJet.update(query1,value1,{upsert:true,new:true}).exec(function(err,result33){
                           //    console.log("result33",result33)
                               cbMap(null,result33);
                         })
                   }
                   else{
                       cbMap(null,{})
                   }
     
               })
               
           },function(err,result){
           
                err?cb(err,null):cb(null,result);
              
          }
                    )
      }],function(err,result){
        if(err)
          console.log(err)
        console.log("missingCities---->",missingCities)
          err?res.status(500).send(err):res.send(missingCities);
      })

        
    },

    testopratoraircraft : function(req,res){
    console.log("enter")
        async.waterfall([
           function(cb){
            xmlCsvParser.parse("temp/1510141436346.xlsx")       
          .then(function(objS){
              cb(null,objS["Operator "])
          },function(objE){
            cb(objE,null)
          })
           },function(xclData,cb){
                   async.map(xclData,function(x,cbMap){
                    opertaor.find({user_name:x["OPERATOR'S NAME"],"type":"oprator"},function(err,result){ 
            //        console.log("result",result)            
                       err?cbMap(err,null):cbMap(null,result);
                    })

                   },function(err,result){
                 err?cb(err):cb(null,result)
            })
           },function(data,cb){  
             var query = [{
                $unwind:"$model"
             }]
   opertaor.aggregate(query).exec(function(err,result2){
     //console.log("result2",result2)
     aircraft.findOne({$and:[{jet_name:result2.model},{jet_name:{$ne:null}}]},function(err,result3){
      console.log("result3",result3)
     })
   })




           }

          ],function(err,result){
err?res.status(500).send(err):res.send(result);

          })




    },



    generateManifest:function(req,res){
      var htmlContent=manifestTemp.style+req.body.htmlContent+manifestTemp.content_end;
      console.log("username =>",config.pdfcredential.username,"APi",config.pdfcredential.api_key)
      var client = new pdf.Pdfcrowd(config.pdfcredential.username, config.pdfcredential.api_key);
      //client.convertHtml(htmlContent, pdf.sendHttpResponse(res));
      var fileName="PDF-"+new Date().getTime()+'.pdf';
      client.convertHtml(htmlContent, pdf.saveToFile(path.join(process.env.PWD,'temp',fileName)));
      res.send({filePath:'/temp/'+fileName})

    },



      bulkUploadAgent : function(req,res,next){
      console.log("enter")
              var missingCities=[]
        async.waterfall([function(cb){
           xmlCsvParser.parse("temp/1510980121417.xlsx")
               //   xmlCsvParser.parse(req.body.filesPath)
         
          .then(function(objS){
              cb(null,objS["IATA & CORP"])
          },function(objE){
            cb(objE,null)
          })
      },
      function(xclData,cb){
          var duplicate = []
           async.map(xclData,function(x,cbMap){
        var query=[{
          $unwind:"$states"
        },{
          $unwind:"$states.cities"
        }];
        country.aggregate(query)
        .exec(function(err,result){
        if(err)
            cb(err,null)
              var getcityState=result.filter(y=>y.states.cities.cityName.toUpperCase()==x["CITY"]);
              var stateId=0,cityId=0;
                 x.cityId=result[0].states.cities._id;
                x.stateId=result[0].states._id;
                var query={
                    user_name:x["CO-NAME"]
                };
                var value={
                    city:x.cityId,
                    state:x.stateId,
                    user_name:x["CO-NAME"],
                    code:x["Code"],
                    address1:x["ADD1"],
                    address2:x["ADD2"],
                    fax:x["FAX"],
                    phone_number:x["PHONE"],
                    pincode:x["PIN"],
                    email:x["EMAIL"],
                    website:x["Website"],
                    keyPerson:[{
                    name:x["Key Person"],
                    designation:x["Key Person Designation"]
                        }],
                   type:"agent",
                   is_active:"active"
                };
                opertaor.findOneAndUpdate(query,value,{upsert:true,new:true})
                .exec(function(err,savedAirport){
                    err?cbMap(err,null):cbMap(null,savedAirport);
                })
            })              
            },function(err,result){
                 err?cb(err):cb(null,result)

            })
    
      }],function(err,result){
        if(err)
          console.log(err)
        console.log("missingCities---->",missingCities)
          err?res.status(500).send(err):res.send(missingCities);
      })
        
        
    },


   revenuBulkUpload : function(req,res){
    console.log("req.body",req.body)
             var missingCities=[]
        async.waterfall([function(cb){
         //xmlCsvParser.parse("temp/1522129098831.xlsx")
              xmlCsvParser.parse(req.body.filesPath)
          .then(function(objS){
              cb(null,objS["Sheet1"])
          },function(objE){
            cb(objE,null)
          })
      },
      function(xclData,cb){
           async.map(xclData,function(x,cbMap){
            console.log("xxxxxxxxxx",x)
                var query={
                    code:x["IATA #"]
                };
                var value={
                    Revenu:[{
                    month:x["Month"],
                    total_amount:x["Total"]
                        }]
                };
                opertaor.findOneAndUpdate(query,value,{multi:true,new:true})
                .exec(function(err,savedAirport){
                    err?cbMap(err,null):cbMap(null,savedAirport);
                })
                        
            },function(err,result){
                 err?cb(err):cb(null,result)

            })
    
      }],function(err,result){
        if(err)
          console.log(err)
        console.log("missingCities---->",missingCities)
          err?res.status(500).send(err):res.send(missingCities);
      })

   },











    login:function(req,res){
    	console.log(req.body);
        // if(req.body.username!=config.adminCredential.username){
        //     // res.status(403).send({msg:"username is not matched"});
        //     user.findOne({user_name:req.body.username,password:req.body.password},function(err,result){
        //       console.log("resultt",result)
        //          var _token=jwt.sign({
        //       exp: Math.floor(Date.now() / 1000) + (60 * 60),
        //       data: req.body
        //     }, config.authSecretKey);
        //     res.send({result:result,token:_token})
        //       return ;
        //     })
            
        // }
        // else if(req.body.password!=config.adminCredential.password){
        //     res.status(403).send({msg:"Password is not matched"});
        //     return ;
        // }
        // else{
        //     var _token=jwt.sign({
        //       exp: Math.floor(Date.now() / 1000) + (60 * 60),
        //       data: req.body
        //     }, config.authSecretKey);
        //     res.send({username:req.body.username,token:_token})
        // }
      opertaor.findOne({user_name:req.body.username,is_active:'active'},function(err,result){
        console.log("result",result)
       if(!result){
        res.status(403).send({msg:"Username does not exist",code:400});
       }
       else{
        opertaor.findOne({user_name:result.user_name,password:req.body.password,is_active:'active'},function(err,result1){
        if(!result1){
          res.status(403).send({msg:"Incorrect Password",code:400});
        }
        else{
           var _token=jwt.sign({
              exp: Math.floor(Date.now() / 1000) + (60 * 60),
              data: req.body
            }, config.authSecretKey);
            res.send({result:result1,token:_token,code:200})

        }         



        })
       }




      })
    

    },

    change_password_operator:function(req,res){
      console.log(req.body);
      opertaor.findOne({user_name:req.body.username,type:req.body.type},function(err,result){
         console.log("result",result)
         if(!result){
          res.status(200).send({msg:"Username does not exist",code:400});
         }
         else{
              opertaor.findOne({type:req.body.type,user_name:req.body.username,password:req.body.password},function(err,result1){
                if(!result1){
                  res.status(200).send({msg:"Incorrect Old Password",code:400});
                }
                else{
                      opertaor.update({type:req.body.type,user_name:req.body.username},{$set:{password : req.body.newPassword}},function(err,result){
                        if(result){
                          res.status(200).send({msg:"Password Changed Successfully",code:200});
                        }
                        else{
                          res.status(403).send({msg:"Something wents wrong",code:400});
                        }
                      })
                }         
              })
            }
      })
    },

    'oprator_signup': function (req, res) {
        console.log("req" + JSON.stringify(req.body));
        opertaor.findOne({$or:[{email: req.body.email},{user_name:req.body.user_name}]}, function (err, result) {
            console.log("signupresult",result);
            if (err) {
                res.status(500).send({msg:"Server error"});
            } else if (result) {
                res.status(404).send({msg:"Email or username is already exist"});
            } else {
                var oprator_save = new opertaor(req.body)
                oprator_save.save(function (err, result) {
                    if (err) {
                        res.status(500).send({msg:"Server error"});
                    } else {
                        res.status(200).send({
                            response_code: 200,
                            response_message: "Register successfully",
                            result: result

                        })
                        
                    }
                })

             }

         })

     },

     oprator_list : function(req,res){
       opertaor.find({type:"oprator",is_active:'active'},function(err,result){
        if(err){
          res.send({response_code:500,response_message:"server err"})
        }
        else{
           res.status(200).send({
              response_code: 200,
              response_message: "Operator List",
              result: result

          })
        }
       })
      // var query  = {type:"oprator",is_active:'active'};
      // var options = {
      //           page:   req.params.page_number, 
      //           limit:    10
      // };
      // opertaor.paginate(query,options,function(err,result){
      //     console.log("result",result)
      //     if(err){
      //         res.send({response_code:500,response_message:"server err"})
      //     }
      //     else{
      //         var pagination ={};
      //                 pagination.total = result.total;
      //                 pagination.limit= result.limit;
      //                 pagination.page= result.page;
      //                 pagination.pages= result.pages;
      //                res.send({result:result.docs ,pagination:pagination})
      //     }
      // })   
        
    },

    add_oprator_aircraft: function(req,res){
      console.log("req.body",req.body)
      delete req.body._id;
      var data = req.body.aircrafts
  for(var i=0;i<data.length;i++){
    if(data[i].newKey){
     delete data[i]._id;
     delete data[i].newKey;
      var saveAircraft = new opertaorJet(data[i])
         saveAircraft.save(function(err,result){
        console.log("err",err)
          if(err){
            res.status(500).send({msg:"Server error"})
          }else{
            res.status(200).send({
                response_code: 200,
                response_message: "Aircraft added successfully",
                result: result
            })
          }
      })
       }

  }


      // var saveAircraft = new opertaorJet(req.body)
      // saveAircraft.save(function(err,result){
      // 	console.log("err",err)
      //     if(err){
      //       res.status(500).send({msg:"Server error"})
      //     }else{
      //       res.status(200).send({
      //           response_code: 200,
      //           response_message: "Aircraft added successfully",
      //           result: result
      //       })
      //     }
      // })

    },

   oprator_detail: function(req,res){
    console.log("request",req.body)
   opertaor.findOne({_id:req.body._id},function(err,result){
     res.status(200).send({
                              response_code: 200,
                              response_message: "detail",
                              result: result

                          })
     })
    },

        view_oprator: function(req,res){
           console.log("req",req.body)
           opertaor.findOne({_id:req.body._id},function(err,result){
           if(err){
                         res.status(500).send({msg:"Server error"});
                    }
                    else{

                         res.status(200).send({
                                    response_code: 200,
                                    response_message: "Oprator Details", 
                                    result: result
                                })
                    }

                })
    }
,

 oprator_aircraft_view_edit : function(req,res){
  console.log("req----",req.body)
opertaorJet.find({opratorId:req.body._id},function(err,result){
  if(err){
res.status(500).send({msg:"Server error"});
  }
else if(result.length == 0 ){
    res.status(200).send({
                                    response_code: 400,
                                    response_message: "No aircrafts"
                                  
                                })

}

  else{
  res.status(200).send({
                                    response_code: 200,
                                    response_message: "aircrafts", 
                                    result: result
                                })

  }


})

 },

oprator_aircraft_edit : function(req,res){
   opertaorJet.findOneAndUpdate({_id:req.params._id},{$set:req.body},function(err,result){

  if(err){
res.status(500).send({msg:"Server error"});
  }
  else{
      res.status(200).send({
                                    response_code: 200,
                                    response_message: "update" 
                                    
                                })

  }

   })


}
,

oprator_aircraft_delete : function(req,res){
 opertaorJet.findByIdAndRemove({_id:req.params._id},function(err,result){
  if(err){

  }
  else{

          res.status(200).send({
                                    response_code: 200,
                                    response_message: "delete" 
                                                                  })

  }


 })


},




    edit_oprator: function(req,res){
   console.log("req",req.body)
   opertaor.findByIdAndUpdate({_id:req.body._id},{$set:req.body},{new:true},function(err,result){
   if(err){
                 res.status(500).send({msg:"Server error"});
            }
            else{
                 res.status(200).send({
                            response_code: 200,
                            response_message: "Oprator updated successfully",
                            result: result
                        })
            }

        })

    }
,

    opertaor_aircrft:function(req,res){
     opertaorJet.find({opratorId:mongoose.Types.ObjectId(req.params._id)},function(err,result){
    if(err){
                 res.status(500).send({msg:"Server error"});
            }
            else{
                 res.status(200).send({
                            response_code: 200,
                            response_message: "All aircraft",
                            result: result

                        })
            }

        })
    },
     opertaor_aircrft_detail:function(req,res){
     opertaorJet.findOne({_id:req.body._id},function(err,result){
      if(err){
                   res.status(500).send({msg:"Server error"});
              }
              else{
                   res.status(200).send({
                              response_code: 200,
                              response_message: "Operator Aircraft Detail",
                              result: result

                          })
              }

          })
    },

     
    'delete_user':function(req,res){
      console.log("delete",req.body);
        user.findByIdAndUpdate({_id:req.body._id},{$set:{is_active:"Block"}},{new:true},function(err,result){
           if(err){
                 res.status(500).send({msg:"Server error"});
            }
            else{
                 res.status(200).send({
                            response_code: 200,
                            response_message: "User deleted successfully",
                            result: result

                        })
            }

        })


    },

    'edit_user':function(req,res){
        console.log("editUserDetail",req.body);
        user.findByIdAndUpdate({_id:req.body._id,is_active:"active"},{$set:req.body},{new:true},function(err,result){
           if(err){
                 res.status(500).send({msg:"Server error"});
            }
            else{
                res.status(200).send({
                            response_code: 200,
                            response_message: "UserInfo Update successfully",
                            result: result

                        })
            }

        })
    },


    opratorsaveflight : function(req,res){
    console.log("req =>",req.body)

   waterfall([
               (callback)=>{
                 get_count({booking_count:1},callback);
               },
               (booking_count,callback)=>{
                 console.log('order_count-->'+booking_count);
                    req.body.bookingNumber = booking_count.booking_count;
                    var flight_save = new saveflight(req.body);
                   flight_save.save(function (err, result) {
                      if(err){
                       callback(err,null)
                      }
                      else{
                      callback(null,result);
                      }
                    });
                
               }
               ],function(err,success){
                console.log("success",success)
                  err?res.status(500).send(err):res.send(success)
               
             })

    },

  oprator_flight_list : function(req,res){
    console.log("req",req.body)
    saveflight.find({created_by:req.params._id,status:'active'}).populate('aircratft_id').exec(function(err,result){
     err?res.status(500).send(err):res.send(result)               
    })
  },
  all_flight_list : function(req,res){
    console.log("req",req.body)
    saveflight.find({status:'active'}).populate('aircratft_id created_by').exec(function(err,result){
     err?res.status(500).send(err):res.send(result)               
    })
  },
   flight_delete : function(req,res){
    console.log("req",req.body)
    saveflight.updateOne({_id:req.body._id},{$set:{status:'Block'}},function(err,result){
     err?res.status(500).send(err):res.send(result)               
    })
  },
  oprator_flight_detail : function(req,res){
    console.log("req",req.body)
    saveflight.findOne({_id:req.body._id},function(err,result){
     err?res.status(500).send(err):res.send(result)               
    })
  },
// noreply@btclarge.com

'emailSend' : function(req,res){
     console.log("req",req.body)
    mailClient.send('noreply@gojetsmart.com', req.body.email_id, 'Genrate Quotation', functions.getTest(req.body.email_id,req.body.path,req.body._id)).then(function(result) {
    if(!result.status) { return res.json({status : false, response : 'Mail not sent!'}) }
                else
                 res.json({ status: true, response : 'Account created, please check your mail inbox.'})
            })
  
                
    },


add_empty_jet : function(req,res){
  console.log("req",req.body)
 waterfall([
   function(cb){
    var saveEmpty = new emptyjet(req.body)
    saveEmpty.save(function(err,result){
        cb(null,result)
    })

   },
   function(result,cb){
  console.log("result=--------",result)
   opertaor.find({type:"user",otp_verify:true},function(err,result2){
    console.log("result2------",result2)
    for(var i=0 ;i< result2.length ;i++){
mailClient.send('noreply@gojetsmart.com',result2[i].email, 'Jet Smart Empty Leg', functions.getEmptyjet(result2[i].email,req.body.departureDate,req.body.originName,req.body.destinationName)).then(function(result) {
  console.log("sendd")
})
  }
   cb(null,result)

   })
   }

  ],function(err,success){
    err?res.status(500).send(err):res.send(success)  

  })


  
//  var saveEmpty = new emptyjet(req.body)
//  saveEmpty.save(function(err,result){
//   //console.log("result",result)
//    if(err){
//     return err
//    }
//    else{
//     opertaor.find({type:"user",otp_verify:true},function(err,data){
//      //  var msg = "Oprator add new Flight. Please check on website Hurry!"
//      //  for(var i=0;i<data.length;i++){
//      // sendMessage.msg(data[i].phone_number,msg)
//      //   console.log("send!!!!")
//      // }
// for(var i=0;i<data.length;i++){
//   mailClient.send('noreply@btclarge.com',data[i].email_id, 'Genrate Invoice', functions.getEmptyjet(data[i].email_id,req.body.departureDate)).then(function(result) {
//          console.log("result2333322"+JSON.stringify(result));
//                   if(!result.status) { return res.json({status : false,responseCode:500, response : 'Mail not sent!'}) }
//                   else
//                    res.json({ responseCode:200,status: true, responseMessage : 'Please check your mail inbox and verify email.'})


// })
// }

//     err?res.status(500).send(err):res.send(result)   
//     })

//    }


//  })

},

  operator_empty_jet : function(req,res){
    console.log("req",req.body)
    emptyjet.find({created_by:req.params.created_by,status:'active'},function(err,result){
     err?res.status(500).send(err):res.send(result)               
    })
  },
  empty_jet_list : function(req,res){
    console.log("req",req.body)
    emptyjet.find({status:'active'}).populate('created_by aircratft_id').exec(function(err,result){
     err?res.status(500).send(err):res.send(result)               
    })
  },
   empty_jet_delete : function(req,res){
    console.log("req",req.body)
    emptyjet.updateOne({_id:req.body._id},{$set:{status:'Block'}},function(err,result){
     err?res.status(500).send(err):res.send(result)               
    })
  },

   

  
  verification : function(req,res){
    console.log("enter in verification")
  opertaor.findOne({email:req.params.email},function(err,result){
     if(err){
             return res.send({responseCode:400,responseMessage:"Server err"})
     }
     else if(!result){
  
      var redirectUrl = "https://gojetsmart.com?code="+req.params._id +"?status="+0
            res.redirect(redirectUrl);

     }
     else{
       var redirectUrl = "https://gojetsmart.com?code="+req.params._id +"?status="+result._id
        console.log("redirectUrl-------",redirectUrl)
        res.redirect(redirectUrl);
                
     }

  })

  },

    show_flight_detail : function(req,res){
   saveflight.findOne({_id:req.params._id}).populate('aircratft_id created_by').exec(function(err,result){
err?res.status(500).send(err):res.send(result)

   })

    },

   particular_employee : function(req,res){
    opertaor.findOne({_id:req.params._id,"type":"employee"},function(err,result){
      err?res.status(500).send(err):res.send(result)
    })

   }
    ,
    edit_employee: function(req,res){
     console.log("req.body",req.body)
     opertaor.findOneAndUpdate({_id:req.body._id},{$set:req.body},{new:true},function(err,result){
      err?res.status(500).send(err):res.send(result)

     })

    },

    mainifist_list : function(req,res){
    console.log("req.body",req.body)
// pessangeBook.find({empty_jetId:req.body._id}).populate('empty_jetId').exec(function(err,result){
// console.log("result",result)
//  err?res.status(500).send(err):res.send(result)

// })
   async.parallel({
    emptyleg: function(callback) {
     emptyjet.findOne({_id:req.body._id},function(err,result){
      callback(null,result)
     })
    },
    pessenger: function(callback) {
         pessangeBook.find({empty_jetId:req.body._id}).populate('empty_jetId').exec(function(err,result){
           callback(null,result)
        })
    }
}, function(err, results) {
     if(err){
        return res.send({responseCode:500,responseMessage:"server error"})
     }
     else{
       return res.send({responseCode:200, responseMessage:"sucess",result:results})
     }
      // err?res.status(500).send(err):res.send({result:results})
    // results now equals to: [one: 'abc\n', two: 'xyz\n']
});




    },

    flight_mainifist_list : function(req,res){
    console.log("req.body",req.params._id)
   async.parallel({
    flight: function(callback) {
     saveflight.findOne({_id:req.params._id},function(err,result){
      callback(null,result)
     })
    },
    pessenger: function(callback) {
         pessangeBook.findOne({flight_id:req.params._id}).exec(function(err,result){
           callback(null,result)
        })
    }
}, function(err, results) {
     if(err){
        return res.send({responseCode:500,responseMessage:"server error"})
     }
     else{
       return res.send({responseCode:200, responseMessage:"sucess",result:results})
     }
      // err?res.status(500).send(err):res.send({result:results})
    // results now equals to: [one: 'abc\n', two: 'xyz\n']
});


    },





  forgot_password: function(req,res){
  console.log(req,res)
  opertaor.findOne({email:req.body.email,type:"user"},function(err,result){
   if(err){
     return res.send({responseCode:500,responseMessage:"server err"})
   }
   else if(!result){
                return res.send({responseCode:204,responseMessage:"Please Enter correct email ID"})
   }
   else{
    function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}
    var rString = randomString(8, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
     
       mailClient.send('noreply@gojetsmart.com', req.body.email, 'Forgot password', functions.getforgot(req.body.email,rString)).then(function(result) {
        if(!result.status) { return res.json({status : false, response : 'Mail not sent!'}) }
                else{
                  opertaor.update({email:req.body.email,type:"user"},{$set:{password:rString}},function(err,result2){

                     res.json({ status: true, response : 'please check your mail inbox.'})
                  })
                
                }
            })
  

   }

  })

},

add_emptyleg : function(req,res){
console.log("req",req.body)
if(req.body.emptyJet_id){
emptyjet.update({_id:req.body.emptyJet_id},{$push:{"crew_members":{name:req.body.name,designation:req.body.designation,nationality:req.body.nationality,passport:req.body.passport,remarks:req.body.remarks}}},function(err,result){
   err?res.status(500).send(err):res.send(result)
})
}
else{
  saveflight.update({_id:req.body.flight_id},{$push:{"crew_members":{name:req.body.name,designation:req.body.designation,nationality:req.body.nationality,passport:req.body.passport,remarks:req.body.remarks}}},function(err,result){
   err?res.status(500).send(err):res.send(result)
})

}


},

edit_emptyleg : function(req,res){
console.log("req",req.body)
if(req.body.emptyJet_id){
emptyjet.update({_id:req.body.emptyJet_id,'crew_members._id':req.body._id},{$set:{"crew_members.$":req.body}},function(err,result){
   err?res.status(500).send(err):res.send(result)
})
}
else{
  saveflight.update({_id:req.body.flight_id,'crew_members._id':req.body._id},{$set:{"crew_members.$":req.body}},function(err,result){
   err?res.status(500).send(err):res.send(result)
})

}


},

add_edit_pessenger : function(req,res){
  console.log("req------------------",req.body)

  pessangeBook.findOne({flight_id :mongoose.Types.ObjectId(req.body.flight_id)},function(err,result){
   if(!result){
   return res.send({"responseCode":204,"responseMessage":"No payment for this flight can not add pessenger without payment."})
   }
   else{
  pessangeBook.findOneAndUpdate({flight_id :mongoose.Types.ObjectId(req.body.flight_id)},{$push:{pessanger_detail:{name:req.body.name,age:req.body.age,gender:req.body.gender,passportNumber:req.body.passportNumber,nationlity:req.body.nationlity}}},function(err,result){
   err?res.status(500).send(err):res.send(result)

})


   }


  })

},

edit_pessenger : function(req,res){
    console.log("req----------------->>>>-",req.params)
  console.log("req----------------->>>>-",req.body)

  pessangeBook.findOneAndUpdate({flight_id :req.params.flight_id,'pessanger_detail._id':req.params.passenger_id},{$set:{'pessanger_detail.$':req.body}},function(err,result){
   err?res.status(500).send(err):res.send(result)

})
}
,

Editemptyleg : function(req,res){
  console.log("reqqqqq",req.body)
  emptyjet.findOneAndUpdate({_id:req.body._id},{$set:req.body},function(err,result){
     err?res.status(500).send(err):res.send(result)

  })

},

addBookingDetail : function(req,res){
console.log("reqqqq",req.body)
pessangeBook.findOneAndUpdate({_id:req.params._id},{$push:{pessanger_detail:req.body}},function(err,result){
 err?res.status(500).send(err):res.send(result)

})

},

editBookingDetail : function(req,res){
console.log("editRequest",req.body)
pessangeBook.findOneAndUpdate({_id:req.params._id,"pessanger_detail._id":req.params.pessenger_id},{$set:{'pessanger_detail.$':req.body}},function(err,result){
  err?res.status(500).send(err):res.send(result)
})


}







}



module.exports = controller;


 var get_count = function (return_object,callback) {
   waterfall([
        (cb)=>{
          booking.findOne({}, return_object,function(err,success_data){
             if(err) cb(err,null);
             else cb(null,success_data);
          })
        },
        (success_data,cb)=>{
          booking.findByIdAndUpdate(success_data._id ,{
            $inc:return_object
          },{ projection: return_object , new :true},function(err,updated_count){
            if(err) cb(err,null);
            else cb(null,updated_count);
          })
        }
    ],function(err,result){
      if(err){
           console.log(err);
      }else{    
        callback(null,result);        
      }
    })
  }          









  // mailClient.send('no-reply@royalclassiccoin.org', req.body.email_id, 'Account verificaion', functions.getVerificationMailHtml(req.body.username, req.body.email_id, code)).then(function(result) {