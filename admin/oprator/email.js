var config = require.main.require('./global/config.js')
module.exports = {

	getVerificationMailHtml : function(username, email, code){
		return '<p>Hey '+username+',<br><br>Thanks for creating account on Rcl Wallet. We would love to help you get started!<br><br>To complete your signup, simply click below to activate your account:</p><br><br><h1><a href="'+config.domain+'/wallet/activate/account/'+email+'/'+code+'">Click Here to Activate Account</a></h1><br><br>Thanks, and Stay Blessed!<br><br>-Team RCL';
	},
	getTest: function(email,path,id){
		return '<p>Hey Thanks for using JetSmart Here is the link of your Quotation <h1><a href=https://gojetsmart.com'+path+'><p>Please click here to open/download Your Quotation!</a></p></h1><br><br><h1><a href = https://gojetsmart.com/admin/oprator/verification/'+email+'/'+id+'>Click here for to make Payment</a></h1><br><br>Thanks, and Stay Blessed!<br><br>-Team JetSmart';

	},

	getforgot :function(email,password){
    return '<p>Hey Thanks for using JetSmart Here is the password of your account <h1>'+password+'</h1><br><br>Thanks, and Stay Blessed!<br><br>-Team JetSmart';

	},

	getEmptyjet : function(email,departureDate,originName,destinationName){
    return '<p>Empty Leg,'+originName+" to "+destinationName+","+departureDate+' available for sale .Please visit https://gojetsmart.com </h1><br><br>Thank you,<br><br>-Team JetSmart';

	}
	
}
 

// Empty Leg, Delhi – Mumbai, 20 Apr 2018 available for sale. Please visit  https://gojetsmart.com

 

// Thank you,

 

// Team JetSmart