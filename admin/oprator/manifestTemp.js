module.exports = {
	style:`
		<!DOCTYPE html>
		<html>
		<head>
			<title></title>
			<style>
			#pdfFormat .pdf-header ul li img {
			    margin-bottom: 20px;
			    margin-top: 20px;
			    width: 150px !important;
			}
			#pdfFormat .pdf-sub-header ul {
			    padding: 0;
			}
			#pdfFormat .invoice-header {
			    text-align: center;
			    font-size: 16px;
			    margin: 15px 0 10px;
			}
			#pdfFormat .pdf-section > span {
			    font-size: 24px;
			    font-weight: 500;
			    margin: 20px 0 5px;
			    display: block;
			    text-transform: uppercase;
			    letter-spacing: 2px;
			}
			p.note {
			    font-weight: 300;
			    font-size: 16px;
			    margin-bottom: 20px;
			}
			#pdfFormat{
			    width:1300px;
			    background-color: #fff;
			}
			#pdfFormat .pdf-header{
			    padding: 10px;
			}
			#pdfFormat .pdf-header ul{
			}
			#pdfFormat .pdf-header ul li{
			    font-size: 24px;
			    list-style: none;
			    text-align: right;
			}
			#pdfFormat .pdf-sub-header{
			    border-bottom:1px dotted black; 
			}
			#pdfFormat .pdf-sub-header ul{
			    font-weight: 600;
			}
			#pdfFormat .pdf-sub-header ul li{
			    list-style: none;
			    font-size: 20px;
			}
			#pdfFormat .pdf-section ul.bullet-list li{
			    list-style: none;
			}
			#pdfFormat .invoice-header{
			    text-align: center;
			}
			ul.bullet-list {
			    padding: 0 0 0 30px;
			}
			.end-note {
			    text-align: center;
			    margin: 50px 0 30px;
			}
			.end-note p {
			    margin: 0;
			    font-size: 30px;
			    font-weight: 500;
			}
			#pdfFormat .pdf-section ul.bullet-list li {
			    position: relative;
			}
			#pdfFormat .pdf-section ul.bullet-list li b {
			    position: absolute;
			    margin: 0 !important;
			    left: -15px;
			}
			#pdfFormat .pdf-section {
			    margin-bottom: 15px;
			    padding: 0 15px;
			}

			#pdfFormat .pdf-section > span {
			    font-size: 24px;
			    font-weight: 600;
			}
			#pdfFormat .pdf-section > table {
			    width: 100%;
			    background-color: transparent;
			    font-size: 20px
			}
			#pdfFormat .pdf-section > table tr.tr-head td{
			    background-color: #cecece;
			    font-weight: 600;
			    border: 1px solid #000;
			    padding-left: 5px;
			}
			#pdfFormat .pdf-section > table tr td{
			    border: 1px solid #000;
			    padding-left: 5px;
			}
			#pdfFormat .trmc{
			    padding: 0 10px;
			    font-size: 20px;
			    word-break: break-all;
			    text-align: left;
			}
			.bookmark-bottom {
				position: absolute;
				bottom: 15px;
				right: 15px;
			}
			</style>
		</head>
	`,
	content_start:`
		<body>
			<div id="pdfFormat">
				<section class="page1" style="padding-left: 50px; padding-right: 50px;">
					<div class="pdf-header">
						<ul>
							<li>Operator Name</li>
							<li>logo</li>
						</ul>
					</div>
	`,
	content_end:`
					
				<section class="page2" style="padding-left: 50px; padding-right: 50px;">
					<br>
					 <div class="pdf-section">
					 	<span>
					 		Conf<b style="font-weight:600">i</b>rmation of Flight will depend on the following factors 
					 	</span>
						<ul class="bullet-list">
					      <li class="trmc">
					         <b style="margin-right:15px;font-weight: 900;">o</b>Subject to availability of Aircraft 
					      </li>
					      <li class="trmc">
					         <b style="margin-right:15px;font-weight: 900;">o</b>100% payment in advance 
					      </li>
					      <li class="trmc">
					         <b style="margin-right:15px;font-weight: 900;">o</b>Subject to the approval from Regulatory Authority 
					      </li>
						</ul>
					</div>
					<div class="pdf-section">
					 	<span>
					 		Terms & Conditions:-
					 	</span>
						<ul class="bullet-list">
					      <li class="trmc" style="width: 100%">
					        <b style="margin-right:15px;font-weight: 900;">o</b>Mentioned flight timings &amp; cost are indicative under NIL wind conditions and billing would be on actual time taken &nbsp; &nbsp; for the flight “chokes off to chokes on”, depending upon weather / wind/ fog / visibility condition and flight &nbsp; &nbsp; &nbsp; &nbsp; schedules are subject to change / divert due toweather conditions, flight safety and operational restrictions.
					      </li>
					      <li class="trmc">
					         <b style="margin-right:15px;font-weight: 900;">o</b>Flying hour charges include the cost of the Aircraft fuel, maintenance, standard in-flight meals, navigation fees and dispatch cost of the aircraft.
					      </li >
					      <li class="trmc">
					         <b style="margin-right:15px;font-weight: 900;">o</b>Confirmation of the flight is subject to availability and booking can be confirmed after the receipt of Advance payment &nbsp; &nbsp; only.
					      </li >
					      <li class="trmc">
					         <b style="margin-right:15px;font-weight: 900;">o</b>Minimum 02:00 hr flying time or Actual flying time per aircraft per day (whichever is higher) will be charged on everyday basis. The flying time quoted above is approximate and May vary.
					      </li>
					      <li class="trmc"> 
					         <b style="margin-right:15px;font-weight: 900;">o</b>Minimum 02 Hours of charges of Flying time will be charged as Non-Flying day with Night halt charges of the crew.
					      </li>
					      <li class="trmc">
					         <b style="margin-right:15px;font-weight: 900;">o</b>Crew Layover in case of prolonged halts during the course of single day at particular station,the “charterer” will make suitable arrangements for boarding, lodging &amp; local transport or we will make the necessary <br>arrangements on additional price
					      </li>
					      <li class="trmc">
					         <b style="margin-right:15px;font-weight: 900;">o</b>We shall not be liable for any delay/inconvenience caused towards diversion, due to bad weather/VIP <br>movements, unforeseen circumstances caused by nature or due to force majeure reasons in such condins; <br>charterer has to bear the cost of extra flying/overnight halt/waiting etc.
					      </li>
					      <li class="trmc">
					         <b style="margin-right:15px;font-weight: 900;">o</b>In case of non-availability of Parking space at the selected station, the aircraft have to be moved or re-<br>positioned to closest city (subject to availability of parking space at that station) or the aircraft has to take a <br>go-around flight in the local area to clear the parking bay for a short period. This cost shall be billed on actuals <br>whilst preparation of Final Invoice
					      </li>
						</ul>
					</div>

					<div class="pdf-section">
					 	<span>
					 		Payments: - 
					 	</span>
						<ul class="bullet-list">
					      <li class="trmc">
					         <b style="margin-right:15px;font-weight: 900;">o</b>25% of the charter estimated cost shall be payable in advance at the time of blocking of the aircraft. Balae of <br>estimated cost shall be payable 04 days prior to the day of flight and actual cost shall be payable within 7 days <br>of actual billing.
					      </li>
					      <li class="trmc">
					         <b style="margin-right:15px;font-weight: 900;">o</b>100% of the estimated cost shall be payable in case the flight is confirmed less than 03 days to the actual date of flying.
					      </li>
						</ul>
					</div>

					<div class="pdf-section">
					 	<span>
					 		Cancellation Charges: - 
					 	</span>
					 	<table cellspacing="0" cellpadding="0">
					 		<tr>
					 			<td style="width:20%; padding: 5px 10px; font-weight: 600">Full Refund</td>
					 			<td style="padding: 5px 10px;">If flight cancelled before 72 hrs of departure time. Transaction fees will be levied.</td>
					 		</tr>
					 		<tr>
					 			<td style="padding: 5px 10px; font-weight: 600">25% Deduction</td>
					 			<td style="padding: 5px 10px;">Flight Cancelled within 72 hrs of departure time.</td>
					 		</tr>
					 		<tr>
					 			<td style="padding: 5px 10px; font-weight: 600">50% Deduction</td>
					 			<td style="padding: 5px 10px;">Flight Cancelled within 48 hrs of departure time.</td>
					 		</tr>
					 		<tr>
					 			<td style="padding: 5px 10px; font-weight: 600">75% Deduction</td>
					 			<td style="padding: 5px 10px;">Flight Cancelled within 24 hrs of departure time.</td>
					 		</tr>
					 		<tr>
					 			<td style="padding: 5px 10px; font-weight: 600">100% Deduction</td>
					 			<td style="padding: 5px 10px;">Flight Cancelled in between 24 hrs of departure time.</td>
					 		</tr>
					 		<tr>
					 			<td style="padding: 5px 10px; font-weight: 600">Admin Charges</td>
					 			<td style="padding: 5px 10px;">Flight cancelled due to force majeure or adverse weather condition, INR 50,000/- will be applicable as admin charges. Balance will be refunded.</td>
					 		</tr>
					 	</table>
					</div>

					<div class="pdf-section">
					 	<span>
					 		Important Note: 
					 	</span>
						<ul class="bullet-list">
					      <li class="trmc">
					      	<b style="margin-right:15px;font-weight: 900;">o</b>We require passenger names, nationality, passport no., and date of issue and date of expiry of passport well in <br>advance to obtain necessary security clearances.
					      </li>
					      <!-- <li class="trmc">
					      	<b style="margin-right:15px;font-weight: 900;">o</b>
					      </li> -->
						</ul>
					</div>
					<div class="end-note">
						<p>WE WISH YOU A VERY HAPPY JOURNEY</p>
						<p>JETSMART</p>
					</div>
				</section>
			</div>
		</body>
		</html>
	`
}