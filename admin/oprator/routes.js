// admin routes//

var express = require('express'); 
var path=require('path');   
var router = express.Router();  
var authorisation = require('../authorisation');
var controller = require('./controller');

router.post('/login',controller.login);
router.post('/oprator_signup',controller.oprator_signup);
router.get('/oprator_list/:page_number',controller.oprator_list);
router.post('/delete_user', controller.delete_user);
router.post('/edit_user', controller.edit_user);
router.post('/oprator_detail', controller.oprator_detail);
router.post('/add_oprator_aircraft', controller.add_oprator_aircraft);

router.post('/change_password_operator', controller.change_password_operator);
router.get('/bulkUploadOprator',controller.bulkUploadOprator);
router.get('/opertaor_aircrft/:_id', controller.opertaor_aircrft);
router.post('/opertaor_aircrft_detail', controller.opertaor_aircrft_detail);
 router.get('/bulkUploadAgent',controller.bulkUploadAgent);
 router.get('/testopratoraircraft',controller.testopratoraircraft);
router.post('/edit_oprator', controller.edit_oprator);
router.post('/view_oprator', controller.view_oprator);
router.post('/generateManifest', controller.generateManifest);

// router.get('/',authorisation.);  
router.post('/opratorsaveflight', controller.opratorsaveflight);
router.get('/oprator_flight_list/:_id',controller.oprator_flight_list);
router.post('/oprator_flight_detail',controller.oprator_flight_detail);
router.post('/emailSend',controller.emailSend);
router.post('/add_empty_jet',controller.add_empty_jet);
router.get('/operator_empty_jet/:created_by',controller.operator_empty_jet);
router.post('/forgot_password',controller.forgot_password);
router.get('/verification/:email/:_id',controller.verification);
router.get('/show_flight_detail/:_id',controller.show_flight_detail);
router.get('/particular_employee/:_id',controller.particular_employee);
router.post('/edit_employee',controller.edit_employee);
router.post('/revenuBulkUpload',controller.revenuBulkUpload);
router.post('/mainifist_list',controller.mainifist_list);
router.get('/empty_jet_list',controller.empty_jet_list);
router.get('/all_flight_list',controller.all_flight_list);
router.post('/empty_jet_delete',controller.empty_jet_delete);
router.post('/flight_delete',controller.flight_delete);
router.post('/add_emptyleg',controller.add_emptyleg);
router.post('/edit_emptyleg',controller.edit_emptyleg);
router.get('/flight_mainifist_list/:_id',controller.flight_mainifist_list);
router.post('/add_edit_pessenger',controller.add_edit_pessenger);
router.put('/edit_pessenger/:flight_id/:passenger_id',controller.edit_pessenger);
router.post('/Editemptyleg',controller.Editemptyleg);
router.post('/oprator_aircraft_view_edit',controller.oprator_aircraft_view_edit);
router.put('/oprator_aircraft_edit/:_id',controller.oprator_aircraft_edit);
router.get('/oprator_aircraft_delete/:_id',controller.oprator_aircraft_delete);
router.put('/addBookingDetail/:_id',controller.addBookingDetail);
router.put('/editBookingDetail/:_id/:pessenger_id',controller.editBookingDetail);

// router.post('/add_flight_crew',controller.add_flight_crew);
// router.post('/edit_flight_crew',controller.edit_flight_crew);


module.exports = router;  