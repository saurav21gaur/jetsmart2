// admin routes//

var express = require('express'); 
var controller = require('./controller'); 
var path=require('path'); 
var authorisation = require('./authorisation');  
var router = express.Router();  
var userRouter=require('./oprator/routes');
var masterRoutes=require('./master/routes.js');
var airRoutes=require('./aircraft/routes.js');
var agentRoutes = require('./agent/routes.js');
var airport = require('./airport/routes.js');




// router.post('/uploadChatFile',airRoutes.uploadChatFile)

router.use('/oprator',userRouter)
router.use('/master',masterRoutes);
router.use('/aircraft',airRoutes)
router.use('/agent',agentRoutes)
router.use('/airport',airport)

router.get("/",function(req,res){
	console.log("__dirname-->",__dirname)
	return res.sendFile(path.join('backend','index.html'),{ root: __dirname });
})


// router.post('/login',controller.login)
// router.get('/getAddresses',authorisation.isAuthenticate,controller.getAddresses)
// router.post('/distributeGas',authorisation.isAuthenticate,controller.distributeGas)
// router.post('/moveToMaster',authorisation.isAuthenticate,controller.moveToMaster)


// router.get('/',authorisation.);  

module.exports = router;  