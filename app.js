var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cors=require('cors');
var app = express();
var forceSsl = require('express-force-ssl')
var path = require('path');
var fs=require('fs');
var waterfall = require('async-waterfall');
var async = require('async');
var aircraft = require('./db/aircraftSchema.js')
var https = require('https')
var http = require('http')

var config=require('./global/config')
// var gatewayRoutes=require('./gateway/routes.js')
var adminRoutes=require('./admin/routes.js');

var userRoutes = require('./fareportal/user/routes.js')
console.log("config.port----",config.port)


var key = fs.readFileSync('certificates/jetSmart.key');
var cert = fs.readFileSync('certificates/jetSmart.crt');
var ca = fs.readFileSync('certificates/jetSmart.ca-bundle');

var options = {
  key: key,
  cert: cert,
  ca: ca
};
  https.createServer(options, app).listen(443);
var server=app.listen(config.port,function(){
  var host = server.address().address;
  console.log("host",host)
  var port = server.address().port;
  require('dns').lookup(require('os').hostname(), function (err, add, fam) {
        console.info('\n\n\nServer is listening at http://%s:%s\n\n\n', add, port);
    })
});

// https.createServer(options, app).listen(443);
// var listener = app.listen(config.prt, function (err,success) {
//    console.log('Listening on port-->> ');
// })




mongoose.Promise = global.Promise;
var connStr="mongodb://"+config.db.gateway.user+":"+config.db.gateway.password+"@"+config.db.gateway.host+":"+config.db.gateway.port+"/"+config.db.gateway.database
mongoose.connect(connStr,{ useMongoClient: true },function(error,success){
	if(error)
		console.log(error)
	//else
		//console.log("success--->",success);
});

app.use(forceSsl)

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
app.use(cors());
console.log('app---->')
// app.use('/gateway',gatewayRoutes)

/*aircraft*/


app.use('/media',express.static(path.join(__dirname, 'media')));
app.use('/temp',express.static(path.join(__dirname, 'temp')));
const fileUpload = require('express-fileupload');
app.use('/media',fileUpload());

app.post('/media/remove',function(req,res){
  var _filepath=path.join(__dirname,req.body.file);
  fs.unlink(_filepath, function(err,result){
    console.log("err---->",err);
    console.log("result--->",result);
    err?res.status(500).send(err):res.send(result)
  })
})
app.post('/media/upload',function(req,res,next){
  //var _dir=req.params.sectionName;
  var _dir="temp";
  async.waterfall([function(cb){
    var _dirName=path.join(__dirname,_dir);
    var _isDirExist=fs.existsSync(_dirName);
    if(_isDirExist)
      cb(null,_dirName)
    else{
      fs.mkdir(_dirName, function(){
        cb(null,_dirName)
      })
    }
  },function(filePath,cb){
    console.log(req.files);
    console.log(req.files.imgs);
    var _content=req.files.imgs;
    // console.log("_content--->",_content);

    var checkFileName=function(_name,filePath,_ext){
          var _fileName=path.join(filePath,_name+"."+_ext);
          if (fs.existsSync(_fileName)) {
              return checkFileName(new Date().getTime().toString(),filePath,_ext)
          }
          else{
            return _fileName;
          }
        }
    var _isArray=Array.isArray(_content)
    if(_isArray){
      async.map(_content,function(data,cbmap){
        var _name=data.name.split('.');
        console.log("name--->",_name);
        var fileName=checkFileName(new Date().getTime().toString(),filePath,_name[1]);
        console.log("fileName--->",fileName);
        //
        data.mv(fileName, function(err) {
          if(err)
            cbmap(err,null);
          else{
            cbmap(null,{file:path.join(_dir,fileName.split('/')[fileName.split('/').length-1]),isTemp:true});
          }
        });
      },function(err,resmap){
        if(err)
          cb(err,null)
        cb(null,resmap)
      })
    }
    else{
      var _name=_content.name.split('.');

      console.log("name--->",_name);
      var fileName=checkFileName(new Date().getTime().toString(),filePath,_name[1]);
      // var fileName=path.join(filePath,new Date().getTime().toString()+"."+_name[1]);
      console.log("fileName--->",fileName);

      //
      _content.mv(fileName, function(err) {
        if(err)
          cb(err,null);
        else{
          cb(null,{file:path.join(_dir,fileName.split('/')[fileName.split('/').length-1]),isTemp:true});
        }
      });
    }
    
  }],function(err,result){
    err?res.status(500).send(err):res.send(result);
  })
})
// app.post('/media/uploadAircraft/:jet_name',function(req,res,next){
  app.post('/media/uploadAircraft',function(req,res,next){

    console.log("req---",req.body);
      var _name=req.files.aircraftFile.name;
       var _id="";

    if(!req.body._id){
   
        if (!req.body.jet_name)
            return res.status(403).send({
                msg: "jet_name is requierd."
            })
    
        aircraft.find({
              jet_name : req.body.jet_name
        }, function (err, result) {
            console.log("resultfirst" + JSON.stringify(result));
            if (err) {
                res.status(500).send(err)
                return;
            } else if (result.length != 0) {
    
                   return res.status(403).send({
                message: "jetName must me unique "
            })                            
            }
    else { 
    async.waterfall([function(cb){
        console.log("req.files---->",req.files);

                        var aircraft_save = new aircraft(req.params);
                        aircraft_save.save(function (err, result) {
                            if (err) {
                                 res.status(500).send(err)
                            } else {
                                console.log("result------"+JSON.stringify(result));
                                     _id = result._id
                                        cb(null,_id);
                            }
                        })
        
    },function(saveData,cb){
          console.log(saveData);
        var _dirName=path.join(__dirname,"media");
        var _isDirExist=fs.existsSync(_dirName);
          console.log("_name--->",_name)
                console.log("_dirName_dirName--->",_dirName)
              if(!_isDirExist){
            fs.mkdir(_dirName, function(){
              fs.mkdir(path.join(__dirname,"media",JSON.stringify(saveData)),function(){
                var _filepath=path.join(__dirname,"media",JSON.stringify(saveData),_name);
                cb(null,_filepath)
              })
                
            })
        }
        else{
            var _filepath=path.join(_dirName,"media",_name);
            cb(null,_filepath,_name)
        }
        
    },function(filepath,_name,cb){
        console.log("fileee",filepath);
        // console.log("_name----",_name);
        req.files.aircraftFile.mv(filepath, function(err) {
          console.log("err",err);
            if (err){
            cb(err,null);
          }
            else{
                 req.body.profilePic=path.join("media",_id,_name);
                console.log("reqbody",req.body);
             cb(null,req.body);
            }
      });
    },function(saveFiledata,cb){
        console.log("saveFiledata"+JSON.stringify(saveFiledata));
        aircraft.findByIdAndUpdate({_id:_id},{$set:{airCraftimages:saveFiledata.profilePic}},function(err,result){
            console.log("result",result);
             cb(null,result)
            
        },function(err){
            cb(err,null)
        })
      
    }],function(err,result){
        if(err)
             res.status(500).send(err)

        else
               return res.status(200).send({
                message: "save successfully with images",
                result:result
            })      
//            res.send(result);
    })
    }
        })
            
 }
   else{
            aircraft.findByIdAndUpdate(req.body._id, {$set:req.body}, function(error,result) {
            if(error) { return res.send({status:500,message:"something went wrong"}) }
            error?res.status(500).send(error):res.send(result)
                //...
            })
        }
})



var fs = require('fs');
var pdf = require('html-pdf');
var html = fs.readFileSync('./fareportal/assets/views/search.html', 'utf8');
var options = { format: 'Letter' };
 
pdf.create(html, options).toFile('./fareportal/assets/views/search.pdf', function(err, res) {
  if (err) return console.log(err);
  console.log(res); // { filename: '/app/businesscard.pdf' }
});








app.use('/admin',adminRoutes)

app.use('/static',express.static(path.join(__dirname,"admin","backend","assets")));
app.use('/bower',express.static(path.join(__dirname,"admin","backend","bower_components")));
app.use('/views',express.static(path.join(__dirname,"admin","backend","views")));
app.use('/libs',express.static(path.join(__dirname,"admin","backend","libs")));



app.get('/farePortal', function (req, res) {
    res.sendFile(__dirname + '/fareportal/index.html');
});
app.use(express.static(path.join(__dirname, 'fareportal')));


app.use('/fareportal',userRoutes);
// app.use('/static',express.static(path.join(__dirname,"admin","fareportal","assets")));
// app.use('/bower',express.static(path.join(__dirname,"admin","fareportal","bower_components")));
// app.use('/views',express.static(path.join(__dirname,"admin","fareportal","views")));
// app.use('/libs',express.static(path.join(__dirname,"admin","fareportal","libs")));

app.get('/test',function(req,res){
  res.send("<h1>Server is running.</h1>")
});


// https.createServer(options, app).listen(443);
// var listener = app.listen(config.prt, function (err,success) {
//    console.log('Listening on port-->> ');
// })

// https.createServer(options, app).listen(443);
// var server=app.listen(config.port,function(){
//   var host = server.address().address;
//   var port = server.address().port;
//   require('dns').lookup(require('os').hostname(), function (err, add, fam) {
//         console.info('\n\n\nServer is listening at http://%s:%s\n\n\n', add, port);
//     })
// });