var mongoose = require('mongoose');
mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;

var emptyFlightSchema = new Schema({
   
      bookingNumber:{
          type:Number
      },
       aircratft_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref:"opratorAircraft"
       },
       originName:{
        type:String
       } , 
    aircraft_name:{
      type:String
    },
    aircraft_type:{
           type:String
    },
    aircraft_registration :{
      type:String
    },
    oprator_name:{
      type:String
    },
    

      origin:{
        type:String
      }, 
      destinationName:{
        type:String
      },
      destination: {
        type:String
      },

      departureDate: {
        type:Date
      },

       departureTime:{
        type:String
       },
       no_of_seats: {
       type:Number
       },

       remainingSeats:{
        type:Number
       },

       costs: {
       type:String
       },

       crew_members:[{
        name :{
          type:String
        },
        designation:{
          type:String
        },
        nationality:{
          type:String
        },
        passport:{
          type:String
        },
        remarks:{
          type:String
        }
       }],

       price_per_seat:{
          type:String
       },
       paymentStatus:{
        type:String,
        default:"pending"
       },
        created_at: {
         type: Date,
         default: Date.now
        },
        
       created_by:{
          type: mongoose.Schema.Types.ObjectId,
          ref:"oprator"
        },
      status:{
        type:String,
        default:"active"
      }


});

var emptyjet=mongoose.model('emptyjet',emptyFlightSchema)

module.exports = emptyjet; 



