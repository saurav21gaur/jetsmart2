var mongoose = require('mongoose');
mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;

var flightSchema = new Schema({
   
bookingNumber:{
	type:Number
},
   aircratft_id:{
    type: mongoose.Schema.Types.ObjectId,
    ref:"opratorAircraft"
   },
    aircraft_name:{
      type:String
    },
   
    aircraft_registration :{
      type:String
    },
    oprator_name:{
      type:String
    },

roots:[{
       originName:{
        type:String
       } , 
      origin:{
        type:String
      }, 
      destinationName:{
        type:String
      },
      destination: {
        type:String
      },

      departureDate: {
        type:Date
      },

       departureTime:{
        type:String
       }
     }],


       crew_members:[{
        name :{
          type:String
        },
        designation:{
          type:String
        },
        nationality:{
          type:String
        },
        passport:{
          type:String
        },
        remarks:{
          type:String
        }
       }],


     costs: {

         flyingcost:{
          type:Number
         },
         ground_handling:{
          type:Number
         },
         other_charges:{
          type:Number
         },

         crew_charges:{
          type:Number
         },
         sub_total:{
          type:Number
         },
         gst:{
          type:Number
         },
         grand_total:{
          type:Number
         }

     },
 aircraft_type:{
      type:String
    },


   paymentStatus:{
   	type:String,
    default:"pending"
   },
    created_at: {
       type: Date,
       default: Date.now
      },
     created_by:{
        type: mongoose.Schema.Types.ObjectId,
        ref:"oprator"
      },
      status:{
        type:String,
        default:"active"
      }


});

var flight=mongoose.model('flight',flightSchema)

module.exports = flight; 



