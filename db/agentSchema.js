var mongoose = require('mongoose');
mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;

var agentSchema = new Schema({
   
    agent_name:{
    	type:String
    },

    agency_number :{   // IATA NUMBER
    	type:String
    },

    agency_name:{
      type:String
    },
    
    address1:{
    	typ:String
    },
    address2:{
      typ:String
    },
    city:{
    	type:String
    },
    state:{
      type:String
    },
    country:{
      type:String
    },
    company_type:{
      type:String
    },

  
    pincode:{
    	type:String
    },
    phoneNo1:{
    	type:String
    },
     phoneNo2:{
      type:String
    },
    mobileNo:{
      type:String
    },

    email_id:{
    	type:String
    },
    assigned_sales_person:{
    	type:String
    },
    faxNo:{
    	type:String
    },

    auto_ticket_cap:{
    	type:Boolean
    },

    agency_type:{
    	type:String
    },
    category:{
    	type:String
    },
    bank_guarentee_type:{ 
    	type:String
    },
     bank_guarentee_amount:{ //amount in INR
      type:Number
    },
    bank_guarntee_valid:{
    	type:Date
    },
    prefferd_airline_sold:{
      type:Number
    },
    primary_destination_sold:{
      type:Number
    },

    other_airline :{
      type:Boolean
    },
    corparate_deal:{
      type:Boolean,

    },
    target:{
      type:Boolean
    },

    owner_manegement_team:[{

      name:{
        type:String
      },
      designation:{
        type:String
      },
      date_of_birth:{
        type:Date
      },
      anniversery:{
        type:Date
      },
      phone:{
        type:Number
      }
    }],


      key_personal_detail:[{
      
        name:{
        type:String
      },
      designation:{
        type:String
      },
      date_of_birth:{
        type:Date
      },
      anniversery:{
        type:Date
      },
      phone:{
        type:Number
      }
          
      }],

 name_of_corparate_account:[{
         name:{
          type:String
         },
         designation:{
          type:String
         },
             date_of_birth:{
        type:Date
      },
      anniversery:{
        type:Date
      },
      phone:{
        type:Number
      }

           

    }],



     created_at: {
       type: Date,
       default: Date.now
      },

      created_by:{
        type:String
      },
      updated_at:{
        type:Date
      },
      status:{
        type:String,
        default:"active"
      }


});
agentSchema.plugin(mongoosePaginate);
var agent=mongoose.model('agent',agentSchema)

module.exports = agent; 



