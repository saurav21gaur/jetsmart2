var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var aircraftTypeSchema = new Schema({



    aircraftType:{
      type:String
    },
    description:{
      type:String
    },
     created_at: {
       type: Date,
       default: Date.now
      },

      created_by:{
        type:String
      },
      updated_at:{
        type:Date
      },
      status:{
        type:String,
        default:"active"
      }


});
var aircraftType=mongoose.model('aircraftType',aircraftTypeSchema)
module.exports = aircraftType; 



