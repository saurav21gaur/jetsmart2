var mongoose = require('mongoose');
  var mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;

var airportDistanceSchema = new Schema({
   	fromAirport:{
   		type: mongoose.Schema.Types.ObjectId,
   		ref:'airport'
   	},
    toAirport:{
   		type: mongoose.Schema.Types.ObjectId,
   		ref:'airport'
   	},
    distance:{
      type: Number
    },
    status:{type:String,default:"active"},
   	createdBy:{type:String},
    createdAt:{type:Date, Default:Date.now}
  });
airportDistanceSchema.plugin(mongoosePaginate);
var airportDistance=mongoose.model('airportDistance',airportDistanceSchema)
module.exports = airportDistance; 



