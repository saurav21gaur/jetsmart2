var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var airportSchema = new Schema({
   	city:{
   		type: mongoose.Schema.Types.ObjectId,
   		ref:'country'
   	},
    country:{
   		type: mongoose.Schema.Types.ObjectId,
   		ref:'country'
   	},
    state:{
      type: mongoose.Schema.Types.ObjectId,
      ref:'country'
    },
    runway:{
      type:Number
    },

    airport:{type:String},
    icao:{type:String},
    iata:{type:String},
    category:{type: String,
        enum : ['AAI','Defence','Domestic','Domestic/Defence','Future','HAL Civil Aviation','International','International[1]','International[2]','MADC','Private','Reliance',''],
        default: ''},
    role:{type:String,
    	enum:['Aerospace Engineering','Air Base','Airbase','Airfield','Chartered Flights','Civil Enclave','Civil enclave/ Air Base','Closed','Closed[3]','Coast Guard','Commercial','commercial','Domestic','Flying School','Flying school','Future','General Aviation','Military','No flights scheduled','Not Started','Private','Under Construction',''],
    	default:''
    },
    groundhandling:{
      W05:{type:Number},
      W510:{type:Number},
      W1015:{type:Number},
      W1520:{type:Number},
      W2030:{type:Number},
      W3040:{type:Number},
      W4050:{type:Number},
      W5070:{type:Number},
      W70M:{type:Number}
    },
    status:{type:String,default:"active"},
   	createdBy:{type:String},
    createdAt:{type:Date, Default:Date.now}
  });
var airport=mongoose.model('airport',airportSchema)
module.exports = airport; 



