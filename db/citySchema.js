var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var citySchema = new Schema({
   countryName:{type:String,required:true,unique:true},
    countryCode:{type:String,required:true,unique:true},
    currencyCode:{type:String},
    status:{type:String,default:"active"},
   	createdBy:{type:String},
    createdAt:{type:Date, Default:Date.now}
  });
var city=mongoose.model('city',citySchema)
module.exports = city; 



