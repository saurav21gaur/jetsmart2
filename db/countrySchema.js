var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;

var countrySchema = new Schema({
   countryName:{type:String,required:true,unique:true},
    countryCode:{type:String,required:true,unique:true},
    currencyCode:{type:String},
    states:[{
   		stateName:{type:String},
   		region:{type: mongoose.Schema.Types.ObjectId,ref: 'region'},
   		createdBy:{type:String},
      status:{type:String,default:"active"},
   		createdAt:{type:Date,Default:Date.Now},
   		cities:[{
   			cityName:{type:String},
   			cityCode:{type:String},
   			createdBy:{type:String},
        status:{type:String,default:"active"},
   			createdAt:{type:Date,Default:Date.now}
   		}]
   	}],
    status:{type:String,default:"active"},
   	createdBy:{type:String},
    createdAt:{type:Date, default:Date.now}
  });
countrySchema.plugin(mongoosePaginate);
var country=mongoose.model('country',countrySchema)
module.exports = country; 



