var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var groupSchema = new Schema({


    group_name:{
        type:String
    },

    roles_id:[{
           type:mongoose.Schema.Types.ObjectId,
           ref:"role"
    }],
        created_at: {
        type: Date,
        default: Date.now
    },
    is_active: {
        type: String,
        default: 'active'
    }
  });
var group=mongoose.model('group',groupSchema)
module.exports = group; 



