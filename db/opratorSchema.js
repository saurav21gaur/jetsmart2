var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate')
var opratorSchema = new Schema({


   type:{
    type:String
   },
   code:{
    type:String
   },
   user_name:{
    type:String
   },
   first_name:{
    type:String
   },
   last_name:{
    type:String
   },
   assigned_sales_person:{
    type:String
   },
    company_type:{
      type:String
    },
   logo:{
    type:String
   },
   
   address1:{
    type:String
   },
   address2:{
    type:String
   },

   flight_number:{
    type:String
   },

   
   state:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'country'
   },
   city:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'country'
   },
   fax:{
     type:String
   },
   otp:{
    type:Number
   },
    profitable_percentage:{
    type:Number
   },
   otp_verify:{
    type:Boolean,
    default:false
   },

    phone_number:{
        type:String
    },
    bussiness_detail:{
        type:String
    },
    password:{
        type:String
    },
    email:{
        type:String
    },
    pincode:{
        type:String
    },

    AOP_NO:{
        type:String
    },
    valid_upto:{
        type:String
    },

    website:{
        type:String
    },
    



   // aircraftId:[],
    aircrafts:[{
        jet_name:{
            type:String
        },
        aircraft_type:{
            type:String
        },
        crew_members:{
            type:String
        }
    }],

    // model:{
    //     type:String
    // },

     model:[],
     account_id:{
       type:String 
   },
    bank:{
        bank_name:{
            type:String
        },
        ifsc_code:{
            type:String
        },
        account_no:{
            type:String
        },
        account_type:{
           type:String 
       },
       beneficiary_name:{
        type:String 
        }
    },
      Revenu:[{
                    month:{
                        type:String
                    },
                    total_amount:{
                         type:Number
                        }
                        }],



   gst:{
    type:String
   },
   sinNumber:{
    type:String
   },

   panCardNumber:{
    type:String
   },
   panCardImage:{
    type:String
   },

    keyPerson:[{
         name:{
            type:String
         },
         designation:{
            type:String
         },
         mobile:{
            type:Number
         },
         email:{
            type:String
         },
         date_of_birth:{
            type:Date
         }

    }],
   owner_manegement_team :[{
  
 name:{
            type:String
         },
         designation:{
            type:String
         },
         mobile:{
            type:Number
         },
         email:{
            type:String
         },
         date_of_birth:{
            type:Date
         }

   }],




      access_level: {
        Aircraft: {
            viewAircraft:{
              type: Boolean,
                default: false
           },
           editAircraft :{
            type:Boolean,
            default:false
           }       
        },
         Airport:{
            viewAirport:{
                type:Boolean,
                default:false
            },
            editAirport:{
                type:Boolean,
                default:false
            }
         },
         Country:{
            viewCountry:{
                type:Boolean,
                default:false
            },
            editCountry:{
                type:Boolean,
                default:false
            }
         },
             Payment:{
            viewPayment:{
                type:Boolean,
                default:false
            },
            editPayment:{
                type:Boolean,
                default:false
            }

         },
         State:{
            viewState:{
                type:Boolean,
                default:false
            },
            editState:{
                type:Boolean,
                default:false
            },
        },
            City:{
                viewCity:{
                    type:Boolean,
                    default:false
                },
                editCity:{
                    type:Boolean,
                    default:false
                }
            },
            Booking:{
                viewBooking:{
                    type:Boolean,
                    default:false
                },
                editBooking:{
                    type:Boolean,
                    default:false
                }
            },
            AirportDistance:{
             viewAirportDistance:{
                type:Boolean,
                default:false
             },
             editAirportDistance:{
                type:Boolean,
                default:false
             }

            },
            User:{
                viewUser:{
                    type:Boolean,
                    default:false
                },
                editUser:{
                    type:Boolean,
                    default:false
                }
            },
            Agent:{
                viewAgent:{
                    type:Boolean,
                    default:false
                },
                editAgent:{
                    type:Boolean,
                    default:false
                }
            }
            ,
            Operator:{
                viewOperator:{
                    type:Boolean,
                    default:false
                },
                editOperator:{
                    type:Boolean,
                    default:false
                }
            },
                 Flight:{
                viewFlight:{
                    type:Boolean,
                    default:false
                },
                editFlight:{
                    type:Boolean,
                    default:false
                }
            },
             Emptylegs:{
                viewEmptylegs:{
                    type:Boolean,
                    default:false
                },
                editEmptylegs:{
                    type:Boolean,
                    default:false
                }
            }


         },

         is_login:{
            type:Boolean,
            default:false
         },

          created_at: {
        type: Date,
        default: Date.now
    },
    is_active: {
        type: String,
        default: 'active'
    }
  });
opratorSchema.plugin(mongoosePaginate);
var oprator=mongoose.model('oprator',opratorSchema)

function init() {



    oprator.count({type:"admin"}, function (err, data1) {
        if (err) {
            console.log("Server error");
        } else if (data1 == 0) {
            var obj = {
                "user_name":"admin",
                "password":"admin1234",
                type:"admin",

      access_level: {
        Aircraft: {
            viewAircraft:true,
           editAircraft :true
        },
         Airport:{
            viewAirport:true,
            editAirport:true
         },
         Country:{
            viewCountry:true,
            editCountry:true
         },
             Region:{
            viewRegion:true,
            editRegion:true

         },
         State:{
            viewState:true,
            editState:true
        },
        City:{
                viewCity:true,
                editCity:true
            },
            AirportDistance:{
             viewAirportDistance:true,
             editAirportDistance:true

            },
            User:{
                viewUser:true,
                editUser:true
            },
           Operator:{
            viewOperator:true,
            editOperator:true
           },
            Booking:{
                viewBooking:true,
                editBooking:true
            },

            Agent:{
                viewAgent:true,
                editAgent:true
            },
            Payment:{
                viewPayment:true,
                editPayment:true   
            },
            Emptylegs:{
                  viewEmptylegs:true,
                    editEmptylegs:true,
            },
            Flight:{
                 viewFlight:true,
                  editFlight:true,
            }

          }

        
        }

        

            var savaData = new oprator(obj);
            savaData.save(function (err, user) {
                console.log('success');
            })
        }

    })
}
init();

module.exports = oprator; 



