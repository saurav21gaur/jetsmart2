var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var opratorAircraftSchema = new Schema({


    jet_name:{
      type:String
    },

    flight_number:{
      type:String
    },
   passangers:{
    type:Number
   },
  
  // aircraft_type:{
  //   type:String
  // },
  
    opratorId:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'oprator'
    },
    
    aircraft_type:{
      type:String
    // type: mongoose.Schema.Types.ObjectId,
    // ref:'aircraftType'
    },
    image_gallary:[],
    agent_id:{
      type:mongoose.Schema.Types.ObjectId
    },
   bases:[{
           base_city:{
              type:mongoose.Schema.Types.ObjectId,
               ref:'airport'
           },
           base_price:{
               type:Number
           }
       }],
    baggage:{
      type:String
    },
    pilot:{
      type:Number
    },
    crew_charges:{
      type:Number
    },
    seats:{
      type:Number
    },

    minimum_distance:{
      type:Number
    },
    aircraft_max_range:{
      type:Number
    },

   from_destination:{
    type:String
   },

   to_destination:{
    type:String
   },

 facilities:{
       wifi:{
        type:String
      },
      flight_attendence:{
        type:String
      },
       lavatory:{
        type:String
      },
      satellite_phone:{
        type:String
      },
      baggage:{
        type:String
      }

  },
    passangers:{
      type:Number
    },
    speed:{
      type:Number
    },
    weight:{
      type:Number
    },
    price:{
     type:Number
    },

    image:{
      type:String
    },
    banner_img:{
      type:String
    },
    cackpit_img:{
      type:String
    },

   cabin_information:{
  cabin_hight:{
    type:String
    },

    cabin_width:{
    type:String
    },

    cabin_length:{
    type:String
    }
  },
  time:{
    type:Number,
    default:0
  },
  
 AOP_NO:{
        type:String
    },
    valid_upto:{
        type:String
    },

    created_at: {
        type: Date,
        default: Date.now
    },
    is_active: {
        type: String,
        default: 'active'
    }
  });
var opratorAircraft=mongoose.model('opratorAircraft',opratorAircraftSchema)

module.exports = opratorAircraft; 



