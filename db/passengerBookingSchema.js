var mongoose = require('mongoose');
mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;

var pessangerBookingSchema = new Schema({
   
   aircraftId:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'aircraft'
   },

   user_id:{
    type:String,
    ref:"oprator"
   },

   empty_jetId:{
     type:mongoose.Schema.Types.ObjectId,
        ref:'emptyjet'
   },
   empty_jet_sel_seat:{
     type:Number
   },
    oprator_id:{
      type:mongoose.Schema.Types.ObjectId,
      ref:"oprator"
    },
    flight_id:{
       type:mongoose.Schema.Types.ObjectId,
        ref:'flight'
    }
,

roots:[{
       originName:{
        type:String
       } , 
      origin:{
        type:String
      }, 
      destinationName:{
        type:String
      },
      destination: {
        type:String
      },

      departureDate: {
        type:Date
      },

       departureTime:{
        type:Date
       }
     }],

   bookingNumber:{
    type:Number
   },
   pessanger_detail:[{
    title:{
      type:String
    },

      name:{
        type:String
      },
      age:{
        type:Number
      },
      gender:{
        type:String
      },
      passportNumber:{
        type:String
      },
      food:{
        type:String
      },
      nationlity:{
        type:String
      },
      identification:{
        type:String
      },
      identification_no:{
        type:String
      }
   
   }],
    contact:{

           title:{
               type:String
           },
           name:{
            type:String

           },
           email:{
            type:String
           },
           phone_number:{
                type:Number
           },
           alternate_number:{
               type:Number
           }

    },

  created_at: {
  type: Date,
  default: Date.now
    },
  updated_at:{
    type:Date
    },
    status:{
    type:String,
    default:"active"
      }


});
pessangerBookingSchema.plugin(mongoosePaginate);
var pessanger=mongoose.model('pessangerBooking',pessangerBookingSchema)

module.exports = pessanger; 



