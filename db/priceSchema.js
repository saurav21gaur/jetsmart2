var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var priceSchema = new Schema({
  

    aircraft_id:{
      type: mongoose.Schema.Types.ObjectId,
      ref:'aircraft'
    },
    price:{
      type:Number
    },
    REG:{
      type:String
    },
    
    capacity:{
       type:Number
    },

    status:{type:String,default:"active"},
   	createdBy:{type:String},
    createdAt:{type:Date, Default:Date.now}
  });

var price=mongoose.model('aircraftPrice',priceSchema)
module.exports = price; 




