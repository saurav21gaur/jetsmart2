var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var regionSchema = new Schema({
   	regionName:{type:String,required:true,unique:true},
   	country:{type: mongoose.Schema.Types.ObjectId,ref: 'country'},
   	createdBy:{type:String},
    createdAt:{type:Date, Default:Date.now}
  });
var region=mongoose.model('region',regionSchema)
module.exports = region; 



