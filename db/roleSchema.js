var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var roleSchema = new Schema({

      access_level: {
        Aircraft: {
            viewAircraft:{
              type: Boolean,
                default: false
           },
           editAircraft :{
            type:Boolean,
            default:false
           }       
        },
         Airport:{
            viewAirport:{
                type:Boolean,
                default:false
            },
            editAirport:{
                type:Boolean,
                default:false
            }
         },
         Country:{
            viewCountry:{
                type:Boolean,
                default:false
            },
            editCountry:{
                type:Boolean,
                default:false
            }
         },
             Region:{
            viewRegion:{
                type:Boolean,
                default:false
            },
            editRegion:{
                type:Boolean,
                default:false
            }

         },
         State:{
            viewState:{
                type:Boolean,
                default:false
            },
            editState:{
                type:Boolean,
                default:false
            },
        },
            City:{
                viewCity:{
                    type:Boolean,
                    default:false
                },
                editCity:{
                    type:Boolean,
                    default:false
                }
            },
            DestinationCity:{
                viewDestinationCity:{
                    type:Boolean,
                    default:false
                },
                edit:{
                    type:Boolean,
                    default:false
                }
            },
            AirportDistance:{
             viewAirportDistance:{
                type:Boolean,
                default:false
             },
             editAirportDistance:{
                type:Boolean,
                default:false
             }

            },
            User:{
                viewUser:{
                    type:Boolean,
                    default:false
                },
                editUser:{
                    type:Boolean,
                    default:false
                }
            },
            Agent:{
                viewAgent:{
                    type:Boolean,
                    default:false
                },
                editAgent:{
                    type:Boolean,
                    default:false
                }
            }

         },
        created_at: {
        type: Date,
        default: Date.now
    },
    is_active: {
        type: String,
        default: 'active'
    }
  });
var role=mongoose.model('oprator',roleSchema)
module.exports = role; 



