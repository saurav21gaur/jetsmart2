var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var stateSchema = new Schema({
   stateName:{type:String,required:true,unique:true},
    countryCode:{type:String,required:true,unique:true},
    status:{type:String,default:"active"},
    currencyCode:{type:String},
   	createdBy:{type:String},
    createdAt:{type:Date, default:Date.now}
  });
var country=mongoose.model('state',stateSchema)
module.exports = country; 



