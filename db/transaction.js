var mongoose = require('mongoose');
mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;

var transactionSchema = new Schema({
   

   order_id:{
    type:String
   },
   amount:{
    type:Number
   },
   
   payment_type:{
    type:String
   },

   currency:{
    type:String
   },

split_information:{
  recipient_id:{
    type:String
   },
   transaction_id:{
    type:String
   },
   split_amount:{
    type:Number
   }
 },

   user_id:{
    type:mongoose.Schema.Types.ObjectId,
    ref:"oprator"
   },
   booking_id:{
    type:String,
    ref:"pessangerBooking"
   },
   payment_id:{
    type:String
   }, 
   
  created_at: {
  type: Date,
  default: Date.now
    },
  updated_at:{
    type:Date
    },
    status:{
    type:String,
    default:"active"
      }


});
transactionSchema.plugin(mongoosePaginate);
var transaction=mongoose.model('transaction',transactionSchema)

module.exports = transaction; 



