var mongoose = require('mongoose');
mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;

var userSchema = new Schema({
   
   user_name:{
    type:String
   },
   password:{
    type:String
   },
   phoneNumber:{
    type:Number
   },
   email_id:{
    type:String
   },
  
  created_at: {
  type: Date,
  default: Date.now
    },
  updated_at:{
    type:Date
    },
    status:{
    type:String,
    default:"active"
      }


});

var user=mongoose.model('user',userSchema)

module.exports = user; 



