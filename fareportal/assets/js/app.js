(function(){
	var dependency = ['ui.router','ngStorage', 'angucomplete-alt', 'ui.bootstrap','moment-picker']
	
	angular.module('jetSmart', dependency)
	.run(function($location,user,$rootScope,$state) {
			console.log(JSON.stringify($location.$$absUrl))
			loc1 = $location.$$absUrl; 
			//console.log(loc1)   
	    if(location.href.indexOf('code') > -1){    
	        var code=location.href.split("?")[1].split("=")[1];
	        code=code.substr(0,code.length);
	        //console.log("Code =>"+code);

	        user.showFlightDetail(code)
	        .then(function(detail){
	        	 $rootScope.$broadcast("flight detail found",detail);
	        },function(err){
	        	//console.log("err-->",err)
	        })
	    }
	     console.log("location =>"+location.search);
	    if(location.href.indexOf('status') > -1){    
	        var status=location.search.split("?")[2].split("=")[1];
	         status=status.substr(0,status.length);
	         console.log("status =>"+status);
	         $rootScope.$broadcast("login status",status);
	    }
	})
	.config(['$stateProvider','$urlRouterProvider', '$locationProvider','$httpProvider', function($stateProvider, $urlRouterProvider, $locationProvider,$httpProvider) {
		console.log('app initiate')
		baseUrl = ''
		 $locationProvider.hashPrefix('');
	   // $locationProvider.html5Mode(true);
	    
		$urlRouterProvider.otherwise('/');
		 $httpProvider.interceptors.push(function interceptor($window,$state,$q){
        var interceptor = {
            request: function(config) {
            	console.log("datat",$window.localStorage.usertoken)
                if ($window.localStorage.usertoken) {
                    config.headers['Authorization'] = $window.localStorage.usertoken;
                }
                return config;
            },
            responseError:function(data){
              console.log("interceptor data--->",data)
              if(data.status==401)
                $state.go("home")
              return $q.reject(data);
            }

        };
        return interceptor;
    });
		/**/
		$stateProvider
		.state('home', {
			url: '/',
			views : {
				'mainView':{
					templateUrl: 'assets/views/home.html',
					controller: 'homeCtrl'
				},
			}
		})
		.state('searchFlight', {
			url: '/search',
			views : {
				'mainView':{
					templateUrl: 'assets/views/search.html',
					controller: 'searchCtrl'
				},
			}
		})
		.state('flightDetails', {
			url: '/inquiry',
			views : {
				'mainView':{
					templateUrl: 'assets/views/inquery.html',
					controller: 'flightDetailsCtrl'
				},
			}
		})
		.state('booking_page', {
			url: '/booking_page',
			views : {
				'mainView':{
					templateUrl: 'assets/views/booking_page.html',
					controller: 'helicopterCtrl'
				},
			}
		})
		.state('seatBooking', {
			url: '/seatBooking',
			views : {
				'mainView':{
					templateUrl: 'assets/views/seatBooking.html',
					controller: 'seatBookingCtrl'
				},
			}
		})
		.state('bookingInquiry', {
			url: '/bookingInquiry',
			views : {
				'mainView':{
					templateUrl: 'assets/views/booking_inquiry.html',
					controller: 'flightDetailsCtrl'
				},
			}
		})
		.state('history', {
			url: '/booking/history',
			views : {
				'mainView':{
					templateUrl: 'assets/views/history.html',
					controller: 'historyCtrl'
				},
			}
		})
		.state('viewHistory', {
			url: '/booking/viewHistory',
			views : {
				'mainView':{
					templateUrl: 'assets/views/viewHistory.html',
					controller: 'viewHistoryCtrl'
				},
			}
		})
		.state('about', {
			url: '/about',
			views : {
				'mainView':{
					templateUrl: 'assets/views/about.html',
					controller: 'homeCtrl'
				},
			}
		})
		.state('faqs', {
			url: '/faqs',
			views : {
				'mainView':{
					templateUrl: 'assets/views/faqs.html',
					controller: 'homeCtrl'
				},
			}
		})
		.state('ourTeam', {
			url: '/ourTeam',
			views : {
				'mainView':{
					templateUrl: 'assets/views/ourteam.html',
					controller: 'homeCtrl'
				},
			}
		})
		.state('termsCondition', {
			url: '/termsCondition',
			views : {
				'mainView':{
					templateUrl: 'assets/views/terms-and-conditions.html',
					controller: 'homeCtrl'
				},
			}
		})
		.state('policy', {
			url: '/policy',
			views : {
				'mainView':{
					templateUrl: 'assets/views/policy.html',
					controller: 'homeCtrl'
				},
			}
		})
		.state('disclaimer', {
			url: '/disclaimer',
			views : {
				'mainView':{
					templateUrl: 'assets/views/disclaimer.html',
					controller: 'homeCtrl'
				},
			}
		})
		.state('contact', {
			url: '/contact',
			views : {
				'mainView':{
					templateUrl: 'assets/views/contact.html',
					controller: 'homeCtrl'
				},
			}
		})
		.state('emptyLeg', {
			url: '/empty_leg',
			views : {
				'mainView':{
					templateUrl: 'assets/views/empty_leg.html',
					controller: 'homeCtrl'
				},
			}
		})
	    /**/
	}])
	.filter('startFrom', function() {
	    return function(input, start) {
	        start = +start; //parse to int
	        return input.slice(start);
	    }
	})
	.directive('numOnly', function() {
	return {
	  require: 'ngModel',
	  restrict: 'A',
	  link: function(scope, element, attrs, modelCtrl) {
	    modelCtrl.$parsers.push(function(inputValue) {
	      if (inputValue == null)
	        return ''
	      cleanInputValue = inputValue.replace(/[^0-9]+/, '');
	      if (cleanInputValue != inputValue) {
	        modelCtrl.$setViewValue(cleanInputValue);
	        modelCtrl.$render();
	      }
	      return cleanInputValue;
	    });
	  }
	}
	
	
})
	

})()