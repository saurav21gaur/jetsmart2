angular.module('jetSmart')
.controller('flightDetailsCtrl', ['$window','$scope', 'user', 'custom','$state','$filter','$timeout','$window','$location', function($window,$scope, user, custom,$state,$filter,$timeout,$window,$location){
	$('body').removeClass('active-menu');
	console.log('flightDetailsCtrl init')
	$window.scrollTo(0, 0);
	
	 // $scope.start = true;
	$scope.showAircraft = true;
	$scope.showPaymentDetail = false;
	$scope.arrDetail = [{}];
	if(!localStorage.usertoken){
		$state.go('home');
	}

	// $scope.Identity = "Identity";
	// console.log("data =>"+localStorage.localData)
	var localData = JSON.parse(localStorage.localData)
	$scope.routeDetails = localData.roots;
	$scope.aircraft = JSON.parse(localStorage.aircraft)

	var userInfo = JSON.parse(localStorage.user);
	 console.log('data =>',JSON.stringify($scope.aircraft))
	var dda = parseInt($scope.aircraft.total)

	totalAmount = 100000;
	// var jet = JSON.parse(localStorage.jet)
	$scope.goBack = function(){
		$state.go('searchFlight');
	}


	// $scope.items = [{"name":"Mr."},{"name":"Mrs."},{"name":"Ms."},{"name":"Dr."}];

	$scope.ageType = function(age, idx){
		console.log("Age =>"+age)
		if(age <= 2 && age > 0)
			$scope.arrDetail[idx].ageUserType = 'Infant'
		else if(age >= 2 && age <= 12)
			$scope.arrDetail[idx].ageUserType = 'Child'
		else if(age >= 12)
			$scope.arrDetail[idx].ageUserType = 'Adult'
		else if(age == null)
			$scope.arrDetail[idx].ageUserType = '' 
	}

	$scope.contactDetails = {
		title:'Mr.'
	}
	$scope.addPasenger = function(x){
		var obj=Object.create(x)
		$scope.arrDetail.push(obj);
		console.log("array "+JSON.stringify($scope.arrDetail))
		$scope.arrDetail[$scope.arrDetail.length-1]={};
		
	}
	$scope.rmPasenger = function(x){
		 var index = $scope.arrDetail.indexOf(x);
		  if($scope.arrDetail.length == 1){
		  	return $scope.arrDetail;
		  }
		  else{
		  	$scope.arrDetail.splice(index,1);  
		  }
	}
	$scope.passengerDetails = []
	$scope.isDetailsSubmit = false
	$scope.isAllOk = false

	// $scope.contactDetails.name = 'Abc';
	// $scope.contactDetails.phone_number = 9999999999;
	// $scope.contactDetails.alternate_number = 9999999999;
	// $scope.contactDetails.email = 'abc@gmail.com';

	// for (var i = 0; i < $scope.arr.length; i++) {
	// 	$scope.arr[i] = {
	// 		title : 'Mr.',
	// 		food : 'veg',
	// 		nationlity : 'indian',
	// 	}
	// }

	console.log(localData)

	$scope.createInquiry = function(){
		$scope.arr = $scope.arrDetail;
		console.log("req =>"+JSON.stringify($scope.arr))
		console.log($scope.contactDetails.phone_number.toString())
		// if($scope.contactDetails.phone_number.toString().length != 10){
		// 	custom.alert('Phone Number Must be 10 digit','Error')
		// 	return
		// }
		// if($scope.contactDetails.alternate_number.toString().length != 10){
		// 	custom.alert('Alternate Mobile Number Must be 10 digit','Error')
		// 	return
		// }
		console.log("booking =>",$scope.paymentBookingId)
		$scope.inqueryObj = {
			booking_id:$scope.paymentBookingId,
			pessanger_detail : $scope.arr,
			contact : $scope.contactDetails
		}
		user.updateBooking($scope.inqueryObj)
		.then(function(objS){
			if(objS.status===200){
				localStorage.viewBookingId = $scope.paymentBookingId;
		 		$state.go('viewHistory');	
			}
			console.log("Response=>"+JSON.stringify(objS))
		},function(objE){
			console.log(objE)
		})



		// $scope.arr = $scope.arrDetail;
		// $scope.inqueryObj = {
		// 	user_id: userInfo._id,
		// 	roots : localData.roots,
		// 	pessanger_detail : $scope.arr,
		// 	contact : $scope.contactDetails,
		// 	order_id : localStorage.order_id,
		// 	aircraftId : $scope.aircraft._id
		// }
		// for (var i = 0; i < $scope.inqueryObj.roots.length; i++) {
		// 	delete $scope.inqueryObj.roots[i].departureTime
		// }
		// user.inquiry($scope.inqueryObj)
		// .then(function(objS){
		// 	if(objS.status===200){
		// 		localStorage.viewBookingId = objS.data.booking_id;
		// 		$state.go('viewHistory');
		// 		var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
		// 		var date = new Date(objS.data.created_at)
		// 		$scope.booking = {
		// 			number : objS.data.bookingNumber,
		// 			time : (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?'pm':'am')
		// 		}
		// 	}
		// 	console.log("Response=>"+JSON.stringify(objS))
		// },function(objE){
		// 	console.log(objE)
		// })
	}

	// $scope.skip =function(){
		// 	$scope.arr = [];
		// 	$scope.contactDetails = {};
		// 	$scope.inqueryObj = {
		// 		booking_id:$scope.paymentBookingId,
		// 		pessanger_detail : $scope.arr,
		// 		contact : $scope.contactDetails
		// 	}
		// 	user.updateBooking($scope.inqueryObj)
		// 	.then(function(objS){
		// 		if(objS.status===200){
		// 			$scope.$parent.userInfo = {};
		// 			$window.localStorage.clear();
		//     		$location.path('/');
		// 		}
		// 		console.log("Response=>"+JSON.stringify(objS))
		// 	},function(objE){
		// 		console.log(objE)
		// 	})
		// $scope.arr = [];
		// $scope.contactDetails = {};
		// $scope.inqueryObj = {
		// 	user_id: userInfo._id,
		// 	roots : localData.roots,
		// 	pessanger_detail : $scope.arr,
		// 	contact : $scope.contactDetails,
		// 	order_id : localStorage.order_id,
		// 	aircraftId : $scope.aircraft._id
		// }
		// for (var i = 0; i < $scope.inqueryObj.roots.length; i++) {
		// 	delete $scope.inqueryObj.roots[i].departureTime
		// }
		// user.inquiry($scope.inqueryObj)
		// .then(function(objS){
		// 	$scope.$parent.userInfo = {};
		// 	$window.localStorage.clear();
  		//	$location.path('/');
		// 	if(objS.status===200){

		// 		var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
		// 		var date = new Date(objS.data.created_at)
		// 		$scope.booking = {
		// 			number : objS.data.bookingNumber,
		// 			time : (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?'pm':'am')
		// 		}
		// 	}
		// 	console.log("Response=>"+JSON.stringify(objS))
		// },function(objE){
		// 	console.log(objE)
		// })

		
	// }

	$scope.submitInquiry = function(){
		user.inquiry($scope.inqueryObj)
		.then(function(objS){
			if(objS.status===200){
				$scope.isDetailsSubmit = false;
				$scope.start =false;
				$scope.isAllOk = true
				var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
				var date = new Date(objS.data.created_at)
				$scope.booking = {
					number : objS.data.bookingNumber,
					time : (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?'pm':'am')
					// time : (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?'pm':'am')
				}
				// var req = {
				// 	"user_id": userInfo._id,
				// 	"booking_id":objS.data._id,
				// 	"amount":1000
				// }
				// user.getOrder(req)
				// .then(function(objS){
				// 	if(objS.status===200){
				// 		$scope.order_id = objS.data.order_id;
				// 	}
				// 	console.log("Response order=>"+JSON.stringify(objS))
				// },function(objE){
				// 	console.log(objE)
				// })
			}
			console.log("Response=>"+JSON.stringify(objS))
		},function(objE){
			console.log(objE)
		})
	}

	var minimum_donation_amount = 100; // ₹100
	$scope.payment =function(){
		$scope.payData = {};
		// var rupeeAmount = parseInt(document.getElementById('amount-field').value);
  //     if (!rupeeAmount || rupeeAmount < minimum_donation_amount) {
  //       return alert('Please enter valid amount. minimum_donation_amount is ₹ 100')
  //     }
      var options = {
          "key": "rzp_test_REEE8skw8OMLSt",
          "amount": totalAmount, // we take amount in paise
          "name": "JetSmart",
          "description": localStorage.order_id,
          "image": "assets/img/logo.png",
          "handler": function (response){
			          	var data = {"payment_id" :response.razorpay_payment_id}
					    user.paymentSave(data)
						.then(function(objS){
							$scope.payData = objS;
							$scope.arr = [];
								$scope.contactDetails = {};
								$scope.inqueryObj = {
									user_id: userInfo._id,
									roots : localData.roots,
									pessanger_detail : $scope.arr,
									contact : $scope.contactDetails,
									order_id : localStorage.order_id,
									aircraftId : $scope.aircraft._id
								}
								console.log("req =>"+JSON.stringify($scope.inqueryObj))
								for (var i = 0; i < $scope.inqueryObj.roots.length; i++) {
									delete $scope.inqueryObj.roots[i].departureTime
								}
								user.inquiry($scope.inqueryObj)
								.then(function(objS){
									if(objS.status===200){
										var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
										var date = new Date(objS.data.created_at)
										$scope.booking = {
											number : objS.data.bookingNumber,
											time : (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?'pm':'am')
										}
										$scope.paymentBookingId = objS.data.booking_id;
									}
									console.log("Response=>"+JSON.stringify(objS))
								},function(objE){
									console.log(objE)
								})
							swal({
		          			  title: "Payment Success",
							  text: $scope.payData.id,
							  buttons: true,
							  success: true,
							})
							.then((ok) => {
							  if (ok) {
							  	$scope.paymentDetail = $scope.payData;
							  	console.log("payment =>"+$scope.paymentDetail)
							  } 
							});
							$scope.paymentDetail = objS;
							$scope.showAircraft = false;
							$scope.showPaymentDetail = true;
							console.log("Response order=>"+JSON.stringify(objS))
						},function(objE){
							console.log(objE)
						})

          },
          "prefill": {
              "name": userInfo.user_name,
              "email": userInfo.email
          },
          "notes": {
              "invoice_id": ''
          },
          "theme": {
              "color": "#EE4E0C"
          }
      };
      new Razorpay(options).open();
	}

	$scope.nextForm = function(){
		$scope.showAircraft = false;
		$scope.showPaymentDetail = false;
		$scope.start = true;
	}
}])