angular.module('jetSmart')
.controller('globalCtrl',['$window','$scope', '$location', '$rootScope', 'custom', 'user', '$state', '$window', function($window,$scope, $location, $rootScope, custom, user, $state, $window){
	$('body').removeClass('active-menu');
	$rootScope.signup = {}
	$rootScope.login = {}
	$window.scrollTo(0, 0);
	$rootScope.isLoginForm = false
	$scope.forgot = false;
	if(localStorage.user){
		$scope.userInfo = JSON.parse(localStorage.user)
		console.log($scope.userInfo)
	}

	console.log(location.search)
	if(location.href.indexOf('status') > -1){    
        var status=location.search.split("?")[2].split("=")[1];
	         status=status.substr(0,status.length);
	         console.log("styyyy",status)
    }

	$scope.isActive = function(route) {
		return route === $location.path();
	}



	$scope.submitLogin = function(d){
		user.login(d)
		.then(function(objS){
			console.log(objS)

			if(objS.data.code == 204){
				swal(objS.data.message)
				$rootScope.login.password = ''
			}
			if(angular.isDefined(objS.data.token)){
				if(objS.data.result.type == 'oprator'){
					let base = 'http://35.154.217.225/'
					localStorage.userType = objS.data.result.type
					$window.location.href = 'http://35.154.217.225/admin#/main/dashboard'

				}else{
					localStorage.usertoken = objS.data.token;
					console.log("res =>",objS.data.result._id);
					console.log("sattus =>",status)
			        if(status == objS.data.result._id){
			        	$state.go('seatBooking');
			        }
			        else if(status == 0){
			        	$state.go('seatBooking');
			        }
					localStorage.user = JSON.stringify(objS.data.result)
					// console.log("data =>"+localStorage.user)
					$scope.userInfo = JSON.parse(localStorage.user)
					$scope.pdfShow=true;
					// alert("Login Successfully! This Section is Under Progress");
					console.log(objS.data.result.type)
					localStorage.userType = objS.data.result.type
					$rootScope.isLoginForm = false
					// $state.reload();
					$rootScope.login.password = ''
				}
				// $state.go('flightDetails')
			}
		},function(objE){
			console.log(objE)
		})
	}

	$scope.forgotPassword = function(){
		$scope.forgot = true;
	}
	$scope.loginClick = function(){
		$scope.forgot = false;
	}

	$scope.submitforgotPassword = function(forgotPwd){
		console.log(forgotPwd.email)
		user.forgotPassword({"email":forgotPwd.email})
		.then(function(objS){
			
			if(objS.data.status == true)
				{
					swal(objS.data.response);
					$scope.forgot = false;
				}
			else
				swal(objS.data.responseMessage)	
			console.log(objS)
		},function(objE){
			console.log(objE)
		})
	}
	
	$scope.submitSignup = function(d){
		var dd = angular.copy(d);
		dd.phone_number = parseInt('91'+dd.phone_number.toString());
		$scope.phn =dd.phone_number;
		console.log("phnnnn",dd)
		user.signup(dd)
		.then(function(objS){
			console.log(objS)
			if(objS.status == 200){
				if(objS.data.code == 200){
					$scope.isOTP = true;
					$scope.verifyOtpObj = {
						phone_number : dd.phone_number,
					};
					 // $rootScope.signup={}
					 $rootScope.isLoginForm = true;
					swal("OTP sent successfully to "+dd.phone_number )
				}
				else
					swal(objS.data.message)
			}
			
		},function(objE){
			console.log(objE)
		})
	}

	$scope.resendOTP = function(){
		console.log("data===>",$scope.phn)
		user.resend_otp({phone_number:$scope.phn}).then(objS=>{
			swal("OTP sent successfully.")
			console.log("objS",objS)
		},objE=>{
			console.log("objE",objE)
		})	
	}	

	$scope.changePwd = function(){
		console.log("cahnge")
	}

	$scope.logout = function(){
		localStorage.removeItem(user)
		
		user.logout({_id:(JSON.parse(localStorage.user))._id}).then(objS=>{
			console.log(objS)
			delete localStorage.userType
			delete localStorage.user
			delete localStorage.usertoken
			delete localStorage.userInfo
			delete localStorage.localData
			delete localStorage.order_id
			delete localStorage.aircraft
			delete localStorage.viewBookingId
			$scope.userInfo = undefined;
			// $location.path('/?');
			window.location.href="https://gojetsmart.com";
		},objE=>{
			console.log(objE)
		})
		// $window.localStorage.clear();
    	
    	angular.element('body').removeClass('active-menu');
    	// $state.reload();
	}

	$scope.verifyOtp = function () {
		// if(!$scope.verifyOtpObj.otp || $scope.verifyOtpObj.otp == '') {
		// 	swal('Please enter OTP','error');
		// 	return;
		// };
		console.log("$scope.verifyOtpObj",$scope.verifyOtpObj);

		user.verifyOtp($scope.verifyOtpObj).then(function(objS){
			console.log(objS)
			if(objS.data.code == 200){	
				 swal(objS.data.message,'success')
				 $rootScope.activeJustified = 0;
				// if(status == 0){
		  //       	$state.go('seatBooking');
		  //       }else{
		  //       	$state.reload();
		  //       }
				 
				$scope.verifyOtpObj = {}
				$scope.isOTP = false;
				return
			}
			else
				{swal(objS.data.message,'error')
				$scope.verifyOtpObj.otp = '';}
		},function(objE){
			swal(objE.data.message,'error')
		})
	}
}])
