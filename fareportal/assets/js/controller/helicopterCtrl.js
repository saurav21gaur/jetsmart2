angular.module('jetSmart')
.controller('helicopterCtrl',['$window','$scope', '$rootScope', 'custom', 'user', '$state','pdf', 'search','$timeout', function($window,$scope, $rootScope, custom, user, $state,pdf, search,$timeout){
	$('body').removeClass('active-menu');
	$scope.showEmptyLeg = [];
	$scope.showJet = [];
	$window.scrollTo(0, 0);
	$scope.airportDropdown = [];
	$scope.userData={salutation:"Mr."};
	 $scope.showMail = false;


	var searchDummyObj = {
		tripType : 'oneWay',
		traveller : '1',
		nationality : 'Indian',
		price : '10000000',
		searchType : 'normalSearch',
		roots : [
			{	
				originName : '',
				origin : '',//59d628eeb4dc9618b579e9bc
				destinationName : '',
				destination : '',//59d628eeb4dc9618b579e88f
				departureDate : '',
				departureTime : '',
			},
			{	
				originName : '',
				origin : '',
				destinationName : '',
				destination : '',
				departureDate : '',
				departureTime : '',
			},
		],
		aircraft_type : {
			heavy_jet : false,
			super_mid_jet : false,
			mid_size_jet : false,
			light_jet : false,
			turbo_props : false,
		},
		facility : {
			flight_attendant : false,
			lavatory : false,
			satellite_phone : false,
			wifi : false,
		},
	}

	// var searchDummy2Obj = {
	// 	tripType : 'multicity',
	// 	traveller : '1',
	// 	nationality : 'Indian',
	// 	price : '10000000',
	// 	searchType : 'normalSearch',
	// 	roots : [],
	// 	aircraft_type : {
	// 		heavy_jet : false,
	// 		super_mid_jet : false,
	// 		mid_size_jet : false,
	// 		light_jet : false,
	// 		turbo_props : false,
	// 	},
	// 	facility : {
	// 		flight_attendant : false,
	// 		lavatory : false,
	// 		satellite_phone : false,
	// 		wifi : false,
	// 	},
	// }



	$scope.searchObj = searchDummyObj


	$scope.returnD = function(data){
		console.log("data",data);
		$scope.mm = data;
	}

	$scope.submitForm=function(d){
			// $scope.mailData = $scope.userData;
			 $scope.showMail = true;
			
		var date=new Date();
		$scope.todayDate = date.getDate() +'/'+ (parseInt(date.getMonth())+1) + '/' + date.getFullYear()
		switch (d.tripType){
			case 'oneWay':
				d.roots = [d.roots[0]];
				console.log('oneWay =>', d);
				break;
			case 'roundTrip':
				d.roots[1] = {
					origin: d.roots[0].destination,
					destination : d.roots[0].origin,
					originName: d.roots[0].destinationName,
					destinationName : d.roots[0].originName,
					departureDate : d.roots[1].departureDate,
					departureTime : d.roots[1].departureTime,
				}
				d.roots = [d.roots[0],d.roots[1]];
				console.log('roundTrip =>',d)
				break;
			case 'multicity':
				console.log('multicity =>',d)
				localStorage.localData = d;//obj
				break;
		}
		console.log('$scope.mailData',$scope.searchObj)
		 
		 // pdf.bookHelicopter('pdf-dynamic','#mailFormat');
		 $timeout(function(){
				pdf.bookHelicopter('pdf-dynamic','#mailFormat').then(objS=>{
					console.log(objS);
					custom.alert("Thank you for your query. Your registration no. is:"+objS.data.registration_no,"JetSmart");
					 $state.reload();
				},objE=>{
					console.log(objE)
				})
				$timeout(function(){
					$('#pdf-dynamic').css('display','none');
					$scope.showMail = false;
					
				},100)
			},10);
		 // $scope.showMail = false;
		 // $timeout(function() {$scope.showMail = false;}, 1000);
	}



	

	search.getAll().then(function(objS){
		console.log('all details')
		console.log(objS)
		var useableData = []
		for (var i = 0; i < objS.length; i++) {
			var allData = objS[i]
			/**/
			for (var j = 0; j < allData.airportList.length; j++) {
				var airport = allData.airportList[j]
				useableData.push({
					airport : airport.airport,
					stateName : allData.states.stateName,
					countryName : allData.countryName,
					cityName : allData.states.cities.cityName,
					iata : airport.iata,
					icao : airport.icao,
					_id : airport._id,
				})
			}
			/**/
		}

		$scope.airportDropdown = useableData
		console.log( $scope.airportDropdown)
		// console.log(JSON.stringify($scope.airportDropdown))
		// console.log(JSON.stringify(objS))
	},function(objE){console.log(objE)})


	$scope.changeDat = function(ind){
		console.log("ind",ind)
		for(var i=0 ;i<$scope.searchObj.roots.length;i++){
			if(ind < i)
				$scope.searchObj.roots[i].departureDate = ""
		}
		console.log("ddddd",$scope.searchObj.roots)
		
	}



	$scope.addMulticity = function(index){
		var obj = {	
				originName : $scope.searchObj.roots[$scope.searchObj.roots.length - 1].destinationName,
				origin : $scope.searchObj.roots[$scope.searchObj.roots.length - 1].destination,
				destinationName : '',
				destination : '',
				departureDate : '',
				departureTime : '',
			};
		$scope.searchObj.roots.splice(index+1, 0, obj);
		console.log("addMulticity", $scope.searchObj)
	}
	$scope.removeMulticity = function(indexOffers){
		console.log("removeMulticity", indexOffers, $scope.searchObj.roots.length)
		if($scope.searchObj.roots.length >2)
		$scope.searchObj.roots.splice(indexOffers, 1);
	}


	$scope.autoFill = function(index){
		setTimeout(function(){
			if(angular.isDefined($scope.searchObj.roots[index+1]))
			$scope.searchObj.roots[index+1].originName = $scope.searchObj.roots[index].destinationName;
		console.log("autoFill", $scope.searchObj.roots[index+1].originName)
		}, 500)
	}

	
	
}])