angular.module('jetSmart')
.controller('historyCtrl',['$window','$scope', '$rootScope', 'custom', 'user', '$state', function($window,$scope, $rootScope, custom, user, $state){
	$('body').removeClass('active-menu');
	$scope.showEmptyLeg = [];
	$scope.showJet = [];
	$window.scrollTo(0, 0);
	$scope.booking = function(){
		$state.reload();
	}

	if(!localStorage.usertoken){
		$state.go('home');
	}
	var info = JSON.parse(localStorage.user)
	console.log("value"+JSON.stringify(info))

	user.history({user_id : info._id})
	.then(objS=>{
		$scope.BookingList = objS;
		for(var i=0; i<$scope.BookingList.length;i++){
			if($scope.BookingList[i].empty_jetId){
			 $scope.showEmptyLeg.push($scope.BookingList[i]);
			}
			else{
				$scope.showJet.push($scope.BookingList[i]);
			}
			console.log("sssss",JSON.stringify($scope.showEmptyLeg))
		}
		console.log(JSON.stringify($scope.showJet))
	},objE=>{
		console.log(objE);
	})
	$scope.view = function(id){
		localStorage.viewBookingId = id;
		$state.go('viewHistory');
	}
}])