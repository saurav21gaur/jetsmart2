angular.module('jetSmart')
.controller('homeCtrl', ['$window','$scope', 'search', '$filter', '$localStorage', '$state', 'custom', '$rootScope','user','$timeout','pdf', function($window,$scope, search, $filter, $localStorage, $state, custom, $rootScope,user,$timeout,pdf){
	console.log('homeCtrl atteched')
	$('body').removeClass('active-menu');
	$scope.airportDropdown = [];
	$window.scrollTo(0, 0);
	$scope.showAirportList = false;
	$scope.searchType = false;
	$scope.isPaymentForm = false;
	$scope.paymentData = {};
	$scope.showPayment= false;

	

	$scope.formats = [
      'HH:mm',
      'MMM Do YY',
      'MMMM Do YYYY, h:mm:ss a',
      'YYYY [escaped] YYYY',
      'LT',
      'LTS'
    ];
    $scope.dataVal = function(data){
    	console.log("ddddd",data)
    }

	var owl = $('#common-misconception .owl-carousel.owl-theme');
	var owl1= $('#aircrafts .owl-carousel.owl-theme');
	var obj={
			margin: 0,
			nav: false,
			center:false,
			autoplay: true,	
			autoplayTimeout: 10000,
			autoplayHoverPause: true, 
			loop: true,
			responsive: {
			  0: {
				items: 1
			  },
			  600: {
				items: 1
			  },
			  1000: {
				items: 1
			  }
			}
			}
	if(owl.length>0){
		owl.owlCarousel(obj);
	}
	if(owl1.length>0){
		owl1.owlCarousel(obj);
	}

	$scope.openPayment = function(){
		$scope.isPaymentForm = true;
	}

	$scope.closePayment = function(){
		$scope.isPaymentForm = false;
		// $state.reload();
	}

	$scope.payment= function(){
		$scope.isPaymentForm = false;
		var req = {
					"user_id": $scope.$parent.userInfo._id,
					"amount":$scope.paymentData.amount
				}
		user.getOrder(req)
		.then(function(objS){
			if(objS.status===200){
				// localStorage.order_id = objS.data.order_id;
				var options = {
			         "key": "rzp_live_PVze92nkZ4orDp",
			         "amount": $scope.paymentData.amount*100, // we take amount in paise
			         "name": "JetSmart",
			         "description": objS.data.order_id,
			         "image": "assets/img/logo.png",
			         "handler": function (response){
			                          swal(response.razorpay_payment_id);
			         },
			         "prefill": {
			             "name": $scope.paymentData.user_name,
			             "email": $scope.paymentData.email
			         },
			         "notes": {
			             "invoice_id": ''
			         },
			         "theme": {
			             "color": "#EE4E0C"
			         }
			     };
			     new Razorpay(options).open();
			}
			console.log("Response order=>"+JSON.stringify(objS))
		},function(objE){
			console.log(objE)
		})
		
		
	}
	$scope.closeForm = function(){
		// $scope.isLoginForm = false;
		$state.reload();
	}
	$scope.closeForm11 = function(){
		 $rootScope.isLoginForm = !$rootScope.isLoginForm;
		
	}

	$scope.openLoginForm = function(){
		$rootScope.isLoginForm = !$rootScope.isLoginForm;
		// $state.reload();
	}
	if(localStorage.user)
		{var userInfo = JSON.parse(localStorage.user);
			$scope.showPayment= true;
		console.log(userInfo._id)}

console.log(location.search)
	if(location.href.indexOf('status') > -1){    
	          var status=location.search.split("?")[2].split("=")[1];
	         status=status.substr(0,status.length);
	          console.log("status =>"+status);
	          $rootScope.$broadcast("login status",status);
	         if(status == 0){
	         	console.log("User not registered...");
	        	$rootScope.activeJustified = 1;
	         	$scope.openLoginForm();
	        	
	        }
	        else  if(localStorage.user && status == userInfo._id){
	        	$state.go('seatBooking');
	         }
	        else{
	        	 $scope.openLoginForm();
	        }

	    }


	user.getOffers().then(function(objS){
		  console.log("val =>"+JSON.stringify(objS))
		for(var i=0;i<objS.length;i++){
			var origin = (objS[i].originName).split(",");
			objS[i].originNew =  origin[0];
			var destination = (objS[i].destinationName).split(",");
			objS[i].destinationNew =  destination[0];
		}
		
		$scope.offers = objS;
		if($scope.offers.length == 0)
			$scope.showOffer = false;
		else
			$scope.showOffer = true;

		// console.log("offers len =>",$scope.offers.length)
	},function(objE){consol.log(objE)})

	$scope.returnD = function(data){
		console.log("data",data);
		$scope.mm = data;
	}

	$scope.checkUser = function(data){
		console.log("data =>"+JSON.stringify(data))
		if(typeof localStorage.user == 'undefined' || typeof localStorage.usertoken == 'undefined'){
			$scope.tglOffer = false;
			$scope.openLoginForm();
		}else if(data.remainingSeats == 0){
			console.log("No seats available..")
		}
		else{

			localStorage.setItem('seatDetail', JSON.stringify(data))
			$state.go('seatBooking')
		}
	}

	// $scope.setMaxDeparture = function(vall){
	// 	console.log("valll",vall);
	// 	$scope.maxVal = vall;
	// }
	$scope.changeDat = function(ind){
		console.log("ind",ind)
		for(var i=0 ;i<$scope.searchObj.roots.length;i++){
			if(ind < i)
				$scope.searchObj.roots[i].departureDate = ""
		}
		console.log("ddddd",$scope.searchObj.roots)
		
	}

	search.getAll().then(function(objS){
		console.log('all details')
		console.log(objS)
		var useableData = []
		for (var i = 0; i < objS.length; i++) {
			var allData = objS[i]
			/**/
			for (var j = 0; j < allData.airportList.length; j++) {
				var airport = allData.airportList[j]
				useableData.push({
					airport : airport.airport,
					stateName : allData.states.stateName,
					countryName : allData.countryName,
					cityName : allData.states.cities.cityName,
					iata : airport.iata,
					icao : airport.icao,
					_id : airport._id,
				})
			}
			/**/
		}

		$scope.airportDropdown = useableData
		console.log($scope.airportDropdown)
		// console.log(JSON.stringify($scope.airportDropdown))
		// console.log(JSON.stringify(objS))
	},function(objE){console.log(objE)})

	var searchDummyObj = {
		tripType : 'oneWay',
		traveller : '1',
		nationality : 'Indian',
		price : '10000000',
		searchType : 'normalSearch',
		roots : [
			{	
				originName : '',
				origin : '',//59d628eeb4dc9618b579e9bc
				destinationName : '',
				destination : '',//59d628eeb4dc9618b579e88f
				departureDate : '',
				departureTime : '',
			},
			{	
				originName : '',
				origin : '',
				destinationName : '',
				destination : '',
				departureDate : '',
				departureTime : '',
			},
		],
		aircraft_type : {
			heavy_jet : false,
			super_mid_jet : false,
			mid_size_jet : false,
			light_jet : false,
			turbo_props : false,
		},
		facility : {
			flight_attendant : false,
			lavatory : false,
			satellite_phone : false,
			wifi : false,
		},
	}

	$scope.searchObj = searchDummyObj
	$scope.searchObj2= searchDummyObj
	
	console.log("$scope.searchObj", $scope.searchObj)

	$scope.resetSearchForm = function(){
		console.log(searchDummyObj)
		$scope.searchObj = {
		tripType : 'oneWay',
		traveller : '1',
		nationality : 'Indian',
		price : '10000000',
		searchType : 'normalSearch',
		roots : [
			{	
				originName : '',
				origin : '',//59d628eeb4dc9618b579e9bc
				destinationName : '',
				destination : '',//59d628eeb4dc9618b579e88f
				departureDate : '',
				departureTime : '',
			},
			{	
				originName : '',
				origin : '',
				destinationName : '',
				destination : '',
				departureDate : '',
				departureTime : '',
			},
		],
		aircraft_type : {
			heavy_jet : false,
			super_mid_jet : false,
			mid_size_jet : false,
			light_jet : false,
			turbo_props : false,
		},
		facility : {
			flight_attendant : false,
			lavatory : false,
			satellite_phone : false,
			wifi : false,
		},
	}
	}
	var desDat = [];
	$scope.submitSearch = function(d){
		console.log("datatttttt",d)


		/**/
		console.log("d.roots.length",d.roots)
		var isDateVerified = true;

		for (var i = 0; i < d.roots.length; i++) {
			console.log("dffgdfg",new Date(d.roots[i].departureDate+','+d.roots[i].departureTime))
			desDat[i] = (new Date(d.roots[i].departureDate+','+d.roots[i].departureTime)).getTime();
			console.log("depTime",desDat);

			for(var p=0;p<desDat.length;p++){
				console.log("in for loop", desDat[p] >= desDat[p+1])
				if(desDat[p] >= desDat[p+1]){
					isDateVerified = false;
				}
			}



			var _root = d.roots[i]

			var originArray = _root.originName.split(",");
			var destinationArray = _root.destinationName.split(",");

			for(var j = 0; j < $scope.airportDropdown.length; j++){
				if(angular.isDefined(originArray[1])){
					if(originArray[1].trim() == $scope.airportDropdown[j].airport){
						_root.origin = $scope.airportDropdown[j]._id
						break;
					}
				}
			}
			for(var j = 0; j < $scope.airportDropdown.length; j++){
				if(angular.isDefined(destinationArray[1])){
					if(destinationArray[1].trim() == $scope.airportDropdown[j].airport){
						_root.destination = $scope.airportDropdown[j]._id
						break;
					}
				}
			}
		}
		/**/
		if(!isDateVerified){
			custom.alert("Please Enter correct date and time.");
			desDat=[];
			return;
		}

		switch (d.tripType){
			case 'oneWay':
				d.roots = [d.roots[0]];
				console.log('oneWay =>', d);
				break;
			case 'roundTrip':
				d.roots[1] = {
					origin: d.roots[0].destination,
					destination : d.roots[0].origin,
					originName: d.roots[0].destinationName,
					destinationName : d.roots[0].originName,
					departureDate : d.roots[1].departureDate,
					departureTime : d.roots[1].departureTime,
				}
				d.roots = [d.roots[0],d.roots[1]];
				console.log('roundTrip =>',d)
				break;
			case 'multicity':
				console.log('multicity =>',d)
				localStorage.localData = d;//obj
				break;
		}
		localStorage.setItem('localData', JSON.stringify(d))
		var searchableObj = JSON.parse(localStorage.getItem('localData'))

		search.flights(searchableObj).then(function(objS){
			if(objS.message == 'No route found'){
				$state.reload();
				custom.alert('Thank you for your query, \n please call on (011 28050066) for this particular routing. or send us a query','JET SMART')
				
				// alert('our system is unable to calculate this route, \nplease contact our customer care')
			}else{
				$state.reload();
				$state.go('searchFlight')
			}
		})
	}

	$scope.autoFill = function(index){
		setTimeout(function(){
			if(angular.isDefined($scope.searchObj.roots[index+1]))
			$scope.searchObj.roots[index+1].originName = $scope.searchObj.roots[index].destinationName;
		}, 500)
	}

	$scope.addMulticity = function(index){
		var obj = {	
				originName : $scope.searchObj.roots[$scope.searchObj.roots.length - 1].destinationName,
				origin : $scope.searchObj.roots[$scope.searchObj.roots.length - 1].destination,
				destinationName : '',
				destination : '',
				departureDate : '',
				departureTime : '',
			};
		$scope.searchObj.roots.splice(index+1, 0, obj);
	}
	$scope.removeMulticity = function(indexOffers){
		$scope.searchObj.roots.splice(index, 1);
	}


	$scope.pay = function(){

		user.getOrder(req).then()
	}
	$scope.contact = {};
	$scope.showMail = false;
	$scope.sendMessage = function(){
		 $scope.showMail = true;
		// console.log("message clicked ",$scope.contact);
		// user.contact($scope.contact).then(function(objS){
		//   console.log("val =>e",objS)
		// },function(objE){
		// 	console.log(objE)
		// })

		$timeout(function(){
				pdf.contact('pdf-dynamic','#mailFormat',$scope.contact.subject).then(objS=>{
					console.log(objS);
					 custom.alert("Your query would soon be answered. Thank you for contacting JetSmart.","JetSmart");
					 $state.reload();
				},objE=>{
					console.log(objE)
				})
				$timeout(function(){
					$('#pdf-dynamic').css('display','none');
					$scope.showMail = false;
					
				},100)
			},10);
	}




	
}])
.factory("custom",['$q',function($q){
  var objCustom={}
  objCustom.alert=function(content,title){
    $.alert({
      title: title||"Error",
      content: content,
      animation: 'rotateYR',
      theme: 'supervan',
      closeAnimation: 'rotateYR',
       animationBounce: 2,
       animationSpeed:800
    });
  }

  return objCustom;
}])