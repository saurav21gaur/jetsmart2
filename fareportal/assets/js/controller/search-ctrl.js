angular.module('jetSmart')
.filter("dateconvertor1",function(){
	return function(input){
		var hour=Math.floor(input/60);
		var min=Math.floor(input%60);
		return hour+":"+min;
	}
})
.filter("roundoff",function(){
	return function(input){
		var min=Math.floor(input);
		return min;
	}
})
.filter("dateconvertor",function(){
	return function(input){
		return new Date(input);
	}
})
.controller('searchCtrl', ['$window','$scope', 'search', '$filter', '$state', 'custom', '$location', 'pdf','$timeout', '$rootScope','user', function($window,$scope, search, $filter, $state, custom, $location, pdf,$timeout, $rootScope,user){
	console.log('searchCtrl atteched')
		$scope.booking={};
	$window.scrollTo(0, 0);

	// pdf.create({})
	if(localStorage.user)
		var userInfo = JSON.parse(localStorage.user);
	if(localStorage.getItem('user')){
		 $scope.pdfShow=true;
		}
	$scope.downldPdf=function(aircraft){
		if($scope.pdfShow){
			var date=new Date();
			$scope.todayDate = date.getDate() +'/'+ (parseInt(date.getMonth())+1) + '/' + date.getFullYear()
			$scope.booking.user=JSON.parse(localStorage.user);
			$scope.booking.aircraft=aircraft;
			console.log("dddddd",$scope.booking)
			var len=$scope.booking.aircraft.duration.length;
			var doc = new jsPDF();
			var specialElementHandlers = {
			    '#editor': function (element, renderer) {
			        return true;
			    }
			};
			console.log(aircraft)
			
			$timeout(function(){
				pdf.create('pdf-dynamic','#pdfFormat','JetSmart pro from.pdf');
				$timeout(function(){
					$('#pdf-dynamic').css('display','none');
				},100)
			},10);
		}else{
			$rootScope.isLoginForm = true;
		}
		
	}

	var dropdown = [];

	var searchableObj = JSON.parse(localStorage.getItem('localData'))

	console.log('searchableObj',searchableObj)
	$scope.filterOptions = {
		price : parseInt(searchableObj.price),
		seat : parseInt(searchableObj.traveller),
		aircraft_type : searchableObj.aircraft_type,
		facility : searchableObj.facility,
	}


	search.getAll().then(function(objS){
		var useableData = []
		for (var i = 0; i < objS.length; i++) {
			var allData = objS[i]
			/**/
			for (var j = 0; j < allData.airportList.length; j++) {
				var airport = allData.airportList[j]
				useableData.push({
					airport : airport.airport,
					stateName : allData.states.stateName,
					countryName : allData.countryName,
					cityName : allData.states.cities.cityName,
					iata : airport.iata,
					icao : airport.icao,
					_id : airport._id,
				})
			}
			/**/
		}

		$scope.airportDropdown = useableData

		switch (searchableObj.tripType){
			case 'oneWay':
				searchableObj.roots = [searchableObj.roots[0]];
				// console.log('oneWay =>', d);
				break;
			case 'roundTrip':
				searchableObj.roots[1] = {
					origin: searchableObj.roots[0].destination,
					destination : searchableObj.roots[0].origin,
					originName: searchableObj.roots[0].destinationName,
					destinationName : searchableObj.roots[0].originName,
					departureDate : searchableObj.roots[1].departureDate,
					departureTime : searchableObj.roots[1].departureTime,
				}
				searchableObj.roots = [searchableObj.roots[0],searchableObj.roots[1]];
				// console.log('roundTrip =>',d)
				break;
			case 'multicity':
				// console.log('multicity =>',d)
				searchableObj = searchableObj;//obj
				break;
		}
		searchFlights()

	},function(objE){console.log(objE)})

	$scope.searchObj = searchableObj
	// $scope.$apply(function(){
		if($scope.searchObj.roots.length < 2){
			$scope.searchObj.roots.push({})
		}
	// })
	$scope.addMulticityByRadioBtn = function(){
		if($scope.searchObj.roots.length < 2){
			$scope.searchObj.roots.push({
				originName : $scope.searchObj.roots[$scope.searchObj.roots.length - 1].destinationName,
				origin : $scope.searchObj.roots[$scope.searchObj.roots.length - 1].destination,
				destinationName : '',
				destination : '',
				departureDate : '',
				departureTime : '',
			})
			console.log($scope.searchObj.roots)
		}
	}

	$scope.changeDat = function(ind){
		console.log("ind",ind)
		for(var i=0 ;i<$scope.searchObj.roots.length;i++){
			if(ind < i)
				$scope.searchObj.roots[i].departureDate = ""
		}
		console.log("ddddd",$scope.searchObj.roots)
		
	}


	$scope.departureDate = searchableObj.roots[0].departureDate;
	$scope.departureTime = searchableObj.roots[0].departureTime;
	$scope.traveller = searchableObj.traveller;

	$scope.resetSearchForm = function(){
		$scope.searchObj = {
			tripType : 'oneWay',
			traveller : '1',
			nationality : 'Indian',
			price : '10000000',
			searchType : 'normalSearch',
			roots : [
				{	
					originName : '',
					origin : '',//59d628eeb4dc9618b579e9bc
					destinationName : '',
					destination : '',//59d628eeb4dc9618b579e88f
					departureDate : '',
					departureTime : '',
				},
				{	
					originName : '',
					origin : '',
					destinationName : '',
					destination : '',
					departureDate : '',
					departureTime : '',
				},
			],
			aircraft_type : {
				heavy_jet : false,
				super_mid_jet : false,
				mid_size_jet : false,
				light_jet : false,
				turbo_props : false,
			},
			facility : {
				flight_attendant : false,
				lavatory : false,
				satellite_phone : false,
				wifi : false,
			},
		}
	}
	//
	$scope.isActive = function(route) {
		console.log('is active')
		return route === $location.path();
	}
	/*form */
	$scope.autoFill = function(index){
		setTimeout(function(){
			if(angular.isDefined($scope.searchObj.roots[index+1]))
			$scope.searchObj.roots[index+1].originName = $scope.searchObj.roots[index].destinationName;
		}, 500)
	}

	$scope.addMulticity = function(index){
		var obj = {	
				originName : $scope.searchObj.roots[$scope.searchObj.roots.length - 1].destinationName,
				origin : $scope.searchObj.roots[$scope.searchObj.roots.length - 1].destination,
				destinationName : '',
				destination : '',
				departureDate : '',
				departureTime : '',
			};
		$scope.searchObj.roots.splice(index+1, 0, obj);
	}
	$scope.removeMulticity = function(index){
		$scope.searchObj.roots.splice(index, 1);
	}
	var desDat = [];
	$scope.submitSearch = function(d){

		/**/
		var isDateVerified = true;
		for (var i = 0; i < d.roots.length; i++) {
			console.log("dffgdfg",new Date(d.roots[i].departureDate+','+d.roots[i].departureTime))
			desDat[i] = (new Date(d.roots[i].departureDate+','+d.roots[i].departureTime)).getTime();
			console.log("depTime",desDat);

			for(var p=0;p<desDat.length;p++){
				console.log("in for loop", desDat[p] >= desDat[p+1])
				if(desDat[p] >= desDat[p+1]){
					isDateVerified = false;
				}
			}

			
			var _root = d.roots[i]

			if(angular.isDefined(_root.originName))
				var originArray = _root.originName.split(",");
			if(angular.isDefined(_root.destinationName))
				var destinationArray = _root.destinationName.split(",");

			for(var j = 0; j < $scope.airportDropdown.length; j++){
				if(angular.isDefined(originArray[1])){
					if(originArray[1].trim() == $scope.airportDropdown[j].airport){
						_root.origin = $scope.airportDropdown[j]._id
						break;
					}
				}
			}
			for(var j = 0; j < $scope.airportDropdown.length; j++){
				if(angular.isDefined(destinationArray[1])){
					if(destinationArray[1].trim() == $scope.airportDropdown[j].airport){
						_root.destination = $scope.airportDropdown[j]._id
						break;
					}
				}
			}
		}
		if(!isDateVerified){
			custom.alert("Please Enter correct date and time.");
			desDat=[];
			return;
		}

		/**/
		switch (d.tripType){
			case 'oneWay':
				d.roots = [d.roots[0]];
				console.log('oneWay =>', d);
				break;
			case 'roundTrip':
				d.roots[1] = {
					origin: d.roots[0].destination,
					destination : d.roots[0].origin,
					originName: d.roots[0].destinationName,
					destinationName : d.roots[0].originName,
					departureDate : d.roots[1].departureDate,
					departureTime : d.roots[1].departureTime,
				}
				d.roots = [d.roots[0],d.roots[1]];
				console.log('roundTrip =>',d)
				break;
			case 'multicity':
				console.log('multicity =>',d)
				localStorage.localData = d;//obj
				break;
		}
		localStorage.setItem('localData', JSON.stringify(d))
		var searchableObj = JSON.parse(localStorage.getItem('localData'))
		$('.booking-module-wrapper').removeClass('active-booking');
		searchFlights();
		/*set fiters*/
		$scope.departureDate = searchableObj.roots[0].departureDate;
		$scope.departureTime = searchableObj.roots[0].departureTime;
		$scope.traveller = searchableObj.traveller;
		$scope.filterOptions = {
			price : parseInt(searchableObj.price),
			seat : parseInt(searchableObj.traveller),
			aircraft_type : searchableObj.aircraft_type,
			facility : searchableObj.facility,
		}

	}

	var searchFlights = function(){
		console.log('Search options==>',searchableObj)
		search.flights(searchableObj).then(function(objS){
			if(objS.message == 'No route found'){
				custom.alert('Thank you for your query, \n please call on (011 28050066) for this particular routing. or send us a query','JET SMART')
				return
				// alert('our system is unable to calculate this route, \nplease contact our customer care')
			}
			
			// console.log(objS)
			/* insert Departure date*/

			/* end insert Departure date*/
			for (var i = 0; i < objS.length; i++) {
				// positioning re positioning
				// if(objS[i].documents.hasBase){
					let fromBase = objS[i].documents.positioning.fromAirport;
					let from = objS[i].duration[0].data.fromAirport;
					let toBase = objS[i].documents.repositioning.toAirport;
					let to = objS[i].documents.repositioning.fromAirport;
					//positioning
					if(fromBase == from){
						objS[i].duration[0].data.positioning = false;
						objS[i].duration[0].data.base = true;
					}else{
						// objS[i].duration[0].data.departureDate=''
						objS[i].documents.positioning.positioning = true;
						objS[i].documents.positioning.base = true;
						objS[i].duration.unshift({
							crewCharges :0,
							data : objS[i].documents.positioning,
							duration : objS[i].documents.positioning.duration,
						});
					}
					//repositioning
					if(toBase == to){
						objS[i].duration[objS[i].duration.length - 1].data.repositioning = false;
						// objS[i].duration[objS[i].duration.length - 1].data.base = true;
					}else{
						objS[i].documents.repositioning.repositioning = true;
						// objS[i].documents.repositioning.base = true;
						objS[i].duration.push({
							crewCharges :0,
							data : objS[i].documents.repositioning,
							duration : objS[i].documents.repositioning.duration,
						});
					}
				// }
				//


				if(angular.isDefined(objS[i].duration)){
					for (var j = 0; j < objS[i].duration.length; j++){
						/**/
						var origin = objS[i].duration[j].data.fromAirport
						var destination = objS[i].duration[j].data.toAirport

						objS[i].duration[j].hours=Math.floor(objS[i].duration[j].duration/60);
						objS[i].duration[j].min=Math.floor(objS[i].duration[j].duration%60)
						// var timeArray = objS[i].duration[j].duration.toString().split(".");
						// objS[i].duration[j].hours = timeArray[0];
						// objS[i].duration[j].min = (parseInt(timeArray[1])*60)/timeArray[1].length;
						// objS[i].duration[j].min = parseInt(objS[i].duration[j].min.toString().substring(0, 2))
						/**/
						let roots = JSON.parse(localStorage.localData).roots
						for(let k=0; k < roots.length; k++){
							// console.log('k==>',k,roots[k].origin, objS[i].duration[j].data.fromAirport)
							// console.log('kk==>',k,roots[k].destination, objS[i].duration[j].data.toAirport)
							if(roots[k].origin == objS[i].duration[j].data.fromAirport && roots[k].destination == objS[i].duration[j].data.toAirport){
								objS[i].duration[j].departureDate = roots[k].departureDate
							}else{
								// objS[i].duration[j].departureDate = ''
							}
						}

						objS[i].duration[j].info = {};

						for (var k = 0; k < $scope.airportDropdown.length; k++) {
							var _data = $scope.airportDropdown[k]

							//origin
							if(_data._id == origin){
								objS[i].duration[j].info.originIata = _data.iata
								objS[i].duration[j].info.originIcao = _data.icao
								objS[i].duration[j].info.originName = _data.cityName
							}
							//destination
							if(_data._id == destination){
								objS[i].duration[j].info.destinationIata = _data.iata
								objS[i].duration[j].info.destinationIcao = _data.icao
								objS[i].duration[j].info.destinationName = _data.cityName
							}
						}

					}
				}
				if(angular.isDefined(objS[i].documents.image_gallary)){
					if(objS[i].documents.image_gallary.length > 0){
						for (var j = 0; j < objS[i].documents.image_gallary.length; j++) {
							objS[i].documents.image_gallary[j] = baseUrl + '/' + objS[i].documents.image_gallary[j]
						}
					}else{
						objS[i].documents.image_gallary = ['assets/img/default-slide.jpg']
					}
				}else{
					objS[i].documents.image_gallary = ['assets/img/default-slide.jpg']
				}
			}

			$scope.searchResult = objS

			 console.log($scope.searchResult)

			// console.log('searchResult',$scope.searchResult)
		},function(objE){
			console.log('objE==>', objE)
		})
	}

	$scope.loginForm = function () {
		$rootScope.isLoginForm = !$rootScope.isLoginForm
	}
	$scope.openLoginForm = function(){
		$rootScope.isLoginForm = !$rootScope.isLoginForm;
		$state.reload();
	}
	$scope.bookingInquiry = function(d){
		console.log()
		if(typeof localStorage.user == 'undefined' || typeof localStorage.usertoken == 'undefined'){
			$scope.loginForm();
		}else{
			localStorage.aircraft = JSON.stringify(d);
			var req = {
					"user_id": $scope.$parent.userInfo._id,
					// "booking_id":objS.data._id,
					// var dda = parseInt($scope.searchResult.total)
					// totalAmount = 100*dda;
					"amount":5000
				}
				user.getOrder(req)
				.then(function(objS){
					if(objS.status===200){
						localStorage.order_id = objS.data.order_id;
						console.log("Order_id =>"+$scope.order_id)
						$state.go('flightDetails')
					}
					console.log("Response order=>"+JSON.stringify(objS))
				},function(objE){
					console.log(objE)
				})
			
		}
	}

}])