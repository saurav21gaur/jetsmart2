angular.module('jetSmart')
.controller('seatBookingCtrl', ['$window','$rootScope','$scope', 'user', 'custom','$state','$filter','$timeout','$window','$location', function($window,$rootScope,$scope, user, custom,$state,$filter,$timeout,$window,$location){
	$scope.showAircraft = false;
	$scope.externalUser = false;
	$scope.start = false;
	$window.scrollTo(0, 0);
	$scope.showPaymentDetail = false;
	$scope.arrDetail = [{}];

	if(!localStorage.usertoken){
		$state.go('home');
	}
	// $scope.selectedSeat = 0;
	if(location.href.indexOf('status') > -1){    
        var status=location.href.split("?")[2].split("=")[1];
        status=status.substr(0,status.length-2);
        status=status.split("#");
        status = status[0]
        console.log("status",status[0])
    }
 //    $scope.$on("flight detail found",function(event,data){
	// 	console.log("ddd =>",data);
	// });

	if(location.href.indexOf('code') > -1){    
	        var code=location.href.split("?")[1].split("=")[1];
	        code=code.substr(0,code.length);
	        $scope.flight_id = code;
	        console.log("Code =>"+code);
	        user.showFlightDetail(code)
	        .then(function(detail){
	        	$scope.extUser = detail;
	        	// console.log("ddddd =>"+JSON.stringify(detail));
	        },function(err){
	        	console.log("err-->",err)
	        })
	    }
 	
    
    // console.log("status",status)
    if(status == undefined || status == null)
    	$scope.showAircraft = true;

    if(status)
    	{
    		$scope.showAircraft = false;
    		$scope.externalUser = true;
    	}

	// console.log(localStorage.localData)
	if(localStorage.seatDetail)
	 $scope.detail = JSON.parse(localStorage.seatDetail);
	// console.log("vaggl =>"+JSON.stringify($scope.detail))
	var userInfo = JSON.parse(localStorage.user);

	$scope.nextForm = function(){
		$scope.showAircraft = false;
		$scope.externalUser = false;
		$scope.showPaymentDetail = false;
		$scope.start = true;
	}
	
	$scope.addPasenger = function(x){
		if($scope.arrDetail.length >= $scope.selectedSeat){
			swal("You have selected only "+$scope.selectedSeat+" seats.")
		}
		else{
			var obj=Object.create(x)
			$scope.arrDetail.push(obj);
			console.log("array "+JSON.stringify($scope.arrDetail))
			$scope.arrDetail[$scope.arrDetail.length-1]={};
		}	
	}
	$scope.rmPasenger = function(x){
		 var index = $scope.arrDetail.indexOf(x);
		  if($scope.arrDetail.length == 1){
		  	return $scope.arrDetail;
		  }
		  else{
		  	$scope.arrDetail.splice(index,1);  
		  }
	}

	$scope.createInquiry = function(){
		$scope.arr = $scope.arrDetail;
		if($scope.arrDetail.length < $scope.selectedSeat){
			custom.alert('You have Selected '+$scope.selectedSeat+' seats. So please enter passenger details for '+$scope.selectedSeat+' seats.','Error')
			return
		}
		$scope.inqueryObj = {
			booking_id:$scope.paymentBookingId,
			pessanger_detail : $scope.arr,
			contact : $scope.contactDetails,
			flight_id : $scope.flight_id
		}
		user.updateBooking($scope.inqueryObj)
		.then(function(objS){
			if(objS.status===200){
				localStorage.viewBookingId = $scope.paymentBookingId;
		 		$state.go('viewHistory');	
			}
			console.log("Response=>"+JSON.stringify(objS))
		},function(objE){
			console.log(objE)
		})
	}
	$scope.ageType = function(age, idx){
		console.log("Age =>"+age)
		if(age <= 2 && age > 0)
			$scope.arrDetail[idx].ageUserType = 'Infant'
		else if(age >= 2 && age <= 12)
			$scope.arrDetail[idx].ageUserType = 'Child'
		else if(age >= 12)
			$scope.arrDetail[idx].ageUserType = 'Adult'
		else if(age == null)
			$scope.arrDetail[idx].ageUserType = '' 
	}

	// splitpayment
	$scope.externalPayment = function(){
		var amtt = 2000;
		console.log("sssss==>",$scope.extUser)
		var req = {
					"user_id": $scope.$parent.userInfo._id,
					"amount":amtt
				}
		user.getOrder(req)
		.then(function(objS){
			if(objS.status===200){
				$scope.order_id = objS.data.order_id;
				$scope.payData = {};
				
			      var options = {
			          "key": "rzp_test_REEE8skw8OMLSt",
			          "amount": amtt*100, // we take amount in paise
			          "name": "JetSmart",
			          "description": $scope.order_id,
			          "image": "assets/img/logo.png",
			          "handler": function (response){
			          		var transfer_amt = ((amtt)*$scope.extUser.created_by.profitable_percentage)/100;
			          		console.log("transfer_amt==>",transfer_amt)
						          	var data = {
									          "transfers": [
															   {
															     "account": $scope.extUser.created_by.account_id,
															     "amount" : transfer_amt,
															     "currency": "INR"
															   }
															 ],
															"payment_id": response.razorpay_payment_id
											}
								    user.splitpayment(data)
									.then(function(objS){
										$scope.payData = objS;
										$scope.arr = [];
											$scope.contactDetails = {};
											$scope.inqueryObj = {
												user_id: userInfo._id,
												roots : $scope.extUser.roots,
												pessanger_detail : $scope.arr,
												contact : $scope.contactDetails,
												order_id : $scope.order_id,
												flight_id:$scope.flight_id,
												aircraftId : $scope.extUser.aircratft_id._id
											}
											console.log("req =>"+JSON.stringify($scope.inqueryObj))
											for (var i = 0; i < $scope.inqueryObj.roots.length; i++) {
												delete $scope.inqueryObj.roots[i].departureTime
											}
											user.inquiry($scope.inqueryObj)
											.then(function(objS){
												if(objS.status===200){
													var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
													var date = new Date(objS.data.created_at)
													$scope.booking = {
														number : objS.data.bookingNumber,
														time : (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?'pm':'am')
													}
													$scope.paymentBookingId = objS.data.booking_id;
												}
												console.log("Response=>"+JSON.stringify(objS))
											},function(objE){
												console.log(objE)
											})
										swal({
					          			  title: "Payment Success",
										  text: $scope.payData.id,
										  buttons: true,
										  success: true,
										})
										.then((ok) => {
										  if (ok) {
										  	$scope.paymentDetail = $scope.payData;
										  	console.log("payment =>"+$scope.paymentDetail)
										  } 
										});
										$scope.paymentDetail = objS;
										$scope.showAircraft = false;
										$scope.externalUser = false;
										$scope.showPaymentDetail = true;
										console.log("Response order=>"+JSON.stringify(objS))
									},function(objE){
										console.log(objE)
									})

			          },
			          "prefill": {
			              "name": userInfo.user_name,
			              "email": userInfo.email
			          },
			          "notes": {
			              "invoice_id": ''
			          },
			          "theme": {
			              "color": "#EE4E0C"
			          }
			      };
			      new Razorpay(options).open();
			}
			console.log("Response=>"+JSON.stringify(objS))
		},function(objE){
			console.log(objE)
		})
	}


	$scope.payment = function(){
		var totalAmount = $scope.detail.price_per_seat * $scope.selectedSeat;
		console.log("totalAmount",totalAmount)
		$scope.root = [{
			'originName':$scope.detail.originName,
			'destinationName':$scope.detail.destinationName,
			'departureDate':$scope.detail.departureDate,
			'departureTime':$scope.detail.departureTime
		}]
		console.log(totalAmount)
		if($scope.selectedSeat == 0){
			swal("Select atleast one seat")
		}
		else if($scope.selectedSeat > $scope.detail.remainingSeats){
			swal("Only "+$scope.detail.remainingSeats+" seats are available.")
		}
		else{
			var req = {
					"user_id": $scope.$parent.userInfo._id,
					// "booking_id":objS.data._id,
					// var dda = parseInt($scope.searchResult.total)
					// totalAmount = 100*dda;
					"amount":2000
				}
				user.getOrder(req)
				.then(function(objS){
					if(objS.status===200){
						$scope.order_id = objS.data.order_id;
						console.log("Order_id =>"+$scope.order_id)
						var minimum_donation_amount = 100; 

						$scope.payData = {};
						// var rupeeAmount = parseInt(document.getElementById('amount-field').value);
				  //     if (!rupeeAmount || rupeeAmount < minimum_donation_amount) {
				  //       return alert('Please enter valid amount. minimum_donation_amount is ₹ 100')
				  //     }
				      var options = {
				          "key": "rzp_test_REEE8skw8OMLSt",
				          "amount": 20000, // we take amount in paise
				          "name": "JetSmart",
				          "description": $scope.order_id,
				          "image": "assets/img/logo.png",
				          "handler": function (response){
				          	console.log($scope.detail.created_by.account_id)
							          	var data = {
											          "transfers": [
																	   {
																	     "account": $scope.detail.created_by.account_id,
																	     "amount" : 100,
																	     "currency": "INR"
																	   }
																	 ],
																	"payment_id": response.razorpay_payment_id
													}
									    user.paymentSplitOffers(data)
										.then(function(objS){
											$scope.payData = objS;
											
											$scope.arr = [];
												$scope.contactDetails = {};
												$scope.inqueryObj = {
													user_id: userInfo._id,
													roots : $scope.root,
													pessanger_detail : $scope.arr,
													contact : $scope.contactDetails,
													order_id : $scope.order_id,
													opratorId : $scope.detail.aircratft_id._id,
													empty_jetId : $scope.detail._id,
													empty_jet_sel_seat:$scope.selectedSeat,
													 
												}
												console.log("req =>"+JSON.stringify($scope.inqueryObj))
												for (var i = 0; i < $scope.inqueryObj.roots.length; i++) {
													delete $scope.inqueryObj.roots[i].departureTime
												}
												user.splitBooking($scope.inqueryObj)
												.then(function(objS){
													if(objS.status===200){
														var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
														var date = new Date(objS.data.created_at)
														$scope.booking = {
															number : objS.data.bookingNumber,
															time : (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?'pm':'am')
														}
														$scope.paymentBookingId = objS.data.booking_id;
													}
													console.log("Response=>"+JSON.stringify(objS))
												},function(objE){
													console.log(objE)
												})
											swal({
						          			  title: "Payment Success",
											  text: $scope.payData.id,
											  buttons: true,
											  success: true,
											})
											.then((ok) => {
											  if (ok) {
											  	$scope.paymentDetail = $scope.payData;
											  	console.log("payment =>"+$scope.paymentDetail)
											  } 
											});
											$scope.paymentDetail = objS;
											$scope.externalUser = false;
											$scope.showAircraft = false;
											$scope.showPaymentDetail = true;
											console.log("Response order=>"+JSON.stringify(objS))
										},function(objE){
											console.log(objE)
										})

				          },
				          "prefill": {
				              "name": userInfo.user_name,
				              "email": userInfo.email
				          },
				          "notes": {
				              "invoice_id": ''
				          },
				          "theme": {
				              "color": "#EE4E0C"
				          }
				      };
				      new Razorpay(options).open();
	
					}
					console.log("Response order=>"+JSON.stringify(objS))
				},function(objE){
					console.log(objE)
				})
		}
	}
}])