angular.module('jetSmart')
.controller('viewHistoryCtrl',['pdf','$timeout','$window','$scope', '$rootScope', 'custom', 'user', '$state', function(pdf,$timeout,$window,$scope, $rootScope, custom, user, $state){
	$scope.showform = false;
	$scope.arr = [{}];
	$window.scrollTo(0, 0);
	if(localStorage.user){
		$scope.loginUser = JSON.parse(localStorage.user);
		console.log("ss",JSON.parse(localStorage.user))
	}
	if(!localStorage.usertoken){
		$state.go('home');
	}
	var date=new Date();
	$scope.todayDate = date.getDate() +'/'+ (parseInt(date.getMonth())+1) + '/' + date.getFullYear()
	
	$scope.addPasenger = function(x){
		var obj=Object.create(x)
		$scope.arr.push(obj);
		console.log("array "+JSON.stringify($scope.arr))
		$scope.arr[$scope.arr.length-1]={};
		
	}
	$scope.rmPasenger = function(x){
		 var index = $scope.arr.indexOf(x);
		  if($scope.arr.length == 1){
		  	return $scope.arr;
		  }
		  else{
		  	$scope.arr.splice(index,1);  
		  }
	}
	$scope.downldPdf=function(aircraft){
		
			$timeout(function(){
				pdf.createTicket('pdf-dynamic','#pdfFormat','JetSmart pro from.pdf');
				$timeout(function(){
					$('#pdf-dynamic').css('display','none');
				},100)
			},10);
		
	}

	$scope.ageType = function(age, idx){
		console.log("Age =>"+age)
		if(age <= 2 && age > 0)
			$scope.arr[idx].ageUserType = 'Infant'
		else if(age >= 2 && age <= 12)
			$scope.arr[idx].ageUserType = 'Child'
		else if(age >= 12)
			$scope.arr[idx].ageUserType = 'Adult'
		else if(age == null)
			$scope.arr[idx].ageUserType = '' 
	}

	$('body').removeClass('active-menu');
	user.viewHistory({"_id" :localStorage.viewBookingId})
	.then(objS=>{
		$scope.BookingDetail = objS;
		console.log($scope.BookingDetail)
		if($scope.BookingDetail.result.pessanger_detail == 0)
			$scope.showform = true;
		else
			$scope.showform = false;
		console.log("Show =>"+$scope.showform)
	},objE=>{
		console.log(objE);
	})


	$scope.submit =function(){
		$scope.inqueryObj = {
			booking_id:$scope.BookingDetail.bookingResult.booking_id,
			pessanger_detail : $scope.arr,
			contact : $scope.contactDetails
		}
		user.updateBooking($scope.inqueryObj)
		.then(function(objS){
			if(objS.status===200){
				$state.reload();	
			}
			console.log("Response=>"+JSON.stringify(objS))
		},function(objE){
			console.log(objE)
		})
	}
}])