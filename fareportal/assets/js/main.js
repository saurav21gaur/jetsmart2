$(document).ready(function(){
	// WHAT WE DO Equal Height
	var mod_height = $('.custom-about-content').parent().height();
	$('.custom-about-image').css('height', mod_height);

	// Our Investors Toggle Module
	$('.custom-inverstor-member-section').hide();
	$('.member-toggle ul li.our-investor-tgl').click(function(){
		$(this).addClass('active');
		$(this).siblings().removeClass('active');
		$('.custom-team-member-section').hide();	
		$('.custom-inverstor-member-section').fadeIn();
	});
	$('.member-toggle ul li.our-team-tgl').click(function(){
		$(this).addClass('active');
		$(this).siblings().removeClass('active');
		$('.custom-inverstor-member-section').hide();	
		$('.custom-team-member-section').fadeIn();	
	});
});

/* Tag line */
$(function () {
	var index = 1,
		max = $('.header-tag li span').length;

	function setActive() {
		setTimeout(function () {
			$('.active-rotate').removeClass('active-rotate');
			index++;
			if(index > max) {
				index = 1;    
			}
			$('.header-tag li span:nth-of-type(' + index + ')').addClass('active-rotate');
			setActive();
		}, 2500);
	}
	
	setActive();
	
});

/* Faqs */
$(document).ready(function(){
	$('.faq p').slideUp();
	$('.faq').click(function(){
	    $(this).children('p').slideDown();
	    $(this).css('border-color', '#ee4e0c');
	    $(this).children().children('i').css('color', '#ee4e0c');
		$(this).parent().siblings().children().children('p').slideUp();
		$(this).parent().siblings().children().css('border-color', '#0f1626');
	    $(this).parent().siblings().children().children().children('i').css('color', '#0f1626');
	});
});

/* Tabs */
$(document).ready(function(){
	$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');
		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');
		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})
});

// Booking Toggle
// $(document).ready(function(){
// 	$('a.booking-tgl, .modify-btn .search-booking-tgl').click(function(e){
// 		$('.booking-module-wrapper').toggleClass('active-booking');
// 		e.stopPropagation();
// 	});
// 	$('.booking-area').click(function(e){
// 		e.stopPropagation();
// 	});
// 	$('a.close-booking-tgl, body').click(function(){
// 		$('.booking-module-wrapper').removeClass('active-booking');
// 	});
// });

// datepicker
$(document).ready(function(){
	$('#date, #returndate').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true
	});
	$('#time, #returntime').bootstrapMaterialDatePicker({
		date: false,
		shortTime: true,
		format: 'HH:mm'
	});
});

// slidedown
$(document).ready(function(){
	$('.return-date-wrapper').slideUp();
	$('input#roundTrip + label').click(function(){
       $('.return-date-wrapper').slideDown();
       $('.for-multicity').slideUp();
	});
	$('input#oneWay + label').click(function(){
       $('.return-date-wrapper').slideUp();
       $('.for-multicity').slideUp();
	});
	$('input#multicity + label').click(function(){
       $('.for-multicity').slideDown();
       $('.return-date-wrapper').slideUp();
	});
});

// Filter
$(document).ready(function(){
	$('a.filter-tgl').click(function(){
		$('body').toggleClass('active-filter');
	});
	$('.filter-wrappper, a.filter-tgl').click(function(e){
		e.stopPropagation();
	});
	// $('body').click(function(){
	// 	$('body').removeClass('active-filter');
	// });
});

// flights Image
$(document).ready(function(){
	$('.flights-image .owl-item').each(function(){
		var bgimg = $(this).children().children('img').attr('src');
		$(this).children('.item').css('background-image', 'url('+ bgimg +')');
		$(this).children().children('img').remove();
	});
});

// Flights details
$(document).ready(function(){
	$('.more-info-tgl').click(function(){
		$(this).parents('.available-flights-wrapper').toggleClass('active-details');
		$(this).parents('.available-flights-wrapper').children().children('.flight-fare').slideToggle();
	});
	$('p.close-flights-details').click(function(){
		$(this).parents('.available-flights-wrapper').removeClass('active-details');
		$(this).parents('.available-flights-wrapper').children().children('.flight-fare').slideDown();
	});
});

$(document).ready(function(){
	$('.status .switch').click(function(){
		if ($('.switch input').is(':checked')) {
			$('.advanced-booking-engine').slideDown();
		}
		else{
			$('.advanced-booking-engine').slideUp();
		}
	});
});

// payment toggle
$(document).ready(function(){
	$('a.payment-tgl').click(function(e){
		$('.payment-module-wrapper').toggleClass('active-payment');
		e.stopPropagation();
	});
	$('.payment-area').click(function(e){
		e.stopPropagation();
	});
	// $('a.close-payment-tgl, body').click(function(){
	// 	$('.payment-module-wrapper').removeClass('active-payment');
	// });
});



$(document).ready(function() {
	var owl = $('.flights-image.owl-carousel.owl-theme');
	owl.owlCarousel({
		margin: 0,
		nav: false,
		center:false,
		mouseDrag: true,
		autoplay: true,	
		loop: true,
		responsive: {
		  0: {
			items: 1
		  },
		  600: {
			items: 1
		  },
		  1000: {
			items: 1
		  }
		}
		})
	});