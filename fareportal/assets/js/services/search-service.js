angular.module('jetSmart')
.factory('search', ['$http', '$q', function($http, $q){

	return {
		
		getAll : function(){

			var deff=$q.defer();
			
			$http.get(baseUrl+'/admin/aircraft/from_to_airport').then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		},

		flights : function(obj){

			var deff=$q.defer();

			$http({
				method: 'POST',
				url: baseUrl+'/admin/aircraft/searchFlights',
				data: obj,
				headers: {'Content-Type': 'application/json'}
			})
			.then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		},

	};
}])