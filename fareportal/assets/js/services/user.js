angular.module('jetSmart')
.factory('user', ['$http', '$q', function($http, $q){
	return {

		login : function(data){

			var deff=$q.defer(data);

			var url = baseUrl+'/fareportal/user_login'
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS)
			},function(objE){
				deff.reject(objE);
			})

			return deff.promise;
		},
		forgotPassword : function(data){

			var deff=$q.defer(data);

			var url = baseUrl+'/admin/oprator/forgot_password'
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS)
			},function(objE){
				deff.reject(objE);
			})

			return deff.promise;
		},


		bookHelicopter : function(data){
			var deff=$q.defer(data);

			var url = baseUrl+'/fareportal/bookHelicopter'
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS)
			},function(objE){
				deff.reject(objE);
			})

			return deff.promise;
		},
		

		logout:function(id){
			var deff=$q.defer(id);

			var url = baseUrl+'/fareportal/logout'
			
			$http.post(url,id).then(function(objS){
				deff.resolve(objS)
			},function(objE){
				deff.reject(objE);
			})

			return deff.promise;
		},

		resend_otp : function(data){

			var deff=$q.defer(data);

			var url = baseUrl+'/fareportal/resend_otp'
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS)
			},function(objE){
				deff.reject(objE);
			})

			return deff.promise;
		},

		signup : function(data){

			var deff=$q.defer(data);

			var url = baseUrl+'/fareportal/user_signup'
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS)
			},function(objE){
				deff.reject(objE);
			})

			return deff.promise;
		},

		inquiry : function(data){

			var deff=$q.defer(data);
			console.log(data)

			var url = baseUrl+'fareportal/user_booking_conformation?token='+localStorage.token
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS)
				console.log("Response Booking =>"+objS)
			},function(objE){
				deff.reject(objE);
			})

			return deff.promise;
		},
		splitBooking : function(data){

			var deff=$q.defer(data);
			console.log(data)

			var url = baseUrl+'fareportal/split_user_booking_conformation?token='+localStorage.token
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS)
				console.log("Response Booking =>"+objS)
			},function(objE){
				deff.reject(objE);
			})

			return deff.promise;
		},
		

		updateBooking : function(data){

			var deff=$q.defer(data);
			console.log(data)

			var url = baseUrl+'fareportal/user_booking_updation?token='+localStorage.token
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS)
				console.log("Response Booking Update =>"+objS)
			},function(objE){
				deff.reject(objE);
			})

			return deff.promise;
		},
		getOrder : function(data){

			var deff=$q.defer(data);
			console.log(data)
			var url = baseUrl+'fareportal/orderIdgenrate'
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS)
			},function(objE){
				deff.reject(objE);
			})

			return deff.promise;
		},

		verifyOtp : function(data){

			var deff=$q.defer();

			var url = baseUrl+'/fareportal/verifyotp'
			console.log(data)
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS)
			},function(objE){
				deff.reject(objE);
			})

			return deff.promise;
		},

		history : function(data){

			var deff=$q.defer();

			var url = baseUrl+'/fareportal/booking_history'
			console.log(data)
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				deff.reject(objE.data);
			})

			return deff.promise;
		},
		showFlightDetail : function(data){

			var deff=$q.defer();

			var url = baseUrl+'admin/oprator/show_flight_detail/'+data
			console.log(data)
			
			$http.get(url).then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				deff.reject(objE.data);
			})

			return deff.promise;
		},
		viewHistory : function(data){

			var deff=$q.defer();

			var url = baseUrl+'/fareportal/view_history'
			console.log(data)
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				deff.reject(objE.data);
			})

			return deff.promise;
		},
		paymentSave : function(data){

			var deff=$q.defer();

			var url = baseUrl+'/fareportal/savePayment'
			console.log(data)
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				deff.reject(objE.data);
			})

			return deff.promise;
		},
		paymentSplitOffers : function(data){

			var deff=$q.defer();

			var url = baseUrl+'/fareportal/splitpayment'
			console.log(data)
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				deff.reject(objE.data);
			})

			return deff.promise;
		},
		splitpayment : function(data){

			var deff=$q.defer();

			var url = baseUrl+'/fareportal/splitpayment'
			console.log(data)
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				deff.reject(objE.data);
			})

			return deff.promise;
		},
		getOffers : function(){

			var deff=$q.defer();

			var url = baseUrl+'/fareportal/emptyJet_list'
			
			$http.get(url).then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				deff.reject(objE.data);
			})

			return deff.promise;
		}
	}
}])