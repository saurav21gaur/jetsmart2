var express = require('express'); 
var path=require('path');   
var router = express.Router();  
var controllers = require('./user');


router.post('/user_signup',controllers.user_signup)

router.post('/user_login',controllers.user_login)
router.post('/verifyotp',controllers.verifyotp)

router.post('/user_booking_conformation',controllers.user_booking_conformation)

router.post('/booking_history',controllers.booking_history)
router.post('/orderIdgenrate',controllers.orderIdgenrate)
router.post('/view_history',controllers.view_history)
router.post('/generateQuotation',controllers.generateQuotation)
router.post('/generateMainfist',controllers.generateMainfist)
router.post('/generateQuotOpeartor',controllers.generateQuotOpeartor)
 router.post('/bookingPdf',controllers.bookingPdf)

router.post('/savePayment',controllers.savePayment)
router.post('/user_booking_updation',controllers.user_booking_updation)
router.post('/splitpayment',controllers.splitpayment)
// router.post('/splitpayment',controllers.splitpayment)
router.get('/emptyJet_list',controllers.emptyJet_list)
router.post('/split_user_booking_conformation',controllers.split_user_booking_conformation)
router.post('/change_password',controllers.change_password)
router.post('/resend_otp',controllers.resend_otp)
router.post('/logout',controllers.logout)
router.post('/bookHelicopter',controllers.bookHelicopter)
router.post('/contactUs',controllers.contactUs)

module.exports = router;  