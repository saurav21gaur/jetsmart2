var SparkPost = require('sparkpost');
var config = require.main.require('./global/config.js')
var client = new SparkPost(config.sparkPostApiKey);

module.exports = {
	send : function(from_, to_, subject_, message_) {
		return new Promise(function(resolve, reject) {
			
			client.transmissions.send({
		    options: {
		      sandbox: false
		    },
		    content: {
		      from: from_,
		      subject: subject_,
		      html: message_
		 
		    },
		    recipients: [
		      {address: to_}
		    ]
		  	})
		  	.then(data => {
			    resolve({status: true})
		  	})
		  	.catch(err => {
		  		resolve({status: false})
		  	});

		});
	}
}

