var config=require.main.require('./global/config');
var aircraft=require.main.require('./db/aircraftSchema.js');
var user = require.main.require('./db/opratorSchema.js');
var country = require.main.require('./db/countrySchema.js');
var booking = require.main.require('./db/bookingCountSchema.js')
var pessangeBook = require.main.require('./db/passengerBookingSchema.js')
var transaction =  require.main.require('./db/transaction.js')
var emptyJet =  require.main.require('./db/Emptyjet.js')
var otp = require('./function.js');
var waterfall = require('async-waterfall');
var mongoose = require('mongoose');
var https = require('https');
var async = require('async');
var jwt = require('jsonwebtoken');
var request = require('request');
var path=require('path');
var quotationTemp = require.main.require('./quotationTemp.js');
var mailTemp = require.main.require('./mailTemp.js')
var quotOperator = require.main.require('./quotOperator.js');
var bookingTemp = require.main.require('./bookingTemp.js');
var invoiceTemp = require.main.require('./invoice.js');
var pdf = require('pdfcrowd');
var urllib = require('urllib');
var http = require('http')
var httpclient = require('urllib').create();
var spark = require('./sparkpost.js')
var email = require('./email.js')
// var count = require('./db/bookingCountSchema.js')
// var pessengerBooking = require('./db/passengerBookingschema')




var user_api = {


  'user_signup':function(req,res){
    console.log("req",req.body)

    user.findOne({$or:[{email:req.body.email},{user_name:req.body.user_name},{phone_number : req.body.phone_number}]},function(err,result){
       if(err){
       res.send({code:500,message:"Server err"})
       }
       else if(!result){
        req.body.type = "user"
        req.body.otp = otp.sendOtp(req.body.phone_number);
        var user_save = new user(req.body)
        user_save.save(function(err,result){
          console.log("result",result)
       err?res.status(500).send(err):res.send({code:200,result:result,message:"Otp send successfully to {{result.phone_number}}"})
        })
           
       }
       else{
          res.send({code:204,message:"Please enter unique email id or Phone Number"})
       }

    })

  },


  bookHelicopter : function(req, res){
    var userOtp = '100'+Math.floor(12345 + Math.random() * 90000);
    var newNumber= parseInt(userOtp)
    console.log("userOtp", typeof userOtp, userOtp, typeof newNumber, newNumber)
    var htmlContent=mailTemp.style+mailTemp.content_start1+req.body.dynamicContent+mailTemp.content_end3;
      spark.send('noreply@gojetsmart.com', 'contact@jetsmart.in', 'Helicopter Inquiry',htmlContent ).then(function(result) {
          return res.send({responseCode:200,registration_no:newNumber})
      })
    
  },

  contactUs : function(req, res){
    console.log("req",req.body.subject)
    var a = req.body.subject
    var htmlContent=mailTemp.style+mailTemp.content_start1+req.body.dynamicContent+mailTemp.content_end3;
      spark.send('noreply@gojetsmart.com', 'contact@jetsmart.in', a,htmlContent ).then(function(result) {
          return res.send({responseCode:200,message:"thanku for query"})
      })
    
  },



  resend_otp : function(req,res){
    console.log("req====",req.body)
       req.body.otp = otp.sendOtp(req.body.phone_number);
       console.log("user OTP",req.body.otp)
       user.findOneAndUpdate({phone_number:req.body.phone_number},{$set:{otp:req.body.otp}},function(err,result){
            console.log("res ==>",result)
               if(err){
                return res.send({responseCode:500, responseMessage:"server err"})
               }
               else{
                return res.send({responseCode:200,responseMessage:'otp resend successfully'})
               }
      })
   },

  change_password:function(req,res){
      console.log(req.body);
      user.findOne({_id:req.body._id,user_name:req.body.username,type:req.body.type},function(err,result){
         console.log("result",result)
         if(!result){
          res.status(200).send({msg:"Username does not exist",code:400});
         }
         else{
              opertaor.findOne({_id:req.body._id,type:req.body.type,user_name:req.body.username,password:req.body.password},function(err,result1){
                if(!result1){
                  res.status(200).send({msg:"Incorrect Old Password",code:400});
                }
                else{
                      opertaor.update({_id:req.body._id,type:req.body.type,user_name:req.body.username},{$set:{password : req.body.newPassword}},function(err,result){
                        if(result){
                          res.status(200).send({msg:"Password Changed Successfully",code:200});
                        }
                        else{
                          res.status(403).send({msg:"Something wents wrong",code:400});
                        }
                      })
                }         
              })
            }
      })
    },

  'verifyotp': function (req, res) {
    console.log("request"+JSON.stringify(req.body));
    user.findOne({
      phone_number: req.body.phone_number
    }, function (err, result) {
      console.log("otp result" + JSON.stringify(result));
      if (err) {
         res.send({code:500,message:"Server err"})
      } else if (!result) {
         res.send({code:404,message:"No data found"})
      } else {
        console.log("result =>",result.otp)
        console.log("OTP =>",req.body.otp)
        if (result.otp == req.body.otp) {
          result.otp_verify = true;
          result.save();
           var htmlContent=mailTemp.style+mailTemp.content_start+result.email+mailTemp.content_end;
            spark.send('noreply@gojetsmart.com', result.email, 'Registration successfully',htmlContent ).then(function(result) {
                res.send({code:200,message:'Otp verified successfully'})
            })
        } else {
           res.send({code:204,message:'Please enter correct otp'})
        }
      }


    })

  },


  user_login:function(req,res){
  user.findOne({$or:[{email:req.body.user_name},{user_name:req.body.user_name}]},function(err,result){
    console.log("result",result)
    if(err){
       res.send({code:500,message:"Server err"})
    }
    else if(!result){
         res.send({code:204,message:"Please enter correct email or user name"})     

    }
    else{
     user.findOne({user_name:result.user_name,password:req.body.password},function(err,result1){
    if(err){
       res.send({code:500,message:"Server err"})
    }
    else if(!result1){
     res.send({code:204,message:"Please enter correct Password"})
    }
    else{
    user.update({_id:result1._id},{$set:{is_login:true}},function(err,secoundResult){

             var _token=jwt.sign({
              exp: Math.floor(Date.now() / 1000) + (60 * 60),
              data: req.body
            }, config.authSecretKey);
            res.send({result:result1,token:_token})


    })

    //   err?res.status(500).send(err):res.send(result)
    }


     })

    }


  })

  },

  user_booking_conformation : function(req,res){
    console.log("req =>",req.body)

   waterfall([
               (callback)=>{
                 get_count({booking_count:1},callback);
               },
               (booking_count,callback)=>{
                 console.log('order_count-->'+booking_count);
                    req.body.bookingNumber = booking_count.booking_count;
                    var book_save = new pessangeBook(req.body);
                   book_save.save(function (err, result) {
                      if(err){
                       callback(err,null)
                      }
                      else{
                      callback(null,result);
                      }
                    });
                
               },
               (saveResult,callback)=>{
                console.log("saveResult",saveResult)
                transaction.findOneAndUpdate({order_id:req.body.order_id},{$set:{booking_id:saveResult._id}},{new:true},function(err,result1){
                        callback(null,result1)
                })
               }
               ],function(err,success){
               	console.log("success",success)
                  err?res.status(500).send(err):res.send(success)
               
             })

  },


    split_user_booking_conformation : function(req,res){
    console.log("req =>",req.body)

   waterfall([
               (callback)=>{
                 get_count({booking_count:1},callback);
               },
               (booking_count,callback)=>{
                 console.log('order_count-->'+booking_count);
                    req.body.bookingNumber = booking_count.booking_count;
                    var book_save = new pessangeBook(req.body);
                   book_save.save(function (err, result) {
                      if(err){
                       callback(err,null)
                      }
                      else{
                      callback(null,result);
                      }
                    });
                
               },
               (saveResult,callback)=>{
                console.log("saveResult",saveResult)
                transaction.findOneAndUpdate({order_id:req.body.order_id},{$set:{booking_id:saveResult._id}},{new:true},function(err,result1){
                 emptyJet.findOne({_id:saveResult.empty_jetId},function(err,result){
                      var remaining = result.remainingSeats - saveResult.empty_jet_sel_seat
                      emptyJet.update({_id:saveResult.empty_jetId},{$set:{remainingSeats:remaining}},function(err,result3){
                        callback(null,result1)
                      }) 
                 })
           
                })
               }
               ],function(err,success){
                console.log("success",success)
                  err?res.status(500).send(err):res.send(success)
               
             })

  },











   user_booking_updation : function(req,res){
    console.log("req =>",req.body)
    pessangeBook.findOneAndUpdate({_id:req.body.booking_id},{$set:req.body},function(err,result){
     err?res.status(500).send(err):res.send(result)
    })

  },

  generateQuotation:function(req,res){
    var htmlContent=quotationTemp.style+quotationTemp.content_start+req.body.htmlContent+quotationTemp.content_end;
    console.log("username =>",config.pdfcredential.username,"APi",config.pdfcredential.api_key)
    var client = new pdf.Pdfcrowd(config.pdfcredential.username, config.pdfcredential.api_key);
    //client.convertHtml(htmlContent, pdf.sendHttpResponse(res));
    var fileName="PDF-"+new Date().getTime()+'.pdf';
    client.convertHtml(htmlContent, pdf.saveToFile(path.join(process.env.PWD,'temp',fileName)));
    res.send({filePath:'/temp/'+fileName})
  },

    generateQuotOpeartor:function(req,res){
      //req.body.content_start+
    var htmlContent=quotOperator.style+quotOperator.content_start+req.body.htmlContent+quotOperator.content_end;
    console.log("username =>",config.pdfcredential.username,"APi",config.pdfcredential.api_key)
    var client = new pdf.Pdfcrowd(config.pdfcredential.username, config.pdfcredential.api_key);
    //client.convertHtml(htmlContent, pdf.sendHttpResponse(res));
    var fileName="PDF-"+new Date().getTime()+'.pdf';
    client.convertHtml(htmlContent, pdf.saveToFile(path.join(process.env.PWD,'temp',fileName)));
    res.send({filePath:'/temp/'+fileName})
  },

  generateMainfist:function(req,res){
    var htmlContent=invoiceTemp.style+invoiceTemp.content_start+req.body.htmlContent+invoiceTemp.content_end;
    console.log("username =>",config.pdfcredential.username,"APi",config.pdfcredential.api_key)
    var client = new pdf.Pdfcrowd(config.pdfcredential.username, config.pdfcredential.api_key);
    //client.convertHtml(htmlContent, pdf.sendHttpResponse(res));
    var fileName="PDF-"+new Date().getTime()+'.pdf';
    client.convertHtml(htmlContent, pdf.saveToFile(path.join(process.env.PWD,'temp',fileName)));
    res.send({filePath:'/temp/'+fileName})

  },
  bookingPdf:function(req,res){
    var htmlContent=bookingTemp.style+bookingTemp.content_start+req.body.htmlContent+bookingTemp.content_end;
    console.log("username =>",config.pdfcredential.username,"APi",config.pdfcredential.api_key)
    var client = new pdf.Pdfcrowd(config.pdfcredential.username, config.pdfcredential.api_key);
    //client.convertHtml(htmlContent, pdf.sendHttpResponse(res));
    var fileName="PDF-"+new Date().getTime()+'.pdf';
    client.convertHtml(htmlContent, pdf.saveToFile(path.join(process.env.PWD,'temp',fileName)));
    res.send({filePath:'/temp/'+fileName})

  },
  
  'booking_history' :function(req,res){
     console.log("req",req.body)
    pessangeBook.find({user_id:req.body.user_id}).populate('aircraftId flight_id','jet_name aircraft_type aircraft_name').populate('empty_jetId').exec(function(err,result){
       err?res.status(500).send(err):res.send(result)
    })


  },

  view_history:function(req,res){
    console.log("test",req.body)
  pessangeBook.findOne({_id:req.body._id}).populate('aircraftId empty_jetId flight_id','jet_name aircraft_type aircraft_name').exec(function(err,result){
    if(err){
     return res.status(500)
    }
    else{
      transaction.findOne({booking_id:req.body._id},function(err,result2){
        return res.send({result:result,bookingResult:result2})
      })
    }
  })

  },


// payment api

orderIdgenrate : function(req,res){
    var obj  = {   
          "amount":req.body.amount,
          "currency":"INR"
    }
 request.post('https://rzp_test_REEE8skw8OMLSt:79HUKLq6BHxkDZM8SypWEFvp@api.razorpay.com/v1/orders', {form:obj},function(error, response, body){
 // request.post('https://rzp_test_WnlcrSlqjaYYav:ivw505p4jVseEgCHHSnmpc5s@api.razorpay.com/v1/orders', {form:obj},function(error, response, body){
  console.log(response)
  console.log(body)
var test = JSON.parse(body);
console.log(test.id)
   var secObj = {
    order_id:test.id,
    amount:test.amount,
    currency:"INR"
   }
    var transaction_save = new transaction (secObj)
    transaction_save.save(function(err,result){
    error?res.status(500).send(error):res.send(result)
    })  
}) 

  },

 savePayment : function(req,res){
  console.log("req.body.payment_id",req.body.payment_id)
   request('https://rzp_test_REEE8skw8OMLSt:79HUKLq6BHxkDZM8SypWEFvp@api.razorpay.com/v1/payments/'+req.body.payment_id,function(error, response, body){
 //   request('https://rzp_test_WnlcrSlqjaYYav:ivw505p4jVseEgCHHSnmpc5s@api.razorpay.com/v1/payments/'+req.body.payment_id,function(error, response, body){
  console.log(response)
  console.log(body)
var test = JSON.parse(body);
console.log(test)
   var obj1  = {   
          amount:test.amount
    }
    console.log("obj1",obj1)
  request.post('https://rzp_test_REEE8skw8OMLSt:79HUKLq6BHxkDZM8SypWEFvp@api.razorpay.com/v1/payments/'+req.body.payment_id+'/capture',{form:obj1},function(error,response,body){
   transaction.findOneAndUpdate({order_id:test.description},{$set:{payment_id:test.id}},function(err,result1){
    err?res.status(500).send(err):res.send(test)
   })
 })
}) 

 },

// paymetSettelment : function(req,res){

// },



  emptyJet_list : function(req,res){
   emptyJet.find({"status" : "active"}).populate({path: 'aircratft_id',
              select: 'jet_name flight_number image_gallary'}).populate('created_by').exec(function(err,result){
       err?res.status(500).send(err):res.send(result)
    })

 },

splitpayment : function(req,res){
   console.log("req.body.payment_id",req.body.payment_id)
   var obj  = {   
          transfers:req.body.transfers
    }
    console.log("obj-----",obj)
    console.log("req.body.payment_idd",req.body.payment_id)
  request('https://rzp_test_REEE8skw8OMLSt:79HUKLq6BHxkDZM8SypWEFvp@api.razorpay.com/v1/payments/'+req.body.payment_id,function(error, response, body){
  //console.log(response)
  // console.log(body)
var test = JSON.parse(body);
console.log("console.log(test)",test.amount)
console.log(typeof(test.amount))
if(test.id){
   var obj1  = {   
          amount:test.amount
    }
    console.log("obj1",obj1)
  request.post('https://rzp_test_REEE8skw8OMLSt:79HUKLq6BHxkDZM8SypWEFvp@api.razorpay.com/v1/payments/'+req.body.payment_id+'/capture',{form:obj1},function(error,response,body){
    
request.post('https://rzp_test_REEE8skw8OMLSt:79HUKLq6BHxkDZM8SypWEFvp@api.razorpay.com/v1/payments/'+req.body.payment_id+'/transfers',{form:obj},function(error,response,body){
 var test1 = JSON.parse(body);
 console.log("test---------",test1)
 transaction.findOneAndUpdate({order_id:test.description},{$set:{payment_id:test.id,'split_information.recipient_id':test1.items[0].recipient,'split_information.transaction_id':test1.items[0].id,'split_information.split_amount':test1.items[0].amount}},function(err,result1){
 error?res.status(500).send(error):res.send(test)
})
})
})
}

    
}) 

},

save_emptyjet_pessenger : function(req,res){
 console.log("req.body",req.body)
 var pessangeBook_save = new pessangeBook(req.body)
 pessangeBook_save.save(function(err,result){
 err?res.status(500).send(err):res.send(result)

 })

},

logout : function(req,res){
 console.log("reqqq",req.body)
 user.findOneAndUpdate({_id:req.body._id},{$set:{is_login:false}},function(err,result){
 err?res.status(500).send(err):res.send(result)

 })

}



}

module.exports = user_api;


 var get_count = function (return_object,callback) {
   waterfall([
        (cb)=>{
          booking.findOne({}, return_object,function(err,success_data){
             if(err) cb(err,null);
             else cb(null,success_data);
          })
        },
        (success_data,cb)=>{
          booking.findByIdAndUpdate(success_data._id ,{
            $inc:return_object
          },{ projection: return_object , new :true},function(err,updated_count){
            if(err) cb(err,null);
            else cb(null,updated_count);
          })
        }
    ],function(err,result){
      if(err){
           console.log(err);
      }else{    
        callback(null,result);        
      }
    })
  }





//   console.log("req.body.payment_id",req.body.payment_id)
//    var obj  = {   
//           transfers:req.body.transfers
//     }
//     console.log("obj-----",obj)
//   request('https://rzp_test_lI6hwbHXvRREZX:n2AUA5NUXHsMl5NOR5jbafLL@api.razorpay.com/v1/payments/'+req.body.payment_id,function(error, response, body){
//   //console.log(response)
//   console.log(body)
// var test = JSON.parse(body);
// console.log(test),
// console.log("console.log(test)",test.amount)
// if(test.id){
//    var obj1  = {   
//           amount:test.amount
//     }
//     console.log("obj1",obj1)
//   request.post('https://rzp_test_lI6hwbHXvRREZX:n2AUA5NUXHsMl5NOR5jbafLL@api.razorpay.com/v1/payments/'+req.body.payment_id+'/capture',{form:obj1},function(error,response,body){
    
// request.post('https://rzp_test_lI6hwbHXvRREZX:n2AUA5NUXHsMl5NOR5jbafLL@api.razorpay.com/v1/payments/'+req.body.payment_id+'/transfers',{form:obj},function(error,response,body){
//  console.log("body",JSON.parse(body))
//  var test1 = JSON.parse(body);
//  console.log("test---------",test1)
//  error?res.status(500).send(error):res.send(test1)
// })
// })
// }
//    transaction.findOneAndUpdate({order_id:test.description},{$set:{payment_id:test.id}},function(err,result1){
//     err?res.status(500).send(err):res.send(test)
//    })

    
// }) 