module.exports = {
	style:`
	<!DOCTYPE html>
	<html>
		<head>
			<title></title>
			<style>
			#pdfFormat .pdf-header ul li img {
			    margin-bottom: 20px;
			    margin-top: 20px;
			    width: 150px !important;
			}
			#pdfFormat .pdf-sub-header ul {
			    padding: 0;
			}
			#pdfFormat .invoice-header {
			    text-align: center;
			    font-size: 16px;
			    margin: 15px 0 10px;
			}
			#pdfFormat .pdf-section > span {
			    font-size: 24px;
			    font-weight: 500;
			    margin: 20px 0 5px;
			    display: block;
			    text-transform: uppercase;
			    letter-spacing: 2px;
			}
			p.note {
			    font-weight: 300;
			    font-size: 16px;
			    margin-bottom: 20px;
			}
			#pdfFormat{
			    width:1300px;
			    background-color: #fff;
			}
			#pdfFormat .pdf-header{
			    padding: 10px;
			}
			#pdfFormat .pdf-header ul{
			}
			#pdfFormat .pdf-header ul li{
			    font-size: 24px;
			    list-style: none;
			    text-align: right;
			}
			#pdfFormat .pdf-sub-header{
			    border-bottom:1px dotted black; 
			}
			#pdfFormat .pdf-sub-header ul{
			    font-weight: 600;
			}
			#pdfFormat .pdf-sub-header ul li{
			    list-style: none;
			    font-size: 20px;
			}
			#pdfFormat .pdf-section ul.bullet-list li{
			    list-style: none;
			}
			#pdfFormat .invoice-header{
			    text-align: center;
			}
			ul.bullet-list {
			    padding: 0 0 0 30px;
			}
			.end-note {
			    text-align: center;
			    margin: 50px 0 30px;
			}
			.end-note p {
			    margin: 0;
			    font-size: 30px;
			    font-weight: 500;
			}
			#pdfFormat .pdf-section ul.bullet-list li {
			    position: relative;
			}
			#pdfFormat .pdf-section ul.bullet-list li b {
			    position: absolute;
			    margin: 0 !important;
			    left: -15px;
			}
			#pdfFormat .pdf-section {
			    margin-bottom: 15px;
			    padding: 0 15px;
			}

			#pdfFormat .pdf-section > span {
			    font-size: 24px;
			    font-weight: 600;
			}
			#pdfFormat .pdf-section > table {
			    width: 100%;
			    background-color: transparent;
			    font-size: 20px
			}
			#pdfFormat .pdf-section > table tr.tr-head td{
			    background-color: #cecece;
			    font-weight: 600;
			    border: 1px solid #000;
			    padding-left: 5px;
			}
			#pdfFormat .pdf-section > table tr td{
			    border: 1px solid #000;
			    padding-left: 5px;
			}
			#pdfFormat .trmc{
			    padding: 0 10px;
			    font-size: 20px;
			    word-break: break-all;
			    text-align: left;
			}
			.bookmark-bottom {
				position: absolute;
				bottom: 15px;
				right: 15px;
			}
			</style>
		</head>
	`,
	content_start:`
		<body>
			<div id="pdfFormat">
				<section style="padding-left: 50px; padding-right: 50px;">
					<div class="pdf-header">
	`,
	content_end:`
					</div>
				</section>
			</div>
		</body>
	</html>
	`
}