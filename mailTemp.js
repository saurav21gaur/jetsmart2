module.exports = {
	style:`
<!DOCTYPE html>
<html>
<head>
	<title>JetSmart</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0," />
	<meta name="x-apple-disable-message-reformatting" />
	<style type="text/css">
		@import url('https://fonts.googleapis.com/css?family=Montserrat:400,500');

		body {
			font-family: 'Montserrat', sans-serif, arial;
		}
		.main_table { box-shadow: 0 40px 120px -20px rgba(0,0,0,0.3); -webkit-box-shadow: 0 40px 120px -20px rgba(0,0,0,0.3); margin-top: 20px; margin-bottom: 20px;  }
		.white_color { color: #fff; font-size: 14px; padding: 0 15px; margin: 18px 0; }
		.white_color a { color: #fff; text-decoration: none; font-weight: 500; }
		p.white_color.btn { text-align: right; }
		p.white_color.btn a { background: #ec4f20; padding: 8px 12px; font-size: 11px; letter-spacing: 0.5px; }
		.spacing_internal {
		    padding: 15px 25px;
		}
		.text_right { text-align: right; }

		/* Mailer*/
	    .body_mail_content_spacer {
	       padding: 0 15px;
	    }
	    .body_mail_content_spacer .pdf-sub-header ul {
	        list-style: none;
	        padding: 0;
	        margin: 20px 0 20px;
	    }
	    .body_mail_content_spacer .pdf-sub-header ul li {
	        width: calc(100%/2);
	        display: inline-block;
	        margin: 0 -2px;
	    }
	    .multicity_table_wrapper {
	        width: 100%;
	        margin-top: 15px;
	    }
	    .multicity_table_wrapper tr.tr-head th {
	        padding: 10px 15px;
	    }
	    .multicity_table_wrapper tr td {
	        padding: 10px 15px;
	        border-bottom: 1px solid #ddd;

	    }
	    .multicity_table_wrapper tr td {
	        padding: 10px 15px;
	        border-bottom: 1px solid #ddd;
	        font-size: 13px;
	    }

		@media only screen and (max-width: 630px) and (max-device-width: 630px) {
			.main_table { width: auto !important; margin: 0 15px !important; }
			table { width: 100% !important; }
			table td { width: 100% !important; display: block !important; text-align: center !important; }
			.spacing_internal { padding: 0px 20px 20px; }
			p { font-size: 14px !important; }
			p.white_color.btn { text-align: center !important; }
			.text_right { text-align: center !important; }
			.white_color { margin: 15px 0 0 !important; }
			p.white_color.btn a { margin-bottom: 15px !important; display: inline-block !important; }
			p.white_color.policy { margin-bottom: 15px !important; font-size: 12px !important; margin-top: 5px !important; }
		}
		@media only screen and (max-device-width: 480px) {
			.main_table { width: auto !important; margin: 0 15px !important; }
			table { width: 100% !important; }
			table td { width: 100% !important; display: block !important; text-align: center !important; }
			.spacing_internal { padding: 0px 20px 20px; }
			p { font-size: 14px !important; }
			p.white_color.btn { text-align: center !important; }
			.text_right { text-align: center !important; }
			.white_color { margin: 15px 0 0 !important; }
			p.white_color.btn a { margin-bottom: 15px !important; display: inline-block !important; }
			p.white_color.policy { margin-bottom: 15px !important; font-size: 12px !important; margin-top: 5px !important; }
		}
	</style>
</head>
	`,
	content_start:`
	<body style="background: #ddd;margin: 0;padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td>
				<!-- Main Table -->
				<table class="main_table" align="center" border="0" cellpadding="0" cellspacing="0" width="600px" style="border-collapse: collapse;background: #fff;box-shadow: 0 40px 120px -20px rgba(0,0,0,0.3);-webkit-box-shadow: 0 40px 120px -20px rgba(0,0,0,0.3);margin-top: 20px;margin-bottom: 20px;">
					<!-- Top Header -->
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; background: rgba(15, 22, 38, 1)">
								<tr>
									<!-- Left Side -->
									<td width="50%">
										<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; background: rgba(15, 22, 38, 1)">
											<tr>
												<td>
													<p class="white_color" style="color: #fff;font-size: 14px;padding: 0 15px;margin: 18px 0;">Call Us: <a href="tel:+91 11 2805 0066" style="color: #fff;text-decoration: none;font-weight: 500;">+91 11 2805 0066</a></p>
												</td>
											</tr>
										</table>
									</td>
									<!-- Left Side End -->
									<!-- Right Side -->
									<td width="50%">
										<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; background: rgba(15, 22, 38, 1)">
											<tr>
												<td>
													<p class="white_color btn" style="color: #fff;font-size: 14px;padding: 0 15px;margin: 18px 0;text-align: right;"><a href="https://gojetsmart.com/#/contact" target="_blank" style="color: #fff;text-decoration: none;font-weight: 500;background: #ec4f20;padding: 8px 12px;font-size: 11px;letter-spacing: 0.5px;">Get In Touch</a></p>
												</td>
											</tr>
										</table>
									</td>
									<!-- Right Side End -->
								</tr>
							</table>
						</td>
					</tr>
	`,
	content_end:`
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%;">
								<tr>
									<!-- Left Side -->
									<td width="100%">
										<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%;">
											<tr>
												<td>
												<br><center><a href="https://gojetsmart.com" target="_blank"><img src="https://gojetsmart.com/assets/img/logo.png" width="180px"></a></center><br>
												</td>
											</tr>
										</table>
									</td>
									<!-- Left Side End -->
								</tr>
							</table>
						</td>
					</tr>
					<!-- Logo Header End -->
					<!-- Message Area -->
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%;">
								<tr>
									<td>
										<div class="spacing_internal" style="padding: 15px 25px;">
									
											<p>Thank  you for choosing to fly with JetSmart, the smartest way to travel! Your registration has been successful. Now, enjoy your journey anywhere across India at affordable prices and make your travel a memory.</p>
											<p>
                                               Team JetSmart</p>
										</div>										
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<!-- Message Area -->
					<!-- Footer -->
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%;">
								<tr>
									<!-- Left Side -->
									<td width="50%">
										<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; background: rgba(15, 22, 38, 1)">
											<tr>
												<td>
													<p class="white_color copyright" style="color: #fff;font-size: 14px;padding: 0 15px;margin: 18px 0;">© 2018 <a href="https://gojetsmart.com" target="_blank" style="color: #fff;text-decoration: none;font-weight: 500;">JetSmart</a>. All Rights Reserved.</p>
												</td>
											</tr>
										</table>
									</td>
									<!-- Left Side End -->
									<!-- Right Side -->
									<td width="50%">
										<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; background: rgba(15, 22, 38, 1)">
											<tr>
												<td>
													<p class="white_color policy text_right" style="color: #fff;font-size: 14px;padding: 0 15px;margin: 18px 0;text-align: right;"><a href="https://gojetsmart.com/#/policy" target="_blank" style="color: #fff;text-decoration: none;font-weight: 500;">Privacy Policy</a> | <a href="https://gojetsmart.com/#/termsCondition" target="_blank" style="color: #fff;text-decoration: none;font-weight: 500;">Terms & Conditions</a></p>
												</td>
											</tr>
										</table>
									</td>
									<!-- Right Side End -->
								</tr>
							</table>
						</td>
					</tr>
					<!-- Footer End -->
				</table>
				<!-- Main Table End -->
			</td>
		</tr>
	</table>
</body>
</html>
	`,
	content_start1:`
	<body style="background: #ddd;margin: 0;padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td>
				<!-- Main Table -->
				<table class="main_table" align="center" border="0" cellpadding="0" cellspacing="0" width="600px" style="border-collapse: collapse;background: #fff;box-shadow: 0 40px 120px -20px rgba(0,0,0,0.3);-webkit-box-shadow: 0 40px 120px -20px rgba(0,0,0,0.3);margin-top: 20px;margin-bottom: 20px;">
					<!-- Top Header -->
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; background: rgba(15, 22, 38, 1)"">
								<tr>
									<!-- Left Side -->
									<td width="50%">
										<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; background: rgba(15, 22, 38, 1)">
											<tr>
												<td>
													<p class="white_color" style="color: #fff;font-size: 14px;padding: 0 15px;margin: 18px 0;">Call Us: <a href="tel:+91 11 2805 0066" style="color: #fff;text-decoration: none;font-weight: 500;">+91 11 2805 0066</a></p>
												</td>
											</tr>
										</table>
									</td>
									<!-- Left Side End -->
									<!-- Right Side -->
									<td width="50%">
										<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; background: rgba(15, 22, 38, 1)">
											<tr>
												<td>
													<p class="white_color btn" style="color: #fff;font-size: 14px;padding: 0 15px;margin: 18px 0;text-align: right;"><a href="https://gojetsmart.com/#/contact" target="_blank" style="color: #fff;text-decoration: none;font-weight: 500;background: #ec4f20;padding: 8px 12px;font-size: 11px;letter-spacing: 0.5px;">Get In Touch</a></p>
												</td>
											</tr>
										</table>
									</td>
									<!-- Right Side End -->
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%;">
								<tr>
									<!-- Left Side -->
									<td width="100%">
										<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%;">
											<tr>
												<td>
												<br><center><a href="https://gojetsmart.com" target="_blank"><img src="https://gojetsmart.com/assets/img/logo.png" width="180px"></a></center><br>
												</td>
											</tr>
										</table>
									</td>
									<!-- Left Side End -->
								</tr>
							</table>
						</td>
					</tr>
					<!-- Logo Header End -->
	`,
	content_end2:`
					
					<!-- Message Area -->
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%;">
								<tr>
									<td>
										<div class="spacing_internal" style="padding: 15px 25px;">
									
											<p>Thank  you for choosing to fly with JetSmart, the smartest way to travel! Your registration has been successful. Now, enjoy your journey anywhere across India at affordable prices and make your travel a memory.</p>
											<p>
                                               Team JetSmart</p>
										</div>										
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<!-- Message Area -->
					<!-- Footer -->
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%;">
								<tr>
									<!-- Left Side -->
									<td width="50%">
										<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; background: rgba(15, 22, 38, 1)">
											<tr>
												<td>
													<p class="white_color copyright" style="color: #fff;font-size: 14px;padding: 0 15px;margin: 18px 0;">© 2018 <a href="https://gojetsmart.com" target="_blank" style="color: #fff;text-decoration: none;font-weight: 500;">JetSmart</a>. All Rights Reserved.</p>
												</td>
											</tr>
										</table>
									</td>
									<!-- Left Side End -->
									<!-- Right Side -->
									<td width="50%">
										<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; background: rgba(15, 22, 38, 1)">
											<tr>
												<td>
													<p class="white_color policy text_right" style="color: #fff;font-size: 14px;padding: 0 15px;margin: 18px 0;text-align: right;"><a href="https://gojetsmart.com/#/policy" target="_blank" style="color: #fff;text-decoration: none;font-weight: 500;">Privacy Policy</a> | <a href="https://gojetsmart.com/#/termsCondition" target="_blank" style="color: #fff;text-decoration: none;font-weight: 500;">Terms & Conditions</a></p>
												</td>
											</tr>
										</table>
									</td>
									<!-- Right Side End -->
								</tr>
							</table>
						</td>
					</tr>
					<!-- Footer End -->
				</table>
				<!-- Main Table End -->
			</td>
		</tr>
	</table>
</body>
</html>
	`,
	content_end3:`
					
					<!-- Message Area -->
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%;">
								<tr>
									<td>
										<div class="spacing_internal" style="padding: 15px 25px;">
									
											<p>Thank  you for choosing to fly with JetSmart. Now, enjoy your journey anywhere across India at affordable prices and make your travel a memory.</p>
											<p>
                                               Team JetSmart</p>
										</div>										
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<!-- Message Area -->
					<!-- Footer -->
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%;">
								<tr>
									<!-- Left Side -->
									<td width="50%">
										<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; background: rgba(15, 22, 38, 1)">
											<tr>
												<td>
													<p class="white_color copyright" style="color: #fff;font-size: 14px;padding: 0 15px;margin: 18px 0;">© 2018 <a href="https://gojetsmart.com" target="_blank" style="color: #fff;text-decoration: none;font-weight: 500;">JetSmart</a>. All Rights Reserved.</p>
												</td>
											</tr>
										</table>
									</td>
									<!-- Left Side End -->
									<!-- Right Side -->
									<td width="50%">
										<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; background: rgba(15, 22, 38, 1)">
											<tr>
												<td>
													<p class="white_color policy text_right" style="color: #fff;font-size: 14px;padding: 0 15px;margin: 18px 0;text-align: right;"><a href="https://gojetsmart.com/#/policy" target="_blank" style="color: #fff;text-decoration: none;font-weight: 500;">Privacy Policy</a> | <a href="https://gojetsmart.com/#/termsCondition" target="_blank" style="color: #fff;text-decoration: none;font-weight: 500;">Terms & Conditions</a></p>
												</td>
											</tr>
										</table>
									</td>
									<!-- Right Side End -->
								</tr>
							</table>
						</td>
					</tr>
					<!-- Footer End -->
				</table>
				<!-- Main Table End -->
			</td>
		</tr>
	</table>
</body>
</html>
	`
}